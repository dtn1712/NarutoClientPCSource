using src.lib;
using src.network;

public class GameMidlet
{
    public static string IP = "112.213.94.23";

    public static int PORT = 14444;
    public static sbyte indexClient = 0;
    public static string VERSION = "0.1.2";
    
    private static GameMidlet instance = new GameMidlet();
    public static string IPS1;
    
    public GameCanvas gameCanvas;

    public static GameMidlet GetGameMidlet()
    {
        return instance;
    }
    
    // Cho java
    private GameMidlet()
    {
        LogDebug.println("init GameMIdlet");
        gameCanvas = new GameCanvas();
        gameCanvas.start();
        Session.gI().setHandler(Controller.gI());
        SplashScr.loadSplashScr();
        GameCanvas.currentScreen = new SplashScr();
    }


    public static void exit()
    {
        TCanvas.bRun = false;
        mSystem.gcc();
    }
}