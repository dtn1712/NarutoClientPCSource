using System;
using src.lib;

public class CreatCharScr : tScreen, IActionListener
{
    public const int NAM = 10;
    public const int NU = 111;
    public static CreatCharScr instance;
    public static string[] typeHero = {"Hunter", "Ninja"};
    public static TField tAddName;
    public static int indexHair, selected, indexBody, indexleg;
    public static mBitmap imgItem, characterbar, gender_bar;
    public static mBitmap[] gender = new mBitmap[4];
    private static int[][] hairID = {new[] {0, 26, 27, 28}, new[] {3, 23, 24, 25}}; // Danh
    private static int[] defaultLeg = {2, 5}, defaultBody = {1, 4}; // Quần
    public static mBitmap[] imgClazz = new mBitmap[6];
    public static mBitmap[] imgQuocGia = new mBitmap[10];

    private int curIndex;

    private int dem;
    public int[] hKhung = new int[3];
    public int[][][] idPartHoa, idPartThuy, idPartTho, idPartLoi, idPartPhong;
    public sbyte indexGender, indexCountry, indexClass;
    public string[][] infoClazz, infoQuocGia;
    private readonly Command male;
    private readonly Command female;
    public Scroll scrInfoClass = new Scroll();
    public Scroll scrQuocGia = new Scroll();
    public int wpaintKhung = 110, yKhung1, yKhung2, xkhungTrai = 15, sizeItem = 26;
    public int[][] xyItemClass = new int[3][];
    public int[][] xyItemInfo = new int[2][];
    public int[][] xyItemQuocGia = new int[5][];

    public CreatCharScr()
    {
        hKhung[0] = 60;
        hKhung[1] = 90;
        hKhung[2] = 160;

        if (GameCanvas.w == 128)
        {
            GameScr.setPopupSize(128, 120);
            GameScr.popupX = (GameCanvas.w - 128) / 2;
            GameScr.popupY = 0;
        }
        else
        {
            GameScr.setPopupSize(170, 190);
            GameScr.popupX = (GameCanvas.w - 170) / 2;
            GameScr.popupY = (GameCanvas.h - 220) / 2;
        }

        tAddName = new TField();
        tAddName.isPaintImg = true;
        tAddName.y = GameCanvas.h - 30;
        tAddName.width = 100;

        if (GameCanvas.w == 128)
            tAddName.width = 60;

        tAddName.x = GameCanvas.w / 2 - tAddName.width / 2 + 5; //GameCanvas.w/2, GameCanvas.h-20
        tAddName.height = 21;
        tAddName.setIputType(TField.INPUT_TYPE_ANY);
        indexHair = 0;
        yKhung1 = GameCanvas.h / 6 + 240 >= GameCanvas.h ? GameCanvas.h - 240 : GameCanvas.h / 6;
        LogDebug.println("yKhung1  " + yKhung1);
        male = new Command("", this, NAM, null);
        male.setPos(xkhungTrai + wpaintKhung / 2 - 30, yKhung1, gender[2], gender[3]);
        female = new Command("", this, NU, null);
        female.setPos(xkhungTrai + wpaintKhung / 2 + 5, yKhung1, gender[1], gender[0]);
        male.w = female.w = 25;
        var wkc = (wpaintKhung - sizeItem * xyItemClass.Length) / (xyItemClass.Length + 1);
        var du = (wpaintKhung - sizeItem * xyItemClass.Length) % (xyItemClass.Length + 1);
        var wkc2 = (wpaintKhung - sizeItem * (xyItemClass.Length - 1)) / xyItemClass.Length;
        var du1 = (wpaintKhung - sizeItem * xyItemClass.Length) % xyItemClass.Length;
        for (var i = 0; i < xyItemClass.Length; i++)
        {
            xyItemClass[i] = new int[2];
            xyItemClass[i][0] = xkhungTrai + sizeItem * i + (i + 1) * wkc;
            xyItemClass[i][1] = yKhung1 + 62;
        }
        for (var i = 0; i < xyItemQuocGia.Length; i++)
        {
            xyItemQuocGia[i] = new int[2];
            xyItemQuocGia[i][0] = xkhungTrai + sizeItem * (i % 3) + (i % 3 + 1) * (i <= 2 ? wkc : wkc2);
            xyItemQuocGia[i][1] = yKhung1 + hKhung[0] + 72 + i / 3 * 30;
        }
        for (var i = 0; i < xyItemInfo.Length; i++)
        {
            xyItemInfo[i] = new int[2];
            xyItemInfo[i][0] = GameCanvas.w - wpaintKhung - xkhungTrai + 4;
            xyItemInfo[i][1] = yKhung1 + hKhung[0] + 36 + i * 52;
        }
        center = null;
        left = new Command(mResources.BACK, this, 8001, null);
        left.x = xkhungTrai + wpaintKhung / 2 - LoadImageInterface.img_use.getWidth() / 2;
        right = new Command(mResources.NEWCHAR, this, 8000, null);
        right.x = GameCanvas.w - xkhungTrai - wpaintKhung / 2 - LoadImageInterface.img_use.getWidth() / 2;
        right.img = left.img = LoadImageInterface.img_use;
        right.imgFocus = left.imgFocus = LoadImageInterface.img_use_focus;

//		SmallImage.init();
        idPartHoa = new[]
        {
            new[]
            {
                //the thuat
                new[] {6, 7, 8},
                new[] {51, 52, 53}
            },
            new[]
            {
                //ao thuat
                new[] {54, 55, 56},
                new[] {96, 97, 98}
            },
            new[]
            {
                //nhan thuat
                new[] {60, 61, 62},
                new[] {45, 46, 47}
            }
        };
        idPartThuy = new[]
        {
            new[]
            {
                //the thuat
                new[] {69, 70, 71},
                new[] {30, 31, 32}
            },
            new[]
            {
                //ao thuat
                new[] {48, 49, 50},
                new[] {9, 10, 11}
            },
            new[]
            {
                //nhan thuat
                new[] {33, 34, 35},
                new[] {45, 46, 47}
            }
        };
        idPartTho = new[]
        {
            new[]
            {
                //the thuat
                new[] {102, 103, 104},
                new[] {24, 25, 26}
            },
            new[]
            {
                //ao thuat
                new[] {39, 40, 41},
                new[] {36, 37, 38}
            },
            new[]
            {
                //nhan thuat
                new[] {48, 49, 50},
                new[] {45, 46, 47}
            }
        };
        idPartLoi = new[]
        {
            new[]
            {
                //the thuat
                new[] {0, 1, 2},
                new[] {96, 97, 98}
            },
            new[]
            {
                //ao thuat
                new[] {69, 70, 71},
                new[] {81, 82, 83}
            },
            new[]
            {
                //nhan thuat
                new[] {33, 34, 35},
                new[] {9, 10, 11}
            }
        };
        idPartPhong = new[]
        {
            new[]
            {
                //the thuat
                new[] {78, 79, 80},
                new[] {96, 97, 98}
            },
            new[]
            {
                //ao thuat
                new[] {39, 40, 41},
                new[] {81, 82, 83}
            },
            new[]
            {
                //nhan thuat
                new[] {90, 91, 92},
                new[] {24, 25, 26}
            }
        };
        string[] infoClazzz =
        {
            //the thuat
            "Thể thuật, với lệ thế cận chiến mạnh tốc độ di chuyển nhanh hơn thế là khả năng y thuật tuyệt vời."
            + " Riêng với thể thuật có thể đi theo hướng cận chiến hoặc y liệu cả hai điều có lệ thế riêng của mình.",
            //ao thuat
            "Ảo thuật là những thuật sử dụng chakra trong hệ thần kinh của đối thủ để tạo ra ảo giác; "
            + "về cơ bản được coi là nhẫn thuật cao cấp về trí óc. Ảo thuật mạnh về những chiêu thức gây choáng và làm ngộp đối thủ.",
            //nhan thuat
            "Nhẫn thuật, chủ yếu dùng chakra để hiện thực hóa các đòn tấn công cũng như phòng thủ."
            + " Để trở thành một nhẫn thuật giỏi cần thừa hưởng lượng chakra phong phú. Lợi thế class Nhẫn Thuật dồi giàu về chakra."
        };
        string[] infoQuocGiaa =
        {
            //hoa
            "Giống như tên của nó, Hỏa Quốc có khuynh hướng thiên về yếu tố lửa, với thời tiết thoáng đãng và ấm áp.",
            //thuy
            "Nó có nhiều hòn đảo, mỗi đảo lại có một truyền thống riêng. Thủy Quốc và làng Sương mù chưa bao giờ lộ diện trong truyện, nhưng vài ninja đến từ khu vực này đã xuất hiện.",
            //loi
            "Giữa Lôi Quốc là những dãy núi lớn, nơi có nhiều bão sấm sét, đây là nguồn gốc tên đất nước này. "
            + "Từ những rặng núi này nhiều con sông đổ ra biển, tạo nên bờ biển uốn cong, cảnh biển ấn tượng và ngoạn mục. "
            + "Có nhiều suối nước nóng trong lãnh thổ nước này.",
            // phong
            "Giống như tên của nó, Hỏa Quốc có khuynh hướng thiên về yếu tố Phong là một đất nước rộng lớn, khô cằn, và hoang vắng với những thành phố ở gần nguồn nước. ",
            //tho
            "Thổ Quốc bao gồm chủ yếu các khu vực hoang và nhiều đá, biên giới của nó chạy dọc theo dãy núi đá, ngăn liên lạc với các nước khác."
            + " Gió bắc thổi qua rặng núi này, mang những viên đá nhỏ tới các nước lân cận. Hiện tượng thiên nhiên kì thú này được gọi là Gan'u."
        };
        infoClazz = new string[infoClazzz.Length][];
        infoQuocGia = new string[infoQuocGiaa.Length][];
        for (var i = 0; i < infoClazzz.Length; i++)
            infoClazz[i] = mFont.tahoma_6_white.splitFontArray(infoClazzz[i], wpaintKhung - 26 - 8);
        for (var i = 0; i < infoQuocGiaa.Length; i++)
            infoQuocGia[i] = mFont.tahoma_6_white.splitFontArray(infoQuocGiaa[i], wpaintKhung - 26 - 8);
    }

    public void perform(int idAction, object p)
    {
        switch (idAction)
        {
            case 8000:
                Service.getInstance().createChar(indexClass, indexCountry, indexGender, tAddName.getText());
                break;
            case 8001:
                GameCanvas.loadBG(1);
                SelectCharScr.gI().switchToMe();
                break;
            case NAM:
                female.img = gender[1];
                female.imgFocus = gender[0];
                male.img = gender[2];
                male.imgFocus = gender[3];
                indexGender = 0;
                ResetPart();
                break;
            case NU:
                female.img = gender[0];
                female.imgFocus = gender[1];
                male.img = gender[3];
                male.imgFocus = gender[2];
                indexGender = 1;
                ResetPart();
                break;
        }
    }

    public static CreatCharScr gI()
    {
        if (instance == null)
            instance = new CreatCharScr();
        return instance;
    }

    public static void loadImage()
    {
        //imgItem,characterbar,gender_bar;
        for (var i = 0; i < gender.Length; i++)
            gender[i] = GameCanvas.loadImage("/GuiNaruto/createChar/gender" + (i + 1) + ".png");
        int[] maptop = {0, 1, 4, 5, 2, 3};
        for (var i = 0; i < imgClazz.Length; i++)
            imgClazz[i] = GameCanvas.loadImage("/GuiNaruto/createChar/iconClass" + maptop[i] + ".png");
        for (var i = 0; i < imgQuocGia.Length; i++)
            imgQuocGia[i] = GameCanvas.loadImage("/GuiNaruto/createChar/imgQuocGia" + i + ".png");
        imgItem = GameCanvas.loadImage("/GuiNaruto/createChar/itemCreateC.png");
        characterbar = GameCanvas.loadImage("/GuiNaruto/createChar/characterbar.png");
        gender_bar = GameCanvas.loadImage("/GuiNaruto/createChar/gender_bar.png");
    }

    public override void switchToMe()
    {
        tAddName.setText("");
        indexCountry = 0;
        indexClass = 0;
        GameCanvas.loadBG(1);
        male.performAction();
        ResetPart();
        base.switchToMe();
    }

    public override void keyPress(int keyCode)
    {
        tAddName.keyPressed(keyCode);
        base.keyPress(keyCode);
    }

    public override void update()
    {
        GameScr.cmx++;
        if (selected == 0) // Name
            tAddName.update();
        if (GameCanvas.imgBG[0] == null || GameCanvas.imgBG[1] == null || GameCanvas.imgBG[2] == null)
            GameCanvas.loadBG(1);
        dem++;
        if (dem >= 1000)
            dem = 0;
        if (GameScr.cmx > GameCanvas.w * 3 + 100)
            GameScr.cmx = 100;
        if (GameCanvas.isTouch && GameCanvas.w >= 320)
        {
            if (left != null)
            {
                left.x = GameCanvas.w / 2 - 160;
                left.y = GameCanvas.h - 26;
            }
            if (center != null)
            {
                center.x = GameCanvas.w / 2 - 35;
                center.y = GameCanvas.h - 26;
            }

            if (right != null)
            {
                right.x = GameCanvas.w / 2 + 88;
                right.y = GameCanvas.h - 26;
            }
        }
    }

    public override void updateKey()
    {
        var s1 = scrInfoClass.updateKey();
        scrInfoClass.updatecm();
        var s2 = scrQuocGia.updateKey();
        scrQuocGia.updatecm();
        if (GameCanvas.keyPressed[2])
        {
            selected--;
            if (selected < 0)
                selected = mResources.MENUNEWCHAR.Length - 1;
        }
        if (GameCanvas.keyPressed[8])
        {
            selected++;
            if (selected >= mResources.MENUNEWCHAR.Length)
                selected = 0;
        }


        if (selected == 1)
        {
            // Gender
            if (GameCanvas.keyPressed[4])
            {
                indexGender--;
                if (indexGender < 0)
                    indexGender = (sbyte) (mResources.MENUGENDER.Length - 1);
            }
            if (GameCanvas.keyPressed[6])
            {
                indexGender++;
                if (indexGender > mResources.MENUGENDER.Length - 1)
                    indexGender = 0;
            }
            right = null;
        }

        if (selected == 2) // Loáº¡i 
        {
            if (GameCanvas.keyPressed[4])
            {
                indexClass--;
                if (indexClass < 0)
                    indexClass = (sbyte) (typeHero.Length - 1);
            }

            if (GameCanvas.keyPressed[6])
            {
                indexClass++;
                if (indexClass > typeHero.Length - 1)
                    indexClass = 0;
            }
        }
        if (selected == 3)
        {
            if (GameCanvas.keyPressed[4])
            {
                indexCountry--;
                if (indexCountry < 0)
                    indexCountry = (sbyte) (Text.typeCountry.Length - 1);
            }

            if (GameCanvas.keyPressed[6])
            {
                indexCountry++;
                if (indexCountry > Text.typeCountry.Length - 1)
                    indexCountry = 0;
            }
        }


        if (GameCanvas.isPointerJustRelease)
        {
            if (GameCanvas.isPointerHoldIn(GameScr.popupX + 5, GameScr.popupY + 65, GameScr.popupW - 5, ITEM_HEIGHT))
                selected = 0;

            if (GameCanvas.isPointerHoldIn(GameScr.popupX + 5, GameScr.popupY + 106, GameScr.popupW - 5, ITEM_HEIGHT))
            {
                curIndex = 1;
                if (curIndex == selected)
                {
                    indexGender--;
                    if (indexGender < 0)
                        indexGender = (sbyte) (mResources.MENUGENDER.Length - 1);
                }
                else
                {
                    selected = 1;
                }
            }
            if (GameCanvas.isPointerHoldIn(GameScr.popupX + 5, GameScr.popupY + 150, GameScr.popupW - 5, ITEM_HEIGHT))
            {
                curIndex = 2;
                if (curIndex == selected)
                {
                    indexClass++;
                    if (indexClass > typeHero[0].Length - 1)
                        indexClass = 0;
                }
                else
                {
                    selected = 2;
                }
            }
        }
        for (var i = 0; i < xyItemClass.Length; i++)
            if (GameCanvas.isPointerClick &&
                GameCanvas.isPoint(xyItemClass[i][0], xyItemClass[i][1], sizeItem, sizeItem))
            {
                indexClass = (sbyte) i;
                scrInfoClass.cmtoY = 0;
                ResetPart();
                GameCanvas.clearPointerEvent();
            }
        //indexCountry
        for (var i = 0; i < xyItemQuocGia.Length; i++)
            if (GameCanvas.isPointerClick &&
                GameCanvas.isPoint(xyItemQuocGia[i][0], xyItemQuocGia[i][1], sizeItem, sizeItem))
            {
                indexCountry = (sbyte) i;
                scrQuocGia.cmtoY = 0;
                ResetPart();
                GameCanvas.clearPointerEvent();
            }
        if (getCmdPointerLast(male))
        {
            GameCanvas.isPointerJustRelease = false;
            male.performAction();
        }
        if (getCmdPointerLast(female))
        {
            GameCanvas.isPointerJustRelease = false;
            female.performAction();
        }
        base.updateKey();
        GameCanvas.clearKeyHold();
        GameCanvas.clearKeyPressed();
    }

    public void ResetPart()
    {
        switch (indexCountry)
        {
            case 0: //hoa
                indexHair = idPartHoa[indexClass][indexGender][0];
                indexBody = idPartHoa[indexClass][indexGender][1];
                indexleg = idPartHoa[indexClass][indexGender][2];
                break;
            case 1: //thuy
                indexHair = idPartThuy[indexClass][indexGender][0];
                indexBody = idPartThuy[indexClass][indexGender][1];
                indexleg = idPartThuy[indexClass][indexGender][2];
                break;
            case 2: //loi
                indexHair = idPartLoi[indexClass][indexGender][0];
                indexBody = idPartLoi[indexClass][indexGender][1];
                indexleg = idPartLoi[indexClass][indexGender][2];
                break;
            case 3: //phong
                indexHair = idPartPhong[indexClass][indexGender][0];
                indexBody = idPartPhong[indexClass][indexGender][1];
                indexleg = idPartPhong[indexClass][indexGender][2];
                break;
            case 4: //tho
                indexHair = idPartTho[indexClass][indexGender][0];
                indexBody = idPartTho[indexClass][indexGender][1];
                indexleg = idPartTho[indexClass][indexGender][2];
                break;
        }
    }

    public override void paint(mGraphics g)
    {
        var line = 0;
        try
        {
            GameCanvas.paintBGGameScr(g);

            //khung thong tin
            Paint.SubFrame(GameCanvas.w - wpaintKhung - xkhungTrai, male.y + 40, wpaintKhung, hKhung[2], g, 0x037ddb,
                80);
            line = 1;
            scrInfoClass.setStyle(infoClazz[indexClass].Length, 8, xyItemInfo[0][0] + imgItem.getWidth() + 2,
                xyItemInfo[0][1] - 4, wpaintKhung - imgItem.getWidth() - 8, imgItem.getWidth() * 2, true, 1);
            scrInfoClass.setClip(g, xyItemInfo[0][0] + imgItem.getWidth() + 2, xyItemInfo[0][1] - 4,
                wpaintKhung - imgItem.getWidth() - 8, imgItem.getWidth() * 2 - 4);
            for (var j = 0; j < infoClazz[indexClass].Length; j++)
                mFont.tahoma_6_white.drawString(g, infoClazz[indexClass][j],
                    xyItemInfo[0][0] + imgItem.getWidth() + 2, xyItemInfo[0][1] + j * 8, 0);
            GameCanvas.resetTrans(g);
            scrQuocGia.setStyle(infoQuocGia[indexCountry].Length, 8, xyItemInfo[1][0] + imgItem.getWidth() + 2,
                xyItemInfo[1][1] - 4, wpaintKhung - imgItem.getWidth() - 8, imgItem.getWidth() * 2, true, 1);
            scrQuocGia.setClip(g, xyItemInfo[1][0] + imgItem.getWidth() + 2, xyItemInfo[1][1] - 4,
                wpaintKhung - imgItem.getWidth() - 8, imgItem.getWidth() * 2 - 4);
            for (var j = 0; j < infoQuocGia[indexCountry].Length; j++)
                mFont.tahoma_6_white.drawString(g, infoQuocGia[indexCountry][j],
                    xyItemInfo[1][0] + imgItem.getWidth() + 2, xyItemInfo[1][1] + 8 * j, 0, true);
            GameCanvas.resetTrans(g);
            var r = 40;
            if (GameCanvas.w == 128) r = 20;
            g.drawImage(LoadImageInterface.imgRock, GameCanvas.w / 2,
                GameCanvas.h - 10 - LoadImageInterface.imgRock.getHeight() / 2, mGraphics.VCENTER | mGraphics.HCENTER);

            int cx = GameCanvas.w / 2, cy = GameCanvas.h - LoadImageInterface.imgRock.getHeight() / 2 - 30;
            try
            {
                Part ph = GameScr.parts[indexHair], pl = GameScr.parts[indexleg], pb = GameScr.parts[indexBody];

                SmallImage.drawSmallImage(g, pl.pi[Char.CharInfo[0][1][0]].id, cx
                                                                               + Char.CharInfo[0][1][1] +
                                                                               pl.pi[Char.CharInfo[0][1][0]].dx, cy
                                                                                                                 - Char
                                                                                                                     .CharInfo
                                                                                                                         [dem % 15 < 5 ? 0 : 1]
                                                                                                                     [1]
                                                                                                                     [2] +
                                                                                                                 pl.pi[
                                                                                                                         Char
                                                                                                                             .CharInfo
                                                                                                                                 [dem % 15 < 5 ? 0 : 1]
                                                                                                                             [1]
                                                                                                                             [0]]
                                                                                                                     .dy,
                    0,
                    0);
                SmallImage.drawSmallImage(g, pb.pi[Char.CharInfo[0][2][0]].id, cx
                                                                               + Char.CharInfo[0][2][1] +
                                                                               pb.pi[Char.CharInfo[0][2][0]].dx, cy
                                                                                                                 - Char
                                                                                                                     .CharInfo
                                                                                                                         [dem % 15 < 5 ? 0 : 1]
                                                                                                                     [2]
                                                                                                                     [2] +
                                                                                                                 pb.pi[
                                                                                                                         Char
                                                                                                                             .CharInfo
                                                                                                                                 [dem % 15 < 5 ? 0 : 1]
                                                                                                                             [2]
                                                                                                                             [0]]
                                                                                                                     .dy,
                    0,
                    0);
                SmallImage.drawSmallImage(g, ph.pi[Char.CharInfo[0][0][0]].id, cx
                                                                               + Char.CharInfo[0][0][1] +
                                                                               ph.pi[Char.CharInfo[0][0][0]].dx, cy
                                                                                                                 - Char
                                                                                                                     .CharInfo
                                                                                                                         [dem % 15 < 5 ? 0 : 1]
                                                                                                                     [0]
                                                                                                                     [2] +
                                                                                                                 ph.pi[
                                                                                                                         Char
                                                                                                                             .CharInfo
                                                                                                                                 [dem % 15 < 5 ? 0 : 1]
                                                                                                                             [0]
                                                                                                                             [0]]
                                                                                                                     .dy,
                    0,
                    0);
            }
            catch (Exception e)
            {
                // TODO: handle exception
            }
            g.drawImage(gender_bar, xkhungTrai + wpaintKhung / 2, male.y + male.img.getHeight() / 2,
                mGraphics.VCENTER | mGraphics.HCENTER);
            Paint.SubFrame(xkhungTrai, male.y + 40, wpaintKhung, hKhung[0], g, 0x037ddb, 80);
            mFont.tahoma_7_white.drawString(g, "Class", xkhungTrai + wpaintKhung / 2, male.y + 45, mFont.CENTER);
            for (var i = 0; i < xyItemClass.Length; i++)
            {
                g.drawImage(imgItem, xyItemClass[i][0], xyItemClass[i][1], mGraphics.TOP | mGraphics.LEFT);
                if (i == indexClass)
                {
                    g.drawImage(imgClazz[indexClass * 2 + 1],
                        xyItemClass[i][0] + imgItem.getWidth() / 2, xyItemClass[i][1] + imgItem.getHeight() / 2,
                        mGraphics.VCENTER | mGraphics.HCENTER);

                    g.drawImage(LoadImageInterface.imgFocusSelectItem,
                        xyItemClass[i][0] + imgItem.getWidth() / 2, xyItemClass[i][1] + imgItem.getHeight() / 2,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                }
                else
                {
                    g.drawImage(imgClazz[i * 2],
                        xyItemClass[i][0] + imgItem.getWidth() / 2, xyItemClass[i][1] + imgItem.getHeight() / 2,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                }
            }
            Paint.SubFrame(xkhungTrai, male.y + 110, wpaintKhung, hKhung[1], g, 0x037ddb, 80);
            for (var i = 0; i < xyItemQuocGia.Length; i++)
            {
                g.drawImage(imgItem, xyItemQuocGia[i][0], xyItemQuocGia[i][1], mGraphics.TOP | mGraphics.LEFT);
                if (i == indexCountry)
                {
                    g.drawImage(imgQuocGia[indexCountry * 2 + 1],
                        xyItemQuocGia[i][0] + imgItem.getWidth() / 2, xyItemQuocGia[i][1] + imgItem.getHeight() / 2,
                        mGraphics.VCENTER | mGraphics.HCENTER);

                    g.drawImage(LoadImageInterface.imgFocusSelectItem,
                        xyItemQuocGia[i][0] + imgItem.getWidth() / 2, xyItemQuocGia[i][1] + imgItem.getHeight() / 2,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                }
                else
                {
                    g.drawImage(imgQuocGia[i * 2],
                        xyItemQuocGia[i][0] + imgItem.getWidth() / 2,
                        xyItemQuocGia[i][1] + imgItem.getHeight() / 2, mGraphics.VCENTER | mGraphics.HCENTER);
                }
            }
            mFont.tahoma_7_white.drawString(g, "Quốc gia", xkhungTrai + wpaintKhung / 2, male.y + 115, mFont.CENTER);
            for (var i = 0; i < wpaintKhung / LoadImageInterface.imgLineTrade.getWidth() - 1; i++)
            {
                g.drawImage(LoadImageInterface.imgLineTrade,
                    xyItemInfo[0][0] + Image.getWidth(LoadImageInterface.imgLineTrade) * i, xyItemInfo[0][1] - 8, 0);
                g.drawImage(LoadImageInterface.imgLineTrade,
                    xyItemInfo[1][0] + Image.getWidth(LoadImageInterface.imgLineTrade) * i, xyItemInfo[1][1] - 8, 0);
            }
            mFont.tahoma_7_white.drawString(g, "Gender", GameCanvas.w - wpaintKhung / 2 - xkhungTrai,
                male.y + 45, mFont.CENTER);
            if (gender[0] != null)
                g.drawImage(indexGender == 0 ? gender[2] : gender[0], GameCanvas.w - wpaintKhung / 2 - xkhungTrai,
                    male.y + 70, mGraphics.VCENTER | mGraphics.HCENTER, true);
            for (var i = 0; i < xyItemInfo.Length; i++)
            {
                g.drawImage(imgItem, xyItemInfo[i][0], xyItemInfo[i][1] + 6, mGraphics.TOP | mGraphics.LEFT, true);
                if (i == 1)
                    g.drawRegion(imgQuocGia[indexCountry * 2 + 1], 0, 0, imgQuocGia[indexCountry * 2 + 1].getWidth(),
                        imgQuocGia[indexCountry * 2 + 1].getHeight(), 0,
                        xyItemInfo[i][0] + imgItem.getWidth() / 2, xyItemInfo[i][1] + 6 + imgItem.getHeight() / 2,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                else
                    g.drawRegion(imgClazz[indexClass * 2 + 1], 0, 0, imgClazz[indexClass * 2 + 1].getWidth(),
                        imgClazz[indexClass * 2 + 1].getHeight(), 0,
                        xyItemInfo[i][0] + imgItem.getWidth() / 2, xyItemInfo[i][1] + 6 + imgItem.getHeight() / 2,
                        mGraphics.VCENTER | mGraphics.HCENTER);
            }
            male.paint(g);
            female.paint(g);
            g.drawImage(characterbar, GameCanvas.w / 2, GameCanvas.h - 20, mGraphics.VCENTER | mGraphics.HCENTER, true);


            if (tAddName.isFocus)
                tAddName.paint(g);
            else mFont.tahoma_7b_white.drawString(g, tAddName.getText(), GameCanvas.w / 2, GameCanvas.h - 25, 2);
            // g.fillRect(GameScr.popupX + 5,
            // GameScr.popupY + 65, GameScr.popupW - 5, ITEM_HEIGHT);
        }
        catch (Exception e)
        {
            //System.out.println("CreateCharScr.paint(): " + line);
        }
        base.paint(g);
    }
}