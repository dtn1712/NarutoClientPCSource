using src.lib;
using UnityEngine;

public class ClanScreen : tScreen, IActionListener
{
    public const int CREAT_CLAN_GLOBAL = 0; // tạo clan global
    public const int CREAT_CLAN_LOCAL = 1; // tạo clan local
    public const int LEAVE_CLAN_LOCAL = 2; // rời bỏ clan 
    public const int LEAVE_CLAN_GOBAL = 13;
    public const int DELETE_CLAN = 3; // xóa clan dành cho ban chủ 
    public const int INVITA_CLAN = 4; // mời vào clan 
    public const int KICK_CLAN_GOBAL = 5; // kích khỏi clan
    public const int KICK_CLAN_LOCAL = 12; // kích khỏi clan
    public const int REQUEST_CLAN = 6; // yêu cầu danh sách clan
    public const int REQUETS_NAME = 7;
    public const int GET_LIST_LOCAL = 8;
    public const int GET_LIST_GOBAL = 9;
    public const int INVITE_CLAN_GLOBAL = 11;

    public const int GET_LISTCLAN_GOLBAL = 0; //Update sau
    public const int GET_LISTCLAN_LOCAL = 1; //Update sau
    public static ClanScreen me;
    private static int clickNumber;

    public static int idCharClan;

    private static mBitmap imgClanName;
    public string clanLocalName, clanGolbalName;

    private string clanName;
    public Command cmdClanLocal, cmdClanGobal;
    public Command cmdClose, cmdKich, cmdRoi, CmdGiaiTan;


    private int indexRow;
    public bool isBoss, isBossGobal;
    public bool isTabLocal;

    public int KICK_CLAN;
    public int LEAVE_CLAN;
    public Vector listClan_Golbal = new Vector();

    public Vector listClan_local = new Vector();
    public Vector listUser_gobal = new Vector();

    public Vector listUser_local = new Vector();
    private readonly Scroll scr_listUser;
    private int sizeClan;

    public int wkhung, hKhung, xpaint, ypaint;


    public ClanScreen()
    {
        wkhung = 240;
        hKhung = 200;

        xpaint = GameCanvas.w / 2 - wkhung / 2;
        ypaint = GameCanvas.h / 2 - hKhung / 2 - 10;
        cmdClose = new Command("", this, 2, null);
        cmdKich = new Command("Kích", this, 3, null);
        cmdRoi = new Command("Rời", this, 4, null);
        CmdGiaiTan = new Command("Giải tán", this, 5, null);
        cmdClanLocal = new Command("Clan Local", this, 6, null);
        cmdClanGobal = new Command("Clan Gobal", this, 7, null);

        var wbutton = Image.getWidth(LoadImageInterface.btnTab);
        var hbutton = Image.getHeight(LoadImageInterface.btnTab);
        cmdKich.setPos(xpaint + wkhung / 2 - 7 * wbutton / 4,
            ypaint + hKhung - 10, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);
        cmdRoi.setPos(xpaint + wkhung / 2 - wbutton / 2,
            ypaint + hKhung - 10, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);
        CmdGiaiTan.setPos(xpaint + wkhung / 2 + 3 * wbutton / 4,
            ypaint + hKhung - 10, LoadImageInterface.img_use, LoadImageInterface.img_use_focus);

        cmdClose.setPos(xpaint + wkhung - LoadImageInterface.closeTab.width / 2,
            ypaint, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
        cmdClanLocal.setPos(xpaint + wkhung - 10,
            ypaint + hKhung / 2 - hbutton * 2, LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);
        cmdClanGobal.setPos(xpaint + wkhung - 10,
            ypaint + hKhung / 2 - hbutton / 2, LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);

        scr_listUser = new Scroll();
        for (var i = 0; i < 10; i++)
        {
            //OtherChar user = new OtherChar((short)i, "user name "+(i+1)); 
            var user = new OtherChar((short) i, "user name " + (i + 1), (short) (i + 1), false);
            listUser_local.add(user);
        }
        scr_listUser.selectedItem = -1;
    }

    public void perform(int idAction, object p)
    {
        // TODO Auto-generated method stub
        //Cout.println2222("perform id  "+idAction);

        switch (idAction)
        {
            case 2:
                GameScr.gI().switchToMe();
                break;
            case 3:

                if (scr_listUser.selectedItem > -1)
                {
                    if (isTabLocal && scr_listUser.selectedItem < listUser_local.size())
                    {
                        var other = (OtherChar) listUser_local.elementAt(scr_listUser.selectedItem);
                        if (other != null)
                            Service.getInstance().Clan_KichKhoiBang(other.id);
                    }
                    else if (!isTabLocal && scr_listUser.selectedItem < listUser_gobal.size())
                    {
                        Debug.Log("ngoai");
                        var other = (OtherChar) listUser_gobal.elementAt(scr_listUser.selectedItem);
                        if (other != null)
                        {
                            Debug.Log("trong");
                            Service.getInstance().Clan_KichKhoiBang(other.id);
                        }
                    }
                    Debug.Log("isTab: " + isTabLocal + " |scr_list:" + scr_listUser.selectedItem +
                              " |listUsergolbalSize: " + listUser_gobal.size());
                }
                Service.getInstance().Clan_KichKhoiBang(21992);
                break;
            case 4:
                Service.getInstance().Clan_RoiBang();
                break;
            case 5:
                Service.getInstance().Clan_XoaBang();
                break;
            case 6:
                isTabLocal = true;
                Clan.typeKick = Clan.kickLocal;
                Clan.typeLeave = Clan.leaveLocal;
                clanName = gI().clanLocalName;
                gI().KICK_CLAN = KICK_CLAN_LOCAL;
                gI().LEAVE_CLAN = LEAVE_CLAN_LOCAL;
                break;
            case 7:
                isTabLocal = false;
                Clan.typeKick = Clan.kickGlobal;
                Clan.typeLeave = Clan.leaveGlobal;
                clanName = gI().clanGolbalName;
                gI().KICK_CLAN = KICK_CLAN_GOBAL;
                gI().LEAVE_CLAN = LEAVE_CLAN_GOBAL;
                break;
        }
    }

    public static ClanScreen gI()
    {
        if (me == null)
            return me = new ClanScreen();
        return me;
    }

    public override void update()
    {
        GameScr.gI().update();
        indexRow = scr_listUser.selectedItem;
    }

    public override void updateKey()
    {
        base.updateKey();

        if (GameCanvas.keyPressed[13] || getCmdPointerLast(cmdClose))
            if (cmdClose != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                if (cmdClose != null)
                {
                    cmdClose.performAction();
                    GameCanvas.clearPointerEvent();
                }
            }
        if (getCmdPointerLast(cmdKich))
            if (cmdKich != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                if (cmdKich != null)
                    cmdKich.performAction();
            }

        if (getCmdPointerLast(cmdRoi))
            if (cmdRoi != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                if (cmdRoi != null)
                {
                    cmdRoi.performAction();
                    GameCanvas.clearPointerEvent();
                }
            }
        if (getCmdPointerLast(CmdGiaiTan))
            if (CmdGiaiTan != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                if (CmdGiaiTan != null)
                {
                    CmdGiaiTan.performAction();
                    GameCanvas.clearPointerEvent();
                }
            }
        if (getCmdPointerLast(cmdClanLocal))
            if (cmdClanLocal != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                if (cmdClanLocal != null)
                {
                    cmdClanLocal.performAction();
                    GameCanvas.clearPointerEvent();
                }
            }
        if (getCmdPointerLast(cmdClanGobal))
            if (cmdClanGobal != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                if (cmdClanGobal != null)
                {
                    cmdClanGobal.performAction();
                    GameCanvas.clearPointerEvent();
                }
            }

        var s1 = scr_listUser.updateKey();
        scr_listUser.updatecm();
    }

    //@Override
    public override void paint(mGraphics g)
    {
        // TODO Auto-generated method stub
        base.paint(g);
        GameScr.gI().paint(g);
        g.setColor(0x000000, GameCanvas.opacityTab);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
        g.disableBlending();

        Paint.paintFrameNaruto(xpaint, ypaint, wkhung, hKhung + 2, g);
        Paint.PaintBoxName("Bang hội", xpaint + wkhung / 2 - 40, ypaint, 80, g);
        //Paint background Clan name
        g.drawImage(LoadImageInterface.imgName, xpaint + 120, ypaint + 32, mGraphics.VCENTER | mGraphics.HCENTER);
        //Paint Clan name
        if (isTabLocal) clanName = gI().clanLocalName;
        else clanName = gI().clanGolbalName;
        mFont.tahoma_7_white.drawString(g, clanName, xpaint + 120, ypaint + 1 + 1 * 24, 2);

        if (scr_listUser.selectedItem > -1)
        {
            //Xu ly la chu guid moi kick hoac giai tan
            cmdRoi.paint(g);
            if (gI().isBoss)
            {
                cmdKich.paint(g);
                CmdGiaiTan.paint(g);
            }
            else if (gI().isBossGobal)
            {
                cmdKich.paint(g);
                CmdGiaiTan.paint(g);
            }
        }
        cmdClanGobal.paint(g);
        cmdClanLocal.paint(g);
        cmdClose.paint(g);
        if (scr_listUser.selectedItem > -1)
        {
            if (cmdKich.isFocusing())
                cmdKich.paint(g);
            if (cmdRoi.isFocusing())
                cmdRoi.paint(g);
            if (CmdGiaiTan.isFocusing())
                CmdGiaiTan.paint(g);
        }

        scr_listUser.setStyle(isTabLocal ? listUser_local.size() : listUser_gobal.size(), 50, xpaint + 5,
            ypaint + 40, wkhung - 3, 180, true, 1);
        scr_listUser.setClip(g, xpaint + 5, ypaint + 40, wkhung - 3, 140);


        if (isTabLocal)
        {
            Debug.Log("local");
            var yBGClan = 0;
            for (var i = 0; i < listUser_local.size(); i++)
            {
                var user = (OtherChar) listUser_local.elementAt(i);
                if (user != null && user.name != null)
                {
                    Paint.PaintBGListQuest(xpaint + 5 + 35, ypaint + 40 + yBGClan, 160, g);
                    if (indexRow == i)
                    {
                        Paint.PaintBGListQuestFocus(xpaint + 5 + 35, ypaint + 40 + yBGClan, 160, g);
                        idCharClan = user.idchar;
                    }
                    mFont.tahoma_7_white.drawString(g, "Level: " + i, xpaint + 5 + 73, ypaint + 40 + yBGClan + 24, 0,
                        true);
                    g.drawImage(LoadImageInterface.charPic, xpaint + 5 + 45, ypaint + 40 + yBGClan + 20,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                    g.drawImage(LoadImageInterface.imgName, xpaint + 5 + 100, ypaint + 40 + yBGClan + 15,
                        mGraphics.VCENTER | mGraphics.HCENTER, true);
                    mFont.tahoma_7_white.drawString(g, user.name, xpaint + 5 + 73, ypaint + 40 + yBGClan + 8, 0, true);
                    g.drawImage(LoadImageInterface.imgOnline[1], xpaint + 5 + 180, ypaint + 40 + yBGClan + 20,
                        mGraphics.VCENTER | mGraphics.HCENTER, true);
                }

                yBGClan += 50;
            }
        }
        else
        {
            //Khai bao luc dau load (DEFAULT load), de nhan dien la GOBAL khi kich hoac roi
            Clan.typeKick = Clan.kickGlobal;
            Clan.typeLeave = Clan.leaveGlobal;
            clanName = gI().clanGolbalName;
            gI().KICK_CLAN = KICK_CLAN_GOBAL;
            gI().LEAVE_CLAN = LEAVE_CLAN_GOBAL;
            //END
            var yBGClan = 0;
            for (var i = 0; i < listUser_gobal.size(); i++)
            {
                var user = (OtherChar) listUser_gobal.elementAt(i);
                if (user != null && user.name != null)
                {
                    Paint.PaintBGListQuest(xpaint + 5 + 35, ypaint + 40 + yBGClan, 160, g);
                    if (indexRow == i)
                    {
                        Paint.PaintBGListQuestFocus(xpaint + 5 + 35, ypaint + 40 + yBGClan, 160, g);
                        idCharClan = user.idchar;
                    }
                    mFont.tahoma_7_white.drawString(g, "Level: " + user.lvUser, xpaint + 5 + 73,
                        ypaint + 40 + yBGClan + 24, 0, true);
                    g.drawImage(LoadImageInterface.charPic, xpaint + 5 + 45, ypaint + 40 + yBGClan + 20,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                    g.drawImage(LoadImageInterface.imgName, xpaint + 5 + 100, ypaint + 40 + yBGClan + 15,
                        mGraphics.VCENTER | mGraphics.HCENTER, true);
                    mFont.tahoma_7_white.drawString(g, user.name, xpaint + 5 + 73, ypaint + 40 + yBGClan + 8, 0, true);
                    g.drawImage(LoadImageInterface.imgOnline[1], xpaint + 5 + 180, ypaint + 40 + yBGClan + 20,
                        mGraphics.VCENTER | mGraphics.HCENTER, true);
                }

                yBGClan += 50;
            }
        }
        GameCanvas.resetTrans(g);
        base.paint(g);
    }
}