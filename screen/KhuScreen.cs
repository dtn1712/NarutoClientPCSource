using src.lib;

public class KhuScreen : tScreen, IActionListener
{
    public static KhuScreen instance;
    private readonly Command cmdClose;
    public int coutFc;

    public sbyte[][] listKhu;
    public int minKhu = 15;
    public Scroll srclist = new Scroll();
    public int wKhung = 170, hKhung = 150;
    public int xpaint, ypaint;

    public KhuScreen()
    {
        xpaint = GameCanvas.w / 2 - wKhung / 2;
        ypaint = GameCanvas.h / 2 - hKhung / 2;
        cmdClose = new Command("", this, 2, null);
        cmdClose.setPos(xpaint + wKhung - LoadImageInterface.closeTab.width / 2,
            ypaint, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
    }

    //@Override
    public void perform(int idAction, object p)
    {
        // TODO Auto-generated method stub
        switch (idAction)
        {
            case 2:
                GameScr.gI().switchToMe();
                break;
        }
    }

    public static KhuScreen gI()
    {
        if (instance == null) instance = new KhuScreen();
        return instance;
    }

    //@Override
    public override void updateKey()
    {
        // TODO Auto-generated method stub

        if (GameCanvas.keyPressed[5] || getCmdPointerLast(cmdClose))
            if (cmdClose != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                if (cmdClose != null)
                    cmdClose.performAction();
            }
        var s1 = srclist.updateKey();
        srclist.updatecm();
        if (GameCanvas.isPointerJustRelease && srclist.selectedItem != -1 && srclist.selectedItem < listKhu.Length)
        {
            Service.getInstance().requestChangeRegion((sbyte) srclist.selectedItem);
            GameCanvas.isPointerJustRelease = false;
        }

        base.updateKey();
    }

    public override void update()
    {
        // TODO Auto-generated method stub
        base.update();
        GameScr.gI().update();
        if (GameCanvas.gameTick % 4 == 0)
        {
            coutFc++;
            if (coutFc > 2)
                coutFc = 0;
        }
    }

    public override void paint(mGraphics g)
    {
        // TODO Auto-generated method stub
        base.paint(g);
        GameScr.gI().paint(g);
        Paint.paintFrameNaruto(xpaint, ypaint, wKhung, hKhung + 2, g);
        Paint.PaintBoxName("Khu", xpaint + wKhung / 2 - 40, ypaint, 80, g);
        cmdClose.paint(g);
        if (listKhu != null)
        {
            srclist.setStyle((listKhu.Length > minKhu ? listKhu.Length : minKhu) / 5,
                Image.getWidth(LoadImageInterface.ImgItem),
                xpaint + 15, ypaint + 22 + Image.getHeight(LoadImageInterface.ImgItem) / 4,
                wKhung - 30, hKhung - 32, true, 5);
            srclist.setClip(g, xpaint + 15, ypaint + 22 + Image.getHeight(LoadImageInterface.ImgItem) / 4,
                wKhung - 30, hKhung - 32);
            for (var i = 0; i < (listKhu.Length > minKhu ? listKhu.Length : minKhu) / 5; i++)
            for (var j = 0; j < 5; j++)
            {
                g.drawImage(LoadImageInterface.ImgItem,
                    xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j,
                    ypaint + 30 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2, 0, true);
                if (i * 5 + j < listKhu.Length)
                    if (listKhu[i * 5 + j][0] == 0)
                        mFont.tahoma_7_green.drawString(g, i * 5 + j + 1 + "",
                            xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j +
                            Image.getWidth(LoadImageInterface.ImgItem) / 2,
                            ypaint + 23 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2 +
                            Image.getHeight(LoadImageInterface.ImgItem) / 2, 2);
                    else if (listKhu[i * 5 + j][0] == 1)
                        mFont.tahoma_7_yellow.drawString(g, i * 5 + j + 1 + "",
                            xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j +
                            Image.getWidth(LoadImageInterface.ImgItem) / 2,
                            ypaint + 23 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2 +
                            Image.getHeight(LoadImageInterface.ImgItem) / 2, 2);
                    else
                        mFont.tahoma_7_red.drawString(g, i * 5 + j + 1 + "",
                            xpaint + 15 + Image.getWidth(LoadImageInterface.ImgItem) * j +
                            Image.getWidth(LoadImageInterface.ImgItem) / 2,
                            ypaint + 23 + Image.getHeight(LoadImageInterface.ImgItem) * i + 2 +
                            Image.getHeight(LoadImageInterface.ImgItem) / 2, 2);
            }
            if (srclist.selectedItem > 0 && srclist.selectedItem < listKhu.Length)
                Paint.paintFocus(g,
                    xpaint + 15 + srclist.selectedItem % 5 * Image.getWidth(LoadImageInterface.ImgItem) + 11 -
                    LoadImageInterface.ImgItem.getWidth() / 4,
                    ypaint + 30 + srclist.selectedItem / 5 * Image.getHeight(LoadImageInterface.ImgItem) + 13 -
                    LoadImageInterface.ImgItem.getWidth() / 4
                    , LoadImageInterface.ImgItem.getWidth() - 9, LoadImageInterface.ImgItem.getWidth() - 9, coutFc);
            GameCanvas.resetTrans(g);
        }
    }
}