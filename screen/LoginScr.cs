using System;
using src.lib;
using src.network;

public class LoginScr : tScreen, IActionListener
{
    public static bool isLogout, isTest = true;
    public static int indexLocal;
    public static mBitmap imgTitle;
    public static LoginScr gII;

    public static bool isLoggingIn;
    private readonly Command cmdLogin;
    private Command cmdCheck;
    private readonly Command cmdFogetPass;
    private readonly Command cmdRes;
    private readonly Command cmdMenu;
    private readonly Command cmdLoginFB;
    private readonly Command cmdLoginGoogle;


    private string[] currentTip;
    private int dir = 1;
    private int focus;
    private int g;
    public bool isCheck, isRes = false;
    public bool isFAQ;

    public string listFAQ = "";
    private string numSupport = "";

    private string passRe = "";

    private string strNick = "";
    private string strUser, strPass;
    public TField tfIp;
    public TField tfPass;
    public TField tfRegPass;
    public TField tfUser;
    private int tipid = -1;

    private readonly int v = 2;
    private readonly int wC;
    private int yL;
    private readonly int defYL;
    private int ylogo = -40;

    private readonly int yt;

    public LoginScr()
    {
        imgTitle = GameCanvas.loadImage("/screen/title.png");
        gII = this;
        // ==============================
        var ip = Rms.loadRMSString("ipgame");
        if (ip != null) GameMidlet.IP = ip;
        TileMap.bgID = (sbyte) (mSystem.currentTimeMillis() % 9);
        if (TileMap.bgID == 5 || TileMap.bgID == 6)
            TileMap.bgID = 4;
        GameScr.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
        GameScr.cmx = 100;
        // ==============================
        if (GameCanvas.h > 200)
            defYL = GameCanvas.hh - 80;
        else
            defYL = GameCanvas.hh - 65;
        ResetLogo();
        wC = GameCanvas.w - 30;
        if (wC < 70)
            wC = 70;
        if (wC > 99)
            wC = 99;

        yt = GameCanvas.h - 150;
        yt = GameCanvas.h / 2 - 100 + 60 > 0 ? GameCanvas.h / 2 - 100 + 60 : 0;

        if (GameCanvas.h <= 160)
            yt = 20;

        tfUser = new TField
        {
            x = GameCanvas.hw - 75,
            y = yt + 20,
            width = wC + 50,
            height = ITEM_HEIGHT + 2,
            isFocus = false
        };
        tfUser.setIputType(TField.INPUT_ALPHA_NUMBER_ONLY);
        tfPass = new TField
        {
            x = GameCanvas.hw - 75,
            y = yt + 60,
            width = wC + 50,
            height = ITEM_HEIGHT + 2,
            isFocus = false
        };
        tfPass.setIputType(TField.INPUT_TYPE_PASSWORD);

        tfIp = new TField
        {
            x = GameCanvas.hw - 75,
            y = yt + 100,
            width = wC + 50,
            height = ITEM_HEIGHT + 2,
            isFocus = false
        };
        tfIp.setIputType(TField.INPUT_ALPHA_NUMBER_ONLY);

        tfRegPass = new TField
        {
            x = GameCanvas.hw - 20,
            y = yt += 35,
            width = wC,
            height = ITEM_HEIGHT + 2,
            isFocus = false
        };
        tfRegPass.setIputType(TField.INPUT_TYPE_PASSWORD);

        isCheck = true;

        var listauto = Rms.loadRMS(Rms.rms_Auto);
        if (listauto == null || listauto.Length < SettingScreen.defautAuto.Length)
        {
            Rms.saveRMS(Rms.rms_Auto, SettingScreen.defautAuto);
        }
        else
        {
            GameScr.isAutoDanh = listauto[SettingScreen.AUTO_DANH] == 1;
            GameScr.isAutoNhatItem = listauto[SettingScreen.AUTO_NHAT] == 1;
            Music.isplayMusic = listauto[SettingScreen.MUSIC] == 1;
            Music.isplaySound = listauto[SettingScreen.SOUND] == 1;
            GuiMain.isAnalog = listauto[SettingScreen.TYPE_MOVE] == 1;
        }

        var a = Rms.loadRMSInt("check");
        if (a == 1)
            isCheck = true;
        else if (a == 2)
            isCheck = false;
        
        tfUser.setText(Rms.loadRMSString("acc"));
        tfPass.setText(Rms.loadRMSString("pass"));
        tfPass.setText(Rms.loadRMSString("pass"));

        LogDebug.println("logScr load cmd ");
        focus = 0;
        cmdLogin = new Command(GameCanvas.w > 200 ? mResources.LOGIN1 : mResources.LOGIN2, this, 2000, null);
        cmdLogin.setPos(tfPass.x - 2 + 80, tfPass.y + 30 + (isTest ? 35 : 0), LoadImageInterface.btnLogin0,
            LoadImageInterface.btnLogin1);
        cmdCheck = new Command(mResources.REMEMBER, this, 2001, null);
        cmdRes = new Command(mResources.REGISTER, this, 2002, null);

        cmdRes.setPos(tfPass.x - 2 + 38, tfPass.y + 30 + (isTest ? 35 : 0), LoadImageInterface.btnLogin0,
            LoadImageInterface.btnLogin1);
        cmdFogetPass = new Command(mResources.FORGETPASS, this, 2004, null);
        cmdMenu = new Command(mResources.MENU, this, 2003, null);
        cmdMenu.setPos(tfPass.x - 2 + 40, tfPass.y + 30 + (isTest ? 35 : 0), LoadImageInterface.btnLogin0,
            LoadImageInterface.btnLogin1);

        cmdLoginFB = new Command("", this, 2010, null);
        cmdLoginFB.setPos(tfPass.x - 2, tfPass.y + 30 + (isTest ? 35 : 0), LoadImageInterface.imgIconFb0,
            LoadImageInterface.imgIconFb1);

        cmdLoginGoogle = new Command("", this, 2011, null);
        cmdLoginGoogle.setPos(tfPass.x + 120, tfPass.y + 30 + (isTest ? 35 : 0), LoadImageInterface.imgIconGoogle0,
            LoadImageInterface.imgIconGoogle1);
    }

    public void perform(int idAction, object p)
    {
        switch (idAction)
        {
            case 2010:
                GameCanvas.StartDglThongBao("Chức năng đang phát triển");
                break;
            case 2011:
                GameCanvas.StartDglThongBao("Chức năng đang phát triển");
                break;
            case 1000:
                break;
            case 2000:
                GameCanvas.menu.showMenu = false;
                doLogin();
                break;
            case 20001:
                GameCanvas.menu.showMenu = false;
                doLogin();
                break;
            case 20002:
                GameCanvas.menu.showMenu = false;

                doLogin();
                break;
            case 20003:
                GameCanvas.menu.showMenu = false;

                doLogin();
                break;
            case 20004:
                GameCanvas.menu.showMenu = false;

                doLogin();
                break;
            case 20005:
                GameCanvas.menu.showMenu = false;

                doLogin();
                break;
            case 200041:
                GameCanvas.menu.showMenu = false;

                doLogin();
                break;
            case 200042:
                GameCanvas.menu.showMenu = false;

                doLogin();
                break;
            case 2002:
                doRegister();
                break;
            case 2003:
                doMenu();
                break;
            case 2004:
                GameCanvas.inputDlg.show(mResources.INPUT_NICK, new Command(mResources.OK, this, 20041, null),
                    TField.INPUT_TYPE_ANY);
                break;
            case 20041:

                LogDebug.println("doGetForgetPass   " + 20041);
                strNick = GameCanvas.inputDlg.tfInput.getText();
                GameCanvas.endDlg();
                if (strNick.Equals(""))
                    GameCanvas.startOKDlg(mResources.NOT_INPUT_USERNAME);
                else
                    GameCanvas.startYesNoDlg(mResources.ASK_REG_NUM, new Command(mResources.YES, this, 200421, null),
                        new Command(mResources.NO, this, 200422, null));
                break;
            case 200421:
                LogDebug.println("doGetForgetPass   " + 200421);
                GameCanvas.endDlg();
                doGetForgetPass(strNick);
                break;
            case 200422:
                GameCanvas.startOKDlg(mResources.replace(mResources.GETPASS_BY_NUMPHONE, strNick));
                break;
            case 4000:
                doRegister(tfUser.getText());
                break;
            case 5001:
                GameMidlet.exit();
                break;
            case 5002:
                GameCanvas.currentDialog = null;
                break;
        }
    }

    public override void switchToMe()
    {
        ResetLogo();
        GameScr.gH = GameCanvas.h;
        GameCanvas.loadBG(0);
        base.switchToMe();
        
        if (GameCanvas.menu != null)
            GameCanvas.menu = new Menu();
        GameCanvas.menu = new Menu();

        var isSoftKey = Rms.loadRMSInt("isSoftKey");
        if (isSoftKey <= 0)
        {
            Rms.saveRMSInt("isSoftKey", 1);
            GameScr.isTouchKey = true;
        }
        else if (isSoftKey == 1)
        {
            GameScr.isTouchKey = true;
        }
        else if (isSoftKey == 2)
        {
            GameScr.isTouchKey = false;
        }
        GameScr.isTouchKey = false;

        var sound = Rms.loadRMSInt("isSound");
        if (sound < 0)
            Rms.saveRMSInt("isSound", 1);
        
        Music.init();
        Music.play(Music.MLogin, 5);
        if (isLogout)
        {
            isLogout = false;
            GameCanvas.gameScr = null;
        }
    }

    public static LoginScr gI()
    {
        return gII ?? (gII = new LoginScr());
    }

    private void doMenu()
    {
        var menu = new Vector();

        menu.addElement(new Command(mResources.NEWREGISTER, this, 1002, null));
        menu.addElement(cmdFogetPass);
        menu.addElement(new Command(mResources.FORUM, this, 1003, null));
        menu.addElement(new Command(mResources.CONFIG, this, 1006, null));

        menu.addElement(new Command(mResources.EXIT, GameCanvas.instance, 8885, null));
        GameCanvas.menu.startAt(menu);
    }

    private void doRegister()
    {
        if (tfUser.getText().Equals(""))
        {
            GameCanvas.startOKDlg(mResources.NOT_INPUT_USERNAME);
            return;
        }
        var ch = tfUser.getText().ToCharArray();
        for (var i = 0; i < ch.Length; i++)
            if (!TField.setNormal(ch[i]))
            {
                GameCanvas.startOKDlg(mResources.NOT_SPEC_CHARACTER);
                return;
            }
        if (tfPass.getText().Equals(""))
        {
            GameCanvas.startOKDlg(mResources.NOT_INPUT_PASS1);
            return;
        }

        if (tfUser.getText().Length < 8)
        {
            GameCanvas.startOKDlg(mResources.USERNAME_LENGHT);
            return;
        }

        GameCanvas.msgdlg.setInfo(
            mResources.REGISTER_TEXT[0] + " " + tfUser.getText() + " với mật khẩu " + tfPass.getText() + " " +
            mResources.REGISTER_TEXT[1], new Command(mResources.ACCEPT, this, 4000, null), null,
            new Command(mResources.NO, GameCanvas.instance, 8882, null));
        GameCanvas.currentDialog = GameCanvas.msgdlg;
    }

    private void doRegister(string user)
    {
        isFAQ = false;

        GameCanvas.startWaitDlg(mResources.CONNECTING);
        GameCanvas.connect();
        GameCanvas.startWaitDlg(mResources.REGISTERING);

        passRe = tfPass.getText();
        Service.getInstance().requestRegister(user, tfPass.getText());
    }

    private void doGetForgetPass(string user)
    {
        isFAQ = false;
        GameMidlet.IP = GameMidlet.IPS1;
        GameCanvas.startWaitDlg(mResources.CONNECTING);
        GameCanvas.connect();
        GameCanvas.startWaitDlg(mResources.PLEASEWAIT);
    }

    private void doLogin()
    {
        isFAQ = true;
        GameCanvas.startWaitDlg("Đang đăng nhập");
        var user = tfUser.getText().Trim(); //.ToLower()
        var pass = tfPass.getText().Trim(); //.ToLower()

        if (tfIp.getText() != null && tfIp.getText().Trim().Length > 0)
        {
            var ip = tfIp.getText();
            if (ip.Contains("@"))
            {
                try
                {
                    var text_ip = ip.Split('@');
                    if (text_ip.Length > 1)
                    {
                        Rms.saveRMSString("ipgame", text_ip[0]);
                        GameMidlet.IP = text_ip[0].ToLower().Trim();
                        GameMidlet.VERSION = text_ip[1];
                    }
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                }
            }
            else
            {
                Rms.saveRMSString("ipgame", tfIp.getText());
                GameMidlet.IP = tfIp.getText().ToLower().Trim();
            }
        }

        savePass(user, pass);

        if (user.Equals("a") && pass.Equals("a"))
            indexLocal = 1;
        else if (user.Equals("b") && pass.Equals("b"))
            indexLocal = 2;
        if (user == null || pass == null || GameMidlet.VERSION == null || user.Equals(""))
        {
            LogDebug.println(" reutnr doLogingggggggggggg");
            return;
        }
        if (user.Equals(""))
        {
            LogDebug.println(" reutnr 2 doLogingggggggggggg");
            return;
        }
        if (pass.Equals(""))
        {
            focus = 1;
            tfUser.isFocus = false;
            tfPass.isFocus = true;
            LogDebug.println(" reutnr 3 doLogingggggggggggg");
            return;
        }


        GameCanvas.connect();


        var listauto = Rms.loadRMS(Rms.rms_Auto);
        if (listauto == null || listauto.Length < SettingScreen.defautAuto.Length)
        {
            Rms.saveRMS(Rms.rms_Auto, SettingScreen.defautAuto);
        }
        else
        {
            if (listauto[SettingScreen.AUTO_DANH] == 1) GameScr.isAutoDanh = true;
            else GameScr.isAutoDanh = false;

            if (listauto[SettingScreen.AUTO_NHAT] == 1) GameScr.isAutoNhatItem = true;
            else GameScr.isAutoNhatItem = false;
        }
        Service.getInstance().doLogin(user, pass, (byte) mGraphics.zoomLevel); //
        isLoggingIn = true;
        savePass(user, pass);
        focus = 0;
    }

    private static void savePass(string user, string pass)
    {
        Rms.saveRMSInt("check", 1);
        Rms.saveRMSString("acc", user);
        Rms.saveRMSString("pass", pass);
    }


    public override void update()
    {
        updateKey();
        if (connectCallBack && !isConnect)
        {
            isConnect = connectCallBack = false;
            if (!isCloseConnect)
                GameCanvas.startOKDlg(mResources.SERVER_MAINTENANCE);
            else
                GameCanvas.startOKDlg("Mất kết nối với máy chủ. Vui lòng thử lại !");
            isCloseConnect = false;
        }
        if (GameCanvas.imgCloud == null && GameCanvas.gameTick % 2 == 0 && GameCanvas.imgBG != null &&
            GameCanvas.imgBG[0] == null)
            GameCanvas.loadBG(0);
        GameScr.cmx++;
        if (GameScr.cmx > GameCanvas.w * 3 + 100)
            GameScr.cmx = 100;

        tfUser.update();
        tfPass.update();
        if (isTest)
        {
            tfIp.update();
            if (isRes)
                tfRegPass.update();
        }
        updateLogo();


        //
        if (g >= 0)
        {
            ylogo += dir * g;
            g += dir * v;
            if (g <= 0)
                dir *= -1;
            if (ylogo > 0)
            {
                dir *= -1;
                g -= 2 * v;
            }
        }
        if (tipid >= 0 && GameCanvas.gameTick % 100 == 0)
        {
            tipid++;
            if (tipid >= mResources.tips.Length) tipid = 0;
            currentTip = mFont.tahoma_7_white.splitFontArray(mResources.tips[tipid], GameCanvas.w - 40);
        }
    }

    public void updateLogo()
    {
        if (defYL != yL)
            yL += (defYL - yL) >> 1;
    }

    public override void keyPress(int keyCode)
    {
        if (tfUser.isFocus)
            tfUser.keyPressed(keyCode);

        else if (tfPass.isFocus)

            tfPass.keyPressed(keyCode);

        else if (isRes && tfRegPass.isFocus)

            tfRegPass.keyPressed(keyCode);
        else if (tfIp.isFocus)

            tfIp.keyPressed(keyCode);

        base.keyPress(keyCode);
    }

    public override void paint(mGraphics g)
    {
        g.setColor(0x3aadfe);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);

        GameCanvas.paintBGGameScr(g);
        GameCanvas.resetTrans(g);
        if (GameCanvas.currentDialog == null)
        {
            Paint.SubFrame(GameCanvas.hw - 85, yt - 60, 170, 160 + (isTest ? 40 : 0), g);

            if (imgTitle != null)
                g.drawImage(imgTitle, GameCanvas.hw, yt - 66, 3);
            mFont.tahoma_7_white.drawString(g, "Email or Phone", tfUser.x, tfUser.y - 12, 0);
            mFont.tahoma_7_white.drawString(g, "Password", tfPass.x, tfPass.y - 12, 0);
            tfUser.paint(g);
            tfPass.paint(g);
            if (isTest)
            {
                tfIp.paint(g);
                if (isRes)
                    tfRegPass.paint(g);
            }
            g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
        }
        else
        {
            if (currentTip != null)
                for (var i = 0; i < currentTip.Length; i++)
                    mFont.tahoma_7_white.drawString(g, currentTip[i], GameCanvas.w / 2, tfUser.y - 10 + 10 * i, 2);
        }

        var s = GameMidlet.VERSION;
        if (isLoggingIn)
            s = Session.strRecvByteCount;
        mFont.tahoma_7_grey.drawString(g, s, GameCanvas.w - 5, 5, 1);
        if (GameCanvas.currentDialog == null)
        {
            cmdLoginFB.paint(g);
            cmdLoginGoogle.paint(g);
            cmdLogin.paint(g);
            cmdRes.paint(g);
        }

        base.paint(g);
    }

    public override void updateKey()
    {
        if (getCmdPointerLast(cmdLoginFB))
        {
            cmdLoginFB.performAction();
            GameCanvas.clearPointerEvent();
        }
        if (getCmdPointerLast(cmdLoginGoogle))
        {
            cmdLoginGoogle.performAction();
            GameCanvas.clearPointerEvent();
        }
        if (getCmdPointerLast(cmdLogin))
        {
            cmdLogin.performAction();
            GameCanvas.clearPointerEvent();
        }
        if (getCmdPointerLast(cmdRes))
        {
            cmdRes.performAction();
            GameCanvas.clearPointerEvent();
        }


        base.updateKey();
    }

    private void ResetLogo()
    {
        yL = -50;
    }
}