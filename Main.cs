using System.Threading;
using src.lib;
using src.network;
using UnityEngine;

namespace src
{
    public class Main : MonoBehaviour
    {
        public static Main main;
        public static mGraphics g;
        public static string res = "res";
        public static string mainThreadName;
        public static bool started; //isExit dung cho windows

        // hai cho quan trong can luu y khi build ban client moi -----------------------------
        public static bool isPC, isWindowsPhone, isIPhone, isSprite;


        public static string IMEI;
        public static int level;

        void Start()
        {
            if (!started)
            {
                if (Thread.CurrentThread.Name != "Main")
                    Thread.CurrentThread.Name = "Main";
                mainThreadName = Thread.CurrentThread.Name;
                isPC = true; //nếu build cho Iphone thì đóng chỗ này lại-----------------
                started = true;
                level = Rms.loadRMSInt("levelScreenKN");

                Screen.SetResolution(800, 480, false); //enter res
                ScaleGUI.initScaleGUI();
                g = new mGraphics();
            }
        }


        int cout;

        void OnGUI()
        {
            if (cout < 10)
                return;
            checkInput();
            Session.update();
            if (Event.current.type.Equals(EventType.Repaint))
            {
                GameMidlet.GetGameMidlet().gameCanvas.paint(g);
                g.reset();
            }
        }


        bool isRun;

        void FixedUpdate()
        {
            Rms.update();
            cout++;
            if (cout < 10)
                return;
            if (!isRun)
            {
                isRun = true;
                tScreen.orientation = ScreenOrientation.Landscape;
                Application.runInBackground = true;
                Application.targetFrameRate = 30;
                useGUILayout = false;
                if (main == null)
                    main = this;


                IMEI = SystemInfo.deviceUniqueIdentifier;
                isIPhone = isPC != true;

                if (isPC)
                    Screen.fullScreen = false;

                g.CreateLineMaterial();
                if (mGraphics.zoomLevel == 1 && !isWindowsPhone)
                {
                    isSprite = true;
                }
            }
            ipKeyboard.update();


            GameMidlet.GetGameMidlet().gameCanvas.update();
            Image.update();
            DataInputStream.update();
            SMS.update();
        }

        //--------------------------------------------------
        static Material lineMaterial;


        //----------------------------------------------------------
        private void checkInput()
        {
            if (Input.GetMouseButtonDown(0))
            {    
                Vector3 touch = Input.mousePosition;

                GameMidlet.GetGameMidlet().gameCanvas.pointerPressed((int) (touch.x),
                    (int) ((UnityEngine.Screen.height - touch.y)) + mGraphics.addYWhenOpenKeyBoard * mGraphics.zoomLevel);
                lastMousePos.x = (touch.x);
                lastMousePos.y = (touch.y) + mGraphics.addYWhenOpenKeyBoard * mGraphics.zoomLevel;
            }
            if (Input.GetMouseButton(0))
            {
                //    isRightMouseClick = Input.GetMouseButton(0) ? false : true;    
                Vector3 touch = Input.mousePosition;

                GameMidlet.GetGameMidlet().gameCanvas.pointerDragged((int) (touch.x),
                    (int) ((UnityEngine.Screen.height - touch.y)) + mGraphics.addYWhenOpenKeyBoard * mGraphics.zoomLevel);
                lastMousePos.x = (touch.x);
                lastMousePos.y = (touch.y) + mGraphics.addYWhenOpenKeyBoard * mGraphics.zoomLevel;
            }
            if (Input.GetMouseButtonUp(0))
            {
                Vector3 touch = Input.mousePosition;
                lastMousePos.x = touch.x;
                lastMousePos.y = (touch.y) + mGraphics.addYWhenOpenKeyBoard * mGraphics.zoomLevel;

                GameMidlet.GetGameMidlet().gameCanvas.pointerReleased((int) (touch.x),
                    (int) ((UnityEngine.Screen.height - touch.y)) + mGraphics.addYWhenOpenKeyBoard * mGraphics.zoomLevel);
            }


            if (Input.anyKeyDown && (Event.current.type == EventType.KeyDown))
            {
                int keyCode = MyKeyMap.map(Event.current.keyCode);

                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                {
                    switch (Event.current.keyCode)
                    {
                        case KeyCode.Alpha2:
                            keyCode = 64;
                            break;
                        case KeyCode.Minus:
                            keyCode = 95;
                            break;
                    }
                }

                if (keyCode != 0)

                    GameMidlet.GetGameMidlet().gameCanvas.keyPressedd(keyCode);
            }
            if (Event.current.type == EventType.KeyUp)
            {
                int keyCode = MyKeyMap.map(Event.current.keyCode);
                if (keyCode != 0)


                    GameMidlet.GetGameMidlet().gameCanvas.keyReleasedd(keyCode);
            }   
        }

        void OnApplicationQuit()
        {
            Debug.LogWarning("APP QUIT");

            Session.gI().close();
            if (isPC)
                Application.Quit();
        }

        public static Image[] imgTileMapLogin;
        public static bool isMiniApp = true, isQuitApp, isReSume;

        void OnApplicationPause(bool paused)
        {
            isReSume = false;
            if (paused)
            {
                if (GameCanvas.currentDialog != null)
                {
                }
            }
            else
            {
                isReSume = true;
            }
            // khi build cho may mac dong khoi lenh nay lai 
            //if (TouchScreenKeyboard.visible)
            //{
            //    TField.kb.active = false;
            //    TField.kb = null;
            //}

            if (isQuitApp)
                Application.Quit();
        }
        Vector2 lastMousePos = new Vector2();

    }
}