using src.lib;
using src.Objectgame.quest;

public class GuiQuest : tScreen, IActionListener
{
    public static readonly Vector listNhacNv = new Vector();
    
    private int indexRow = -1, indexRowUnRecive;
    private int indexselectNV; //0 nv moi, 1 nv da nhan
    private int indexSize;
    private readonly Scroll info = new Scroll();
    private readonly string[] nameMenu = {"Chính", "Phụ", "Phó bản"};
    private int selectTab; //chinh,phu,phoban
    private readonly  Vector VecTabScreen = new Vector();
    private readonly int wMenu;
    private readonly int hMenu;
    private int wScollReceived = 100, hScollReceived = 60;

    private readonly int x;
    private readonly int y;
    private const int width = 180;
    private const int height = 232;
    private const int widthSub = 160;
    private const int heightSub = 230;
    private readonly int xSub;

    private readonly int xM; //diem nam giua frame
    private readonly int xMenu;
    private readonly int yMenu;

    public GuiQuest(int x, int yy)
    {
        indexselectNV = 0;
        var wall = width + 30 + widthSub;
        this.x = GameCanvas.w / 2 - wall / 2;
        y = GameCanvas.h / 2 - heightSub / 2 < 0 ? 0 : GameCanvas.h / 2 - heightSub / 2;

        var xdem = this.x + widthSub;
        xSub = this.x;
        this.x = xdem + 10;
        xMenu = this.x + width - 15;
        yMenu = y + 40;
        xM = this.x + width / 2;

        var qMain = new QMain();
        VecTabScreen.addElement(qMain);

        var qMain1 = new QMain();
        VecTabScreen.addElement(qMain1);

        var qMain2 = new QMain();
        VecTabScreen.addElement(qMain2);
    }

    public void perform(int idAction, object p)
    {
    }

    public void SetPosClose(Command cmd)
    {
        cmd.setPos(x + width - Image.getWidth(LoadImageInterface.closeTab), y, LoadImageInterface.closeTab,
            LoadImageInterface.closeTab);
    }

    public void updateKey()
    {
        info.updatecm();
        info.updateKey();
        
        if (GameCanvas.isPointer(x + 40, y + 70, 160, Image.getHeight(LoadImageInterface.bgQuestLineFocus)) &&
            selectTab == 0)
        {
            indexselectNV = 0;
            GameCanvas.isPointerClick = false;
        }
        else if (GameCanvas.isPointer(x + 40, y + 165, 160, Image.getHeight(LoadImageInterface.bgQuestLineFocus)) &&
                 selectTab == 0)
        {
            indexselectNV = 1;
            GameCanvas.isPointerClick = false;
        }
        for (var i = 0; i < VecTabScreen.size(); i++)
        {
            if (!GameCanvas.isPointer(xMenu, yMenu + (5 + Image.getHeight(LoadImageInterface.btnTab)) * i,
                Image.getWidth(LoadImageInterface.btnTab), Image.getHeight(LoadImageInterface.btnTab)))
            {
                continue;
            }
            
            selectTab = i;
            indexselectNV = -1;
            GameCanvas.isPointerClick = false;
        }
    }


    public void paint(mGraphics g)
    {
        GameScr.resetTranslate(g);
        g.setColor(0x000000, GameCanvas.opacityTab);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
        g.disableBlending();


        for (var i = 0; i < VecTabScreen.size(); i++)
        {
            g.drawImage(LoadImageInterface.btnTab, xMenu, yMenu + (5 + Image.getHeight(LoadImageInterface.btnTab)) * i,0);
        }
        
        for (var i = 0; i < VecTabScreen.size(); i++)
        {   
            if (i != selectTab)
            {
                mFont.tahoma_7b_white.drawString(g, nameMenu[i], xMenu + 32,
                    yMenu + Image.getHeight(LoadImageInterface.btnTab) / 4 +
                    (5 + Image.getHeight(LoadImageInterface.btnTab)) * i, 2);
                
            }
            
        }
        
        Paint.paintFrameNaruto(x, y, width, height, g);
        Paint.PaintBoxName("Nhiệm vụ" + nameMenu[selectTab].ToLower(), x + 45, y, width - 90, g);
        Paint.SubFrame(xSub, y, widthSub, heightSub, g); //sub

        paintNewQuest(g);
        Paint.PaintLine(x + 5, y + 125, width - 30, g); //line
        paintWorkingQuest(g);
        
        g.drawImage(LoadImageInterface.btnTabFocus, xMenu, yMenu + (5 + Image.getHeight(LoadImageInterface.btnTabFocus)) * selectTab, 0);
        
        for (var i = 0; i < VecTabScreen.size(); i++)
        {    
            if (i == selectTab)
            {
                mFont.tahoma_7b_white.drawString(g, nameMenu[i], xMenu + 32, yMenu + Image.getHeight(LoadImageInterface.btnTab) / 4 + (5 + Image.getHeight(LoadImageInterface.btnTab)) * i, 2);
            }
        }
    }

    private void paintInfoQuestFocus(mGraphics g, int x, int y, QuestTemplate q, byte typequest)
    {
        var lenghtContentDis = q.ContentPaint.Length;
        var lenghtContentRemind = q.RemindContent.Length;
        var size = 3 + (q.SupportContentPaint != null ? +q.SupportContentPaint.Length : 0);
        size += typequest == 1 ? 3 + q.SupportContentPaint.Length : 0; //yeu cau
        if (q.Gold > 0) size += 2;
        if (q.Xu > 0) size += 2;
        info.setStyle(size, 12, x - 5, y, widthSub, heightSub - 20, true, 1);
        info.setClip(g, x - 5, y, widthSub, heightSub - 20);
        
        //name
        g.drawImage(LoadImageInterface.imgName, x + 30, y + 10, mGraphics.VCENTER | mGraphics.HCENTER);
        mFont.tahoma_7b_white.drawString(g, "Nhiệm vụ", x + 30, y + 5, 2);
        mFont.tahoma_7b_yellow.drawString(g, q.Name, x + 15, y + 20, 0);
        
        // mo ta
        g.drawImage(LoadImageInterface.imgName, x + 30, y + 40, mGraphics.VCENTER | mGraphics.HCENTER);
        mFont.tahoma_7b_white.drawString(g, "Mổ tả", x + 30, y + 35, 2);
        if (q.ContentPaint != null)
        {
            for (var i = 0; i < q.ContentPaint.Length; i++)
            {
                mFont.tahoma_7_white.drawString(g, q.ContentPaint[i], x + 15, y + 50 + 12 * i, 0);
            }
        }
            
        switch (typequest)
        {
            case 0:
                // qua
                g.drawImage(LoadImageInterface.imgName, x + 30, y + 65 + 12 * lenghtContentDis,
                    mGraphics.VCENTER | mGraphics.HCENTER);
                mFont.tahoma_7b_white.drawString(g, "Thưởng", x + 30, y + 60 + 12 * lenghtContentDis, 2);
                if (q.Gold > 0)
                {
                    g.drawImage(LoadImageInterface.coins[1], x + 15, y + 75 + 12 * lenghtContentDis,
                        mGraphics.TOP | mGraphics.LEFT);
                    mFont.tahoma_7_white.drawString(g, q.Gold + "", x + 40, (y + 80 + 12 * lenghtContentDis) + 5,
                        0);
                }
                if (q.Xu > 0)
                {
                    g.drawImage(LoadImageInterface.coins[0], x + 15,
                        y + 75 + (q.Gold > 0 ? 20 : 0) + 12 * lenghtContentDis,
                        mGraphics.TOP | mGraphics.LEFT);
                    mFont.tahoma_7_white.drawString(g, q.Xu + "", x + 40,
                        (y + 75 + (q.Gold > 0 ? 28 : 0) + 12 * lenghtContentDis) + 5, 0);
                }

                break;
            case 1: //dag lam
                g.drawImage(LoadImageInterface.imgName, x + 30, y + 60 + 12 * lenghtContentDis,
                    mGraphics.VCENTER | mGraphics.HCENTER);
                mFont.tahoma_7b_white.drawString(g, "Yêu cầu", x + 30, y + 55 + 12 * lenghtContentDis, 2);
                if (lenghtContentRemind != null)
                    for (var i = 0; i < q.RemindContentPaint.Length; i++)
                        mFont.tahoma_7_white.drawString(g, q.RemindContentPaint[i], x + 15,
                            y + 70 + 12 * q.RemindContentPaint.Length + 12 * i, 0);
                // qua
                g.drawImage(LoadImageInterface.imgName, x + 30,
                    y + 85 + 12 * q.RemindContentPaint.Length + 12 * q.RemindContentPaint.Length,
                    mGraphics.VCENTER | mGraphics.HCENTER);
                
                mFont.tahoma_7b_white.drawString(g, "Thưởng", x + 30,
                    y + 80 + 12 * q.RemindContentPaint.Length + 12 * q.RemindContentPaint.Length, 2);
                if (q.Gold > 0)
                {
                    g.drawImage(LoadImageInterface.coins[1], x + 15,
                        y + 95 + 12 * q.RemindContentPaint.Length + 12 * q.RemindContentPaint.Length,
                        mGraphics.TOP | mGraphics.LEFT);
                    mFont.tahoma_7_white.drawString(g, q.Gold + "", x + 40,
                        (y + 100 + 12 * q.RemindContentPaint.Length + 12 * q.RemindContentPaint.Length) + 5, 
                        mGraphics.TOP | mGraphics.LEFT);
                }
                if (q.Xu > 0)
                {
                    g.drawImage(LoadImageInterface.coins[0], x + 15,
                        y + 95 + (q.Gold > 0 ? 20 : 0) + 12 * q.RemindContentPaint.Length +
                        12 * q.RemindContentPaint.Length, mGraphics.TOP | mGraphics.LEFT);
                    mFont.tahoma_7_white.drawString(g, q.Xu + "", x + 40,
                        (y + 95 + (q.Gold > 0 ? 28 : 0) + 12 * q.RemindContentPaint.Length +
                        12 * q.RemindContentPaint.Length) + 5, mGraphics.TOP | mGraphics.LEFT);
                }
                break;
        }
        GameCanvas.resetTrans(g);
    }

    private void paintNewQuest(mGraphics g)
    {
        //new quest
        g.drawImage(LoadImageInterface.imgName, xM, y + 50, mGraphics.VCENTER | mGraphics.HCENTER);
        mFont.tahoma_7b_white.drawString(g, "Mới", xM, y + 45, 2);
        Paint.PaintBGListQuest(x + 30, y + 70, width - 50, g); //new quest
        
        if (indexselectNV == 0 && MainQuestManager.getInstance().NewQuest != null && GameCanvas.gameTick / 10 % 2 == 0)
        {
            Paint.PaintBGListQuestFocus(x + 30, y + 70, width - 50, g); //new focus quest
        }

        //paint ds nv moi
        var questName = MainQuestManager.getInstance().NewQuest != null && selectTab == 0
            ? MainQuestManager.getInstance().NewQuest.Name
            : "Không có nhiệm vụ";
        
        mFont.tahoma_7b_yellow.drawString(g, questName, x + 30, y + 75, 0);
        
        // thong tin quest focus 
        if (indexselectNV == 0 && MainQuestManager.getInstance().NewQuest != null)
        {
            paintInfoQuestFocus(g, xSub + 15, y + 5, MainQuestManager.getInstance().NewQuest, 0);
        }
    }

    private void paintWorkingQuest(mGraphics g)
    {
        // Received
        g.drawImage(LoadImageInterface.imgName, xM, y + 145, mGraphics.VCENTER | mGraphics.HCENTER);
        mFont.tahoma_7b_white.drawString(g, "Đã nhận", xM, y + 140, 2);
        Paint.PaintBGListQuest(x + 30, y + 165, width - 50, g); //dang lam or hoan thanh

        if (indexselectNV == 1 && (MainQuestManager.getInstance().WorkingQuest != null || MainQuestManager.getInstance().FinishQuest != null) && GameCanvas.gameTick / 10 % 2 == 0)
        {
            Paint.PaintBGListQuestFocus(x + 30, y + 165, width - 50, g); //new focus quest
        }

        var questName = selectTab == 0 && MainQuestManager.getInstance().WorkingQuest != null
            ? MainQuestManager.getInstance().WorkingQuest.Name
            : "Không có nhiệm vụ";
        
        mFont.tahoma_7b_yellow.drawString(g, questName, x + 30, y + 170, 0);
            
        if (indexselectNV == 1 && MainQuestManager.getInstance().WorkingQuest != null)
        {
            paintInfoQuestFocus(g, xSub + 15, y + 5, MainQuestManager.getInstance().WorkingQuest, 1);
        }
    }
}