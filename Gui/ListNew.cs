using src.lib;

public class ListNew
{
    private Command cmdCenter = null;
    private int cmRun;
    public int cmtoX, cmx, cmdy, cmvy, cmxLim;

    private int cmvx, cmdx;
    public int maxW, itemH, maxH, maxSize, x, y, value;
    private int pa = 0;
    private int pointerDownFirstX;
    private readonly int[] pointerDownLastX = new int[3];
    private int pointerDownTime;
    private bool pointerIsDowning, isDownWhenRunning; //, wantUpdateList;
    private bool trans = false;
    private Vector vecCmd;


    public ListNew(int x, int y, int maxW, int maxH, int itemH, int maxSize,
        int limX)
    {
        this.x = x;
        this.y = y;
        this.maxW = maxW;
        this.maxH = maxH;
        this.itemH = itemH;
        this.maxSize = maxSize;
        cmxLim = limX;
    }


    public void update_Pos_UP_DOWN()
    {
        if (GameCanvas.isPointerDown)
            if (!pointerIsDowning && GameCanvas.isPointer(x, y, maxW, maxH))
            {
                for (var i = 0; i < pointerDownLastX.Length; i++)
                    pointerDownLastX[0] = GameCanvas.py;
                pointerDownFirstX = GameCanvas.py;
                pointerIsDowning = true;
                isDownWhenRunning = cmRun != 0;
                cmRun = 0;
            }
            else if (pointerIsDowning)
            {
                pointerDownTime++;
                if (pointerDownTime > 5 && pointerDownFirstX == GameCanvas.py
                    && !isDownWhenRunning)
                    pointerDownFirstX = -1000;
                var dx = GameCanvas.py - pointerDownLastX[0];
                if (dx != 0 && value != -1)
                    value = -1;
                for (var i = pointerDownLastX.Length - 1; i > 0; i--)
                    pointerDownLastX[i] = pointerDownLastX[i - 1];
                pointerDownLastX[0] = GameCanvas.py;

                cmtoX -= dx;
                if (cmtoX < 0)
                    cmtoX = 0;
                if (cmtoX > cmxLim)
                    cmtoX = cmxLim;
                if (cmx < 0 || cmx > cmxLim)
                    dx /= 2;
                cmx -= dx;
            }
        if (GameCanvas.isPointerClick && pointerIsDowning)
        {
            var dx = GameCanvas.py - pointerDownLastX[0];
            GameCanvas.isPointerClick = false;
            if (CRes.abs(dx) < 20
                && CRes.abs(GameCanvas.py - pointerDownFirstX) < 20
                && !isDownWhenRunning && GameCanvas.isPointerClick)
            {
                cmRun = 0;
                cmtoX = cmx;
                pointerDownFirstX = -1000;
                pointerDownTime = 0;
            }
            else if (value != -1 && pointerDownTime > 5)
            {
                pointerDownTime = 0;
            }
            else if (value == -1 && !isDownWhenRunning)
            {
                if (cmx < 0)
                {
                    cmtoX = 0;
                }
                else if (cmx > cmxLim)
                {
                    cmtoX = cmxLim;
                }
                else
                {
                    var s = GameCanvas.py - pointerDownLastX[0]
                            + (pointerDownLastX[0] - pointerDownLastX[1]) +
                            (pointerDownLastX[1] - pointerDownLastX[2]);
                    if (s > 10)
                        s = 10;
                    else if (s < -10)
                        s = -10;
                    else
                        s = 0;
                    cmRun = -s * 100;
                }
            }
            pointerIsDowning = false;
            pointerDownTime = 0;
            GameCanvas.isPointerClick = false;
        }
    }

  

}