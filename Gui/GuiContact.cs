/*
 * Giao dien giao tiep giua cac nhan vat voi nhau
 */

using System;
using src.lib;

public class GuiContact : FatherChat
{
    public const int cTroChuyen = 2;
    public const int cKetban = 3;
    public const int cthongtin = 4;
    public const int cMoinhom = 5;
    public const int cGiaodich = 6;
    public const int cThachdau = 7;
    public const int cMoiVaoBang = 8;
    public const int cMoiVaoBangGobal = 9;

    private readonly string[] listName =
    {
        "Trò chuyện", "Kết bạn", "Thông tin", "Mời nhóm", "Giao dịch", "Thách đấu", "Mời vào bang local",
        "Mời vào bang gobal"
    };

    private readonly int x;
    private readonly int y;
    private readonly int xM; //toan do giua cua ma hinh


    public GuiContact(int x, int y)
    {
        popw = 156;
        poph = 203;
        xM = x;
        this.x = x - popw / 2;
        this.y = y;
        scrMain.selectedItem = -1;
    }


    private void PaintList(mGraphics g, string[] list, int x, int y, int width)
    {
        var nCol = width / Image.getWidth(LoadImageInterface.imgLineTrade);
        var yy = 0;
        for (var i = 0; i < list.Length; i++)
        {
            yy += 30;
            mFont.tahoma_7b_red.drawString(g, list[i], x + width / 2, y + yy - 17, 2);
            for (var j = 0; j < nCol; j++)
                g.drawImage(LoadImageInterface.imgLineTrade,
                    x + (Image.getWidth(LoadImageInterface.imgLineTrade) - 1) * j + 13, y + yy, 0, true);
        }
    }

    public void Update()
    {
        // TODO Auto-generated method stub
        if (scrMain != null)
            scrMain.updatecm();
        if (scrMain.selectedItem > -1)
        {
            MenuIcon.isShowTab = false;

            switch (scrMain.selectedItem + 2)
            {
                case cTroChuyen:
                    scrMain.selectedItem = -1;
                    var isAdded = false;
                    for (var i = 0; i < ChatPrivate.vOtherchar.size(); i++)
                    {
                        var other = (OtherChar) ChatPrivate.vOtherchar.elementAt(i);
                        if (other.name.Equals(Char.myChar().charFocus.cName) ||
                            other.id == Char.myChar().charFocus.charID)
                        {
                            if (other.id != Char.myChar().charFocus.charID)
                                other.id = (short) Char.myChar().charFocus.charID;
                            isAdded = true;
                            break;
                        }
                    }
                    if (!isAdded && Char.myChar().charFocus != null)
                        ChatPrivate.AddNewChater((short) Char.myChar().charFocus.charID, Char.myChar().charFocus.cName);
                    TabChat.gI().switchToMe();
                    GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    break;
                case cKetban:
                    scrMain.selectedItem = -1;
                    if (Char.myChar().charFocus != null)
                        Service.getInstance().requestAddfriend(0, (short) Char.myChar().charFocus.charID);
                    GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    break;
                case cthongtin:
                    scrMain.selectedItem = -1;
                    GameCanvas.StartDglThongBao("Chức năng đang phát triển");
                    GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    break;
                case cMoinhom:
                    scrMain.selectedItem = -1;
                    if (Char.myChar().charFocus != null)
                        Service.getInstance().inviteParty((short) Char.myChar().charFocus.charID, 0);
                    GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    break;
                case cGiaodich:
                    scrMain.selectedItem = -1;
                    Char.myChar().partnerTrade = null;
                    if (Char.myChar().charFocus != null)
                        Service.getInstance().inviteTrade(0, (short) Char.myChar().charFocus.charID);
                    GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    break;
                case cThachdau:
                    if (Char.myChar().charFocus != null)
                        Service.getInstance().ThachDau(0, (short) Char.myChar().charFocus.charID);
                    scrMain.selectedItem = -1;
                    GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    break;
                case cMoiVaoBang:
                    if (Char.myChar().charFocus != null)
                        Service.getInstance().Clan_MoiBang((short) Char.myChar().charFocus.cIdDB);

                    scrMain.selectedItem = -1;
                    GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    break;
                case cMoiVaoBangGobal:
                    if (Char.myChar().charFocus != null)
                        Service.getInstance().Clan_MoiBang_GOBAL((short) Char.myChar().charFocus.cIdDB);

                    scrMain.selectedItem = -1;
                    GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                    break;
            }
        }
    }

    public void UpdateKey()
    {
        // TODO Auto-generated method stub
        if (GameCanvas.isTouch)
        {
            var r = scrMain.updateKey();
            if (r.isDowning || r.isFinish)
                indexRow = r.selected;
        }
    }

    public void Paintt(mGraphics g)
    {
        GameScr.resetTranslate(g);

        g.setColor(0x000000, GameCanvas.opacityTab);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
        Paint.paintFrameNaruto(x, y, popw, poph, g);

        g.drawImage(LoadImageInterface.charPic, xM, y + poph / 3 - 22, mGraphics.VCENTER | mGraphics.HCENTER);
        try
        {
            var ph = GameScr.parts[Char.myChar().charFocus.head];
            SmallImage.drawSmallImage(g, ph.pi[Char.CharInfo[0][0][0]].id,
                xM - 1, y + poph / 3 - 24, 0, mGraphics.VCENTER | mGraphics.HCENTER);
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
        //		g.drawImage(Char.myChar().charFocus.,xM-3,y+poph/3-24,g.VCENTER|g.HCENTER);

        g.drawImage(LoadImageInterface.imgName, xM, y + poph / 3 + 8, mGraphics.VCENTER | mGraphics.HCENTER);

        if (Char.myChar().charFocus != null && Char.myChar().charFocus.cName != null)
            mFont.tahoma_7b_white.drawString(g, Char.myChar().charFocus.cName, xM, y + poph / 3 + 3, mGraphics.VCENTER);


        Paint.PaintBoxName("Giao Tiếp", xM - 40, y, 80, g);
        if (scrMain != null)
        {
            scrMain.setStyle(listName.Length, 30, x, y + 80, popw, 100, true, 0);
            scrMain.setClip(g);
        }

        if (indexRow >= 0)
        {
            g.setColor(0x964210);
            g.fillRect(x + 10, y + 87 + 30 * indexRow, popw - 20, 20);
        }
        PaintList(g, listName, x, y + 80, popw);
        GameCanvas.resetTrans(g);
    }

    public void SetPosClose(Command cmdClose)
    {
        // TODO Auto-generated method stub
        cmdClose.setPos(x + popw - Image.getWidth(LoadImageInterface.closeTab), y, LoadImageInterface.closeTab,
            LoadImageInterface.closeTab);
    }
}