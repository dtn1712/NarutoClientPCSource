using src.lib;

public class iCommand
{
    public static int wButtonCmd = 70, hButtonCmd = 24;
    public IAction action;
    public string caption, clickCaption = "";
    private FrameImage fraImgCaption, fraImageCmd;
    public int IdGiaotiep = 0;
    public sbyte indexMenu, subIndex = -1;
    public bool isSelect = false;
    private int nframe, beginframe;
    public tScreen Pointer;
    public string[] subCaption;
    private int wimgCaption, himgCaption, wstr, wimgCmd, himgCmd;
    public int xCmd = -1, yCmd = -1, frameCmd, xdich, ydich;


    public void setPos(int x, int y, FrameImage fra, string caption)
    {
        this.caption = caption;
        xCmd = x;
        yCmd = y;
        fraImageCmd = fra;
        if (fraImageCmd != null)
        {
            wimgCmd = fraImageCmd.frameWidth;
            himgCmd = fraImageCmd.frameHeight;
            if (wimgCmd < 28)
                wimgCmd = 28;
            if (himgCmd < 28)
                himgCmd = 28;
        }
        else
        {
            wimgCmd = 70;
            himgCmd = hButtonCmd;
        }
    }


    public void perform()
    {
        if (action != null)
        {
            GameCanvas.clearKeyHold();
            GameCanvas.clearKeyPressed();
        }
        else
        {
            if (Pointer != null)
            {
            }
            else
            {
                if (GameCanvas.currentDialog != null)
                {
                    GameCanvas.isPointerClick = false;
                    GameCanvas.clearKeyHold();
                    GameCanvas.clearKeyPressed();
                    return;
                }
                if (ChatTextField.gI().isShow)
                    return;
            }
        }
        GameCanvas.isPointerClick = false;
        GameCanvas.clearKeyHold();
        GameCanvas.clearKeyPressed();
    }

    public void paint(mGraphics g, int x, int y)
    {
        if (isPosCmd())
        {
            if (fraImageCmd != null)
                fraImageCmd.drawFrame(frameCmd, xCmd, yCmd, 0, 3, g);
            else
                paintbutton(g, xCmd, yCmd);
            paintCaptionImage(g, xCmd, yCmd - 6, 2);
        }
        else
        {
            if (fraImageCmd != null)
                fraImageCmd.drawFrame(frameCmd, x, y, 0, 3, g);
            else
                paintbutton(g, x, y);
            
            paintCaptionImage(g, x, y - 6, 2);
        }
    }

    public void paintCaptionImage(mGraphics g, int x, int y, int pos)
    {
        if (caption == null)
            return;
        
        if (fraImgCaption != null)
            if (pos == 2)
            {
                fraImgCaption.drawFrame(beginframe + GameCanvas.gameTick / 2
                                        % nframe, x - wimgCaption / 2 - wstr / 2, y
                                                                                  + himgCaption / 2 - 3, 0, 3, g);
            }
            else if (pos == 0)
            {
                fraImgCaption.drawFrame(beginframe + GameCanvas.gameTick / 2
                                        % nframe, x + wimgCaption / 2, y + himgCaption / 2 - 4,
                    0, 3, g);
            }
    }


    public void updatePointer()
    {
        if (isPosCmd())
            if (GameCanvas.isPointerDown)
                if (GameCanvas.isPoint(xCmd - wimgCmd / 2 - 5, yCmd - himgCmd
                                                               / 2 - 5, wimgCmd + 10, himgCmd + 10))
                {
                    frameCmd = 1;
                    perform();
                    GameCanvas.isPointerClick = false;
                }
                else
                {
                    frameCmd = 0;
                }
            else
                frameCmd = 0;
    }


    public void paintbutton(mGraphics g, int x, int y)
    {
        //if (GameCanvas.isTouch)
            //PaintInfoGameScreen.fraButton.drawFrame(frameCmd, x, y, 0, 3, g);
    }

    public bool isPosCmd()
    {
        if (xCmd > 0 && yCmd > 0)
            return true;
        return false;
    }
}