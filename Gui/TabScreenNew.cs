/*
 * this class is all menu game
 */

using System;
using src.lib;

public class TabScreenNew : tScreen, IActionListener
{
    public static Command cmdBack;
    public static int timeRepaint;
    public static int xback, yback;

    public static Command cmdClose;
    private readonly string[] nameMenu = {"Hành trang", "Trang bị", "Nhân vật", "Kỹ năng", "Nâng cấp"};
    public int selectTab;
    public Vector VecTabScreen = new Vector();
    private readonly int wMenu;
    private readonly int hMenu;


    private readonly int xMenu;
    private readonly int yMenu;


    public TabScreenNew()
    {
        try
        {
            cmdClose = new Command(" ", this, Constants.CMD_TAB_CLOSE, null, 0, 0);
            xback = 300;
            yback = 20;
            cmdClose.setPos(MainTabNew.gI().xTab + MainTabNew.gI().wtab2 - LoadImageInterface.closeTab.width / 2,
                MainTabNew.gI().yTab, LoadImageInterface.closeTab, LoadImageInterface.closeTab);
            var xm = GameCanvas.w - MainTabNew.gI().wtab2 - MainTabNew.gI().widthSubFrame > 0;
            xMenu = xm == false
                ? MainTabNew.gI().xTab - LoadImageInterface.btnTab.width / 4
                : MainTabNew.gI().xTab + MainTabNew.gI().wtab2 - 10;
            yMenu = 50;

            wMenu = Image.getWidth(LoadImageInterface.btnTab);
            hMenu = Image.getHeight(LoadImageInterface.btnTab);
        }
        catch (Exception e)
        {
            LogDebug.println(e.ToString());
        }
       
    }

    public void perform(int idAction, object p)
    {
        // TODO Auto-generated method stub
        switch (idAction)
        {
            case Constants.CMD_TAB_CLOSE:
                GameScr.gI().guiMain.menuIcon.cmdClose.performAction();
                GameScr.isBag = false;
                break;
        }
    }

    public void addMoreTab(Vector t)
    {
        VecTabScreen.removeAllElements();
        VecTabScreen = t;
    }

    public void paint(mGraphics g)
    {
        var tab = getCurrentTab();
        GameScr.resetTranslate(g);
        for (var i = 0; i < VecTabScreen.size(); i++)
            g.drawImage(LoadImageInterface.btnTab, xMenu, yMenu + (5 + Image.getHeight(LoadImageInterface.btnTab)) * i,
                0);
        for (var i = 0; i < VecTabScreen.size(); i++)
            if (i != selectTab)
                mFont.tahoma_7b_white.drawString(g, nameMenu[i],
                    xMenu + Image.getWidth(LoadImageInterface.btnTab) / 2,
                    yMenu + (5 + Image.getHeight(LoadImageInterface.btnTab)) * i
                    + Image.getHeight(LoadImageInterface.btnTab) / 4, 2);
        MainTabNew.gI().paintTab(g, tab.nameTab, selectTab, VecTabScreen, false);


        //paint tab is selected

        tab.paint(g);
        GameScr.resetTranslate(g);


        g.drawImage(LoadImageInterface.btnTabFocus, xMenu,
            yMenu + (5 + Image.getHeight(LoadImageInterface.btnTab)) * selectTab, 0);
        for (var i = 0; i < VecTabScreen.size(); i++)
            if (i == selectTab)
                mFont.tahoma_7b_white.drawString(g, nameMenu[i],
                    xMenu + Image.getWidth(LoadImageInterface.btnTab) / 2,
                    yMenu + (5 + Image.getHeight(LoadImageInterface.btnTab)) * i
                    + Image.getHeight(LoadImageInterface.btnTab) / 4, 2);

        if (tab.isPaintCmdClose)
            cmdClose.paint(g);
    }

    public void update()
    {
        if (lastScreen == GameCanvas.currentScreen)
            lastScreen.update();
        var tab = getCurrentTab();
        tab.update();
        if (timeRepaint > 0)
            timeRepaint--;
    }

    private MainTabNew getCurrentTab()
    {
        return (MainTabNew) VecTabScreen.elementAt(selectTab);
    }

    public void updatekey()
    {
        if (MainTabNew.Focus == MainTabNew.TAB)
        {
            timeRepaint = 10;
            left = null;
            center = null;
            right = cmdBack;
            var cur = selectTab;
            if (GameCanvas.keyHold[2])
            {
                selectTab--;
                GameCanvas.clearKeyHold();
            }
            else if (GameCanvas.keyHold[8])
            {
                selectTab++;
                GameCanvas.clearKeyHold();
            }
            else if (GameCanvas.keyHold[4] || GameCanvas.keyHold[6])
            {
                GameCanvas.clearKeyHold();
                GameCanvas.clearKeyPressed();
                SetInit();
            }

            var msg = getCurrentTab();
            if (msg.typeTab == MainTabNew.CONFIG || msg.typeTab == MainTabNew.FUNCTION)
                msg.init();
        }
        var tab = getCurrentTab();
        tab.updateKey();

        if (tab.isPaintCmdClose && (GameCanvas.keyPressed[13] || getCmdPointerLast(cmdClose)))
            if (cmdClose != null)
            {
                GameCanvas.isPointerJustRelease = false;
                GameCanvas.keyPressed[5] = false;
                keyTouch = -1;
                if (cmdClose != null)
                    cmdClose.performAction();
            }

        updateKey();
        GameCanvas.clearKeyPressed();
    }

    //execute touch
    public override void updatePointer()
    {
        var msg = getCurrentTab();
        if (msg.typeTab == MainTabNew.REBUILD)
        {
        }

        var ySelect = yMenu;
        if (GameCanvas.isPointer(xMenu, ySelect, wMenu, hMenu))
        {
            selectTab = 0;
            var b = (TabBag) getCurrentTab();
            b.idSelect = -1;
            //Cout.println("selectTab  " + selectTab);
            GameCanvas.isPointerClick = false;
        }
        ySelect += 5 + hMenu;
        if (GameCanvas.isPointer(xMenu, ySelect, wMenu, hMenu))
        {
            selectTab = 1;
            GameCanvas.isPointerClick = false;
        }
        ySelect += 5 + hMenu;
        if (GameCanvas.isPointer(xMenu, ySelect, wMenu, hMenu))
        {
            selectTab = 2;
            GameCanvas.isPointerClick = false;
        }
        ySelect += 5 + hMenu;
        if (GameCanvas.isPointer(xMenu, ySelect, wMenu, hMenu))
        {
            selectTab = 3;
            GameCanvas.isPointerClick = false;
        }
        ySelect += 5 + hMenu;
        if (GameCanvas.isPointer(xMenu, ySelect, wMenu, hMenu))
        {
            selectTab = 4;
            GameCanvas.isPointerClick = false;
        }
        SetInit();
        msg = getCurrentTab();
        msg.updatePointer();
    }

    public void SetInit()
    {
        MainTabNew.Focus = MainTabNew.INFO;
        var msg = getCurrentTab();
        msg.init();
    }

    public override void keyPress(int keyCode)
    {
        LogDebug.println("keycode  " + keyCode);
        var msg = getCurrentTab();
        msg.keypress(keyCode);
        base.keyPress(keyCode);
    }
}