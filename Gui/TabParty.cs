using System;
using src.lib;

public class TabParty : tScreen, IActionListener
{
    public static Scroll scrMain = new Scroll();

    public bool isLead = false;
    public Command kich, giaitan, roi;

    public int popupY,
        popupX,
        isborderIndex,
        indexRow = -1,
        widthGui = 235,
        heightGui = 240,
        indexSize = 50,
        indexRowMax;

    private int x, y;
    public int xstart, ystart, popupW = 163, popupH = 160, cmySK, cmtoYSK, cmdySK, cmvySK, cmyLimSK;

    public TabParty(int x, int y)
    {
        this.x = GameCanvas.w / 2 - widthGui / 2; //GameCanvas.w/2-GameScr.widthGui/2;
        this.y = y;
        popupX = x - widthGui / 2;
        popupY = GameCanvas.h / 2 - heightGui / 2;
    }

    public void perform(int idAction, object p)
    {
        // TODO Auto-generated method stub
        switch (idAction)
        {
            case 111111: // giai tan
                Service.getInstance().removeParty(PartyType.DISBAND_PARTY);
                break;
            case 270192: // kich ra khoi nhom
                var party = (Party) GameScr.hParty.get(Char.myChar().idParty + "");
                if (party == null)
                    return;
                if (((Char) Party.vCharinParty.elementAt(0)).charID == Char.myChar().charID)
                    if (indexRow < Party.vCharinParty.size())
                    {
                        var c = (Char) Party.vCharinParty.elementAt(indexRow);
                        if (c.charID != Char.myChar().charID)
                            Service.getInstance().kickPlayeleaveParty(PartyType.KICK_OUT_PARTY, (short) c.charID);
                    }
                break;
            case 231291: // tu roi nhom
                Service.getInstance().leaveParty(PartyType.OUT_PARTY, (short) Char.myChar().charID);
                break;
        }
    }

    public override void updateKey()
    {
        // TODO Auto-generated method stub
        if (kich != null && Party.gI().isLeader && Party.vCharinParty.size() >= 2)
            if (getCmdPointerLast(kich))
                kich.performAction();
        if (giaitan != null && Party.gI().isLeader && Party.vCharinParty.size() >= 2)
            if (getCmdPointerLast(giaitan))
                giaitan.performAction();
        if (roi != null && Party.vCharinParty.size() >= 2)
            if (getCmdPointerLast(roi))
                roi.performAction();

        var s1 = scrMain.updateKey();
        base.updateKey();
    }

    public override void update()
    {
        // TODO Auto-generated method stub
        if (GameCanvas.keyPressed[Key.NUM8])
        {
            indexRow++;
            if (indexRow >= GameScr.hParty.size())
                indexRow = GameScr.hParty.size() - 1;
            scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
        }
        else if (GameCanvas.keyPressed[Key.NUM2])
        {
            indexRow--;
            if (indexRow < 0)
                indexRow = 0;
            scrMain.moveTo(indexRow * scrMain.ITEM_SIZE);
        }
        scrMain.updatecm();

        if (GameScr.hParty.size() == 0 && scrMain.selectedItem != -1)
        {
            indexRow = scrMain.selectedItem;
            scrMain.selectedItem = -1;
        }
        else if (GameScr.hParty.size() != 0 && scrMain.selectedItem != -1)
        {
            indexRow = scrMain.selectedItem;
            scrMain.selectedItem = -1;
            setPartyCommand();
        }
        base.update();
    }

    private void setPartyCommand()
    {
        var party = (Party) GameScr.hParty.get(Char.myChar().idParty + "");
        if (party == null)
            return;
        if (indexRow == -1)
            return;
        if (party != null)
            if (Char.myChar().charID == Party.idleader)
            {
                var c = (Char) Party.vCharinParty.elementAt(indexRow);
                if (c != null)
                {
                    kich = new Command("Kích", this, 270192, null, 0, 0);
                    kich.setPos(popupX + popupW + LoadImageInterface.btnTab.getWidth() - 10,
                        popupY + LoadImageInterface.btnTab.getHeight() * 2, LoadImageInterface.btnTab,
                        LoadImageInterface.btnTab);
                    giaitan = new Command("Giải tán", this, 111111, null, 0, 0);
                    giaitan.setPos(popupX + popupW + LoadImageInterface.btnTab.getWidth() - 10,
                        popupY + LoadImageInterface.btnTab.getHeight() * 2 + cmdH * 4,
                        LoadImageInterface.btnTab, LoadImageInterface.btnTab);
                    roi = new Command("Rời", this, 231291, null, 0, 0);
                    roi.setPos(popupX + popupW + LoadImageInterface.btnTab.getWidth() - 10,
                        popupY + LoadImageInterface.btnTab.getHeight() * 2 + cmdH * 2,
                        LoadImageInterface.btnTab, LoadImageInterface.btnTab);
                    giaitan.img = roi.img = kich.img = LoadImageInterface.btnTab;
                    giaitan.imgFocus = roi.imgFocus = kich.imgFocus = LoadImageInterface.btnTabFocus;
                    giaitan.w = roi.w = kich.w = Image.getWidth(LoadImageInterface.btnTab);
                    giaitan.h = roi.h = kich.h = Image.getHeight(LoadImageInterface.btnTabFocus);
                }
            }
            else
            {
                kich = new Command("Kích", this, 270192, null, 0, 0);
                kich.setPos(popupX + popupW + LoadImageInterface.btnTab.getWidth() - 10,
                    popupY + LoadImageInterface.btnTab.getHeight() * 2, LoadImageInterface.btnTab,
                    LoadImageInterface.btnTab);
                giaitan = new Command("Giải tán", this, 111111, null, 0, 0);
                giaitan.setPos(popupX + popupW + LoadImageInterface.btnTab.getWidth() - 10,
                    popupY + LoadImageInterface.btnTab.getHeight() * 2 + cmdH * 4, LoadImageInterface.btnTab,
                    LoadImageInterface.btnTab);
                roi = new Command("Rời", this, 231291, null, 0, 0);
                roi.setPos(popupX + popupW + LoadImageInterface.btnTab.getWidth() - 10,
                    popupY + LoadImageInterface.btnTab.getHeight() * 2 + cmdH * 2, LoadImageInterface.btnTab,
                    LoadImageInterface.btnTab);
                giaitan.img = roi.img = kich.img = LoadImageInterface.btnTab;
                giaitan.imgFocus = roi.imgFocus = kich.imgFocus = LoadImageInterface.btnTabFocus;
                giaitan.w = roi.w = kich.w = Image.getWidth(LoadImageInterface.btnTab);
                giaitan.h = roi.h = kich.h = Image.getHeight(LoadImageInterface.btnTabFocus);
            }
    }

    public override void paint(mGraphics g)
    {
        // TODO Auto-generated method stub

        g.setColor(0x000000, GameCanvas.opacityTab);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
        g.disableBlending();
        Paint.paintFrameNaruto(popupX, popupY, widthGui, heightGui, g, true);

        xstart = popupX + 5;
        ystart = popupY + 40;

        if (GameScr.hParty.size() == 0)
        {
            mFont.tahoma_7_white.drawString(g, mResources.NOT_TEAM, popupX + widthGui / 2, popupY + 40, FontSys.CENTER);
        }
        else
        {
            resetTranslate(g);
            scrMain.setStyle(Party.vCharinParty.size(), 50, xstart, ystart, widthGui - 3, heightGui - 50, true, 1);
            scrMain.setClip(g, xstart, ystart, widthGui - 3, heightGui - 50);
            var yBGFriend = 0;
            indexRowMax = Party.vCharinParty.size();
            for (var i = 0; i < Party.vCharinParty.size(); i++)
            {
                var c = (Char) Party.vCharinParty.elementAt(i);

                Paint.PaintBGListQuest(xstart + 35, ystart + yBGFriend, 160, g); //new quest	
                if (indexRow == i)
                    Paint.PaintBGListQuestFocus(xstart + 35, ystart + yBGFriend, 160, g);
                g.drawImage(LoadImageInterface.charPic, xstart + 45, ystart + yBGFriend + 20,
                    mGraphics.VCENTER | mGraphics.HCENTER);
                mFont.tahoma_7_white.drawString(g, "Level: " + c.clevel, xstart + 73, ystart + yBGFriend + 24, 0, true);
                if (c.isOnline)
                {
                    g.drawImage(LoadImageInterface.imgName, xstart + 100, ystart + yBGFriend + 15,
                        mGraphics.VCENTER | mGraphics.HCENTER, true);
                    mFont.tahoma_7_white.drawString(g, c.cName, xstart + 73, ystart + yBGFriend + 8, 0, true);
                }

                else
                {
                    g.drawImage(LoadImageInterface.imgName, xstart + 100, ystart + yBGFriend + 15,
                        mGraphics.VCENTER | mGraphics.HCENTER, true);
                    mFont.tahoma_7_white.drawString(g, c.cName, xstart + 73, ystart + yBGFriend + 8, 0, true);
                }

                if (c != null)
                {
                    if (c.isLeader)
                        mFont.tahoma_7_yellow.drawString(g, c.cName, xstart + 73, ystart + yBGFriend + 8, 0);
                    try
                    {
                        var ph2 = GameScr.parts[c.head];
                        SmallImage.drawSmallImage(g, ph2.pi[Char.CharInfo[0][0][0]].id,
                            xstart + 44, ystart + yBGFriend + 20, 0, mGraphics.VCENTER | mGraphics.HCENTER);
                    }
                    catch (Exception e)
                    {
                        // TODO: handle exception
                    }
                    //						else
                    //							mFont.tahoma_7_red.drawString(g, "Hoahoa"/*c.cName*/, xstart + widthGui/2, ystart + i * indexSize + 20, 2);
                }

                yBGFriend += 50;
            }
        }

        GameCanvas.resetTrans(g);
        if (indexRow != -1 && Party.vCharinParty.size() >= 2)
        {
            if (Party.gI().isLeader)
            {
                if (kich != null) kich.paint(g);
                if (giaitan != null) giaitan.paint(g);
            }
            if (roi != null) roi.paint(g);
        }


        //paint name box 
        Paint.PaintBoxName("DANH SÁCH NHÓM", popupX + widthGui / 2 - 50, popupY + 10, 100, g);
        //	}
        base.paint(g);
    }

    public void SetPosClose(Command cmdClose)
    {
        // TODO Auto-generated method stub
        scrMain.selectedItem = -1;
        cmdClose.setPos(popupX + widthGui - Image.getWidth(LoadImageInterface.closeTab), popupY,
            LoadImageInterface.closeTab, LoadImageInterface.closeTab);
    }
}