using src.lib;

public class ImageEffect
{
    public static MyHashtable hashImageEff = new MyHashtable();
    private int IdImage;
    private readonly mBitmap img;

    private long timeRemove;

    public ImageEffect(int Id)
    {
        IdImage = Id;
        var imgg = GameCanvas.loadImage("/eff/g" + Id + ".png");
        if (imgg != null) img = imgg;
    }

    public static mBitmap setImage(int Id)
    {
        var img = (ImageEffect) hashImageEff.get("" + Id);

        if (img == null)
        {
            img = new ImageEffect(Id);
            if (img.img != null && img.img.image != null && img.img.image.texture != null)
            {
                hashImageEff.put("" + Id, img);
                return img.img;
            }
            return null;
        }
        return img.img;
    }
}