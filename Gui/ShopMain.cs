/*
 * Paint and execute shop gui
 */

using src.lib;

public class ShopMain : tScreen, IActionListener
{
    public static Scroll scrMain = new Scroll();
    public static int[] idMenu;
    public static int indexidmenu;
    public static string[] nameMenu;
    public static string[] dis;
    public static Command[] cmdShop;
    public static Command cmdMua;
    public static Item[][] itemshop;
    public static short[] idItemtemplate;
    private readonly mBitmap[] arrayBGShop = new mBitmap[25];
    private int coutFc;
    public mBitmap imgCoins1, imgCoins2;
    private int indexRow = -1, indexRowUnRecive;
    private readonly int witem = 34;
    private int indexSize, xBegin;
    private readonly int yBegin;
    public int selectTab;
    public TField tfsl;

    public Vector VecTabScreen = new Vector();
    private readonly int wMenu;
    private readonly int hMenu;
    private int wScollReceived = 100, hScollReceived = 60;
    private readonly int x;
    private readonly int y;
    private readonly int width = 180;
    private readonly int height = 232;
    private readonly int widthSub = 170;
    private readonly int heightSub = 230;
    private readonly int wKhoangCach = 4;

    private int xM; //diem nam giua frame
    private readonly int xMenu;
    private readonly int yMenu;
    private readonly int xsub;

    public ShopMain(int x, int y)
    {
        var wAll = width + widthSub + wKhoangCach + Image.getWidth(LoadImageInterface.btnTab);
        this.y = GameCanvas.h - y - heightSub < 0 ? GameCanvas.h - heightSub : y;
        y = this.y;
        xsub = GameCanvas.w / 2 - wAll / 2 < 2 ? 2 : GameCanvas.w / 2 - wAll / 2;
        yMenu = y + 40;
        this.x = xsub + widthSub + wKhoangCach + 5;
        xMenu = this.x + width - 5;
        wMenu = Image.getWidth(LoadImageInterface.btnTab);
        hMenu = Image.getHeight(LoadImageInterface.btnTab);

        indexSize = mFont.tahoma_7_yellow.getHeight() + 2;

        var sWeapon = new ShopWeapon();
        VecTabScreen.addElement(sWeapon);

        var sWeapon1 = new ShopWeapon();
        VecTabScreen.addElement(sWeapon1);

        var sWeapon2 = new ShopWeapon();
        VecTabScreen.addElement(sWeapon2);
  
        LoadImage();
        creatMenuShop(nameMenu, cmdShop);
        cmdMua = new Command("Mua ngay", this, 1, null, 0, 0);
        cmdMua.setPos(xsub + widthSub / 2 - LoadImageInterface.img_use.getWidth() / 2 + 7,
            y + heightSub - 2 * LoadImageInterface.img_use.getHeight() - 2, LoadImageInterface.img_use,
            LoadImageInterface.img_use_focus);
        xBegin = xsub - wKhoangCach / 2 + 4;
        yBegin = y + heightSub - 5;
        tfsl = new TField();
        tfsl.width = LoadImageInterface.img_use.getWidth();
        tfsl.x = cmdMua.x;
        tfsl.y = cmdMua.y - 25;
    }

    public void perform(int idAction, object p)
    {
        // TODO Auto-generated method stub
        if (idAction != 1 && idAction != 100)
            if (idAction - 2 <= nameMenu.Length && itemshop[idMenu[idAction - 2]] == null)
                Service.getInstance().requestShop(idMenu[idAction - 2]);
            else if (itemshop[idMenu[idAction - 2]] != null)
                indexidmenu = idMenu[idAction - 2];
        switch (idAction)
        {
            case 1: // mua item
                if (scrMain.selectedItem != -1 && scrMain.selectedItem < itemshop[indexidmenu].Length)
                {
                    var textsl = tfsl.getText();
                    GameCanvas.startYesNoDlg("Bạn có muốn mua " +
                                             itemshop[indexidmenu][scrMain.selectedItem].template.name
                                             + " với giá " + itemshop[indexidmenu][scrMain.selectedItem].template.gia +
                                             (itemshop[indexidmenu][scrMain.selectedItem].template.typeSell == 0
                                                 ? "xu"
                                                 : "gold") + " ?",
                        new Command("Có", this, 100, null), new Command("Không", GameCanvas.instance, 8882, null));
                   
                }
                break;
            case 100:
                Service.getInstance().BuyItemShop((sbyte) indexidmenu, (short) scrMain.selectedItem);
                GameCanvas.endDlg();
                break;
        }

    }

    public void LoadImage()
    {
        for (var i = 0; i < 25; i++)
            arrayBGShop[i] = GameCanvas.loadImage("/GuiNaruto/shop/shell_" + (i + 1) + ".png");
    }

    public void update()
    {
        if (GameCanvas.gameTick % 4 == 0)
        {
            coutFc++;
            if (coutFc > 2)
                coutFc = 0;
        }
        if (imgCoins1 == null)
        {
            imgCoins1 = GameCanvas.loadImage("/GuiNaruto/myseft/coins1.png");
            imgCoins2 = GameCanvas.loadImage("/GuiNaruto/myseft/coins2.png");
        }
    }

    public void updateKey()
    {
        // TODO Auto-generated method stub
        scrMain.updatecm();
        var s1 = scrMain.updateKey();
        if (scrMain.selectedItem >= itemshop[indexidmenu].Length)
            scrMain.selectedItem = -1;
        if (getCmdPointerLast(cmdMua))
            cmdMua.performAction();
        if (cmdShop != null)
            for (var i = 0; i < cmdShop.Length; i++)
                if (getCmdPointerLast(cmdShop[i]))
                {
                    cmdShop[i].performAction();
                    GameCanvas.clearPointerEvent();
                }
    }



    //paint bg human
    private void PaintBGShell(int x, int y, mGraphics g)
    {
        var wImage = Image.getWidth(arrayBGShop[0]);
        var index = 0;

        for (var j = 1; j < 5; j++)
        {
            g.drawImage(arrayBGShop[index == 3 ? 4 : index], x + j * wImage, y, 0, true);
            index++;
        }
    }

    public void SetPosClose(Command cmd)
    {
        // TODO Auto-generated method stub
        cmd.setPos(x + width - Image.getWidth(LoadImageInterface.closeTab), y, LoadImageInterface.closeTab,
            LoadImageInterface.closeTab);
    }

    public void paint(mGraphics g)
    {
       
        g.setColor(0x000000, GameCanvas.opacityTab);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
        g.disableBlending();
        if (cmdShop != null)
            for (var i = 0; i < cmdShop.Length; i++)
            {
                if (cmdShop[i] == null) continue;
                if (i != indexidmenu)
                    cmdShop[idMenu[i]].paint(g);
                //			else cmdShop[i].paintFocus(g,true);
            }

        Paint.paintFrameNaruto(x, y, width, height, g);

        Paint.PaintBoxName(nameMenu[idMenu[indexidmenu]], x + 55, 20, width - 110, g);

        Paint.SubFrame(xsub, y, widthSub, heightSub, g); //sub
        if (itemshop == null) return;
        if (scrMain.selectedItem >= 0 && scrMain.selectedItem < itemshop[indexidmenu].Length)
        {
            if (cmdMua != null)
                cmdMua.paint(g);
            
            //money
            g.drawImage(imgCoins1, xsub, yBegin - Image.getHeight(imgCoins1) - 1, mGraphics.TOP | mGraphics.LEFT);
            if (itemshop != null && itemshop[indexidmenu] != null && scrMain.selectedItem != -1
                && itemshop[indexidmenu][scrMain.selectedItem] != null)
                mFont.tahoma_7b_white.drawString(g, Char.myChar().xu + "",
                    xsub + 25, yBegin - Image.getHeight(imgCoins1) / 2 - 2, 0);

            //gold
            g.drawImage(imgCoins2, xsub + Image.getWidth(imgCoins2) + 6, yBegin - Image.getHeight(imgCoins2) - 1,
                mGraphics.TOP | mGraphics.LEFT);
            mFont.tahoma_7b_white.drawString(g, Char.myChar().luong + "",
                xsub + Image.getWidth(imgCoins2) + 6 + 27, yBegin - Image.getHeight(imgCoins1) / 2 - 2, 0);
        }
       
        scrMain.setStyleWH(itemshop[indexidmenu].Length / 4 + (itemshop[indexidmenu].Length % 4 != 0 ? 1 : 0), witem,
            50,
            x + 25, y + 50, width - 55, height - 70, true, 4);
        scrMain.setClip(g, x, y + 50, width, height - 70);
        //new quest
        for (var i = 0; i < itemshop[indexidmenu].Length / 4 + (itemshop[indexidmenu].Length % 4 != 0 ? 1 : 0); i++)
            PaintBGShell(x - 30, y + 50 * (i + 1) + 20, g);
     
        for (var i = 0; i < itemshop[indexidmenu].Length; i++)
        {
         
            g.drawImage(LoadImageInterface.ImgItem, x + 20 + i % 4 * witem + 17, y + (i / 4 + 1) * 50 + 14,
                mGraphics.VCENTER | mGraphics.HCENTER);

            if (itemshop[indexidmenu][i] != null && itemshop[indexidmenu][i] != null)
                itemshop[indexidmenu][i].paintItem(g, x + 20 + i % 4 * witem + 17, y + (i / 4 + 1) * 50 + 14);

          
        }
        if (scrMain.selectedItem >= 0)
            Paint.paintFocus(g,
                x + 20 + scrMain.selectedItem % 4 * witem + 14 - LoadImageInterface.ImgItem.getWidth() / 4,
                y + (scrMain.selectedItem / 4 + 1) * 50 + 11 - LoadImageInterface.ImgItem.getWidth() / 4
                , LoadImageInterface.ImgItem.getWidth() - 9, LoadImageInterface.ImgItem.getWidth() - 9, coutFc);
        
        GameCanvas.resetTrans(g);
        for (var i = 0; i < cmdShop.Length; i++)
        {
            if (cmdShop[i] == null) continue;
            if (i != indexidmenu)
            {
                //				cmdShop[i].paint(g);
            }
            else
            {
                cmdShop[idMenu[i]].paintFocus(g, true);
            }
        }
        if (scrMain.selectedItem >= 0 && scrMain.selectedItem < itemshop[indexidmenu].Length)
            Paint.paintItemInfo(g, itemshop[indexidmenu][scrMain.selectedItem], xsub + 10, y + 10, true);
      
    }

    private void creatMenuShop(string[] name, Command[] menubtn)
    {
        if (name == null) return;
        for (var i = 0; i < name.Length; i++)
        {
            menubtn[i] = new Command(name[i], this, i + 2, null, 0, 0);
            menubtn[i].setPos(xMenu - 8, yMenu + (5 + Image.getHeight(LoadImageInterface.btnTab)) * i,
                LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);
        }
    }


    public static void getItemList(int idmenu, short[] itemtemplate)
    {
        if (itemtemplate == null) return;
        if (itemshop == null)
            itemshop = new Item[nameMenu.Length][];
        itemshop[idmenu] = new Item[itemtemplate.Length];
        for (var i = 0; i < itemtemplate.Length; i++)
        {
            itemshop[idmenu][i] = new Item();
            itemshop[idmenu][i].itemId = itemtemplate[i];
            itemshop[idmenu][i].template = ItemTemplates.get(itemtemplate[i]);
        }
    }

}