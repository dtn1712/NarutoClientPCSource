using src.lib;

public class IconChat
{
    public const int COLUMNS = 6;
    
    private const int ZONE_COLUMN = 6;
    
    private int rows;

    public int indexSize = 28,
        indexSelect = -1;

    public int popupY, popupX;
    public Scroll scrMain = new Scroll();
    public int xstart, ystart;
    

    public IconChat(int x, int y)
    {
        popupX = x;
        popupY = y;
    }

    public void paint(mGraphics g)
    {
        GameScr.resetTranslate(g);

        paintFrameIconchat(g);
        var r = indexSelect / ZONE_COLUMN;
        var c = indexSelect % ZONE_COLUMN;

        rows = LoadImageInterface.imgEmo.Length / ZONE_COLUMN;
        if (LoadImageInterface.imgEmo.Length % ZONE_COLUMN > 0)
            rows += 1;
        if (rows < 5)
            rows = 5;

        scrMain.setStyle(rows, indexSize, xstart, ystart, COLUMNS * indexSize + 2, 5 * indexSize + 2, true, 6);
        scrMain.setClip(g, xstart, ystart, COLUMNS * indexSize + 2, 5 * indexSize + 2);
        g.drawImage(LoadImageInterface.imgEmo[0], xstart + 10, ystart + 10, 0);
        var index = 0;
        for (var i = 0; i < rows; i++)
        for (var j = 0; j < ZONE_COLUMN; j++)
        {
            if (index >= LoadImageInterface.imgEmo.Length)
                continue;
            if (LoadImageInterface.imgEmo[index] != null)
                g.drawImage(LoadImageInterface.imgEmo[index], xstart + j * 28 + 28 / 2 - 8,
                    ystart + i * 28 + 28 / 2 - 8, 0, true);
            index++;
        }
        if (indexSelect >= 0)
        {
            g.setColor(0xFFFFFF);
            g.drawRect(xstart + c * indexSize, ystart + r * indexSize, indexSize, indexSize);
        }
        GameScr.resetTranslate(g);
    }

    public void Updatecm()
    {
        //scrMain.updatecm();
    }

    public void updateKeySelectIconChat()
    {
        if (GameCanvas.currentDialog != null)
            return;
        
        if (GameCanvas.isTouch)
        {
            var r = scrMain.updateKey();
            scrMain.updatecm();
            if (r.isDowning || r.isFinish)
                indexSelect = scrMain.selectedItem;
        }
    }

    private void paintFrameIconchat(mGraphics g)
    {
        xstart = popupX + 3;
        ystart = popupY + 32;
        g.setColor(0x001919);
        g.fillRect(xstart - 1, ystart - 1, COLUMNS * indexSize + 3, 5 * indexSize + 3);
    }
}