using System;
using src.lib;
using UnityEngine;

public class GuiFriend : tScreen, IActionListener
{
    public static Scroll scrMain = new Scroll();

    //friend list gui
    public static Command btnUnfriend; //display quest info

    public static Command btnChat; //display quest info
    public int indexRow;

    public int popupX, popupY, popupW, popupH = 220;
    public long timestartUpdate;

    public GuiFriend()
    {
        popupX = GameCanvas.w / 2 - GameScr.widthGui / 2;
        popupY = GameScr.popupY;
        popupW = GameScr.widthGui;
        btnUnfriend = new Command("Xóa bạn", this, Constants.UN_FRIEND, null, 0, 0);
        btnChat = new Command("Chat riêng", this, Constants.CHAT_PRIVATE, null, 0, 0);
        btnUnfriend.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.btnTab) / 2,
            popupY + 40, LoadImageInterface.btnTab, LoadImageInterface.btnTabFocus);
        btnChat.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.btnTab) / 2,
            popupY + 3 * Image.getHeight(LoadImageInterface.btnTab), LoadImageInterface.btnTab,
            LoadImageInterface.btnTabFocus);
    }

    public void perform(int idAction, object p)
    {
        // TODO Auto-generated method stub
        switch (idAction)
        {
            case Constants.UN_FRIEND:
                if (indexRow >= 0 && indexRow < Char.myChar().vFriend.size())
                {
                    var charF = (Char) Char.myChar().vFriend.elementAt(indexRow);
                    GameCanvas.startYesNoDlg("Bạn muốn tuyệt giao với " + charF.cName,
                        new Command("Có", this, 13, null),
                        new Command("Không", GameCanvas.instance, GameCanvas.cEndDgl, null));
                    ;
                }
                break;
            case Constants.CHAT_PRIVATE:
                Debug.Log(indexRow);
                GameScr.gI().guiMain.menuIcon.cmdClose.performAction();
                if (indexRow >= 0 && indexRow < Char.myChar().vFriend.size())
                {
                    var charFf = (Char) Char.myChar().vFriend.elementAt(indexRow);
                    if (charFf != null)
                    {
                        var isAdded = false;
                        for (var i = 0; i < ChatPrivate.vOtherchar.size(); i++)
                        {
                            var other = (OtherChar) ChatPrivate.vOtherchar.elementAt(i);
                            if (other.name.Equals(charFf.cName) || other.id == charFf.charID)
                            {
                                isAdded = true;
                                break;
                            }
                        }
                        if (!isAdded)
                            ChatPrivate.AddNewChater((short) charFf.charID, charFf.cName);
                    }
                }
                indexRow = -1;
                left = null;
                center = null;
                TabChat.gI().switchToMe();
                //			isLockKey = true;
                GameScr.setPopupSize(175, 200);
                left = center = null;

                break;
            case 13: //ok unfrien
                if (indexRow >= 0 && indexRow < Char.myChar().vFriend.size())
                {
                    var charF = (Char) Char.myChar().vFriend.elementAt(indexRow);
                    Service.getInstance().deleteFriend(Friend.UNFRIEND, (short) Char.myChar().charID,
                        charF.CharidDB);

                    indexRow = -1;
                }

                GameCanvas.endDlg();
                break;
        }
    }

    public override void updateKey()
    {
        // TODO Auto-generated method stub
        scrMain.updatecm();
        var s1 = scrMain.updateKey();
        if (getCmdPointerLast(btnChat))
            if (btnChat != null)
            {
                GameCanvas.isPointerJustRelease = false;
                keyTouch = -1;
                if (btnChat != null)
                    btnChat.performAction();
            }
        //unfriend
        if (getCmdPointerLast(btnUnfriend))
            if (btnUnfriend != null)
            {
                GameCanvas.isPointerJustRelease = false;
                keyTouch = -1;
                if (btnUnfriend != null)
                    btnUnfriend.performAction();
            }
    }

    public void update()
    {
        // TODO Auto-generated method stub
        if ((mSystem.currentTimeMillis() - timestartUpdate) / 1000 > 30)
        {
            timestartUpdate = mSystem.currentTimeMillis();
            Service.getInstance().requestFriendList(Friend.REQUEST_FRIEND_LIST, (short) Char.myChar().charID);
        }
        if (scrMain.selectedItem == -1)
            return;
        indexRow = scrMain.selectedItem;
        if (indexRow < Char.myChar().vFriend.size() && Char.myChar().vFriend.size() > 0)
        {
            var c = (Char) Char.myChar().vFriend.elementAt(indexRow);
            Char.toCharChat = c;
        }
    }

    public void paint(mGraphics g)
    {
        // TODO Auto-generated method stub
        g.setColor(0x000000, GameCanvas.opacityTab);
        g.fillRect(0, 0, GameCanvas.w, GameCanvas.h);
        g.disableBlending();
        paintFriend(g);
    }

    public void SetPosClose(Command cmd)
    {
        // TODO Auto-generated method stub
        cmd.setPos(popupX + popupW - Image.getWidth(LoadImageInterface.closeTab), popupY, LoadImageInterface.closeTab,
            LoadImageInterface.closeTab);
    }


    public void paintFriend(mGraphics g)
    {
        var str = mResources.FRIENDS[0];

        Paint.paintFrameNaruto(popupX, popupY, popupW, popupH, g);
        if (Char.myChar().vFriend.size() > 0)
        {
            GameScr.xstart = popupX + 5;
            GameScr.ystart = popupY + 40;

            resetTranslate(g);
            scrMain.setStyle(Char.myChar().vFriend.size(), 50, GameScr.xstart, GameScr.ystart, popupW - 3, 180, true,
                1);
            scrMain.setClip(g, GameScr.xstart, GameScr.ystart - 10, popupW - 3, 180);
            var friendCount = 0;
            var yBGFriend = 0;
            for (var i = 0; i < Char.myChar().vFriend.size(); i++)
            {
                var c = (Char) Char.myChar().vFriend.elementAt(i);

                Paint.PaintBGListQuest(GameScr.xstart + 35, GameScr.ystart + yBGFriend, 160, g); //new quest	
                if (indexRow == i)
                    Paint.PaintBGListQuestFocus(GameScr.xstart + 35, GameScr.ystart + yBGFriend, 160, g);


                mFont.tahoma_7_white.drawString(g, "Level: " + c.clevel, GameScr.xstart + 73,
                    GameScr.ystart + yBGFriend + 24, 0, true);

                g.drawImage(LoadImageInterface.charPic, GameScr.xstart + 45, GameScr.ystart + yBGFriend + 20,
                    mGraphics.VCENTER | mGraphics.HCENTER);
                if (c.isOnline)
                {
                    g.drawImage(LoadImageInterface.imgName, GameScr.xstart + 100, GameScr.ystart + yBGFriend + 15,
                        mGraphics.VCENTER | mGraphics.HCENTER, true);
                    mFont.tahoma_7_white.drawString(g, c.cName, GameScr.xstart + 73, GameScr.ystart + yBGFriend + 8, 0,
                        true);
                    g.drawImage(LoadImageInterface.imgOnline[1], GameScr.xstart + 180, GameScr.ystart + yBGFriend + 20,
                        mGraphics.VCENTER | mGraphics.HCENTER, true);
                }

                else
                {
                    g.drawImage(LoadImageInterface.imgName, GameScr.xstart + 100, GameScr.ystart + yBGFriend + 15,
                        mGraphics.VCENTER | mGraphics.HCENTER, true);
                    mFont.tahoma_7_white.drawString(g, c.cName, GameScr.xstart + 73, GameScr.ystart + yBGFriend + 8, 0,
                        true);
                    g.drawImage(LoadImageInterface.imgOnline[0], GameScr.xstart + 180, GameScr.ystart + yBGFriend + 20,
                        mGraphics.VCENTER | mGraphics.HCENTER, true);
                }

                try
                {
                    var ph2 = GameScr.parts[c.head];
                }
                catch (Exception e)
                {
                    // TODO: handle exception
                }
                friendCount++;
                yBGFriend += 50;
            }
            resetTranslate(g);
            if (btnChat != null)
            {
                btnChat.paint(g);
                btnUnfriend.paint(g);
            }
        }
        else
        {
            mFont.tahoma_7_white.drawString(g, mResources.NO_FRIEND, popupX + popupW / 2, popupY + 40,
                mFont.CENTER);
        }
        GameScr.resetTranslate(g);
        //paint name box 
        Paint.PaintBoxName("DANH SÁCH BẠN BÈ", popupX + 55, popupY, 130, g);
    }
}