﻿using System;
using src.lib;
using src.network;

namespace src.utils
{
    public class IOUtils
    {
        public static void WriteMessage(short cmd, byte[] data)
        {
            try
            {
                var m = new Message(cmd);
                var finalData = Converter.convertByteToSbyte(data);
                m.writer().writeInt(finalData.Length);
                m.writer().write(finalData);
                Session.gI().sendMessage(m);
                m.cleanup();
            }
            catch (Exception e)
            {
            }
        }

        public static void WriteMessage(short cmd)
        {
            try
            {
                var m = new Message(cmd);
                Session.gI().sendMessage(m);
                m.cleanup();
            }
            catch (Exception e)
            {
            }
        }

        public static byte[] ReadMessage(Message message)
        {
            var length = message.reader().readInt(); // Doc do dai cua byte truoc
            LogDebug.println("_________>>>>>> "+length);
            var data = new sbyte[length]; // Tao sbyte data
            message.reader().readFully(ref data); // read data
            return MyReader.convertSbyteToByte(data); // Convert tu sbyte sang byte	
        }
    }
}