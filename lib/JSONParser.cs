using System;
using System.Collections.Generic;
using UnityEngine;

namespace src.lib
{
    public class JSONParser {

        static string json = "";
	
        
        public static List<ItemPayment> parseItemPayment(string text)
        {
            LogDebug.println("parseItemPayment  " + text);

            List<ItemPayment> listItem_add = new List<ItemPayment>();
            try
            {
                var data = JSON.Parse(text);
                var listitem = data["data"];
                for (int i = 0; i < listitem.Count; i++)
                {
                    listItem_add.Add(new ItemPayment(int.Parse(listitem[ItemPayment.KEY_VALUE_MONEY_AMOUNT].Value),
                        listitem[ItemPayment.KEY_NAME_MONEY_AMOUNT].Value,
                        int.Parse(listitem[ItemPayment.KEY_VALUE_MONEY_AMOUNT].Value),
                        listitem[ItemPayment.KEY_NAME_MONEY_AMOUNT].Value));
                }
            }
            catch (Exception e)
            {

                LogDebug.println("error  " + e);
            }
            if(listItem_add.Count>0)
                return listItem_add;
            try
            {

                if (text.Trim().Length == 0)
                {
                    text = mSystem.loadfile(Main.res+"/file");
                }
                text = JsonHelper.fixJson(text);

                var listitem = JsonHelper.FromJson<ItemPaymentDTO>(text);

                for (var j = 0; j < listitem.Length; j++)
                {
                    var itempayment = listitem[j];
                    if (itempayment != null)
                    {
                        listItem_add.Add(new ItemPayment(itempayment.gameMoneyAmount, itempayment.gameMoneyUnit, itempayment.realMoneyAmount, itempayment.realMoneyUnit));
                    }
                }
               
            }
            catch (Exception)
            {

                return listItem_add;
            }

            return listItem_add;
        }
        
        [Serializable]
        private class ItemPaymentDTO
        {
            public int gameMoneyAmount;
            public string gameMoneyUnit;
            public int realMoneyAmount;
            public string realMoneyUnit;

        }
    }
}
