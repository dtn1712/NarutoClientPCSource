﻿using System;

namespace src.lib
{
    public class Converter
    {
        public static byte convertSbyteToByte(sbyte var)
        {
            if (var > 0)
                return (byte) var;

            return (byte) (var + 256);
        }

        public static byte[] convertSbyteToByte(sbyte[] var)
        {
            byte[] temp = new byte[var.Length];
            for (int i = 0; i < var.Length; i++)
            {
                if (var[i] > 0)
                    temp[i] = (byte) var[i];

                else temp[i] = (byte) (var[i] + 256);
            }

            return temp;
        }

        public static sbyte[] convertByteToSbyte(byte[] var)
        {
            sbyte[] result = new sbyte[var.Length];
            for (int i = 0; i < var.Length; i++)
            {
                result[i] = unchecked((sbyte) var[i]);
            }
            return result;
        }

        public static byte[] convertIntToBytes(int intValue)
        {
            byte[] intBytes = BitConverter.GetBytes(intValue);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(intBytes);
            byte[] result = intBytes;
            return result;
        }
    }
}