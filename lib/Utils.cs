﻿using System;
using src.network;

namespace src.lib
{
    public static class Utils
    {

        public static sbyte[] readByteArray(Message msg)
        {
            try
            {
                int lengh = msg.reader().available();
                sbyte[] data = new sbyte[lengh];
                msg.reader().read(ref data);
                return data;
            }
            catch (Exception e)
            {
            }
            return null;
        }

        public static sbyte[] readByteArray(DataInputStream dos)
        {
            try
            {
                short lengh = dos.readShort();
                sbyte[] data = new sbyte[lengh];
                dos.read(ref data);
                return data;
            }
            catch (Exception ex)
            {
          
            }
            return null;
        }

        public static string replace(String text, String regex, String replacement)
        {
            System.Text.StringBuilder sBuffer = new System.Text.StringBuilder();
            int pos = 0;
            while ((pos = text.IndexOf(regex)) != -1)
            {
                sBuffer.Append(mSystem.substring(text,0, pos) + replacement);
                text = mSystem.substring(text,pos + regex.Length);
            }
            sBuffer.Append(text);
            return sBuffer.ToString();
        }

        public static string numberToString(String number)
        {
            string value = "", value1 = "";
            if (number.Equals(""))
                return value;

            if (number[0] == '-')
            {
                value1 = "-";
                number = mSystem.substring(number,1);
            }
            for (int i = number.Length - 1; i >= 0; i--)
            {
                if ((number.Length - 1 - i) % 3 == 0
                    && (number.Length - 1 - i) > 0)
                    value = number[i] + "." + value;
                else
                    value = number[i] + value;
            }
            return value1 + value;
        }

        public static string[] split(String original, String separator)
        {
            Vector nodes = new Vector();
            int index = original.IndexOf(separator);
            while (index >= 0)
            {
                nodes.addElement(mSystem.substring(original,0, index));
                original = mSystem.substring(original,index + separator.Length);
                index = original.IndexOf(separator);
            }
            nodes.addElement(original);
            string[] result = new string[nodes.size()];
            if (nodes.size() > 0)
            {
                for (int loop = 0; loop < nodes.size(); loop++)
                {
                    result[loop] = (String)nodes.elementAt(loop);
                }

            }
            return result;
        }


        public static int distance(int x1, int y1, int x2, int y2)
        {
            return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
        }

        public static int sqrt(int a)
        {
            int x, x1;

            if (a <= 0)
                return 0;
            x = (a + 1) / 2;
            do
            {
                x1 = x;
                x = x / 2 + a / (2 * x);
            } while (Math.abs(x1 - x) > 1);
            return x;
        }

    
        public static sbyte[] readByteArray(Message msg,int leng) {
            try {
                sbyte[] data = new sbyte[leng];
                msg.reader().read(ref data);
                return data;
            } catch (Exception e) {
            }
            return null;
        }
    }
}
