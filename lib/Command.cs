﻿using System;

namespace src.lib
{
    public class Command
    {
        public string caption;
        public string[] subCaption;
        public IActionListener actionListener;
        public int idAction;
        public mBitmap back, focus, img, imgFocus;
        public int x = 0, y = 0, w = tScreen.cmdW, h = tScreen.cmdH;
        int lenCaption = 0;
        private bool isFocus = false;
        public bool isNoPaintImage;

        public Object p;

        public Command(string caption, IActionListener actionListener, int action, Object p, int x, int y)
        {
            this.caption = caption;
            this.idAction = action;
            this.actionListener = actionListener;
            this.p = p;
            this.x = x;
            this.y = y;
            this.w = tScreen.cmdW;
            this.h = tScreen.cmdH;
            this.back = null;
            this.focus = null;
        }

        public Command(string caption, IActionListener actionListener, int action, Object pp)
        {
            // super();
            if (caption.Contains("#"))
            {
                subCaption = mFont.split(caption, "#");
            }
            this.caption = caption;
            this.idAction = action;
            this.actionListener = actionListener;
            if (pp != null)
                this.p = pp;
        }

        public Command(string caption, IActionListener actionListener, int action, Object pp, bool ispaintimg)
        {
            // super();
            if (caption.Contains("#"))
            {
                subCaption = mFont.split(caption, "#");
            }
            this.isNoPaintImage = ispaintimg;
            this.caption = caption;
            this.idAction = action;
            this.actionListener = actionListener;
            if (pp != null)
                this.p = pp;
        }

        public Command(string caption, int action, Object p)
        {
            //  super();
            this.caption = caption;
            this.idAction = action;
            this.p = p;
        }

        public Command(string caption, int action)
        {
            // super();
            this.caption = caption;
            this.idAction = action;
        }

        public Command(string caption, int action, int x, int y)
        {
            //super();
            this.caption = caption;
            this.idAction = action;
            this.x = x;
            this.y = y;
        }

        public void performAction()
        {
            if (idAction > 0)
            {
                if (actionListener != null)
                    actionListener.perform(idAction, p);
                else
                {
                    GameScr.gI().actionPerform(idAction, p);
                }

                GameCanvas.clearPointerEvent();
            }
        }

        public void paint(mGraphics g)
        {
            if (back != null && !isNoPaintImage)
                g.drawImage(back, x, y, 0);
            if (focus != null && isFocus && !isNoPaintImage)
                g.drawImage(focus, x, y, 0);
            if (img != null)
            {
                if (isFocus && !isNoPaintImage)
                {
                    g.drawImage(imgFocus, x + Image.getWidth(img) / 2, y + Image.getHeight(img) / 2,
                        mGraphics.HCENTER | mGraphics.VCENTER);
                }
                else if (!isNoPaintImage)
                    g.drawImage(img, x + Image.getWidth(img) / 2, y + Image.getHeight(img) / 2,
                        mGraphics.HCENTER | mGraphics.VCENTER);
                if (img != null)
                {
                    if (subCaption == null)
                        mFont.tahoma_7b_white.drawString(g, caption, x + w / 2,
                            y + Image.getHeight(img) / 2 - mFont.tahoma_7b_white.getHeight() / 2 + 2, 2);
                    else
                    {
                        for (int i = 0; i < subCaption.Length; i++)
                        {
                            mFont.tahoma_7b_white.drawString(g, subCaption[i], x + w / 2,
                                y + Image.getHeight(img) / 2 - mFont.tahoma_7b_white.getHeight() / 2 + 2 - 5 + i * 10, 2);
                        }
                    }
                }
                else
                {
                    if (subCaption == null)
                        mFont.tahoma_7b_white.drawString(g, caption, x + w / 2,
                            y + Image.getHeight(img) / 2 - mFont.tahoma_7b_white.getHeight() / 2 + 2, 2);
                    else
                    {
                        for (int i = 0; i < subCaption.Length; i++)
                        {
                            mFont.tahoma_7b_white.drawString(g, subCaption[i], x + w / 2,
                                y + Image.getHeight(img) / 2 - mFont.tahoma_7b_white.getHeight() / 2 + 2 - 5 + i * 10, 2);
                        }
                    }
                }
            }
            else
            {
                if (subCaption == null)
                    mFont.tahoma_7b_white.drawString(g, caption, x + w / 2, y + 6, 2);
                else
                {
                    for (int i = 0; i < subCaption.Length; i++)
                    {
                        mFont.tahoma_7b_white.drawString(g, subCaption[i], x + w / 2,
                            y + Image.getHeight(img) / 2 - mFont.tahoma_7b_white.getHeight() / 2 + 2 - 5 + i * 10, 2);
                    }
                }
            }
        }

        public void paintW(mGraphics g)
        {
            if (back != null)
                g.drawImage(back, x, y, 0);
            if (focus != null && isFocus)
                g.drawImage(focus, x, y, 0);
            if (img != null)
            {
                if (isFocus)
                {
                    g.drawRegion(img, 0, 0, w / 2 - 2, img.getHeight(), 0,
                        x + w / 4, y + Image.getHeight(img) / 2, mGraphics.HCENTER | mGraphics.VCENTER);
                    g.drawRegion(img, 0, 0, w / 2 - 2, img.getHeight(), 2,
                        x + 3 * w / 4 - 2, y + Image.getHeight(img) / 2, mGraphics.HCENTER | mGraphics.VCENTER);

                }
                else
                {
                    g.drawRegion(img, 0, 0, w / 2 - 2, img.getHeight(), 0,
                        x + w / 4, y + Image.getHeight(img) / 2, mGraphics.HCENTER | mGraphics.VCENTER);
                    g.drawRegion(img, 0, 0, w / 2 - 2, img.getHeight(), 2,
                        x + 3 * w / 4 - 2, y + Image.getHeight(img) / 2, mGraphics.HCENTER | mGraphics.VCENTER);
                }
                if (img != null)
                {
                    if (caption.Equals("Đăng nhập"))
                    {
                        mFont.tahoma_7b_white.drawString(g, caption, x + this.w / 2 - 1 /*- Image.getHeight(img)/2 - 8 */
                            , y + Image.getHeight(img) / 2 - mFont.tahoma_7b_white.getHeight() / 2 + 2, 2);
                    }
                    else
                        mFont.tahoma_7b_white.drawString(g, caption, x + this.w / 2 - 1 /*- Image.getHeight(img)/2 */,
                            y + Image.getHeight(img) / 2 - mFont.tahoma_7b_white.getHeight() / 2 + 2, 2);
                }
                else
                {
                    mFont.tahoma_7b_white.drawString(g, caption, x + this.w / 2 - 1,
                        y + Image.getHeight(img) / 2 - mFont.tahoma_7b_white.getHeight() / 2 + 2, 2);
                }
            }
            else
            {
                mFont.tahoma_7b_white.drawString(g, caption, x + this.w / 2 - 1, y + 6, 2);
            }
        }

        public void paintFocus(mGraphics g, bool isFocus)
        {
            if (back != null)
                g.drawImage(back, x, y, 0);
            if (focus != null && isFocus)
                g.drawImage(focus, x, y, 0);
            if (img != null)
            {
                if (isFocus)
                {
                    g.drawImage(imgFocus, x + Image.getWidth(img) / 2, y + Image.getHeight(img) / 2,
                        mGraphics.HCENTER | mGraphics.VCENTER);
                }
                else
                    g.drawImage(img, x + Image.getWidth(img) / 2, y + Image.getHeight(img) / 2,
                        mGraphics.HCENTER | mGraphics.VCENTER);
                if (img != null)
                {
                    if (caption.Equals("Đăng nhập"))
                    {
                        mFont.tahoma_7b_white.drawString(g, caption, x + w / 2
                            , y + Image.getHeight(img) / 2 - mFont.tahoma_7b_white.getHeight() / 2 + 2, 2);
                    }
                    else
                        mFont.tahoma_7b_white.drawString(g, caption, x + w / 2,
                            y + Image.getHeight(img) / 2 - mFont.tahoma_7b_white.getHeight() / 2 + 2, 2);
                }
                else
                {
                    mFont.tahoma_7b_white.drawString(g, caption, x + w / 2,
                        y + Image.getHeight(img) / 2 - mFont.tahoma_7b_white.getHeight() / 2 + 2, 2);
                }
            }
            else
            {
                mFont.tahoma_7b_white.drawString(g, caption, x + w / 2, y + 6, 2);
            }
        }

        public void paint(mGraphics g, int transform)
        {
            //lat hinh
            if (back != null)
                g.drawImage(back, x, y, 0);
            if (focus != null && isFocus)
                g.drawImage(focus, x, y, 0);
            if (img != null)
            {
                if (isFocus)
                {
                    g.drawRegion(imgFocus, 0, 0, Image.getWidth(img), Image.getHeight(img), transform,
                        x + Image.getWidth(img) / 2, y + Image.getHeight(img) / 2, mGraphics.HCENTER | mGraphics.VCENTER,
                        false);
               
                }
                else
                    g.drawRegion(img, 0, 0, Image.getWidth(img), Image.getHeight(img), transform,
                        x + Image.getWidth(img) / 2, y + Image.getHeight(img) / 2, mGraphics.HCENTER | mGraphics.VCENTER,
                        false);
                if (img != null)
                {
                    if (caption.Equals("Đăng nhập"))
                    {
                        mFont.tahoma_7b_white.drawString(g, caption, x + this.w / 2 - Image.getHeight(img) / 2 - 8, y + 6,
                            2);
                    }
                    else
                        mFont.tahoma_7b_white.drawString(g, caption, x + this.w / 2 - Image.getHeight(img) / 2, y + 6, 2);
                }
                else
                {
                    mFont.tahoma_7b_white.drawString(g, caption, x + this.w / 2, y + 6, 2);
                }
            }
            else
            {
                mFont.tahoma_7b_white.drawString(g, caption, x + this.w / 2, y + 6, 2);
            }
        }

        public void paint(mGraphics g, Boolean isUclip)
        {
            if (back != null)
                g.drawImage(back, x, y, 0);
            if (focus != null && isFocus)
                g.drawImage(focus, x, y, 0);
            if (img != null)
            {
                if (isFocus)
                {
                    g.drawImage(imgFocus, x + Image.getWidth(img) / 2, y + Image.getHeight(img) / 2,
                        mGraphics.HCENTER | mGraphics.VCENTER, true);
                }
                else
                    g.drawImage(img, x + Image.getWidth(img) / 2, y + Image.getHeight(img) / 2,
                        mGraphics.HCENTER | mGraphics.VCENTER, true);
                if (img != null)
                {
                    if (caption.Equals("Đăng nhập"))
                    {
                        mFont.tahoma_7b_white.drawString(g, caption, x + this.w / 2 /*- Image.getHeight(img)/2 - 8 */
                            , y + Image.getHeight(img) / 2 - mFont.tahoma_7b_white.getHeight(), 2, true);
                    }
                    else
                        mFont.tahoma_7b_white.drawString(g, caption, x + this.w / 2 /*- Image.getHeight(img)/2 */,
                            y + 3 * Image.getHeight(img) / 4 - mFont.tahoma_7b_white.getHeight(), 2, true);
                }
                else
                {
                    mFont.tahoma_7b_white.drawString(g, caption, x + this.w / 2,
                        y + Image.getHeight(img) / 2 - mFont.tahoma_7b_white.getHeight(), 2, true);
                }
            }
            else
            {
                mFont.tahoma_7b_white.drawString(g, caption, x + this.w / 2, y + 6, 2);
            }
        }

        public bool input()
        {
            isFocus = false;
            if (GameCanvas.isPointerHoldIn(x, y, w, h))
            {
                if (GameCanvas.isPointerDown)
                    isFocus = true;
                if (GameCanvas.isPointerJustRelease && GameCanvas.isPointerClick)
                {
                    return true;
                }
            }
            if (GameCanvas.isPoint(x, y, w, h) && GameCanvas.isPointerJustRelease)
            {
                GameCanvas.isPointerJustRelease = false;
                return true;
            }
            return false;
        }


        public void setPos(int x, int y, mBitmap img, mBitmap imgFocus)
        {
            this.img = img;
            this.imgFocus = imgFocus;
            this.x = x;
            this.y = y;
            if (img != null)
            {
                w = Image.getWidth(img);
                h = Image.getHeight(img);
            }
        }

        public bool isFocusing()
        {
            return isFocus;
        }
    }
}