﻿using System;
using UnityEngine;

namespace src.lib
{
    public class ipKeyboard
    {
        private static TouchScreenKeyboard tk;
        private static Command act;
        
        public static void update()
        {
            try
            {      
                if(tk==null)return;
                if(tk.done)
                {
                    if(act!=null)act.performAction();
                    tk.text = "";
                    tk = null;
                }
            }
            catch (Exception e) { }
        }

   
    }
}

