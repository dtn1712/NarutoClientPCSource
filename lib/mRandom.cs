using System;

namespace src.lib
{
	public class mRandom {
		public Random r;
		public mRandom(){
			r=new Random();
		}
		public mRandom(long c){
			r=new Random();		
		}
		public int nextInt(){
			return r.Next();
		}
		public int nextInt(int n){
			return r.Next(n);
		}
	}
}
