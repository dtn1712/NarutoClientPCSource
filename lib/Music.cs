
using System;

namespace src.lib
{
	public class Music {
		public static Music gII;
		public static bool isSound = true;
		public static bool isPlayingMusic = false;
		public static int volume = 0;
		public const int MAX_VOLUME = 10;
//	public static int[] soundID;

		// MUSIC
		public const int BATTLE = 0;
		public const int WIND = 1;

		// SOUND
		public const int ATTACK_0 = 50;
		public const int ATTACK_1 = 51;
		public const int ATTACK_2 = 52;
		public const int RUN = 53;
		public const int LENLV = 54;
		public const int SKILL2 = 55;

		public const int nMusic = 5;
		public const int nSound = 6;
		public const int MLogin = 0;
		public const int MGameScr = 1;


		public static int currentMusic = 0;
		public static int currentSound = 50;
		public static bool isplaySound = true;
		public static bool isplayMusic = true;

		public static void init() {

			int[] listMusic= new int[6];
			for (int i = 0; i < listMusic.Length; i++)
			{
				listMusic[i] = i;
			}
			int[] listSound = new int[5];
			for (int i = 0; i < listSound.Length; i++)
			{
				listSound[i] = i;
			}
			Sound.init(listMusic, listSound);
			play(MLogin, 5);
		}


		public static void play(int id, float volume) {
			//if (true) return;
			try
			{
				if (id < 50)
				{
					currentMusic = id;
					for (int i = 0; i < nMusic; i++)
					{
						Sound.stopMusic(i);
					}
					if (isplayMusic)
					{
						Sound.playMus(id, volume, true);
					}
				}
				else
				{
					currentSound = id;
					for (int i = 0; i < nSound; i++)
					{
						Sound.stop(i + nMusic);
					}
					if (isplaySound)
						Sound.playSound(id - 50, volume);
				}
			}
			catch (Exception e)
			{
				//e.printStackTrace();
			}

		}

		public static void pauseMusic() {
			Sound.pauseMusic();
		}
		public static void resumeMusic()
		{
			isPlayingMusic = true;
			play(currentMusic, 5);
      
		}

		public static void stopSound(int id) {
		
		}
	}
}
