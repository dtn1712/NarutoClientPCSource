using System.Threading;
using UnityEngine;

namespace src.lib
{
    public class Sound
    {
        const int INTERVAL = 5;
        const int MAXTIME = 100;
        public static int status, postem, timestart;
        static string filenametemp;
        static float volumetem;
        public static bool isSound = true, isNotPlay, stopAll;

        public static AudioSource SoundBGLoop;
        //sua cho giong android

        //--sua cho giong android

        public static AudioClip[] music;
        public static GameObject[] player;


        public static void init()
        {
            GameObject AudioObj = new GameObject();
            AudioObj.name = "Audio Player";
            AudioObj.transform.position = Vector3.zero;
            AudioObj.AddComponent<AudioListener>();
            SoundBGLoop = AudioObj.AddComponent<AudioSource>();
        }

        //----moi 
        public static int l1;

        public static void init(int[] musicID, int[] sID)
        {
            if (player != null || music != null) return;
            init();
            l1 = musicID.Length;
            player = new GameObject[musicID.Length + sID.Length];
            music = new AudioClip[musicID.Length + sID.Length];
            for (int i = 0; i < player.Length; i++)
            {
                string name = (i < l1 ? "/music/" + i : "/sound/" + (i - l1));
                getAssetSoundFile(name, i);
            }
            //  bMuzikDisable = false;
        }

        public static void playSound(int id, float volume)
        {
            play(id + l1, volume);
        }

        //----moi 
        public static void getAssetSoundFile(string fileName, int pos)
        {
            stop(pos);
            string t = "";
            t = Main.res + fileName;
            load(t, pos);
        }


        public static void stopMusic(int x)
        {
            LogDebug.println("stopMusic  " + x);
        }

        public static void play(int id, float volume)
        {
            if (isNotPlay) return;
            if (GameCanvas.isPlaySound)
                start(volume, id);
        }

        public static void pauseMusic()
        {
            SoundBGLoop.GetComponent<AudioSource>().Stop();
        }


        public static void playMus(int type, float vl, bool loop)
        {
            if (isNotPlay) return;
            vl -= 0.3f;
            if (vl <= 0)
                vl = 0.010f;
            playSoundBGLoop(type, vl);
        }

        public static void playSoundBGLoop(int id, float volume)
        {
            //	if (!bMuzikDisable) {
            if (GameCanvas.isPlaySound)
            {
                if (SoundBGLoop == null)
                    return;
                if (isPlayingSoundBG(id))
                {
                    SoundBGLoop.GetComponent<AudioSource>().clip = music[id];
                    SoundBGLoop.GetComponent<AudioSource>().Play();
                    return;
                }
                SoundBGLoop.GetComponent<AudioSource>().loop = true;
                SoundBGLoop.GetComponent<AudioSource>().clip = music[id];
                SoundBGLoop.GetComponent<AudioSource>().volume = volume;
                SoundBGLoop.GetComponent<AudioSource>().Play();
            }
        }


        public static bool isPlayingSoundBG(int id)
        {
            if (SoundBGLoop == null)
                return false;
            bool isPlay = SoundBGLoop.GetComponent<AudioSource>().isPlaying;
            return isPlay;
        }

        //-----------
        public static void load(string filename, int pos)
        {
            if (Thread.CurrentThread.Name == Main.mainThreadName)
                __load(filename, pos);
            else
                _load(filename, pos);
        }

        private static void _load(string filename, int pos)
        {
            if (status != 0)
            {
                LogDebug.LogError("CANNOT LOAD AUDIO " + filename + " WHEN LOADING " + filenametemp);
                return;
            }
            filenametemp = filename;
            postem = pos;
            status = 2;
            int i = 0;
            while (i < MAXTIME)
            {
                Thread.Sleep(INTERVAL);
                if (status == 0)
                {
                    break; // load done
                }
                i++;
            }
            if (i == MAXTIME)
                LogDebug.LogError("TOO LONG FOR LOAD AUDIO " + filename);
            else
                LogDebug.Log("Load Audio " + filename + " done in " + (i * INTERVAL) + "ms");
        }

        private static void __load(string filename, int pos)
        {
            try
            {
                music[pos] = (AudioClip) Resources.Load(filename, typeof(AudioClip));
                player[pos] = GameObject.Find("Camera");
                GameObject.Find("Camera").AddComponent<AudioSource>();
            }
            catch (System.Exception)
            {
            }
        }

        public static void start(float volume, int pos)
        {
            if (Thread.CurrentThread.Name == Main.mainThreadName)
                __start(volume, pos);
            else
                _start(volume, pos);
        }

        public static void _start(float volume, int pos)
        {
            if (status != 0)
            {
                Debug.LogError("CANNOT START AUDIO WHEN STARTING");
                return;
            }
            volumetem = volume;
            postem = pos;
            status = 3;
            int i = 0;
            while (i < MAXTIME)
            {
                Thread.Sleep(INTERVAL);
                if (status == 0)
                    break; // start done
                i++;
            }
            if (i == MAXTIME)
                Debug.LogError("TOO LONG FOR START AUDIO");
            else
                Debug.Log("Start Audio done in " + (i * INTERVAL) + "ms");
        }

        public static void __start(float volume, int pos)
        {
            if (player[pos] == null)
                return;
            player[pos].GetComponent<AudioSource>().PlayOneShot(music[pos], volume);
        }

        //====================
        public static void stop(int pos)
        {
            if (Thread.CurrentThread.Name == Main.mainThreadName)
                __stop(pos);
            else
                _stop(pos);
        }

        public static void _stop(int pos)
        {
            if (status != 0)
            {
                Debug.LogError("CANNOT STOP AUDIO WHEN STOPPING");
                return;
            }
            postem = pos;
            status = 4;
            int i = 0;
            while (i < MAXTIME)
            {
                Thread.Sleep(INTERVAL);
                if (status == 0)
                    break; // start done
                i++;
            }
            if (i == MAXTIME)
                Debug.LogError("TOO LONG FOR STOP AUDIO");
            else
                Debug.Log("Stop Audio done in " + (i * INTERVAL) + "ms");
        }

        public static void __stop(int pos)
        {
            if (player[pos] != null)
                player[pos].GetComponent<AudioSource>().Stop();
        }
    }
}