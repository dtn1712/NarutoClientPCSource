﻿using System;

namespace src.lib
{
    public class mBitmap
    {
        public Image image;
        public int w, h, width, height;
        private string key;
        
        public int getWidth()
        {
            if (image == null) return 1;
            return image==null?2:image.getWidth();
        }
        public int getHeight()
        {
            if (image == null) return 1;
            return image == null ? 2 : image.getHeight();
        }
        public mBitmap(Image imgg) {
            image = imgg;
        }

        public mBitmap() {}

        public void cleanImg()
        {
            try
            {
                if (image != null)
                    image.texture = null;
                mBitmap imgg = (mBitmap)Image.listImgLoad.get(key);
                if (imgg != null)
                {
                    Image.listImgLoad.Remove(key);
                    Image.listImgLoad.remove(imgg);
                }
            }
            catch (Exception e)
            {
            }
        }
    }
}

