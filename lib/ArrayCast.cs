﻿namespace src.lib
{
    public class ArrayCast
    {
        public static sbyte[] cast(byte[] data)
        {
            sbyte[] data2=new sbyte[data.Length];
            for (int i = 0; i < data2.Length; i++)
                data2[i] = (sbyte) data[i];
            return data2;
        }
        public static byte[] cast(sbyte[] data)
        {
            byte[] data2 = new byte[data.Length];
            for (int i = 0; i < data2.Length; i++)
                data2[i] = (byte)data[i];
            return data2;
        }
    }
}
