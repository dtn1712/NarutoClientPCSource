using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using src.lib;
using UnityEngine;

namespace src.network
{
    public class Session : ISession
    {
        private static readonly Session instance = new Session();

        public static Session gI()
        {
            return instance;
        }

        private static NetworkStream dataStream;
        private static BinaryReader dis;
        private static BinaryWriter dos;

        private static IMessageHandler messageHandler;

        //	private static Socket sc;
        private static TcpClient sc;

        private static bool connected;
        private static bool connecting;
        private static readonly Sender sender = new Sender();
        private static Thread initThread;
        private static Thread collectorThread;
        private static int sendByteCount;
        private static int recvByteCount;
        private static bool getKeyComplete;
        private static sbyte[] key;
        private static sbyte curR, curW;
        private static int timeConnected;
        public static string strRecvByteCount = "";
        
        private int timeOut;

        private string host;
        private int port;

        public bool isConnected()
        {
            return connected;
        }

        public void setHandler(IMessageHandler msgHandler)
        {
            messageHandler = msgHandler;
        }

        public void connect(string host, int port)
        {
            Debug.Log("connect ...!" + connected + "  ::  " + connecting);
            if (connected || connecting) return;
            
            this.host = host;
            this.port = port;

            getKeyComplete = false;
            sc = null;

            Debug.Log("connecting...!");
            Debug.Log("host: " + host);
            Debug.Log("port: " + port);

            initThread = new Thread(NetworkInit);
            initThread.Start();
        }

        private void NetworkInit()
        {
            connecting = true;
            Thread.CurrentThread.Priority = System.Threading.ThreadPriority.Highest;
            connected = true;
            try
            {
                doConnect(host, port);
                messageHandler.onConnectOK();
            }
            catch (Exception)
            {
                if (messageHandler != null)
                {
                    close();
                    messageHandler.onConnectionFail();
                }
            }
        }

        private void doConnect(string host, int port)
        {
            timeOut = (int) (mSystem.currentTimeMillis() / 1000);
            sc = new TcpClient();
            sc.Connect(host, port);
            dataStream = sc.GetStream();
            dis = new BinaryReader(dataStream, new System.Text.UTF8Encoding());
            dos = new BinaryWriter(dataStream, new System.Text.UTF8Encoding());
            new Thread(sender.run).Start();

            var myCollect = new MessageCollector();
            collectorThread = new Thread(myCollect.run);
            collectorThread.Start();
            timeConnected = currentTimeMillis();

            connecting = false;
            doSendMessage(new Message(-27));
            timeOut = 0;
        }


        public void sendMessage(Message message)
        {
            sender.AddMessage(message);
        }

        private static void doSendMessage(Message m)
        {
            sbyte[] data = m.getData();
            try
            {
                var cmd = m.command;
                int size;
                byte[] encryptedData = null;
                if (data != null)
                {
                    size = data.Length;
                    if (getKeyComplete)
                    {
                        // Encrypt message
                        encryptedData = MessageCipher.Encrypt(Converter.convertSbyteToByte(key)
                            , Converter.convertSbyteToByte(data));
                    }
                
                    sendByteCount += 5 + data.Length;
                }
                else
                {
                    size = 0;
                    sendByteCount += 5;
                }

                var finalDataSize = size;
                if (encryptedData != null)
                {
                    finalDataSize = encryptedData.Length;
                }
                byte[] finalData = new byte[5 + finalDataSize];
                finalData[0] = Converter.convertSbyteToByte((sbyte)cmd);

                byte[] sizeBytes = Converter.convertIntToBytes(finalDataSize);
                finalData[1] = sizeBytes[0];
                finalData[2] = sizeBytes[1];
                finalData[3] = sizeBytes[2];
                finalData[4] = sizeBytes[3];

                if (finalDataSize > 0 && encryptedData != null)
                {
                    Buffer.BlockCopy(encryptedData, 0, finalData, 5, finalDataSize);
                }
                dos.Write(finalData);
                dos.Flush();
            }
            catch (Exception e)
            {
            }
        }

        private class Sender
        {
            private List<Message> sendingMessage;

            public Sender()
            {
                sendingMessage = new List<Message>();
            }

            public void AddMessage(Message message)
            {
                sendingMessage.Add(message);
            }

            public void run()
            {
                while (connected)
                {
                    try
                    {
                        if (getKeyComplete)
                            while (sendingMessage.Count > 0)
                            {
                                var m = sendingMessage[0];
                                doSendMessage(m);
                                sendingMessage.RemoveAt(0);
                            }
                        try
                        {
                            Thread.Sleep(5);
                        }
                        catch (Exception e)
                        {
                            LogDebug.LogError(e.ToString());
                        }
                    }
                    catch (Exception)
                    {
                        Debug.Log("error send message! ");
                    }
                }
            }
        }

        private class MessageCollector
        {
            public void run()
            {
                try
                {
                    while (connected)
                    {
                        var message = readMessage();
                        if (message != null)
                        {
                            try
                            {
                                if (message.command == -27)
                                {
                                    getKey(message);
                                }
                                else
                                {
                                    onRecieveMsg(message);
                                }
                            }
                            catch (Exception)
                            {
                                LogDebug.LogError("LOI NHAN  MESS THU 1");
                            }
                        }
                        else
                        {
                            break;
                        }
                        try
                        {
                            Thread.Sleep(5);
                        }
                        catch (Exception)
                        {
                            LogDebug.println("LOI NHAN  MESS THU 2");
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("error read message!");
                    Debug.Log(e.Message);
                }
                
                if (!connected) return;
                
                Debug.Log("error read message!");
                if (messageHandler != null)
                {
                    if (currentTimeMillis() - timeConnected > 500)
                        messageHandler.onDisconnected();
                    else
                        messageHandler.onConnectionFail();
                }
                if (sc != null)
                    cleanNetwork();
            }

            private static void getKey(Message message)
            {
                try
                {
                    var keySize = message.reader().readSByte();
                    key = new sbyte[keySize];
                    for (var i = 0; i < keySize; i++)
                    {
                        key[i] = message.reader().readSByte();
                    }
                    for (var i = 0; i < key.Length - 1; i++)
                    {
                        key[i + 1] ^= key[i];
                    }
                
                    getKeyComplete = true;
                }
                catch (Exception)
                {
                }
            }
            private static Message readMessage()
            {
                try
                {
                    // read message command
                    sbyte cmd = dis.ReadSByte();
                
                    sbyte[] b = {dis.ReadSByte(), dis.ReadSByte(), dis.ReadSByte(), dis.ReadSByte()};
                
                    int size = ((b[0] & 0xff) << 24) | ((b[1] & 0xff) << 16) |
                               ((b[2] & 0xff) << 8) | (b[3] & 0xff);
                
                
                    byte[] dataTemp = dis.ReadBytes(size);
                
                    recvByteCount += (5 + size);
                    int Kb = (recvByteCount + sendByteCount);
                    strRecvByteCount = Kb / 1024 + "." + Kb % 1024 / 102 + "Kb";

                    Message msg;
                    if (getKeyComplete)
                    {
                        // Decrypt the message
                        byte[] data =  MessageCipher.Decrypt(Converter.convertSbyteToByte(key), dataTemp);
                        msg = new Message(cmd, Converter.convertByteToSbyte(data));
                    }
                    else
                    {
                        sbyte[] data = new sbyte[size];
                        Buffer.BlockCopy(dataTemp, 0, data, 0, size);
                        msg = new Message(cmd, data);
                    }
                    return msg;
                }
                catch (Exception e)
                {
                    Debug.Log(e.StackTrace);
                }

                return null;
            }
        }

        private static Vector receiveMsg = new Vector();

        private static void onRecieveMsg(Message msg)
        {
            if (Thread.CurrentThread.Name == Main.mainThreadName)
                messageHandler.onMessage(msg);
            else
                receiveMsg.addElement(msg);
        }

        public static void update()
        {
            if (instance.timeOut != 0)
            {
                if ((mSystem.currentTimeMillis() / 1000 - instance.timeOut) > 10)
                {
                    instance.timeOut = 0;
                    if (sc != null)
                        cleanNetwork();
                    return;
                }
            }
            while (receiveMsg.size() > 0)
            {
                var msg = (Message) receiveMsg.elementAt(0);

                if (msg == null)
                {
                    receiveMsg.removeElementAt(0);
                    return;
                }
                messageHandler.onMessage(msg);
                receiveMsg.removeElementAt(0);
            }
        }

        public void close()
        {
            receiveMsg.removeAllElements();
            cleanNetwork();
        }

        private static void cleanNetwork()
        {
            key = null;
            curR = 0;
            curW = 0;
            try
            {
                connected = false;
                connecting = false;
                if (sc != null)
                {
                    sc.Close();
                    sc = null;
                }
                if (dataStream != null)
                {
                    dataStream.Close();
                    dataStream = null;
                }
                if (dos != null)
                {
                    dos.Close();
                    dos = null;
                }
                if (dis != null)
                {
                    dis.Close();
                    dis = null;
                }
                collectorThread = null;
            }
            catch (Exception)
            {
            }
        }


        private static int currentTimeMillis()
        {
            return Environment.TickCount;
        }

  
    }
}