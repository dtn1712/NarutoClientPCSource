using src.lib;

namespace src.network
{
	public class Message
	{
	
		public short command;

		private int dataLength;
	
		private MyReader dis = null; 
		private MyWriter dos = null; 
	
		public Message(int command) {
			this.command = (sbyte) command;
			dos = new MyWriter();
		}
		public Message() {
			dos = new MyWriter();
		}

		public Message(sbyte command) {
			this.command = (sbyte)command;
			dos = new MyWriter();
		}

		public Message(sbyte command, sbyte[] data) {
			this.command = (sbyte)command;
			dis = new MyReader(data);
		}

		public sbyte[] getData() {
			return dos.getData();
		}

		public MyReader reader() {
			return dis;
		}

		public MyWriter writer() {
			return dos;
		}

		public void cleanup() {
//        try {
//            if (dis != null)
//                dis.Close();
//			dis = null;
//            if (dos != null)
//                dos.Close();
//			dos = null;
//        } catch (IOException) {
//        }
		}
	
	}
}


