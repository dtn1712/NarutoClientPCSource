using System;
using System.Collections.Generic;
using src.real.dto.quest;
using src.lib;

namespace src.Objectgame.quest
{
    public class MainQuestManager
    {

        private MainQuestManager()
        {
        }
        
        private static readonly MainQuestManager instance = new MainQuestManager();

        public static MainQuestManager getInstance()
        {
            return instance;
        }

        public QuestTemplate NewQuest { get; set; }
        public QuestTemplate FinishQuest { get; set; }
        public QuestTemplate WorkingQuest { get; set; }
        

        public void startQuest()
        {
            GameCanvas.menu.startAtNPC(new Vector(), 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus, NewQuest.ShortContent);
            Service.getInstance().Quest(0, QuestType.MAIN_QUEST.ToString());
        }
        
        public void completeQuest()
        {
            GameCanvas.menu.startAtNPC(new Vector(), 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus, FinishQuest.ResolveContent);
            Service.getInstance().Quest(1, QuestType.MAIN_QUEST.ToString());
        }
        
        
        public void showWorkingQuestInfo()
        {
            GameCanvas.menu.startAtNPC(new Vector(), 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus, WorkingQuest.SupportContent);
        }

    }
}