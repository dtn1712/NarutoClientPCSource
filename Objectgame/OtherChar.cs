using src.lib;

public class OtherChar
{
    public static Scroll scrMain = new Scroll();
    public int hChat = 10;
    public short id, x, y;
    public short idchar;

    public int indexSize = 28,
        indexTitle = 0,
        indexSelect = 0,
        indexRow = -1,
        indexRowMax,
        indexMenu = 0,
        indexCard = -1;

    private bool isHostClan;
    public bool isKhoa = false;
    public Vector listChat = new Vector();
    public short lvUser;
    public string name;

    public int nItemChat;
    public int nTinChuaDoc;

    public OtherChar(short idd, string namee)
    {
        id = idd;
        name = namee;
    }

    public OtherChar(short idd, string namee, short lv, bool isHostC)
    {
        lvUser = lv;
        isHostClan = isHostC;
        id = idd;
        name = namee;
    }

    //Them Id Char
    public OtherChar(short idd, string namee, short lv, bool isHostC, short idchar)
    {
        lvUser = lv;
        isHostClan = isHostC;
        id = idd;
        name = namee;
        this.idchar = idchar;
    }

    public void Paint(mGraphics g, int xTab, int yTab, int wND, int hND)
    {
        GameScr.resetTranslate(g);
        var notehdem = 0;
        hChat = hND;
        scrMain.setStyle(nItemChat, NodeChat.HSTRING, xTab, yTab, wND - 3, hND, true, 1);
        scrMain.setClip(g, xTab, yTab, wND - 3, hND);
        for (var i = listChat.size() - 1; i >= 0; i--)
        {
            var node = (NodeChat) listChat.elementAt(i);
            if (node != null)
            {
                node.paint(g, xTab + NodeChat.HSTRING, yTab + NodeChat.HSTRING / 2 + notehdem, node.isMe, false);

                notehdem += node.hnode + NodeChat.HSTRING / 2;
            }
        }
        GameScr.resetTranslate(g);
    }

    public void update()
    {
        var dem = 0;
        for (var i = listChat.size() - 1; i >= 0; i--)
        {
            var node = (NodeChat) listChat.elementAt(i);
            if (node != null)
                dem += node.hnode + NodeChat.HSTRING / 2;
        }
        scrMain.cmtoY = -hChat + dem;
        nItemChat = dem / NodeChat.HSTRING + 1;
        if (GameCanvas.isTouch)
        {
            var r = scrMain.updateKey();
            scrMain.updatecm();
            if (r.isDowning || r.isFinish)
                indexRow = r.selected;
        }
    }
}