using src.lib;

public class ItemTemplates
{
    private static MyHashtable itemTemplates = new MyHashtable();


    public static void add(ItemTemplate it)
    {
        itemTemplates.put(it.id + "", it);
    }

    public static ItemTemplate get(short id)
    {
        return (ItemTemplate) itemTemplates.get("" + id);
    }
}