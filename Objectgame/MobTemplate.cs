using src.lib;

public class MobTemplate
{
    public EffectData data;
    public Frame[] frameBoss;
    public sbyte[][] frameBossAttack;
    public sbyte[] frameBossMove;
    public int hp;
    public ImageInfo[] imginfo;
    public mBitmap[] imgs = new mBitmap[8];
    public short mobTemplateId, idloadimage;
    public string name;
    public sbyte rangeMove, speed, type;

    public ImageInfo getImgInfo(sbyte frame)
    {
        return imginfo[frame];
    }
}