using src.lib;

public class Laroi
{
    public static mRandom rd = new mRandom(mSystem.currentTimeMillis());
    public static Vector la = new Vector();

    private static Laroi sn;

    private int id_frame;
    private int tickk;
    private int type;

    // int dirX, dirY;
    private int vx, vy;

    private int X, Y;

    public Laroi(int x, int y)
    {
        X = x;
        Y = y;
        vx = vy = 0;
    }

    public static void load()
    {
        la.removeAllElements();
        var kc = TileMap.pxw / 50;
        for (var i = 0; i < 50; i++)
        {
            sn = new Laroi(-20 + random(-5, 45) * kc * kc, -10 - random(1, 30) * 50);
            la.addElement(sn);
        }
    }

    public static int absb(int x)
    {
        if (x < 0)
            return -x;
        return x;
    }

    public void update()
    {
        tickk = (tickk + 1) % 100;
        if (tickk % 4 == 0)
        {
            id_frame++;
            if (id_frame > 3)
                id_frame = 0;
        }
        X += random(1, 3);
        Y += random(1, 3);
        if (Y > ((GameCanvas.h + TileMap.pxh) / TileMap.size + 2) * 24 + 240
            || X > ((GameCanvas.w + TileMap.pxw) / TileMap.size + 1) * 24 + 240
            || X < -600)
        {
            X = random(-5, TileMap.pxh / 50 - 5) * 50;
            Y = -random(0, 20) * 50;
        }
    }

    private static int random(int imin, int imax)
    {
        var delta = absb(imax - imin);
        return absb(rd.nextInt(delta)) + imin;
    }

    public static void update_Laroi()
    {
        for (var i = 0; i < la.size(); i++)
        {
            var obj = (Laroi) la.elementAt(i);
            obj.update();
        }
    }

    public static void paint_Laroi(mGraphics g)
    {
        for (var i = 0; i < la.size(); i++)
        {
            var obj = (Laroi) la.elementAt(i);
            obj.paint(g);
        }
    }

    private void paint(mGraphics g)
    {
        g.drawRegion(LoadImageInterface.imgLa, 0, id_frame * 14, 20, 14, Sprite.TRANS_NONE, X,
            Y, mGraphics.VCENTER | mGraphics.BOTTOM);
    }
}