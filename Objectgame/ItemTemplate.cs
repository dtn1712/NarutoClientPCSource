public class ItemTemplate
{
    public const sbyte ITEM_TEMPLATE = 5;
    public const sbyte CHAR_WEARING = 0;
    public sbyte clazz, quocgia;
    public string description;
    public string[] despaint;
    public sbyte gender; // 1 là đồ nam, 0 là đồ nữ, 2 là đồ chung
    public long gia;
    public short iconID; // icon small image id

    public short id;
    public bool isUpToUp;
    public sbyte level;
    public short lvItem;
    public string name;
    public short part;
    public short partquan, partdau;
    public sbyte type;
    public sbyte typeSell = 0; //0 Xu  //1 gold
    public int w, h;

    // ngược(xu)

    public ItemTemplate(short templateID, sbyte type, sbyte gender, string name, string description, sbyte level,
        short iconID, short part, bool isUpToUp)
    {
        //super();
        id = templateID;
        this.type = type;
        this.gender = gender;
        this.name = name;
        this.description = description;
        this.level = level;
        this.iconID = iconID;
        this.part = part;
        this.isUpToUp = isUpToUp;
    }

    public ItemTemplate(short templateID, sbyte type, sbyte gender, string name, string description, sbyte level,
        short iconID, short part, short partquan, short partdau, bool isUpToUp)
    {
        //super();
        id = templateID;
        this.type = type;
        this.gender = gender;
        this.name = name;
        this.description = description;
        this.level = level;
        this.iconID = iconID;
        this.part = part;
        this.partquan = partquan;
        this.partdau = partdau;
        //System.out.println("PART QUAN THEM VAO SAU KHI THANH 1 SET ----> "+partquan+" AOOOOO "+part);
        this.isUpToUp = isUpToUp;
    }
}