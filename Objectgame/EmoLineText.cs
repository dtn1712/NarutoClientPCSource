using src.lib;

public class EmoLineText
{
    public Vector listEmo;
    public string[] listtext;
    public int loai;
    public int[] wlist;

    public EmoLineText(int loaii, string[] text, Vector listEmoo)
    {
        wlist = new int[text.Length];
        wlist[0] = 0;
        for (var i = 0; i < text.Length; i++)
        {
            var with = mFont.tahoma_7_yellow
                .getWidth(text[i]) ;
            if (i < wlist.Length - 1)
                wlist[i + 1] = with + wlist[i];
        }
        loai = loaii;
        listtext = text;
        listEmo = listEmoo;
    }
}