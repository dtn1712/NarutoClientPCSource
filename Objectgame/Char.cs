using System;
using src.lib;

public class Char
{
    public const sbyte A_STAND = 1,
        A_RUN = 2,
        A_JUMP = 3,
        A_FALL = 4,
        A_DEADFLY = 5,
        A_NOTHING = 6,
        A_ATTK = 7,
        A_INJURE = 8,
        A_AUTOJUMP = 9,
        A_WATERRUN = 10,
        A_WATERDOWN = 11,
        SKILL_STAND = 12,
        SKILL_FALL = 13,
        A_DEAD = 14,
        A_HIDE = 15;

    public const sbyte PK_NORMAL = 0;
    public const sbyte PK_NHOM = 1;
    public const sbyte PK_BANG = 2;
    public const sbyte PK_DOSAT = 3;
    public const sbyte PK_PHE1 = 4;
    public const sbyte PK_PHE2 = 5;
    public static Clan clan;
    public static bool fallAttack;

    public static bool isAHP;
    public static bool isAMP;
    public static bool isAFood;
    public static bool isABuff;
    public static bool isAPickYen;
    public static bool isAPickYHM;
    public static bool isAPickYHMS;
    public static bool isANoPick;
    public static bool isAResuscitate;
    public static bool isAFocusDie;

    public static int aHpValue = 20;
    public static int aMpValue = 20;
    public static int aFoodValue = 70;
    public static short aCID; // int sua lai short
    public static int pointPB, pointChienTruong;

    public static Char toCharChat;
    public static OtherChar toCharChatSelected;


    public static int[][][] CharInfo =
    {
        // Head, Leg, Body, Weapon
        new[]
        {
            new[] {0, -10, 32}, new[] {1, -7, 7}, new[] {1, -11, 15}, new[] {1, -9, 45}
        }, // Stand0
        // -
        // [0]
        new[]
        {
            new[] {0, -10, 33}, new[] {1, -7, 7}, new[] {1, -11, 16}, new[] {1, -9, 46}
        }, // Stand1
        // -
        // [1]
        // ==
        new[]
        {
            new[] {1, -10, 33}, new[] {2, -10, 11}, new[] {2, -9, 16}, new[] {1, -12, 49}
        }, // Run0
        // -
        // [2]
        new[]
        {
            new[] {1, -10, 32}, new[] {3, -11, 9}, new[] {3, -11, 16}, new[] {1, -13, 47}
        }, // Run1
        // -
        // [3]
        new[]
        {
            new[] {1, -10, 34}, new[] {4, -9, 9}, new[] {4, -8, 16}, new[] {1, -12, 47}
        }, // Run2
        // -
        // [4]
        new[]
        {
            new[] {1, -10, 34}, new[] {5, -11, 11}, new[] {5, -10, 17}, new[] {1, -13, 49}
        }, // Run3
        // -
        // [5]
        new[]
        {
            new[] {1, -10, 33}, new[] {6, -9, 9}, new[] {6, -8, 16}, new[] {1, -12, 47}
        }, // Run4

//			{ { 1, -10, 32 }, { 6, -9, 9 }, { 6, -8, 16 }, { 1, -12, 47 } }, // Run them 1 frame
        // -
        // [6]
        // ==
        new[]
        {
            new[] {0, -9, 36}, new[] {7, -5, 15}, new[] {7, -10, 21}, new[] {1, -8, 49}
        }, // Jump0
        // -
        // [7]
        new[]
        {
            new[] {4, -13, 26}, new[] {0, 0, 0}, new[] {0, 0, 0}, new[] {0, 0, 0}
        }, // JumpRotate0
        // - [8]
        new[]
        {
            new[] {5, -13, 25}, new[] {0, 0, 0}, new[] {0, 0, 0}, new[] {0, 0, 0}
        }, // JumpRotate1
        // - [9]
        new[]
        {
            new[] {6, -12, 26}, new[] {0, 0, 0}, new[] {0, 0, 0}, new[] {0, 0, 0}
        }, // JumpRotate2
        // -
        // [10]
        new[]
        {
            new[] {7, -13, 25}, new[] {0, 0, 0}, new[] {0, 0, 0}, new[] {0, 0, 0}
        }, // JumpRotate3
        // -
        // [11]
        new[]
        {
            new[] {0, -9, 35}, new[] {8, -4, 13}, new[] {8, -14, 27}, new[] {1, -9, 49}
        }, // Fall0
        // -
        // [12]
        // ==
        new[]
        {
            new[] {0, -9, 31}, new[] {9, -11, 8}, new[] {10, -10, 17}, new[] {0, 0, 0}
        }, // Attak0
        // -
        // [13]
        new[]
        {
            new[] {2, -7, 33}, new[] {9, -11, 8}, new[] {11, -8, 15}, new[] {0, 0, 0}
        }, // Attak1
        // -
        // [14]
        new[]
        {
            new[] {2, -8, 32}, new[] {9, -11, 8}, new[] {12, -8, 14}, new[] {0, 0, 0}
        }, // Attak2
        // -
        // [15]
        new[]
        {
            new[] {2, -7, 32}, new[] {9, -11, 8}, new[] {13, -12, 15}, new[] {0, 0, 0}
        }, // Attak3
        // -
        // [16]
        new[]
        {
            new[] {0, -11, 31}, new[] {9, -11, 8}, new[] {14, -15, 18}, new[] {0, 0, 0}
        }, // Attak4
        // -
        // [17]
        new[]
        {
            new[] {2, -9, 32}, new[] {9, -11, 8}, new[] {15, -13, 19}, new[] {0, 0, 0}
        }, // Attak5
        // -
        // [18]
        new[]
        {
            new[] {2, -9, 31}, new[] {9, -11, 8}, new[] {16, -7, 22}, new[] {0, 0, 0}
        }, // Attak6
        // -
        // [19]
        new[]
        {
            new[] {2, -9, 32}, new[] {9, -11, 8}, new[] {17, -11, 18}, new[] {0, 0, 0}
        }, // Attak7
        // -
        // [20]
        // ==
        new[]
        {
            new[] {3, -12, 34}, new[] {8, -4, 13}, new[] {8, -15, 25}, new[] {1, -10, 46}
        }, // Injure
        // -
        // [21]
        // ==
        new[]
        {
            new[] {0, -9, 32}, new[] {8, -4, 9}, new[] {10, -10, 18}, new[] {0, 0, 0}
        }, // FallAttak0
        // -
        // 22
        new[]
        {
            new[] {2, -7, 34}, new[] {8, -4, 9}, new[] {11, -8, 16}, new[] {0, 0, 0}
        }, // FallAttak1
        // -
        // 23
        new[]
        {
            new[] {2, -8, 33}, new[] {8, -4, 9}, new[] {12, -8, 15}, new[] {0, 0, 0}
        }, // FallAttak2
        // -
        // 24
        new[]
        {
            new[] {2, -7, 33}, new[] {8, -4, 9}, new[] {13, -12, 16}, new[] {0, 0, 0}
        }, // FallAttak3
        // -
        // 25
        new[]
        {
            new[] {0, -11, 32}, new[] {7, -5, 9}, new[] {14, -15, 19}, new[] {0, 0, 0}
        }, // FallAttak4
        // -
        // 26
        new[]
        {
            new[] {2, -9, 33}, new[] {7, -5, 9}, new[] {15, -13, 20}, new[] {0, 0, 0}
        }, // FallAttak5
        // -
        // 27
        new[]
        {
            new[] {2, -9, 32}, new[] {7, -5, 9}, new[] {16, -7, 23}, new[] {0, 0, 0}
        }, // FallAttak6
        // -
        // 28
        new[]
        {
            new[] {2, -9, 33}, new[] {7, -5, 9}, new[] {17, -11, 19}, new[] {0, 0, 0}
        } // FallAttak7
    };

    public static Char myCharr;
    public static bool ischangingMap, isLockKey;
    public static long timedelayloadmap;

    public static bool isManualFocus;
    public Item[] arrItemBag;
    public Item[] arrItemBody;
    public Item[] arrItemBox;

    public Char[] attChars;
    public Mob[] attMobs;
    public int bom = 0, count = 0;
    public bool canJumpHigh = true, cmtoChar, me, isStartSoundRun, cchistlast, isAttack, isAttFly;

    public int cBonusSpeed,
        cspeed,
        ccurrentAttack,
        cdame,
        cdameDown,
        clevel,
        cClass,
        cCountry,
        cMP,
        cMaxMP = 100,
        cHP,
        cHpNew,
        cMaxHP = 100,
        cMaxEXP = 100,
        HPShow,
        xReload,
        yReload,
        cyStartFall,
        saveStatus,
        eff5BuffHp,
        eff5BuffMp,
        autoUpHp;

    public int cCountryHoa = 0,
        cCountryThuy = 1,
        cCountryLoi = 2,
        cCountryPhong = 3,
        cCountryTho = 4,
        cIdClanLocal,
        cIdClanGlobal;

    public short cExactly;
    public long cEXP = 0, cExpDown;

    public long cExpR;
    public int cf, tick; // Weapon
    public short cFatal;
    public Char charFocus;
    public short CharidDB;

    public ChatPopup chatPopup;

    public long cIdDB;
    public short cMiss;
    public string cName, cClanName = "";
    public int countKill, countKillMax, tickCoat, tickEffmoto, tickEffFireW;
    public byte cPk, cTypePk;

    public MovePoint currentMovePoint;
    public int cvyJump;
    public int cw = 22, ch = 50, chw = 11, chh = 16;
    public int cx = 24, cy = 24, cybong;

    public int cxSend, cySend, cdirSend = 1, cxFocus, cyFocus, cxMoveLast, cyMoveLast, cactFirst = 5;

    private int cxtemp;
    public int[] diemTN, subTn;
    private int dxmove1, dxmove2;
    public EffectCharPaint eff, effTask;
    public EffectCharPaint eff0, eff1, eff2;
    public EffectPaint[] effPaints;
    public Vector focus = new Vector();


    public short head, leg, body;
    public short idFriend = -1;

    public short idParty = -1;
    public int indexEff = -1, indexEffTask = -1;
    public int indexSkill, i0, i1, i2, dx0, dx1, dx2, dy0, dy1, dy2;
    private int indexUseSkill = -1;
    public bool isDirtyPostion;
    public bool isEffBatTu;

    public byte isInjure;
    public bool isInvisible;

    public bool isLeader = false;
    public bool isLeaderParty = false;
    public bool isLockMove, isLockAttack, isBlinking;
    public bool isMob, isCrit, isDie;
    public bool isOnline;
    public ItemMap itemFocus;
    public Item[] ItemMyTrade = new Item[8];
    public Item[] ItemParnerTrade = new Item[8];
    public int killCharId = -9999;
    public Skill lastNormalSkill;
    public long lastTimeUseSkill = 0;
    public long lastUpdateTime;
    public long lastUseHP = mSystem.currentTimeMillis();
    public int lcx, lcy, cvx, cvy, cp1, cp2, cp3, statusMe = 5, cdir = 1, cgender;
    public long charID;

    public Mob mobFocus;
    public short[] moveFast;
    public QuickSlot[] mQuickslot;
    public Skill myskill;

    private int nInjure;
    public Npc npcFocus;
    public bool paintName = true;
    public Char partnerTrade;

    public int pPoint,
        sPoint,
        pointUydanh,
        pointNon,
        pointVukhi,
        pointAo,
        pointLien,
        pointGangtay,
        pointNhan,
        pointQuan,
        pointNgocboi,
        pointGiay,
        pointPhu,
        pointTinhTu,
        countFinishDay,
        countLoopBoos,
        limitTiemnangso,
        limitKynangso,
        limitPhongLoi,
        limitBangHoa,
        countPB;

    public byte resultTest;
    public SkillPaint skillPaint;

    public int skillTemplateId;

    public int sleepaddEffBui;
    public int sType;
    public short sysDown;
    public short sysUp;
    public Vector taskOrders = new Vector();

    public int testCharId = -9999;

    private int tickviBody = 0;
    public long timeRequesChangeMap; // time cấm chat
    public long timedelay = 5000;
    public long timeLastchangeMap;
    public int totalTN = 0;
    public sbyte typePk = -1;

    public Vector vFriend = new Vector();

    public Vector vMovePoints = new Vector(); // luu lai nhung vi tri nguoi choi ko phai la minh phai di qua.
    public Vector vSkill = new Vector(), vSkillFight = new Vector(), vEff = new Vector(), vDomsang = new Vector();

    public short wdx, wdy;

    public int xSd, ySd;

    public long xu, luong;

    public Char()
    {
        statusMe = A_STAND;
    }

    public int getdxSkill()
    {
        if (myChar().mQuickslot[0] != null)
        {
            var ql = myChar().mQuickslot[0];
            if (ql.idSkill != -1)
            {
                var skill = (SkillTemplate) SkillTemplates.hSkilltemplate.get("" + ql.idSkill);
                if (skill != null && skill.level >= 0 && skill.level < skill.rangelv.Length)
                    return skill.rangelv[skill.level];
                if (skill != null)
                    return skill.rangelv[0];
            }
        }
        else if (myskill != null)
        {
            return myskill.dx;
        }
        return 0;
    }

    private int getdySkill()
    {
        return myskill != null ? myskill.dy : 0;
    }

    public int getSpeed()
    {
        if (cspeed < 6)
            return 6;
        return cspeed < 5 ? 5 : 5;
    }

    public static Char myChar()
    {
        if (myCharr != null) return myCharr;
        myCharr = new Char
        {
            me = true,
            cmtoChar = true,
            mQuickslot = new QuickSlot[5]
        };
        for (var i = 0; i < 5; i++)
            myCharr.mQuickslot[i] = new QuickSlot();
        
        return myCharr;
    }


    private bool isInWaypoint()
    {
        var size = TileMap.vGo.size();
        for (byte i = 0; i < size; i++)
        {
            var wp = (Waypoint) TileMap.vGo.elementAt(i);
            if (cx < wp.minX || cx > wp.maxX || cy < wp.minY || cy > wp.maxY)
                continue;
            return true;
        }
        return false;
    }

    public void update()
    {
        if (ischangingMap)
            return;
        if (statusMe == A_DEAD)
        {
            if (!((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP))
            {
                cvy++;
                cy += cvy;
                Service.getInstance().charMove();
                if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP)
                {
                    cvy = 0;
                    cy = TileMap.tileXofPixel(cy + 3);
                    Service.getInstance().charMove();
                }
                if (cmtoChar)
                {
                    GameScr.cmtoX = cx - GameScr.gW2;
                    GameScr.cmtoY = cy - GameScr.gH23;

                    if (!GameCanvas.isTouchControl)
                        GameScr.cmtoX += GameScr.gW6 * cdir;
                }
            }
            return;
        }
        updateShadown();


        if (cmtoChar)
        {
            GameScr.cmtoX = cx - GameScr.gW2;
            GameScr.cmtoY = cy - GameScr.gH23;
            if (!GameCanvas.isTouchControl)
                GameScr.cmtoX += GameScr.gW6 * cdir;
        }
        updateSkillPaint();
        tick = (tick + 1) % 100;
        if (me)
        {
            if (statusMe != A_RUN && isStartSoundRun && GameCanvas.gameTick % 11 == 0)
            {
                isStartSoundRun = false;
                Music.stopSound(Music.RUN);
            }
            if (charFocus != null && !GameScr.vCharInMap.contains(charFocus))
                charFocus = null;

            if (cx < 40)
            {
                cvx = 0;
                cx = 40;
            }
            else if (cx > TileMap.pxw - 10)
            {
                cx = TileMap.pxw - 10;
                cvx = 0;
            }
            if (!ischangingMap && isInWaypoint())
            {
                timeRequesChangeMap--;
                if (timeRequesChangeMap <= 0)
                {
                    timeRequesChangeMap = 20;
                    Service.getInstance().charMove();
                }
                if (mSystem.currentTimeMillis() - (timeLastchangeMap + timedelay) > 0)
                {
                    timeLastchangeMap = mSystem.currentTimeMillis();
                    Service.getInstance().requestChangeMap();
                }
                timedelayloadmap = mSystem.currentTimeMillis();
            }
            else if (statusMe != A_FALL)
            {
                if (CRes.abs(cx - cxSend) >= 40 || CRes.abs(cy - cySend) >= 90)
                    if (cy - cySend <= 0)
                        if (me)
                            Service.getInstance().charMove();
            }
            if (isLockMove)
                currentMovePoint = null;

            if (currentMovePoint != null && (statusMe == A_STAND || statusMe == A_RUN))
            {
                statusMe = A_RUN;
                if (cx - currentMovePoint.xEnd > 0)
                {
                    cdir = -1;
                    if (cx - currentMovePoint.xEnd <= 10)
                        currentMovePoint = null;
                }
                else
                {
                    cdir = 1;
                    if (cx - currentMovePoint.xEnd >= -10)
                        currentMovePoint = null;
                }
                if (currentMovePoint != null)
                {
                    cvx = getSpeed() * cdir;
                    cvy = 0;
                }
            }

            autoPickItemMap();
            searchFocus();
        }
        else // char khác không phải là mình
        {
            if (GameCanvas.gameTick % 20 == 0 && charID >= 0) // Auto hide name
            {
                paintName = true;
                for (var i = 0; i < GameScr.vCharInMap.size(); i++)
                {
                    Char c = null;
                    try
                    {
                        c = (Char) GameScr.vCharInMap.elementAt(i);
                    }
                    catch (Exception e)
                    {
                    }
                    if (c == null || c.Equals(this))
                        continue;
                    if (c.cy == cy && CRes.abs(c.cx - cx) < 35 ||
                        cy - c.cy < 32 && cy - c.cy > 0 && CRes.abs(c.cx - cx) < 24)
                        paintName = false;
                }
            }

            if (statusMe == A_STAND || statusMe == A_NOTHING)
            {
                var processNextPoint = false;
                if (currentMovePoint != null)
                    if (abs(currentMovePoint.xEnd - cx) < 4 && abs(currentMovePoint.yEnd - cy) < 4)
                    {
                        cx = currentMovePoint.xEnd;
                        cy = currentMovePoint.yEnd;
                        currentMovePoint = null;

                        if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP)
                        {
                            changeStatusStand();
                            GameCanvas.gI().startDust(-1, cx - (-1 << 3), cy);
                            GameCanvas.gI().startDust(1, cx - (1 << 3), cy);
                        }
                        else
                        {
                            statusMe = A_FALL;
                            cvy = 0;
                        }
                        processNextPoint = true;
                    }
                    else
                    {
                        if (cy == currentMovePoint.yEnd) // RUNNING
                        {
                            if (cx != currentMovePoint.xEnd)
                            {
                                cx = (cx + currentMovePoint.xEnd) / 2;
                                cf = GameCanvas.gameTick % 5 + 2;
                            }
                        }
                        else if (cy < currentMovePoint.yEnd)
                        {
                            cf = 12;
                            cx = (cx + currentMovePoint.xEnd) / 2;
                            if (cvy < 0)
                                cvy = 0;
                            cy += cvy;
                            if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP)
                            {
                                GameCanvas.gI().startDust(-1, cx - (-1 << 3), cy);
                                GameCanvas.gI().startDust(1, cx - (1 << 3), cy);
                            }
                            cvy++;
                            if (cvy > 16)
                                cy = (cy + currentMovePoint.yEnd) / 2;
                        }
                        else
                        {
                            cf = 7;
                            cx = (cx + currentMovePoint.xEnd) / 2;
                            cy = (cy + currentMovePoint.yEnd) / 2;
                        }
                    }
                else
                    processNextPoint = true;
                if (processNextPoint && vMovePoints.size() > 0)
                {
                    currentMovePoint = (MovePoint) vMovePoints.firstElement();
                    vMovePoints.removeElementAt(0);
                    if (currentMovePoint.status == A_RUN)
                    {
                        statusMe = A_RUN;
                        if (cx - currentMovePoint.xEnd > 0)
                            cdir = -1;
                        else if (cx - currentMovePoint.xEnd < 0)
                            cdir = 1;
                        cvx = 5 * cdir;
                        cvy = 0;
                    }
                    else if (currentMovePoint.status == A_JUMP)
                    {
                        statusMe = A_JUMP;
                        GameCanvas.gI().startDust(-1, cx - (-1 << 3), cy);
                        GameCanvas.gI().startDust(1, cx - (1 << 3), cy);
                        if (cx - currentMovePoint.xEnd > 0)
                            cdir = -1;
                        else if (cx - currentMovePoint.xEnd < 0)
                            cdir = 1;
                        cvx = abs(cx - currentMovePoint.xEnd) / 9 * cdir;
                        cvy = -10;
                    }
                    else if (currentMovePoint.status == A_FALL)
                    {
                        statusMe = A_FALL;
                        if (cx - currentMovePoint.xEnd > 0)
                            cdir = -1;
                        else if (cx - currentMovePoint.xEnd < 0)
                            cdir = 1;
                        cvx = abs(cx - currentMovePoint.xEnd) / 9 * cdir;
                        cvy = 0;
                    }
                    else
                    {
                        cx = currentMovePoint.xEnd;
                        cy = currentMovePoint.yEnd;
                        currentMovePoint = null;
                    }
                }
                if (statusMe == A_NOTHING)
                {
                    if (cf >= 8 && cf <= 11)
                    {
                        cf++;
                        cp1++;
                        if (cf > 11)
                            cf = 8;
                        if (cp1 > 5)
                            cf = 0;
                    }
                    if (cf <= 1)
                    {
                        cp1++;
                        if (cp1 > 6)
                            cf = 0;
                        else
                            cf = 1;
                        if (cp1 > 10)
                            cp1 = 0;
                    }
                }
                lcx = cx;
                lcy = cy;
            }
        }


        if (isInjure > 0)
        {
            cf = 21;
            isInjure--;
        }
        else
        {
            switch (statusMe)
            {
                case A_STAND:
                    updateCharStand();
                    break;
                case A_RUN:
                    if (me)
                        if (GameCanvas.gameTick % 33 == 0)
                        {
                            Music.play(Music.RUN, 12);
                            isStartSoundRun = true;
                        }
                    updateCharRun();
                    break;
                case A_JUMP:
                    updateCharJump();
                    break;
                case A_FALL:
                    updateCharFall();
                    break;
                case A_DEADFLY:
                    updateCharDeadFly();
                    break;
                case A_AUTOJUMP:
                    updateCharAutoJump();
                    break;
                case A_WATERRUN:
                    break;
                case SKILL_STAND:
                    updateSkillStand();
                    break;
                case SKILL_FALL:
                    break;
                case A_DEAD:

                    break;
                case A_NOTHING:
                    if (cf == 21 && isInjure <= 0)
                        cf = 0;
                    break;
                case A_INJURE:
                    if (cf == 21 && isInjure <= 0)
                        cf = 0;
                    break;
            }
        }
        if (wdx != 0 || wdy != 0)
        {
            startDie(wdx, wdy);
            wdx = 0;
            wdy = 0;
        }
        if (moveFast != null)
            if (moveFast[0] == 0)
            {
                moveFast[0]++;
            }
            else if (moveFast[0] < 10)
            {
                moveFast[0]++;
            }
            else
            {
                cx = moveFast[1];
                cy = moveFast[2];
                moveFast = null;
                if (me)
                    if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) != TileMap.T_TOP)
                        statusMe = A_FALL;
                    else
                        Service.getInstance().charMove();
            }
        if (!me && vMovePoints.size() == 0 && cxMoveLast != 0 && cyMoveLast != 0 && currentMovePoint == null)
        {
            if (cxMoveLast != cx)
                cx = cxMoveLast;
            if (cyMoveLast != cy)
                cy = cyMoveLast;
            if (cHP > 0)
                statusMe = A_NOTHING;
        }


        if (me && statusMe != A_DEAD)
            Service.getInstance().charMove();
    }

    public void updateShadown()
    {
        var wCount = 0;
        xSd = cx;
        if (TileMap.tileTypeAt(cx, cy, TileMap.T_TOP))
        {
            cybong = cy;
        }
        else
        {
            cybong = cy;
            while (wCount < 30)
            {
                wCount++;
                cybong += 24;

                if (TileMap.tileTypeAt(xSd, cybong, TileMap.T_TOP))
                {
                    if (cybong % 24 != 0)
                        cybong -= cybong % 24;
                    break;
                }
            }
        }
    }

    private void autoPickItemMap()
    {
        if (!me || cHP <= 0 || statusMe == A_DEAD || statusMe == A_DEADFLY || testCharId != -9999)
            return;

        int rangePick = 30, dxx, dyy;
        if (isAPickYen || isAPickYHM || isAPickYHMS)
            for (var i = 0; i < GameScr.vItemMap.size(); i++)
            {
                var itemMap = (ItemMap) GameScr.vItemMap.elementAt(i);
                if (itemMap == null)
                    continue;
                dxx = src.lib.Math.abs(myChar().cx - itemMap.x);
                dyy = src.lib.Math.abs(myChar().cy - itemMap.y);
            }
    }

    private void updateSkillPaint()
    {
        if (GameCanvas.gameTick % 2 == 0 && skillPaint != null)
            indexSkill++;

        if (skillPaint != null && indexSkill >= skillInfoPaint().Length)
        {
            if (!me)
                if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP)
                    changeStatusStand();
                else
                    statusMe = A_NOTHING;
            indexSkill = -1;
            skillPaint = null;
            eff0 = eff1 = eff2 = null;
            i0 = i1 = i2 = 0;
        }
        // Gui message tan cong hoac buff khi trinh dien nua chung
    }

    private void updateCharDeadFly()
    {
        cp1++;
        cx += (cp2 - cx) / 4;
        if (cp1 > 7)
            cy += (cp3 - cy) / 4;
        else
            cy += cp1 - 10;
        if (CRes.abs(cp2 - cx) < 4 && CRes.abs(cp3 - cy) < 10)
        {
            cx = cp2;
            cy = cp3;
            statusMe = A_DEAD;
            callEff(60);
            if (me)
                GameScr.gI().resetButton();
        }
        cf = 21;
    }

    public void setAttk()
    {
        cp1++;
        if (cdir == 1)
        {
            if ((TileMap.tileTypeAtPixel(cx + chw, cy - chh) & TileMap.T_LEFT) == TileMap.T_LEFT)
                cvx = 0;
        }
        else
        {
            if ((TileMap.tileTypeAtPixel(cx - chw, cy - chh) & TileMap.T_RIGHT) == TileMap.T_RIGHT)
                cvx = 0;
        }
        //
        if (cy > ch && TileMap.tileTypeAt(cx, cy - ch, TileMap.T_BOTTOM))
            if (!TileMap.tileTypeAt(cx, cy, TileMap.T_TOP))
            {
                statusMe = A_FALL;
                cp1 = 0;
                cp2 = 0;
                cvy = 1;
            }
            else
            {
                cy = TileMap.tileYofPixel(cy);
            }
        //

        cx += cvx;
        cy += cvy;
        if (cy < 0)
            cy = cvy = 0;
        if (cvy == 0)
        {
            // Stand + Attack
            if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) != TileMap.T_TOP)
            {
                statusMe = A_FALL;

                cvx = (getSpeed() >> 1) * cdir;
                cp1 = cp2 = 0;
            }
        }
        else if (cvy < 0)
        {
            // Jump + Attack
            cvy++;
            if (cvy == 0)
                cvy = 1;
        }
        else
        {
            // Fall + Attack
            if (cvy < 20 && cp1 % 5 == 0)
                cvy++;
            if (cvy > 3)
                cvy = 3;
            if ((TileMap.tileTypeAtPixel(cx, cy + 3) & TileMap.T_TOP) == TileMap.T_TOP &&
                cy <= TileMap.tileXofPixel(cy + 3))
            {
                cvx = cvy = 0;
                cy = TileMap.tileXofPixel(cy + 3);
            }
            if (TileMap.tileTypeAt(cx, cy, TileMap.T_WATERFLOW) && cy % TileMap.size > 8)
            {
                statusMe = A_WATERRUN;
                cvx = cdir << 1;
                cvy = cvy >> 2;
                cy = TileMap.tileYofPixel(cy) + 12;
                statusMe = A_WATERDOWN;
                return;
            }
            if (TileMap.tileTypeAt(cx, cy, TileMap.T_UNDERWATER))
            {
                statusMe = A_WATERDOWN;
                return;
            }
        }
        if (cvx > 0)
            cvx--;
        else if (cvx < 0)
            cvx++;
    }


    public void updateSkillStand()
    {
        setAttk();
    }

    public void updateCharAutoJump()
    {
        cx += cvx * cdir;
        cy += cvyJump;
        cvyJump++;
        if (cp1 == 0)
            cf = 7;
        else
            cf = 23;
        if (canJumpHigh)
            if (cvyJump == -3)
                cf = 8;
            else if (cvyJump == -2)
                cf = 9;
            else if (cvyJump == -1)
                cf = 10;
            else if (cvyJump == 0)
                cf = 11;

        if (cvyJump == 0)
        {
            statusMe = A_NOTHING;
            ((MovePoint) vMovePoints.firstElement()).status = A_FALL;
            cp1 = 0;
            cvy = 1;
        }
    }

    public void updateCharStand()
    {
        isAttack = false;
        isAttFly = false;
        cvx = 0;
        cvy = 0;
        cp1++;
        lcx = cx;
        lcy = cy;
        if (cp1 > 30)
            cp1 = 0;
        if (cp1 % 15 < 5)
            cf = 0;
        else
            cf = 1;
        if (me && !ischangingMap && isInWaypoint())
        {
            Service.getInstance().charMove();
            if (mSystem.currentTimeMillis() - (timeLastchangeMap + timedelay) > 0)
            {
                timeLastchangeMap = mSystem.currentTimeMillis();
                Service.getInstance().requestChangeMap();
            }
        }
    }

    public void updateCharRun()
    {
        sleepaddEffBui--;
        if (sleepaddEffBui <= 0)
        {
            ServerEffect.addServerEffect(26, cx, cy + 8, -1, (sbyte) cdir);
            sleepaddEffBui = 10;
        }


        var spaceBefore = 0;
        if (!me)
            if (currentMovePoint != null)
                spaceBefore = abs(cx - currentMovePoint.xEnd);
        cp1++;

        if (cp1 >= 5)
        {
            cp1 = 0;
            cBonusSpeed = 0;
        }

        cf = (cp1 >> 1) + 2;

        if ((TileMap.tileTypeAtPixel(cx, cy - 1) & TileMap.T_WATERFLOW) == TileMap.T_WATERFLOW)
            cx += cvx >> 1;
        else
            cx += cvx;

        if (cdir == 1)
        {
            if (TileMap.tileTypeAt(cx + chw, cy - chh, TileMap.T_LEFT))
                if (me)
                {
                    cvx = 0;
                    cx = TileMap.tileXofPixel(cx + chw) - chw;
                }
                else
                {
                    stop();
                }
        }
        else
        {
            if (TileMap.tileTypeAt(cx - chw - 1, cy - chh, TileMap.T_RIGHT))
                if (me)
                {
                    cvx = 0;
                    cx = TileMap.tileXofPixel(cx - chw - 1) + TileMap.size + chw;
                }
                else
                {
                    stop();
                }
        }

        if (me)
        {
            if (cvx >= getSpeed() && CRes.abs(cvx) % 2 == 0) //auto move main char toi
                Service.getInstance().charMove();
            if (cvx > 0)
            {
                cvx--;
            }
            else if (cvx < 0)
            {
                cvx++;
            }
            else
            {
                changeStatusStand();
                cBonusSpeed = 0;
            }
        }

        if ((TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) != TileMap.T_TOP)
            if (me)
            {
                if (cx - cxSend != 0 || cy - cySend != 0)
                    Service.getInstance().charMove();
                cf = 7;
                statusMe = A_FALL;
                cvx = 3 * cdir;
                cp2 = 0;
            }
            else
            {
                stop();
            }
        //	
        if (me)
        {
        }
        else
        {
            if (currentMovePoint != null)
            {
                var spaceAfter = abs(cx - currentMovePoint.xEnd);
                if (spaceAfter > spaceBefore)
                    stop();
            }
        }

        if (me && !ischangingMap && isInWaypoint())
        {
            Service.getInstance().charMove();
            if (mSystem.currentTimeMillis() - (timeLastchangeMap + timedelay) > 0)
            {
                timeLastchangeMap = mSystem.currentTimeMillis();
                Service.getInstance().requestChangeMap();
            }
        }
    }

    private void stop()
    {
        statusMe = A_NOTHING;
        cvx = 0;
        cvy = 0;
        cp1 = cp2 = 0;
    }

    public static int abs(int i)
    {
        return i > 0 ? i : -i;
    }

    public void updateCharJump()
    {
        cx += cvx;
        cy += cvy;
        if (cy < 0)
        {
            cy = 0;
            cvy = -1;
        }
        cvy++;
        if (!me)
            if (currentMovePoint != null)
            {
                var d = currentMovePoint.xEnd - cx;
                if (d > 0)
                {
                    if (cvx > d)
                        cvx = d;
                    if (cvx < 0)
                        cvx = d;
                }
                else if (d < 0)
                {
                    if (cvx < d)
                        cvx = d;
                    if (cvx > 0)
                        cvx = d;
                }
                else
                {
                    cvx = d;
                }
            }
        if (cp1 == 0)
            cf = 8;
        else
            cf = 24;
        if (canJumpHigh)
            if (cvy == -3)
                cf = 9;
            else if (cvy == -2)
                cf = 10;
            else if (cvy == -1)
                cf = 11;
            else if (cvy == 0)
                cf = 12;
        if (cdir == 1)
        {
            if ((TileMap.tileTypeAtPixel(cx + chw, cy - 1) & TileMap.T_LEFT) == TileMap.T_LEFT &&
                cx <= TileMap.tileXofPixel(cx + chw) + 12)
            {
                cx = TileMap.tileXofPixel(cx + chw) - chw;
                cvx = 0;
            }
        }
        else
        {
            if ((TileMap.tileTypeAtPixel(cx - chw, cy - 1) & TileMap.T_RIGHT) == TileMap.T_RIGHT &&
                cx >= TileMap.tileXofPixel(cx - chw) + 12)
            {
                cx = TileMap.tileXofPixel(cx + 24 - chw) + chw;
                cvx = 0;
            }
        }
        if (cvy == 0)
            if (!isAttFly)
                if (me)
                    setCharFallFromJump();
                else
                    stop();
            else
                setCharFallFromJump();
        if (me && !ischangingMap && isInWaypoint())
        {
            Service.getInstance().charMove();
            Service.getInstance().requestChangeMap();
            return;
        }
        if (cp3 < 0)
            cp3++;
        if (cy > ch && TileMap.tileTypeAt(cx, cy - ch, TileMap.T_BOTTOM))
            if (me)
            {
                if (cx - cxSend != 0 || cy - cySend != 0)
                    Service.getInstance().charMove();
                statusMe = A_FALL;
                cp1 = 0;
                cp2 = 0;
                cvy = 1;
            }
            else
            {
                stop();
            }
        if (me)
        {
        }
        else
        {
            if (currentMovePoint != null)
                if (cy < currentMovePoint.yEnd)
                    stop();
        }
    }


    public void setCharFallFromJump()
    {
        cyStartFall = cy;
        statusMe = A_FALL;
        cp1 = 0;
        if (canJumpHigh)
            cp2 = 1; // Rotate in STrong Jump only
        else
            cp2 = 0;
        cvy = 1;
        updateCharFall();
        if (me)
            if (cx - cxSend != 0 || cy - cySend != 0)
                Service.getInstance().charMove();
    }

    public void updateCharFall()
    {
        if (cy + 4 >= TileMap.pxh)
        {
            changeStatusStand();
            cvx = cvy = 0;
            return;
        }
        if (cy % 24 == 0 && (TileMap.tileTypeAtPixel(cx, cy) & TileMap.T_TOP) == TileMap.T_TOP)
            if (me)
            {
                if (cy - cySend > 0)
                    Service.getInstance().charMove();
                else if (cx - cxSend != 0 || cy - cySend < 0)
                    Service.getInstance().charMove();
                cvx = cvy = 0;
                cp1 = cp2 = 0;


                changeStatusStand();
                return;
            }
            else
            {
                stop();
                cf = 0;
            }
        //
        cf = 13;
        cx += cvx;
        if (!me)
            if (currentMovePoint != null)
            {
                var d = currentMovePoint.xEnd - cx;
                if (d > 0)
                {
                    if (cvx > d)
                        cvx = d;
                    if (cvx < 0)
                        cvx = d;
                }
                else if (d < 0)
                {
                    if (cvx < d)
                        cvx = d;
                    if (cvx > 0)
                        cvx = d;
                }
                else
                {
                    cvx = d;
                }
            }

        cy += cvy;

        if (me && cvy % 8 == 0)
            Service.getInstance().charMove();
        //		Cout.println("--updatefall----cvy---  "+cvy);
        if (cvy < 20)
            cvy++;

        if (cdir == 1)
        {
            if ((TileMap.tileTypeAtPixel(cx + chw, cy - 1) & TileMap.T_LEFT) == TileMap.T_LEFT &&
                cx <= TileMap.tileXofPixel(cx + chw) + 12)
            {
                cx = TileMap.tileXofPixel(cx + chw) - chw;
                cvx = 0;
            }
        }
        else
        {
            if ((TileMap.tileTypeAtPixel(cx - chw, cy - 1) & TileMap.T_RIGHT) == TileMap.T_RIGHT &&
                cx >= TileMap.tileXofPixel(cx - chw) + 12)
            {
                cx = TileMap.tileXofPixel(cx + 24 - chw) + chw; //
                cvx = 0;
            }
        }

        if (cvy > 4)
            if ((cyStartFall == 0 || cyStartFall <= TileMap.tileYofPixel(cy + 3))
                && (TileMap.tileTypeAtPixel(cx, cy + 3) & TileMap.T_TOP) == TileMap.T_TOP)
            {
                if (me)
                {
                    cyStartFall = 0;
                    cvx = cvy = 0;
                    cp1 = cp2 = 0;
                    cy = TileMap.tileXofPixel(cy + 3);
                    changeStatusStand();
                    // =====================
                    if (cy - cySend > 0)
                    {
                        if (me)
                            Service.getInstance().charMove();
                    }
                    else if (cx - cxSend != 0 || cy - cySend < 0)
                    {
                        if (me)
                            Service.getInstance().charMove();
                    }

                    //					Cout.println("-----------------stand----22222222-----");
                }
                else
                {
                    stop();
                    cy = TileMap.tileXofPixel(cy + 3);
                    cf = 0;
                }
                return;
            }
        if (cp2 == 1) // That means fall rotate
        {
            if (cvy == 3)
                cf = 12;
            else if (cvy == 2)
                cf = 9;
            else if (cvy == 1)
                cf = 10;
            else if (cvy == 0)
                cf = 11;
        }
        else
        {
            cf = 13;
        }

        if (cvy > 6)
        {
            if (TileMap.tileTypeAt(cx, cy, TileMap.T_WATERFLOW) && cy % TileMap.size > 8)
            {
                cy = TileMap.tileYofPixel(cy) + 8;
                statusMe = A_WATERRUN;
                cvx = cdir << 1;
                cvy = cvy >> 2;
                cy = TileMap.tileYofPixel(cy) + 12;
                if (cx - cxSend != 0 || cy - cySend != 0)
                {
                    if (me)
                    {
                        Service.getInstance().charMove();
                    }
                }
            }
        }


        if (!me)
        {
            if ((TileMap.tileTypeAtPixel(cx, cy + 1) & TileMap.T_TOP) == TileMap.T_TOP)
                cf = 0;
            
            if (currentMovePoint != null)
                if (cy > currentMovePoint.yEnd)
                    stop();
        }
    }

    public void setSkillPaint(SkillPaint skillPaint, int sType)
    {
        setAutoSkillPaint(skillPaint, sType);
    }

    public void setAutoSkillPaint(SkillPaint skillPaint, int sType)
    {
        try
        {
            this.skillPaint = skillPaint;
            this.sType = sType;
            indexSkill = -1;
            i0 = i1 = i2 = dx0 = dx1 = dx2 = dy0 = dy1 = dy2 = 0;
            eff0 = null;
            eff1 = null;
            eff2 = null;
        }
        catch (Exception e)
        {
        }
    }

    private SkillInfoPaint[] skillInfoPaint()
    {
        if (skillPaint == null)
            return null;
        return sType == 0 ? skillPaint.skillStand : skillPaint.skillfly;
    }

    protected bool isPaint()
    {
        if (cx < GameScr.cmx)
            return false;
        if (cx > GameScr.cmx + GameScr.gW)
            return false;
        if (cy < GameScr.cmy)
            return false;
        
        return cy <= GameScr.cmy + GameScr.gH + 30;
    }

    private void paintShadow(mGraphics g)
    {
        int size = TileMap.size;
        if (TileMap.tileTypeAt(cx + size / 2, cybong + 1, TileMap.T_LEFT))
            g.setClip(xSd / size * size, (cybong - 30) / size * size, size,
                100);
        else if (TileMap
                     .tileTypeAt((cx - size / 2) / size, (cybong + 1) / size) == 0)
            g
                .setClip(cx / size * size, (cybong - 30) / size * size,
                    100, 100);
        else if (TileMap
                     .tileTypeAt((cx + size / 2) / size, (cybong + 1) / size) == 0)
            g.setClip(cx / size * size, (cybong - 30) / size * size, size,
                100);
        else if (TileMap.tileTypeAt(xSd - size / 2, cybong + 1, TileMap.T_RIGHT))
            g.setClip(cx / 24 * size, (cybong - 30) / size * size, size, 100);

        g.drawImage(LoadImageInterface.bongChar, cx,
            cybong + 15, mGraphics.VCENTER | mGraphics.HCENTER);
        g.setClip(GameScr.cmx, GameScr.cmy - GameCanvas.transY, GameScr.gW,
            GameScr.gH + 2 * GameCanvas.transY);
    }

    public virtual void paint(mGraphics g)
    {
        paintCharName_HP_MP_Overhead(g);

        if (typePk >= 0)

            g.drawRegion(LoadImageInterface.iconpk, 0, 12 * (typePk * 3 + GameCanvas.gameTick / 3 % 3), 12, 12, 0,
                cx,
                cy - 75, mGraphics.VCENTER | mGraphics.HCENTER);
        if (statusMe == A_DEAD || statusMe == A_DEADFLY)
        {
            g.drawImage(LoadImageInterface.chardie, cx - LoadImageInterface.chardie.getWidth() / 2,
                cy + 10 - LoadImageInterface.chardie.getHeight() + 4, mGraphics.TOP | mGraphics.LEFT);

            return;
        }
        paintShadow(g);

        if (!isPaint())
        {
            if (skillPaint != null)
            {
                indexSkill = skillInfoPaint().Length;
                skillPaint = null;
                effPaints = null;
                eff = null;
                effTask = null;
                indexEff = -1;
                indexEffTask = -1;
            }
            return;
        }

        if (skillPaint != null)
            paintCharWithSkill(g);
        else
            paintCharWithoutSkill(g);

        if (effPaints != null)
            for (var i = 0; i < effPaints.Length; i++)
                if (effPaints[i] != null)
                {
                    if (effPaints[i].eMob != null)
                    {
                        if (!effPaints[i].isFly)
                        {
                            effPaints[i].eMob.setInjure();
                            effPaints[i].eMob.injureBy = this;

                            effPaints[i].isFly = true;
                        }
                        SmallImage.drawSmallImage(g, effPaints[i].getImgId(), effPaints[i].eMob.x, effPaints[i].eMob.y,
                            0, mGraphics.BOTTOM | mGraphics.HCENTER);
                    }
                    else if (effPaints[i].eChar != null)
                    {
                        if (!effPaints[i].isFly)
                        {
                            if (effPaints[i].eChar.charID >= 0)
                                effPaints[i].eChar.doInjure();
                                effPaints[i].isFly = true;
                        }
                        SmallImage.drawSmallImage(g, effPaints[i].getImgId(), effPaints[i].eChar.cx,
                            effPaints[i].eChar.cy + 8, 0, mGraphics.BOTTOM | mGraphics.HCENTER);
                    }
                    effPaints[i].index++;
                    if (effPaints[i].index >= effPaints[i].effCharPaint.arrEfInfo.Length)
                        effPaints[i] = null;
                }
        if (indexEff >= 0 && eff != null)
        {
            SmallImage.drawSmallImage(g, eff.arrEfInfo[indexEff].idImg, cx + eff.arrEfInfo[indexEff].dx,
                cy + 8 + eff.arrEfInfo[indexEff].dy, 0, mGraphics.VCENTER | mGraphics.HCENTER);
            if (GameCanvas.gameTick % 2 == 0)
            {
                indexEff++;
                if (indexEff >= eff.arrEfInfo.Length)
                {
                    indexEff = -1;
                    eff = null;
                }
            }
        }


        if (indexEffTask >= 0 && effTask != null)
        {
            SmallImage.drawSmallImage(g, effTask.arrEfInfo[indexEffTask].idImg, cx + effTask.arrEfInfo[indexEffTask].dx,
                cy + 8 + effTask.arrEfInfo[indexEffTask].dy, 0, mGraphics.VCENTER | mGraphics.HCENTER);
            if (GameCanvas.gameTick % 2 == 0)
            {
                indexEffTask++;
                if (indexEffTask >= effTask.arrEfInfo.Length)
                {
                    indexEffTask = -1;
                    effTask = null;
                }
            }
        }


        if (typePk >= 0)

            g.drawRegion(LoadImageInterface.iconpk, 0, 12 * (typePk * 3 + GameCanvas.gameTick / 3 % 3), 12, 12, 0,
                cx,
                cy - 75, mGraphics.VCENTER | mGraphics.HCENTER);
    }


    //Pain charname by country Huy (18/07/2017)
    private void paintCharName_HP_MP_Overhead(mGraphics g)
    {
        var height = ch + 5;
        if (myChar().cCountry == myChar().cCountryHoa)
            mFont.tahoma_7_red_charname.drawStringShadown(g, cName, cx, cy - height - 15, mFont.CENTER);
        else if (myChar().cCountry == myChar().cCountryThuy)
            mFont.tahoma_7_blue_charname.drawStringShadown(g, cName, cx, cy - height - 15, mFont.CENTER);
        else if (myChar().cCountry == myChar().cCountryLoi)
            mFont.tahoma_7_yellow_charname.drawStringShadown(g, cName, cx, cy - height - 15, mFont.CENTER);
        else if (myChar().cCountry == myChar().cCountryTho)
            mFont.tahoma_7_brown_charname.drawStringShadown(g, cName, cx, cy - height - 15, mFont.CENTER);
        else if (myChar().cCountry == myChar().cCountryPhong)
            mFont.tahoma_7_green_charname.drawStringShadown(g, cName, cx, cy - height - 15, mFont.CENTER);
    }

    private void paintCharWithoutSkill(mGraphics g)
    {
        try
        {
            if (ischangingMap)
                return;


            Part ph = GameScr.parts[head], pl = GameScr.parts[leg], pb = GameScr.parts[body];
            cxtemp = cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx;


            if (cdir == 1)
            {
                if (statusMe != A_RUN)
                {
                    SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                        cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx,
                        cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                    SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                        cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx,
                        cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                    SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                        cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx,
                        cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                }
                else
                {
                    if (cf != 7)
                    {
                        //						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx + CharInfo[cf][3][1] + pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                            cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx,
                            cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                            cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx,
                            cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                            cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx,
                            cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                    }
                    else
                    {
                        //						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx + CharInfo[cf][3][1] + pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id + 1,
                            cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx,
                            cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id + 1,
                            cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx,
                            cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                            cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx,
                            cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                    }
                }
            }
            else
            {
                if (statusMe != A_RUN)
                {
                    //				SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx - CharInfo[cf][3][1] - pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 2, 24);
                    SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                        cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx,
                        cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                    SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                        cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx,
                        cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                    SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                        cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx,
                        cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                }
                else
                {
                    if (cf != 7)
                    {
                        //						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx - CharInfo[cf][3][1] - pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                            cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx,
                            cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                            cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx,
                            cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                            cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx,
                            cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                    }
                    else
                    {
                        //						SmallImage.drawSmallImage(g, pW.pi[CharInfo[cf][3][0]].id, cx - CharInfo[cf][3][1] - pW.pi[CharInfo[cf][3][0]].dx, cy - CharInfo[cf][3][2] + pW.pi[CharInfo[cf][3][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id + 1,
                            cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx,
                            cy + 8 - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id + 1,
                            cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx,
                            cy + 8 - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                            cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx,
                            cy + 8 - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                    }
                }
            }
            if (GameCanvas.gameTick % 3 != 0)
                g.drawImage(
                    LoadImageInterface.list_thunder4[
                        GameCanvas.gameTick / 2 % LoadImageInterface.list_thunder4.Length], cx, cy,
                    mGraphics.VCENTER | mGraphics.HCENTER);
        }
        catch (Exception e)
        {
        }
    }

    private void paintCharWithoutSkill(mGraphics g, int cx, int cy)
    {
        try
        {
            if (ischangingMap)
                return;
            Part ph = GameScr.parts[head], pl = GameScr.parts[leg], pb = GameScr.parts[body];
            cxtemp = cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx;

            if (cdir == 1)
            {
                if (statusMe != A_RUN)
                {
                    SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                        cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx,
                        cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                    SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                        cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx,
                        cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                    SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                        cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx,
                        cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                }
                else
                {
                    if (cf != 7)
                    {
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                            cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx,
                            cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                            cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx,
                            cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                            cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx,
                            cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                    }
                    else
                    {
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id + 1,
                            cx + CharInfo[cf][1][1] + pl.pi[CharInfo[cf][1][0]].dx,
                            cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id + 1,
                            cx + CharInfo[cf][2][1] + pb.pi[CharInfo[cf][2][0]].dx,
                            cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 0, 0);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                            cx + CharInfo[cf][0][1] + ph.pi[CharInfo[cf][0][0]].dx,
                            cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 0, 0);
                    }
                }
            }
            else
            {
                if (statusMe != A_RUN)
                {
                    SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                        cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx,
                        cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                    SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                        cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx,
                        cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                    SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                        cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx,
                        cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                }
                else
                {
                    if (cf != 7)
                    {
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id,
                            cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx,
                            cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id,
                            cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx,
                            cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                            cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx,
                            cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                    }
                    else
                    {
                        SmallImage.drawSmallImage(g, pl.pi[CharInfo[cf][1][0]].id + 1,
                            cx - CharInfo[cf][1][1] - pl.pi[CharInfo[cf][1][0]].dx,
                            cy - CharInfo[cf][1][2] + pl.pi[CharInfo[cf][1][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, pb.pi[CharInfo[cf][2][0]].id + 1,
                            cx - CharInfo[cf][2][1] - pb.pi[CharInfo[cf][2][0]].dx,
                            cy - CharInfo[cf][2][2] + pb.pi[CharInfo[cf][2][0]].dy, 2, 24);
                        SmallImage.drawSmallImage(g, ph.pi[CharInfo[cf][0][0]].id,
                            cx - CharInfo[cf][0][1] - ph.pi[CharInfo[cf][0][0]].dx,
                            cy - CharInfo[cf][0][2] + ph.pi[CharInfo[cf][0][0]].dy, 2, 24);
                    }
                }
            }
        }
        catch (Exception e)
        {
        }
    }

    public void paintCharWithSkill(mGraphics g)
    {
        var line = 0;
        try
        {
            var skillInfoPaintt = skillInfoPaint();
            cf = skillInfoPaintt[indexSkill].status;
            if (skillInfoPaintt[indexSkill].effS0Id != 0)
            {
                eff0 = GameScr.efs[skillInfoPaintt[indexSkill].effS0Id - 1];
                i0 = dx0 = dy0 = 0;
            }
            line = 1;
            if (skillInfoPaintt[indexSkill].effS1Id != 0)
            {
                eff1 = GameScr.efs[skillInfoPaintt[indexSkill].effS1Id - 1];
                i1 = dx1 = dy1 = 0;
            }
            if (skillInfoPaintt[indexSkill].effS2Id != 0)
            {
                eff2 = GameScr.efs[skillInfoPaintt[indexSkill].effS2Id - 1];
                i2 = dx2 = dy2 = 0;
            }
            var sp = skillInfoPaintt;


            paintCharWithoutSkill(g);
            line = 1;
            if (cdir == 1)
            {
                if (eff0 != null)
                {
                    if (dx0 == 0)
                        dx0 = skillInfoPaintt[indexSkill].e0dx;
                    if (dy0 == 0)
                        dy0 = skillInfoPaintt[indexSkill].e0dy;
                    SmallImage.drawSmallImage(g, eff0.arrEfInfo[i0].idImg, cx + dx0 + eff0.arrEfInfo[i0].dx,
                        cy + 8 + dy0 + eff0.arrEfInfo[i0].dy, 0,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                    i0++;
                    if (i0 >= eff0.arrEfInfo.Length)
                    {
                        eff0 = null;
                        i0 = dx0 = dy0 = 0;
                    }
                }
                if (eff1 != null)
                {
                    if (dx1 == 0)
                        dx1 = skillInfoPaintt[indexSkill].e1dx;
                    if (dy1 == 0)
                        dy1 = skillInfoPaintt[indexSkill].e1dy;
                    SmallImage.drawSmallImage(g, eff1.arrEfInfo[i1].idImg, cx + dx1 + eff1.arrEfInfo[i1].dx,
                        cy + 8 + dy1 + eff1.arrEfInfo[i1].dy, 0,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                    i1++;
                    if (i1 >= eff1.arrEfInfo.Length)
                    {
                        eff1 = null;
                        i1 = dx1 = dy1 = 0;
                    }
                }
                if (eff2 != null)
                {
                    if (dx2 == 0)
                        dx2 = skillInfoPaintt[indexSkill].e2dx;
                    if (dy2 == 0)
                        dy2 = skillInfoPaintt[indexSkill].e2dy;
                    SmallImage.drawSmallImage(g, eff2.arrEfInfo[i2].idImg, cx + dx2 + eff2.arrEfInfo[i2].dx,
                        cy + 8 + dy2 + eff2.arrEfInfo[i2].dy, 0,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                    i2++;
                    if (eff2.arrEfInfo != null)
                        if (i2 >= eff2.arrEfInfo.Length)
                        {
                            eff2 = null;
                            i2 = dx2 = dy2 = 0;
                        }
                }
            }
            else
            {
                if (eff0 != null)
                {
                    if (dx0 == 0)
                        dx0 = skillInfoPaintt[indexSkill].e0dx;
                    if (dy0 == 0)
                        dy0 = skillInfoPaintt[indexSkill].e0dy;
                    SmallImage.drawSmallImage(g, eff0.arrEfInfo[i0].idImg, cx - dx0 - eff0.arrEfInfo[i0].dx,
                        cy + 8 + dy0 + eff0.arrEfInfo[i0].dy, 2,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                    i0++;
                    if (i0 >= eff0.arrEfInfo.Length)
                    {
                        eff0 = null;
                        i0 = 0;
                        dx0 = 0;
                        dy0 = 0;
                    }
                }
                if (eff1 != null)
                {
                    if (dx1 == 0)
                        dx1 = skillInfoPaintt[indexSkill].e1dx;
                    if (dy1 == 0)
                        dy1 = skillInfoPaintt[indexSkill].e1dy;
                    SmallImage.drawSmallImage(g, eff1.arrEfInfo[i1].idImg, cx - dx1 - eff1.arrEfInfo[i1].dx,
                        cy + 8 + dy1 + eff1.arrEfInfo[i1].dy, 2,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                    i1++;
                    if (i1 >= eff1.arrEfInfo.Length)
                    {
                        eff1 = null;
                        i1 = 0;
                        dx1 = 0;
                        dy1 = 0;
                    }
                }
                if (eff2 != null)
                {
                    if (dx2 == 0)
                        dx2 = skillInfoPaintt[indexSkill].e2dx;
                    if (dy2 == 0)
                        dy2 = skillInfoPaintt[indexSkill].e2dy;
                    SmallImage.drawSmallImage(g, eff2.arrEfInfo[i2].idImg, cx - dx2 - eff2.arrEfInfo[i2].dx,
                        cy + 8 + dy2 + eff2.arrEfInfo[i2].dy, 2,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                    i2++;
                    if (eff2.arrEfInfo != null)
                        if (i2 >= eff2.arrEfInfo.Length)
                        {
                            eff2 = null;
                            i2 = 0;
                            dx2 = 0;
                            dy2 = 0;
                        }
                }
            }
        }
        catch (Exception e)
        {
        }
    }

    public void callEff(int effId)
    {
        indexEff = 0;
        eff = GameScr.efs[effId];
    }

    public static int getIndexChar(int ID)
    {
        for (var i = 0; i < GameScr.vCharInMap.size(); i++)
        {
            var c = (Char) GameScr.vCharInMap.elementAt(i);
            if (c.charID == ID)
                return i;
        }
        return -1;
    }

    public void moveTo(int toX, int toY)
    {
        //		Cout.println(cy+" moveto  "+toY);
        if (CRes.abs(toX - cx) > 200 || CRes.abs(toY - cy) > 200)
        {
            cx = toX;
            cy = toY;
            vMovePoints.removeAllElements();
            statusMe = A_NOTHING;
            return;
        }

        int dirX = 0, cact = 0;
        var dx = toX - cx;
        var dy = toY - cy;
        if (dx == 0 && dy == 0)
        {
            cact = A_STAND;
        }
        else if (dy == 0)
        {
            cact = A_RUN;

            if (vMovePoints.size() > 0)
            {
                MovePoint mp = null;
                try
                {
                    mp = (MovePoint) vMovePoints.lastElement();
                }
                catch (Exception e)
                {
                }
                if (mp != null && TileMap.tileTypeAt(mp.xEnd, mp.yEnd, TileMap.T_WATERFLOW) &&
                    mp.yEnd % TileMap.size > 8)
                    cact = A_WATERRUN;
            }
            if (dx > 0)
                dirX = 1;

            if (dx < 0)
                dirX = -1;
        }
        else if (dy != 0)
        {
            if (dy < 0)
                cact = A_JUMP;
            if (dy > 0)
                cact = A_FALL;

            if (dx < 0)
                dirX = -1;
            if (dx > 0)
                dirX = 1;
        }
        int x = 0, y = 0;
        x = cx + dx;
        y = cy + dy;
        vMovePoints.addElement(new MovePoint(x, y, cact, dirX));
    }

    public void searchFocus()
    {
        var deltaH = 0;

        int[] d = {-1, -1, -1, -1};
        var minx = myChar().cx - myChar().getdxSkill();
        var maxx = myChar().cx + myChar().getdxSkill();
        var miny = GameScr.cmy - 15;
        var maxy = GameScr.cmy + GameCanvas.h - GameScr.cmdBarH + 100 - 15;
        if (isManualFocus)
            if (mobFocus != null &&
                minx <= mobFocus.x
                && mobFocus.x <= maxx && miny <= mobFocus.y && mobFocus.y <= maxy
            )
                return;
            else
                isManualFocus = false;
        if (itemFocus == null)
        {
            for (var i = 0; i < GameScr.vItemMap.size(); i++)
            {
                var itemMap = (ItemMap) GameScr.vItemMap.elementAt(i);
                var dxx = src.lib.Math.abs(myChar().cx - itemMap.x);
                var dyy = src.lib.Math.abs(myChar().cy - itemMap.y);
                var dd = dxx > dyy ? dxx : dyy;
                if (dxx <= 50 && dyy <= 80)
                    if (itemFocus == null || dd < d[3])
                    {
                        itemFocus = itemMap;
                        d[3] = dd;
                    }
            }
        }
        else if (minx > itemFocus.x || itemFocus.x > maxx || miny > itemFocus.y || itemFocus.y > maxy)
        {
            itemFocus = null;
            for (var i = 0; i < GameScr.vItemMap.size(); i++)
            {
                var itemMap = (ItemMap) GameScr.vItemMap.elementAt(i);
                var dxx = src.lib.Math.abs(myChar().cx - itemMap.x);
                var dyy = src.lib.Math.abs(myChar().cy - itemMap.y);
                var dd = dxx > dyy ? dxx : dyy;
                if (minx <= itemMap.x && itemMap.x <= maxx && miny <= itemMap.y && itemMap.y <= maxy)
                    if (itemFocus == null || dd < d[3])
                    {
                        itemFocus = itemMap;
                        d[3] = dd;
                    }
            }
        }
        else
        {
            clearFocus(3);
            return;
        }

        // ------mob auto focus
        minx = myChar().cx - myChar().getdxSkill();
        maxx = myChar().cx + myChar().getdxSkill();
        miny = myChar().cy - myChar().getdySkill() - 80;
        maxy = myChar().cy + myChar().getdySkill() + 60;

        if (maxy > myChar().cy + 30)
            maxy = myChar().cy + 30;

        if (mobFocus == null)
        {
            for (var i = 0; i < GameScr.vMob.size(); i++)
            {
                var mob = (Mob) GameScr.vMob.elementAt(i);
                var dxx = src.lib.Math.abs(myChar().cx - mob.x);
                var dyy = src.lib.Math.abs(myChar().cy - mob.y);
                var dd = dxx > dyy ? dxx : dyy;

                if (minx <= mob.x && mob.x <= maxx && miny <= mob.y && mob.y <= maxy && mob.status != Mob.MA_INHELL
                    && mob.status != Mob.MA_DEADFLY)
                    if (mobFocus == null || dd < d[0])
                    {
                        if (mob.isBoss && !Mob.isBossAppear) continue;
                        mobFocus = mob;
                        if (!mob.isGetInfo)
                        {
                            mob.isGetInfo = true;
                            Service.getInstance().requetsInfoMod(mob.mobId);
                        }
                        d[0] = dd;
                    }
            }
        }
        else if (minx > mobFocus.x || mobFocus.x > maxx
                 || miny > mobFocus.y || mobFocus.y > maxy)
        {
            mobFocus = null;
            for (var i = 0; i < GameScr.vMob.size(); i++)
            {
                var mob = (Mob) GameScr.vMob.elementAt(i);
                var dxx = src.lib.Math.abs(myChar().cx - mob.x);
                var dyy = src.lib.Math.abs(myChar().cy - mob.y);
                var dd = dxx > dyy ? dxx : dyy;

                if (minx <= mob.x && mob.x <= maxx && miny <= mob.y && mob.y <= maxy && mob.status != Mob.MA_INHELL
                    && mob.status != Mob.MA_DEADFLY)
                    if (mobFocus == null || dd < d[0])
                    {
                        if (mob.isBoss && !Mob.isBossAppear) continue;
                        if (!mob.isGetInfo)
                        {
                            mob.isGetInfo = true;
                            Service.getInstance().requetsInfoMod(mob.mobId);
                        }
                        mobFocus = mob;
                        d[0] = dd;
                    }
            }
        }
        else
        {
            clearFocus(0);
            return;
        }
        minx = myChar().cx - 50;
        maxx = myChar().cx + 50;
        miny = myChar().cy - 30;
        maxy = myChar().cy + 30;
        if (npcFocus != null && npcFocus.template.npcTemplateId == 13)
        {
            minx = myChar().cx - 20;
            maxx = myChar().cx + 20;
            miny = myChar().cy - 10;
            maxy = myChar().cy + 10;
        }
        if (npcFocus == null)
        {
            for (var i = 0; i < GameScr.vNpc.size(); i++)
            {
                var npc = (Npc) GameScr.vNpc.elementAt(i);
                if (npc.statusMe == A_HIDE)
                    continue;
                var dxx = src.lib.Math.abs(myChar().cx - npc.cx);
                var dyy = src.lib.Math.abs(myChar().cy - npc.cy);
                var dd = dxx > dyy ? dxx : dyy;
                minx = myChar().cx - 80;
                maxx = myChar().cx + 80;
                miny = myChar().cy - 30;
                maxy = myChar().cy + 30;
                if (npc.template != null && npc.template.npcTemplateId == 13)
                {
                    minx = myChar().cx - 20;
                    maxx = myChar().cx + 20;
                    miny = myChar().cy - 10;
                    maxy = myChar().cy + 10;
                }
                if (minx <= npc.cx && npc.cx <= maxx && miny <= npc.cy && npc.cy <= maxy)
                    if (npcFocus == null || dd < d[1])
                    {
                        npcFocus = npc;
                        d[1] = dd;
                    }
            }
        }
        else if (minx > npcFocus.cx || npcFocus.cx > maxx || miny > npcFocus.cy || npcFocus.cy > maxy)
        {
            deFocusNPC();
            for (var i = 0; i < GameScr.vNpc.size(); i++)
            {
                var npc = (Npc) GameScr.vNpc.elementAt(i);
                if (npc.statusMe == A_HIDE)
                    continue;
                var dxx = src.lib.Math.abs(myChar().cx - npc.cx);
                var dyy = src.lib.Math.abs(myChar().cy - npc.cy);
                var dd = dxx > dyy ? dxx : dyy;
                minx = myChar().cx - 80;
                maxx = myChar().cx + 80;
                miny = myChar().cy - 30;
                maxy = myChar().cy + 30;
                if (npc.template.npcTemplateId == 13)
                {
                    minx = myChar().cx - 20;
                    maxx = myChar().cx + 20;
                    miny = myChar().cy - 10;
                    maxy = myChar().cy + 10;
                }
                if (minx <= npc.cx && npc.cx <= maxx && miny <= npc.cy && npc.cy <= maxy)
                    if (npcFocus == null || dd < d[1])
                    {
                        npcFocus = npc;
                        d[1] = dd;
                    }
            }
        }
        else
        {
            clearFocus(1);
            return;
        }
        //
        if (charFocus == null)
        {
            for (var i = 0; i < GameScr.vCharInMap.size(); i++)
            {
                var c = (Char) GameScr.vCharInMap.elementAt(i);
                if (c.Equals(myChar())) continue;
                // if (TileMap.typeMap != TileMap.MAP_DAUTRUONG) {
                if (c.statusMe == A_HIDE || c.isInvisible)
                    continue;
                if (c.charID >= -1)
                    continue;
                if (wdx != 0 || wdy != 0 || c.statusMe == A_DEAD || c.statusMe == A_DEADFLY)
                    continue;
                var dxx = src.lib.Math.abs(myChar().cx - c.cx);
                var dyy = src.lib.Math.abs(myChar().cy - c.cy);
                var dd = dxx > dyy ? dxx : dyy;
                if (minx <= c.cx && c.cx <= maxx && miny <= c.cy && c.cy <= maxy && !c.Equals(myChar()))
                {
                    if (charFocus == null)
                        Service.getInstance().requestPlayerInfo((short) c.charID);
                    if (charFocus == null || dd < d[2])
                    {
                        charFocus = c;
                        d[2] = dd;
                    }
                }
            }
        }
        else if (minx > charFocus.cx || charFocus.cx > maxx || miny > charFocus.cy || charFocus.cy > maxy ||
                 charFocus.statusMe == A_HIDE
                 || charFocus.isInvisible)
        {
            charFocus = null;
            for (var i = 0; i < GameScr.vCharInMap.size(); i++)
            {
                var c = (Char) GameScr.vCharInMap.elementAt(i);
                if (c.Equals(myChar())) continue;
                if (c.statusMe == A_HIDE || c.isInvisible)
                    continue;
                if (c.charID >= 0)
                    continue;
                if (wdx != 0 || wdy != 0 || c.statusMe == A_DEAD || c.statusMe == A_DEADFLY)
                    continue;
                var dxx = src.lib.Math.abs(myChar().cx - c.cx);
                var dyy = src.lib.Math.abs(myChar().cy - c.cy);
                var dd = dxx > dyy ? dxx : dyy;
                if (minx <= c.cx && c.cx <= maxx && miny <= c.cy && c.cy <= maxy)
                {
                    if (charFocus == null)
                        Service.getInstance().requestPlayerInfo((short) c.charID);
                    if (charFocus == null || dd < d[2])
                    {
                        charFocus = c;
                        d[2] = dd;
                    }
                }
            }
        }
        else
        {
            clearFocus(2);
            return;
        }
        var index = -1;

        for (var i = 0; i < d.Length; i++)
            if (index == -1)
            {
                if (d[i] != -1)
                    index = i;
            }
            else if (d[i] < d[index] && d[i] != -1)
            {
                index = i;
            }
        clearFocus(index);
        //		}
    }

    public void clearAllFocus()
    {
        charFocus = null;
        itemFocus = null;
        mobFocus = null;
        npcFocus = null;
    }

    public void clearFocus(int index)
    {
        if (index == 0)
        {
            deFocusNPC();
            charFocus = null;
            itemFocus = null;
        }
        else if (index == 1)
        {
            mobFocus = null;
            charFocus = null;
            itemFocus = null;
        }
        else if (index == 2)
        {
            mobFocus = null;
            deFocusNPC();
            itemFocus = null;
        }
        else if (index == 3)
        {
            mobFocus = null;
            deFocusNPC();
            charFocus = null;
        }
    }

    public static bool isCharInScreen(Char c)
    {
        var minx = GameScr.cmx;
        var maxx = GameScr.cmx + GameCanvas.w;
        var miny = GameScr.cmy + 10;
        var maxy = GameScr.cmy + GameScr.gH;
        if (c.statusMe != A_HIDE && !c.isInvisible && minx <= c.cx
            && c.cx <= maxx && miny <= c.cy && c.cy <= maxy)
            return true;
        return false;
    }

    public void deFocusNPC()
    {
        if (me && npcFocus != null)
        {
            npcFocus.chatPopup = null;
            npcFocus = null;
        }
    }


    public void doInjure(int HPShow, int MPShow, bool isBoss, int idBoss)
    {
        isInjure = 4;
        cf = 21;
    }

    public void doInjure()
    {
        isInjure = 4;
        callEff(49);
    }

    public void startDie(short toX, short toY)
    {
        if (me)
        {
            isLockKey = true;
            for (var i = 0; i < GameScr.vCharInMap.size(); i++)
            {
                var c = (Char) GameScr.vCharInMap.elementAt(i);
                c.killCharId = -9999;
            }
        }
        statusMe = A_DEADFLY;
        cp2 = toX;
        cp3 = toY;
        cp1 = 0;
        cHP = 0;
        testCharId = -9999;
        killCharId = -9999;
    }

    public void changeStatusStand()
    {
        statusMe = A_STAND;
    }

    public void paintChar(mGraphics g, int x, int y)
    {
        try
        {
            var a = GameCanvas.isTouchControlLargeScreen ? -25 : 16;
            Part ph = GameScr.parts[GameScr.currentCharViewInfo.head],
                pl = GameScr.parts[GameScr.currentCharViewInfo.leg],
                pb = GameScr.parts[GameScr.currentCharViewInfo.body];

            SmallImage.drawSmallImage(g,
                pl.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][0]].id,
                x + CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][1] +
                pl.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][0]].dx,
                y + a - CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][2] +
                pl.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][1][0]].dy, 0, 0);
            SmallImage.drawSmallImage(g,
                pb.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][0]].id,
                x + CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][1] +
                pb.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][0]].dx,
                y + a - CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][2] +
                pb.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][2][0]].dy, 0, 0);
            SmallImage.drawSmallImage(g,
                ph.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][0]].id,
                x + CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][1] +
                ph.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][0]].dx,
                y + a - CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][2] +
                ph.pi[CharInfo[GameScr.currentCharViewInfo.cp1 % 15 < 5 ? 0 : 1][0][0]].dy, 0, 0);
        }
        catch (Exception e)
        {
        }
    }

    public void paintCharWithSkill(mGraphics g, int index, int xchar, int ychar)
    {
        var line = 0;
        try
        {
            var skillInfoPaintt = skillInfoPaint();
            if (skillInfoPaintt == null)
                return;
            cf = skillInfoPaintt[index].status;
            if (skillInfoPaintt[index].effS0Id != 0)
            {
                eff0 = GameScr.efs[skillInfoPaintt[index].effS0Id - 1];
                i0 = dx0 = dy0 = 0;
            }
            if (skillInfoPaintt[index].effS1Id != 0)
            {
                eff1 = GameScr.efs[skillInfoPaintt[index].effS1Id - 1];
                i1 = dx1 = dy1 = 0;
            }
            if (skillInfoPaintt[index].effS2Id != 0)
            {
                eff2 = GameScr.efs[skillInfoPaintt[index].effS2Id - 1];
                i2 = dx2 = dy2 = 0;
            }

            paintCharWithoutSkill(g, xchar, ychar);
            if (cdir == 1)
            {
                if (eff0 != null)
                {
                    if (dx0 == 0)
                        dx0 = skillInfoPaintt[index].e0dx;
                    if (dy0 == 0)
                        dy0 = skillInfoPaintt[index].e0dy;
                    SmallImage.drawSmallImage(g, eff0.arrEfInfo[i0].idImg, xchar + dx0 + eff0.arrEfInfo[i0].dx,
                        ychar + dy0 + eff0.arrEfInfo[i0].dy, 0,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                    i0++;
                    if (i0 >= eff0.arrEfInfo.Length)
                    {
                        eff0 = null;
                        i0 = dx0 = dy0 = 0;
                    }
                }
                if (eff1 != null)
                {
                    if (dx1 == 0)
                        dx1 = skillInfoPaintt[index].e1dx;
                    if (dy1 == 0)
                        dy1 = skillInfoPaintt[index].e1dy;
                    SmallImage.drawSmallImage(g, eff1.arrEfInfo[i1].idImg, xchar + dx1 + eff1.arrEfInfo[i1].dx,
                        ychar + dy1 + eff1.arrEfInfo[i1].dy, 0,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                    i1++;
                    if (i1 >= eff1.arrEfInfo.Length)
                    {
                        eff1 = null;
                        i1 = dx1 = dy1 = 0;
                    }
                }
                if (eff2 != null)
                {
                    if (dx2 == 0)
                        dx2 = skillInfoPaintt[index].e2dx;
                    if (dy2 == 0)
                        dy2 = skillInfoPaintt[index].e2dy;
                    SmallImage.drawSmallImage(g, eff2.arrEfInfo[i2].idImg, xchar + dx2 + eff2.arrEfInfo[i2].dx,
                        ychar + dy2 + eff2.arrEfInfo[i2].dy, 0,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                    i2++;
                    if (eff2.arrEfInfo != null)
                        if (i2 >= eff2.arrEfInfo.Length)
                        {
                            eff2 = null;
                            i2 = dx2 = dy2 = 0;
                        }
                }
            }
            else
            {
                if (eff0 != null)
                {
                    if (dx0 == 0)
                        dx0 = skillInfoPaintt[index].e0dx;
                    if (dy0 == 0)
                        dy0 = skillInfoPaintt[index].e0dy;
                    SmallImage.drawSmallImage(g, eff0.arrEfInfo[i0].idImg, xchar - dx0 - eff0.arrEfInfo[i0].dx,
                        ychar + dy0 + eff0.arrEfInfo[i0].dy, 2,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                    i0++;
                    if (i0 >= eff0.arrEfInfo.Length)
                    {
                        eff0 = null;
                        i0 = 0;
                        dx0 = 0;
                        dy0 = 0;
                    }
                }
                if (eff1 != null)
                {
                    if (dx1 == 0)
                        dx1 = skillInfoPaintt[index].e1dx;
                    if (dy1 == 0)
                        dy1 = skillInfoPaintt[index].e1dy;
                    SmallImage.drawSmallImage(g, eff1.arrEfInfo[i1].idImg, xchar - dx1 - eff1.arrEfInfo[i1].dx,
                        ychar + dy1 + eff1.arrEfInfo[i1].dy, 2,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                    i1++;
                    if (i1 >= eff1.arrEfInfo.Length)
                    {
                        eff1 = null;
                        i1 = 0;
                        dx1 = 0;
                        dy1 = 0;
                    }
                }
                if (eff2 != null)
                {
                    if (dx2 == 0)
                        dx2 = skillInfoPaintt[index].e2dx;
                    if (dy2 == 0)
                        dy2 = skillInfoPaintt[index].e2dy;
                    SmallImage.drawSmallImage(g, eff2.arrEfInfo[i2].idImg, xchar - dx2 - eff2.arrEfInfo[i2].dx,
                        ychar + dy2 + eff2.arrEfInfo[i2].dy, 2,
                        mGraphics.VCENTER | mGraphics.HCENTER);
                    i2++;
                    if (eff2.arrEfInfo != null)
                        if (i2 >= eff2.arrEfInfo.Length)
                        {
                            eff2 = null;
                            i2 = 0;
                            dx2 = 0;
                            dy2 = 0;
                        }
                }
            }
        }
        catch (Exception e)
        {
        }
    }
}