using System;
using src.lib;

public class TileMap
{
    public const int T_TOP = 2;
    public const int T_LEFT = 2 << 1;
    public const int T_RIGHT = 2 << 2;
    public const int T_WATERFLOW = 2 << 5;
    public const int T_UNDERWATER = 2 << 10;
    public const int T_BOTTOM = 2 << 12;


    public static int tmw, tmh, pxw, pxh, tileID;
    public static int[] maps;

    public static int[] types;

    // Tile
    public static mBitmap imgMaptile;

    public static byte size = 24;

    private static int bx, dbx, fx, dfx;

    public static string mapName = "";
    public static short mapID;
    public static sbyte  zoneID, bgID;
    public static Vector vGo = new Vector();
    public static Vector vItemBg = new Vector();

    public static Vector vCurrItem = new Vector();

    public static MyHashtable listNameAllMap = new MyHashtable();

    public static int[][] tileType;
    public static int[][][] tileIndex;

    public static int gssx, gssxe, gssy, gssye, countx, county;


    public static BgItem getBIById(int id)
    {
        for (var i = 0; i < vItemBg.size(); i++)
        {
            var bi = (BgItem) vItemBg.elementAt(i);
            if (bi.id == id)
                return bi;
        }
        return null;
    }

    public static void setTile(int index, int[] mapsArr, int type)
    {
        for (var i = 0; i < mapsArr.Length; i++)
            if (maps[index] == mapsArr[i])
            {
                types[index] |= type;
                return;
            }
    }

    public static void loadMap(int tileId)
    {
        LogDebug.println(tileId + " size " + size + "leng " + tileType.Length + "TILEMAP WIDTH ::: HIEGHT ----> " +
                         tmw + " :::: " + tmh);
        pxh = tmh * size;
        pxw = tmw * size;


        var tile = tileId - 1;

        try
        {
            for (var i = 0; i < tmw * tmh; i++)
            for (var a = 0; a < tileType[tile].Length; a++)
                setTile(i, tileIndex[tile][a], tileType[tile][a]);
        }
        catch (Exception e)
        {
            GameMidlet.exit();
        }
    }

    public static void loadMapFromResource(int mapID)
    {
        InputStream iss = null;
        DataInputStream isss = new DataInputStream("/map/" + TileMap.mapID);
        tmw = (char) isss.read();
        tmh = (char) isss.read();

        LogDebug.println(tmw + "  TileMap loadma " + tmh);
        maps = new int[isss.available()];
        for (var i = 0; i < tmw * tmh; i++)
            maps[i] = (char) isss.read();
        types = new int[maps.Length];
    }


    public static int tileTypeAt(int x, int y)
    {
        try
        {
            return types[y * tmw + x];
        }
        catch (Exception ex)
        {
            return 1000;
        }
    }

    public static int tileTypeAtPixel(int px, int py)
    {
        try
        {
            return types[py / size * tmw + px / size];
        }
        catch (Exception ex)
        {
            return 1000;
        }
    }

    public static bool tileTypeAt(int px, int py, int t)
    {
        try
        {
            return (types[py / size * tmw + px / size] & t) == t;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public static int tileYofPixel(int py)
    {
        return py / size * size;
    }

    public static int tileXofPixel(int px)
    {
        return px / size * size;
    }


    public static void loadMapfile(int MapID)
    {
        try
        {
            vCurrItem.removeAllElements();
            vItemBg.removeAllElements();
            loadMapFromResource(MapID);
        }
        catch (Exception e)
        {
        }
    }

    public static void loadimgTile(int TileID)
    {
        TileID = TileID - 1;
        if (imgMaptile != null && imgMaptile.image != null)
            imgMaptile.cleanImg();
        imgMaptile = null;
        imgMaptile = GameCanvas.loadImage("/tile" + TileID + ".png");
        LogDebug.println("loadimgTile ---> " + imgMaptile);
    }

    public static void paintMap(mGraphics g)
    {
        GameScr.paintBgItem(g, 1);
        GameScr.gssw = GameCanvas.w / size + 2;
        GameScr.gssh = GameCanvas.h / size + 2;
        if (GameCanvas.w % 24 != 0)
            GameScr.gssw += 1;

        GameScr.gssx = GameScr.cmx / size - 1;
        if (GameScr.gssx < 0)
            GameScr.gssx = 0;
        GameScr.gssy = GameScr.cmy / size;
        GameScr.gssxe = GameScr.gssx + GameScr.gssw;
        GameScr.gssye = GameScr.gssy + GameScr.gssh;
        if (GameScr.gssy < 0)
            GameScr.gssy = 0;
        if (GameScr.gssye > tmh - 1)
            GameScr.gssye = tmh - 1;
        if (imgMaptile != null)
            for (var a = GameScr.gssx; a < GameScr.gssxe; a++)
            for (var b = GameScr.gssy; b < GameScr.gssye; b++)
                try
                {
                    var t = maps[b * tmw + a] - 1;
                    if ((t + 1) * size > imgMaptile.getHeight())
                        return;
                    if (t >= 0)
                        g.drawRegion(imgMaptile, 0, t * size, size, size, 0, a * size, b * size, 0);
                }
                catch (Exception e)
                {
                }
    }
}