using System;
using System.Collections.Generic;
using System.Linq;
using MessagePack;
using src.lib;
using src.network;
using src.Objectgame.quest;
using src.real.dto;
using src.real.dto.@char;
using src.real.dto.quest;
using src.real.dto.response;
using src.utils;
using UnityEngine;

public class Controller : IMessageHandler
{
    private static Controller me;
    
    public static int cmdTroupleShoot;
    public static sbyte cmdClanAvtive;
    public static sbyte typeClanGL;

    public void onConnectOK()
    {
        LogDebug.println("Connect ok");
    }

    public void onConnectionFail()
    {
        GameCanvas.currentScreen.isConnect = false;
        GameCanvas.currentScreen.connectCallBack = true;
    }

    public void onDisconnected()
    {
        GameCanvas.currentScreen.isConnect = false;
        GameCanvas.currentScreen.isCloseConnect = true;
        GameCanvas.currentScreen.connectCallBack = true;
        GameCanvas.resetToLoginScr();
    }

    private static void resetNpcQuest(int typeNV)
    {
        for (var j = 0; j < GameScr.vNpc.size(); j++)
        {
            var npc = (Npc) GameScr.vNpc.elementAt(j);
            if (npc != null && npc.typeNV == typeNV)
            {
                npc.typeNV = -1;
            }
        }
    }

    public void onMessage(Message msg)
    {
        try
        {
            var c = new Char();
            switch (msg.command)
            {
                case Cmd.QUEST:
                    GuiQuest.listNhacNv.removeAllElements();
                    var charQuestData = IOUtils.ReadMessage(msg);
                    var charQuestDto = MessagePackSerializer.Deserialize<CharQuestDTO>(charQuestData);
                    
                    if (InfoItem.wcat == -1)
                    {
                        InfoItem.wcat = 10 * mGraphics.getImageWidth(LoadImageInterface.imgChatConner) - 10;
                    }

                    var quest = DTOMapper.mapQuestTemplateDTOToModel(charQuestDto.QuestTemplate);
                    if (charQuestDto.Status == QuestStatus.NEW.ToString())
                    {
                        resetNpcQuest(0);
                        MainQuestManager.getInstance().FinishQuest = null;
                        MainQuestManager.getInstance().NewQuest = quest;
                        MainQuestManager.getInstance().NewQuest.paint();
                        
                    }
                    else if (charQuestDto.Status == QuestStatus.WORKING.ToString())
                    {
                        resetNpcQuest(1);
                        MainQuestManager.getInstance().NewQuest = null;
                        MainQuestManager.getInstance().WorkingQuest = quest;
                        MainQuestManager.getInstance().WorkingQuest.paint();
                    }
                    else if (charQuestDto.Status == QuestStatus.COMPLETED.ToString())
                    {
                        resetNpcQuest(2);
                        MainQuestManager.getInstance().WorkingQuest = null;
                        MainQuestManager.getInstance().FinishQuest = quest;
                        MainQuestManager.getInstance().FinishQuest.paint();
                    }

                    foreach (var t in quest.RemindContentPaint)
                    {
                        GuiQuest.listNhacNv.add(new InfoItem(t));
                    }
                    break;
                case Cmd.TALK_NPC:
                    var talkNpcData = IOUtils.ReadMessage(msg);
                    var talkNpcDto = MessagePackSerializer.Deserialize<TalkNPCResponseDTO>(talkNpcData);
                    GameCanvas.menu.startAtNPC(new Vector(), 0, Char.myChar().npcFocus.npcId, Char.myChar().npcFocus,
                        talkNpcDto.ContentChat);
                    break;          
                case Cmd.SKILL_CHAR:
                    int sizeKill = msg.reader().readShort();
                    for (var i = 0; i < sizeKill; i++)
                    {
                        var id = msg.reader().readByte();
                        var idIcon = msg.reader().readShort();
                        var idIconTron = msg.reader().readShort(); //icon skill tron
                        var name = msg.reader().readUTF();
                        var typeskill = msg.reader().readByte(); //chu dong or bi dong
                        var mota = msg.reader().readUTF(); //mo ta
                        var ntaget = msg.reader().readByte(); //so luong muc tieu danh
                        var typebuff = msg.reader().readByte(); //loai skill buff
                        var subeff = msg.reader().readByte(); //so luong muc tieu danh
                        var range = msg.reader().readShort(); //pham vi
                        var ideff = msg.reader().readShort();
                        var nlevel = msg.reader().readByte();
                        var skilltemplate = new SkillTemplate(id, name, idIcon, typeskill, ntaget, typebuff,
                            subeff, range, mota);
                        skilltemplate.iconTron = idIconTron;
                        skilltemplate.ideff = ideff;

                        LogDebug.println(id + " SKILL_CHAR nlevel " + nlevel);
                        skilltemplate.khoitaoLevel(nlevel); //// tong so level cua 1 skill
                        for (var j = 0; j < nlevel; j++)
                        {
                            skilltemplate.levelChar = new short[nlevel];
                            skilltemplate.levelChar[j] = msg.reader().readShort(); // mp hao
                            skilltemplate.mphao[j] = msg.reader().readShort(); // mp hao
                            skilltemplate.cooldown[j] = msg.reader().readInt(); // cooldow thoi gian cho skill
                            skilltemplate.timebuffSkill[j] = msg.reader().readInt(); /// thoi gian buff cua skill
                            skilltemplate.pcSubEffAppear[j] = msg.reader().readByte(); // ty le xuat hien hieu ung phu
                            skilltemplate.tExistSubEff[j] = msg.reader().readShort(); // thoi gian ton tai cua hieu ung phu
                            skilltemplate.usehp[j] = msg.reader().readShort(); // ty le tang % su dung binh hp
                            skilltemplate.usemp[j] = msg.reader().readShort(); // ty le tang % su dung binh mp
                            skilltemplate.ntargetlv[j] = msg.reader().readByte();
                            skilltemplate.rangelv[j] = msg.reader().readShort(); //pháº¡m vi áº£nh hÆ°á»Ÿng cá»§a skill
                            skilltemplate.dame[j] = msg.reader().readShort();
                        }

                        SkillTemplates.add(skilltemplate); // lÃ m giá»‘ng nhÆ° itemtemplate
                    }
                    break;
                case Cmd.REQUEST_SHOP:
                    ShopMain.idItemtemplate = null;
                    var menuId = msg.reader().readByte();

                    int itemlistSize = msg.reader().readShort();
                    ShopMain.indexidmenu = menuId;
                    ShopMain.idItemtemplate = new short[itemlistSize];
                    for (var i = 0; i < itemlistSize; i++)
                    {
                        var idTemplate = msg.reader().readShort();
                        ShopMain.idItemtemplate[i] = idTemplate;
                    }
                    ShopMain.getItemList(ShopMain.indexidmenu, ShopMain.idItemtemplate);
                    break;
                case Cmd.CMD_DYNAMIC_MENU:
                    // LogDebug.println(getClass(), " nhan ve list tabshop");
                    var menuShopsize = msg.reader().readByte(); // soluong menu shop
                    ShopMain.idMenu = new int[menuShopsize];
                    ShopMain.nameMenu = new string[menuShopsize];
                    ShopMain.dis = new string[menuShopsize];
                    ShopMain.cmdShop = new Command[menuShopsize];
                    // ShopMain.idItemtemplate = new int[menuShopsize];
                    for (var i = 0; i < menuShopsize; i++)
                    {
                        var idmenu = msg.reader().readByte();
                        ShopMain.idMenu[i] = idmenu;
                        var menuname = msg.reader().readUTF();
                        ShopMain.nameMenu[i] = menuname;
                        var dis = msg.reader().readUTF();
                        ShopMain.dis[i] = dis;
                    }
                    break;
                case Cmd.TRADE:
                    var typeTrade = msg.reader().readByte();
                    if (typeTrade == Constants.INVITE_TRADE)
                    {
                        // moi giao dich 
                        var CharIDpartner = msg.reader().readShort();
                        Char.myChar().partnerTrade = null;
                        Char.myChar().partnerTrade = GameScr.findCharInMap(CharIDpartner);
                        LogDebug.println(CharIDpartner + " moigiao dich " + Char.myChar().partnerTrade);
                        GameCanvas.startYesNoDlg(msg.reader().readUTF(), GameScr.cmdAcceptTrade,
                            new Command(mResources.CANCEL, GameCanvas.instance, 8882, null));
                    }
                    else if (typeTrade == Constants.ACCEPT_INVITE_TRADE)
                    {
                        // dong y giao dich 
                        var CharIDpartner = msg.reader().readShort();
                        if (Char.myChar().partnerTrade == null)
                            Char.myChar().partnerTrade = GameScr.findCharInMap(CharIDpartner);

                        GameScr.isBag = false;
                        GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                        GameCanvas.gameScr.guiMain.menuIcon.iconTrade.performAction();
                        Service.getInstance().requestinventory();
                    }
                    else if (typeTrade == Constants.MOVE_ITEM_TRADE)
                    {
                        var typeItem = msg.reader().readByte(); // loại
                        var typeItemmove = msg.reader().readByte(); // đưa xuống hay đưa lên 
                        var charId = msg.reader().readShort();
                        if (typeItemmove == 0)
                        {
                            // add item
                            if (charId == Char.myChar().charID)
                            {
                                for (var i = 0; i < Char.myChar().ItemMyTrade.Length; i++)
                                {
                                    if (Char.myChar().ItemMyTrade[i] == null)
                                    {
                                        Char.myChar().ItemMyTrade[i] = new Item();
                                        var IdItem = msg.reader().readShort();
                                        Char.myChar().ItemMyTrade[i].itemId = IdItem;
                                        var iditemTemplate = msg.reader().readShort();
                                        //LogDebug.println(IdItem + " iditemTemplate " + iditemTemplate);
                                        if (iditemTemplate != -1)
                                            Char.myChar().ItemMyTrade[i].template = ItemTemplates.get(iditemTemplate);

                                        break;
                                    }
                                }
                            }
                            else
                            {
                                for (var i = 0; i < Char.myChar().partnerTrade.ItemParnerTrade.Length; i++)
                                {
                                    // add vÃ o danh sÃ¡ch item cá»§a parner
                                    if (Char.myChar().partnerTrade.ItemParnerTrade[i] == null)
                                    {
                                        Char.myChar().partnerTrade.ItemParnerTrade[i] = new Item();
                                        var IdItem = msg.reader().readShort();
                                        Char.myChar().partnerTrade.ItemParnerTrade[i].itemId = IdItem;
                                        var iditemTemplate = msg.reader().readShort();

                                        if (iditemTemplate != -1)
                                            Char.myChar().partnerTrade.ItemParnerTrade[i].template =
                                                ItemTemplates.get(iditemTemplate);

                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            // remove item
                            if (Char.myChar().charID == charId)
                            {
                                //	LogDebug.println(getClass(), "remove Me item ");
                                var IdItem = msg.reader().readShort();
                                for (var i = 0; i < Char.myChar().ItemMyTrade.Length; i++)
                                    if (Char.myChar().ItemMyTrade[i] != null)
                                        if (Char.myChar().ItemMyTrade[i].itemId == IdItem)
                                        {
                                            //	LogDebug.println(getClass(),i+ "remove item null");
                                            Char.myChar().ItemMyTrade[i] = null;
                                            GameCanvas.gameScr.guiMain.menuIcon.trade.indexSelect1 = -1;
                                            break;
                                        }
                            }
                            else
                            {
                                var IdItem = msg.reader().readShort();
                                for (var i = 0; i < Char.myChar().partnerTrade.ItemParnerTrade.Length; i++)
                                    if (Char.myChar().partnerTrade.ItemParnerTrade[i] != null)
                                        if (Char.myChar().partnerTrade.ItemParnerTrade[i].itemId == IdItem)
                                            Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
                            }
                        }
                    }
                    else if (typeTrade == Constants.CANCEL_TRADE)
                    {
                        var charidP = msg.reader().readShort();
                        var charPar = GameScr.findCharInMap(charidP);
                        LogDebug.println(" CANCEL_TRADE ");
                        GameCanvas.gameScr.guiMain.menuIcon.trade = null;
                        GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                        GameCanvas.startOKDlg(charPar.cName + msg.reader().readUTF());
                    }
                    else if (typeTrade == Constants.LOCK_TRADE)
                    {
                        var charidP = msg.reader().readShort();
                        if (Char.myChar().charID != charidP)
                        {
                            LogDebug.println(" LOCK_TRADE other ");
                            GameCanvas.gameScr.guiMain.menuIcon.trade.block2 = true;
                        }
                        else
                        {
                            LogDebug.println(" LOCK_TRADE me ");
                            GameCanvas.gameScr.guiMain.menuIcon.trade.block1 = true;
                        }
                    }
                    else if (typeTrade == Constants.TRADE)
                    {
                        var charidP = msg.reader().readShort();
                        var cTrade = GameScr.findCharInMap(charidP);
                        GameCanvas.startYesNoDlg(cTrade.cName + msg.reader().readUTF(), TradeGui.cmdTradeEnd,
                            new Command(mResources.CANCEL, GameCanvas.instance, 8882, null));
                        //GameScr.gI().tradeGui = null;
                        for (var i = 0; i < Char.myChar().ItemMyTrade.Length; i++)
                        {
                            Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
                            Char.myChar().ItemMyTrade[i] = null;
                        }
                    }
                    else if (typeTrade == Constants.END_TRADE)
                    {
                        LogDebug.println(" end trade ");
                        for (var i = 0; i < Char.myChar().ItemMyTrade.Length; i++)
                        {
                            Char.myChar().partnerTrade.ItemParnerTrade[i] = null;
                            Char.myChar().ItemMyTrade[i] = null;
                        }
                        Char.myChar().partnerTrade = null;
                        GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                        GameCanvas.gameScr.guiMain.menuIcon.trade = null;
                    }
                    break;
                case Cmd.FRIEND:
                    var typeFriend = msg.reader().readByte();
                    if (typeFriend == Friend.INVITE_ADD_FRIEND)
                    {
                        Char.myChar().idFriend = msg.reader().readShort();
                        var tempDebug = msg.reader().readUTF();
                        GameCanvas.startYesNoDlg(tempDebug, GameScr.gI().cmdComfirmFriend,
                            new Command(mResources.CANCEL, GameCanvas.instance, 8882, null));
                    }
                    else if (typeFriend == Friend.ACCEPT_ADD_FRIEND)
                    {
                        var CharIDFriend = msg.reader().readShort();
                        c = GameScr.findCharInMap(CharIDFriend);
                        Char.myChar().vFriend.addElement(c);
                    }
                    else if (typeFriend == Friend.REQUEST_FRIEND_LIST)
                    {
                        Char.myChar().vFriend.removeAllElements();
                        var sizeFriendList = msg.reader().readByte();
                        for (var i = 0; i < sizeFriendList; i++)
                        {
                            var ch = new Char();
                            ch.isOnline = msg.reader().readbool();
                            if (ch.isOnline)
                                ch.charID = msg.reader().readShort(); // không online defaut là 0							
                            ch.CharidDB = msg.reader().readShort();
                            ch.cName = msg.reader().readUTF();
                            ch.clevel = msg.reader().readShort();
                            //ch.head = msg.reader().readShort();
                            Char.myChar().vFriend.addElement(ch);
                        }
                    }
                    else if (typeFriend == Friend.UNFRIEND)
                    {
                        var CharID = msg.reader().readShort();
                        var charUser = msg.reader().readShort();
                        //	System.out.println("Unfriend -----> "+CharID);
                        for (var i = 0; i < Char.myChar().vFriend.size(); i++)
                        {
                            var chardel = (Char) Char.myChar().vFriend.elementAt(i);
                            if (chardel.charID != 0)
                            {
                                // khi online 
                                if (chardel.charID == CharID)
                                    Char.myChar().vFriend.removeElementAt(i);
                            }
                            else
                            {
                                if (chardel.CharidDB == charUser)
                                    Char.myChar().vFriend.removeElementAt(i);
                            }
                        }
                    }
                    break;
                case Cmd.PARTY:
                    var typeParty = msg.reader().readByte();
                    LogDebug.println("type Party  " + typeParty);
                    if (typeParty == PartyType.INVITE_PARTY)
                    {
                        Party.gI().charId = msg.reader().readShort();
                        GameCanvas.startYesNoDlg(msg.reader().readUTF(), GameScr.gI().cmdAcceptParty,
                            new Command(mResources.CANCEL, GameCanvas.instance, 8882, null));
                    }
                    else if (typeParty == PartyType.GET_INFOR_PARTY)
                    {
                        var PartyId = msg.reader().readShort();
                        var CharLeaderid = msg.reader().readShort();
                        if (CharLeaderid == Char.myChar().charID)
                            Party.gI().isLeader = true;
                        var membersize = msg.reader().readByte();
                        var charIDmeber = new short[membersize];
                        var lvchar = new short[membersize];
                        var idhead = new short[membersize];
                        for (var i = 0; i < membersize; i++)
                        {
                            charIDmeber[i] = msg.reader().readShort();
                            lvchar[i] = msg.reader().readShort();
                            idhead[i] = msg.reader().readShort();
                            //m.dos.writeShort(members.get(i).getLevel());
                            //  m.dos.writeShort(members.get(i).getIdPartHead());
                        }
                        GameScr.hParty.containsKey(PartyId + "");
                        GameScr.hParty.put(PartyId + "",
                            new Party(PartyId, charIDmeber, CharLeaderid, lvchar, idhead));
                    }
                    else if (typeParty == PartyType.OUT_PARTY)
                    {
                        var IdChar = msg.reader().readShort();
                        if (IdChar == Char.myChar().charID)
                        {
                            LogDebug.println("bi kichkkkkkkkkkkkkkk");
                            GameScr.hParty.clear();
                            var party = (Party) GameScr.hParty.get(Char.myChar().idParty + "");
                            Party.vCharinParty.removeAllElements();
                        }

                        var chaRe = GameScr.findCharInMap(IdChar);
                        Party partyy = null;
                        if (chaRe != null)
                        {
                            partyy = (Party) GameScr.hParty.get(chaRe.idParty + "");
                            for (var i = 0; i < Party.vCharinParty.size(); i++)
                            {
                                var charRemove = (Char) Party.vCharinParty.elementAt(i);
                                if (charRemove.charID == IdChar)
                                {
                                    charRemove.idParty = -1;
                                    charRemove.isLeader = false;
                                    Party.vCharinParty.removeElementAt(i);
                                }
                            }
                            if (Party.vCharinParty.size() < 2)
                            {
                                GameScr.hParty.clear();
                                Party.vCharinParty.removeAllElements();
                            }
                        }
                        else
                        {
                            chaRe = Char.myChar();
                            partyy = (Party) GameScr.hParty.get(chaRe.idParty + "");
                            for (var i = 0; i < Party.vCharinParty.size(); i++)
                            {
                                var charRemove = (Char) Party.vCharinParty.elementAt(i);
                                if (charRemove.charID == IdChar)
                                {
                                    charRemove.idParty = -1;
                                    charRemove.isLeader = false;
                                    Party.vCharinParty.removeElementAt(i);
                                }
                            }
                        }
                        if (Party.vCharinParty.size() <= 0)
                            GameScr.hParty.remove(chaRe.idParty + "");
                    }
                    else if (typeParty == PartyType.DISBAND_PARTY)
                    {
                        var idParty = msg.reader().readShort();
                        var party = (Party) GameScr.hParty.get(idParty + "");

                        for (var i = 0; i < Party.vCharinParty.size(); i++)
                        {
                            var ch = (Char) Party.vCharinParty.elementAt(i);
                            ch.idParty = -1;
                            ch.isLeader = false;
                            Party.vCharinParty.removeElementAt(i);
                        }
                        GameScr.hParty.remove(idParty + "");
                    }
                    else if (typeParty == PartyType.GET_INFOR_NEARCHAR)
                    {
                        GameScr.charnearByme.removeAllElements();
                        var sizeCharlist = msg.reader().readByte();
                        for (var i = 0; i < sizeCharlist; i++)
                        {
                            var ch = new Char();
                            ch.charID = msg.reader().readShort();
                            ch.cName = msg.reader().readUTF();
                            ch.idParty = msg.reader().readShort();
                            GameScr.charnearByme.addElement(ch);
                        }
                    }
                    break;
                case Cmd.MENU_NPC:
                    Npc npcc = null;
                    var idNpc_menu = msg.reader().readInt();
                    int size_menu = msg.reader().readByte();

                    var vmenu = new Vector();

                    for (var i = 0; i < size_menu; i++)
                    {
                        var text_cmd = msg.reader().readUTF();
                        vmenu.addElement(new Command(text_cmd, GameCanvas.instance, GameCanvas.cMenuNpc,
                            i + "")); //i index_menu
                    }
                    Npc npc_dem = null;
                    for (var i = 0; i < GameScr.vNpc.size(); i++)
                    {
                        var npc = (Npc) GameScr.vNpc.elementAt(i);
                        if (npc != null && npc.template != null && npc.template.npcTemplateId == idNpc_menu &&
                            npc.Equals(Char.myChar().npcFocus))
                            npc_dem = npc;
                    }
                    if (npc_dem != null && vmenu.size() > 0)
                    {
                        GameCanvas.menu.doCloseMenu();
                        GameCanvas.menu.startAtNPC(vmenu, 0, npc_dem.npcId, npc_dem, "");
                    }
                    break;
                case Cmd.NPC:
                    GameScr.vNpc.removeAllElements();
                    var sizeNpc = msg.reader().readByte();
                    for (var i = 0; i < sizeNpc; i++)
                    {
                        var idNpc = msg.reader().readShort();
                        var npcX = msg.reader().readShort();
                        var npcY = msg.reader().readShort();
                        var npcIdtemplate = msg.reader().readShort();
                        
                        var npcz = new Npc(npcX, npcY, npcIdtemplate);

                        if (MainQuestManager.getInstance().NewQuest != null &&
                            MainQuestManager.getInstance().NewQuest.IdNpcReceive == npcIdtemplate)
                        {
                            npcz.typeNV = 0;
                        }
                        
                        if (MainQuestManager.getInstance().WorkingQuest != null &&
                            MainQuestManager.getInstance().WorkingQuest.IdNpcResolve == npcIdtemplate)
                        {
                            npcz.typeNV = 1;
                        }
                        
                        if (MainQuestManager.getInstance().FinishQuest != null &&
                            (MainQuestManager.getInstance().FinishQuest.IdNpcReceive == npcIdtemplate
                            || MainQuestManager.getInstance().FinishQuest.IdNpcResolve == npcIdtemplate))
                        {
                            npcz.typeNV = 1;
                        }
                        
//                        for (var j = 0; j < MainQuestManager.listUnReceiveQuest.size(); j++)
//                        {
//                            var q1 = (MainQuestManager) MainQuestManager.listUnReceiveQuest.elementAt(j);
//                            if (q1.idNPC_From == npcIdtemplate)
//                                npcz.typeNV = 0;
//                        }
//                        for (var j = 0; j < MainQuestManager.vecQuestDoing_Main.size(); j++)
//                        {
//                            var q2 = (MainQuestManager) MainQuestManager.vecQuestDoing_Main.elementAt(j);
//                            if (q2.idNPC_To == npcIdtemplate)
//                                npcz.typeNV = 1;
//                        }
//                        for (var j = 0; j < MainQuestManager.vecQuestFinish.size(); j++)
//                        {
//                            var q4 = (MainQuestManager) MainQuestManager.vecQuestFinish.elementAt(j);
//                            if (q4.idNPC_From == npcIdtemplate || q4.idNPC_To == npcIdtemplate)
//                                npcz.typeNV = 1;
//                        }
                        GameScr.vNpc.addElement(npcz);
                    }

                    break;
                case Cmd.PICK_REMOVE_ITEM:
                    var pickItemMapData = IOUtils.ReadMessage(msg);
                    var pickItemMapDTO = MessagePackSerializer
                        .Deserialize<PickUpItemResponseDTO>(pickItemMapData);
                    var typeItemPick = pickItemMapDTO.type;
                    var idItemPick = pickItemMapDTO.idItem;
                    if (typeItemPick == -1)
                    {
                        for (var i = 0; i < GameScr.vItemMap.size(); i++)
                        {
                            var itempick = (ItemMap) GameScr.vItemMap.elementAt(i);
                            if (itempick.itemMapID == idItemPick)
                            {
                                if (Char.myChar().itemFocus != null && Char.myChar().itemFocus.itemMapID == idItemPick)
                                    Char.myChar().itemFocus = null;
                                GameScr.vItemMap.removeElementAt(i);
                                break;
                            }
                        }
                        return;
                    }
                    if (Char.myChar().itemFocus != null && Char.myChar().itemFocus.itemMapID == idItemPick)
                    {
                        Char.myChar().itemFocus = null;
                        Char.myChar().clearFocus(10);
                    }
                    var idPlayerPick = pickItemMapDTO.id;
                    c = GameScr.findCharInMap(idPlayerPick);
                    if (c != null)
                    {
                        for (var i = 0; i < GameScr.vItemMap.size(); i++)
                        {
                            var itempick = (ItemMap) GameScr.vItemMap.elementAt(i);
                            if (itempick.itemMapID == idItemPick)
                            {
                                itempick.setPoint(c.cx, c.cy);
                                if (c.charID == Char.myChar().charID)
                                    itempick.isMe = true;
                            }
                        }
                    }
                    break;
                case Cmd.DROP_ITEM:
                    var typeDrop = msg.reader().readByte();
                    var typeItemRotRa = msg.reader().readByte(); //1 Ä‘Ã¡nh quÃ¡i rá»›t ra, 0 char vá»©t ra.
                    var IdMonterDie = msg.reader().readShort();
                    var idd = msg.reader().readShort();
                    var idTemplatee = msg.reader().readShort();
                    ItemMap itemDrop = null;
                    if (typeItemRotRa == 1)
                    {
                        var mons = GameScr.findMobInMap(IdMonterDie);
                        if (mons != null)
                        {
                            itemDrop = new ItemMap(idd, idTemplatee, mons.x, mons.yFirst + 10);
                        }
                        else
                        {
                            var xItem = msg.reader().readShort();
                            var yItem = msg.reader().readShort();
                            itemDrop = new ItemMap(idd, idTemplatee, xItem, yItem);
                        }
                        GameScr.vItemMap.addElement(itemDrop);
                        GameScr.vNhatItemMap.addElement(itemDrop);
                    }
                    else
                    {
                        var ch = GameScr.findCharInMap(IdMonterDie);
                        if (ch != null)
                        {
                            itemDrop = new ItemMap(idd, idTemplatee, ch.cx, ch.cy);
                            GameScr.vItemMap.addElement(itemDrop);
                            GameScr.vNhatItemMap.addElement(itemDrop);
                        }
                    }
                    itemDrop.type = typeDrop;
                    break;
                case Cmd.DIE:
                    var byteDie = msg.reader().readByte(); // 
                    if (byteDie == ObjectType.CAT_PLAYER)
                    {
                        var playerID = msg.reader().readShort();
                        if (playerID == Char.myChar().charID)
                        {
                            GameScr.gI().guiMain.moveClose = true;
                            if (MenuIcon.isShowTab)
                                GameCanvas.gameScr.guiMain.menuIcon.cmdClose.performAction();
                            if (GameScr.isBag)
                                TabScreenNew.cmdClose.performAction();
                        }
                        c = GameScr.findCharInMap(playerID);

                        if (c != null)
                        {
                            c.cHP = 0;
                            c.statusMe = Char.A_DEAD;
                        }
                    }
                    else if (byteDie == ObjectType.CAT_MONSTER)
                    {
                        var mobID = msg.reader().readShort();
                        for (var i = 0; i < GameScr.vMob.size(); i++)
                        {
                            var mob = (Mob) GameScr.vMob.elementAt(i);
                            if (mob.mobId == mobID)
                            {
                                mob.status = Mob.MA_DEADFLY;
                                mob.startDie();

                                if (Char.myChar().mobFocus != null && Char.myChar().mobFocus.mobId == mobID)
                                    Char.myChar().mobFocus = null;
                                break;
                            }
                        }
                    }

                    break;
                case Cmd.ATTACK:
                    var attackData = IOUtils.ReadMessage(msg);
                    var attackDto = MessagePackSerializer
                        .Deserialize<AttackResponseDTO>(attackData);
                    
                    if (attackDto.AttackerType == Constants.TYPE_PLAYER)
                    {
                        c = GameScr.findCharInMap(attackDto.AttackerId) ?? Char.myChar();
                        c.cMP = attackDto.AttackerMp;
                        if (attackDto.TargetType == Constants.TYPE_MONSTER)
                        {
                            for (var i = 0; i < GameScr.vMob.size(); i++)
                            {
                                var target = attackDto.Targets.ElementAt(0);
                                var mob = (Mob) GameScr.vMob.elementAt(i);
                                if (mob.mobId == target.Id)
                                {
                                    c.mobFocus = mob;
                                    c.mobFocus.hp = target.Hp;
                                    c.cdame = target.HitDamage;
                                    c.mobFocus.hp = c.mobFocus.hp < 0 ? 0 : c.mobFocus.hp;
                                    if (c.cdame > 0)
                                        GameScr.startFlyText("-" + c.cdame, c.mobFocus.x, c.mobFocus.y - 2 * mob.h - 5,
                                            0, -2, mFont.RED);
                                    else
                                        GameScr.startFlyText("miss", c.mobFocus.x, c.mobFocus.y - 2 * mob.h - 5, 0, -2,
                                            mFont.RED);
                                    
                                    if (attackDto.AttackerId != Char.myChar().charID)
                                    {
                                        c.cdir = c.cx - c.mobFocus.x > 0 ? -1 : 1;
                                        c.mobFocus.setInjure();
                                        c.mobFocus.injureBy = c;
                                        c.mobFocus.status = Mob.MA_INJURE;
                                    }
                                    if (mob.hp <= 0)
                                        mob.status = Mob.MA_DEADFLY;
                                }
                            }

                            if (attackDto.SkillAttackId > -1 && attackDto.SkillAttackId < GameScr.sks.Length - 1)
                            {
                                if (c.charID != Char.myChar().charID)
                                {
                                    Music.play(attackDto.SkillAttackId == 0 ? Music.ATTACK_0 : Music.SKILL2, 0.5f);
                                    c.setSkillPaint(GameScr.sks[attackDto.SkillEffectId], Skill.ATT_STAND);
                                }
                            }
                        }
                        else if (attackDto.TargetType == Constants.TYPE_PLAYER)
                        {
                            //player attack player
                            c.cMP = attackDto.AttackerMp;
                            for (var i = 0; i < attackDto.Targets.Count; i++)
                            {
                                var target = attackDto.Targets.ElementAt(i);
                                var charTarget = GameScr.findCharInMap(target.Id);
                                if (charTarget != null)
                                {
                                    charTarget.cHP = target.Hp;
                                    charTarget.doInjure(1, 0, false, 1);
                                    c.cdir = c.cx - charTarget.cx > 0 ? -1 : 1;
                                    ServerEffect.addServerEffect(25, charTarget.cx, charTarget.cy - 20, 1);
                                    GameScr.startFlyText("-" + target.HitDamage, charTarget.cx, charTarget.cy - 60, 0, -2, mFont.RED);
                                }
                            }
                            if (attackDto.SkillAttackId > -1 && attackDto.SkillAttackId < GameScr.sks.Length - 1)
                            {
                                if (c.charID != Char.myChar().charID)
                                {
                                    Music.play(Music.ATTACK_0, 0.5f);
                                    c.setSkillPaint(GameScr.sks[attackDto.SkillEffectId], Skill.ATT_STAND);
                                }
                            }
                        }
                        //paint skill for all char
                    }
                    else if (attackDto.AttackerType == Constants.TYPE_MONSTER)
                    {
                        var mobAttId = attackDto.AttackerId;
                        Mob mob = null;
                        for (var i = 0; i < GameScr.vMob.size(); i++)
                        {
                            var mobs = (Mob) GameScr.vMob.elementAt(i);
                            if (mobs.mobId == mobAttId)
                            {
                                mob = mobs;
                            }
                        }
                        
                        if (mob != null)
                        {
                            var target = attackDto.Targets.ElementAt(0);
                            var victimChar = GameScr.findCharInMap(target.Id);
                            mob.cFocus = victimChar;
                            if (mob.cFocus == null)
                            {
                                mob.f = -1;
                                mob.cFocus = Char.myChar();
                            }
                            mob.dir = mob.x - mob.cFocus.cx > 0 ? -1 : 1;

                            var hpmat = target.Hp;
                            mob.cFocus.cHP = hpmat;
                            if (victimChar != null)
                                victimChar.cHP = hpmat;

                            mob.dame = target.HitDamage;
                            
                            mob.status = Mob.MA_ATTACK;
                            GameScr.startFlyText("-" + mob.dame, mob.cFocus.cx, mob.cFocus.cy - 60, 0, -3, mFont.RED);

                            if (mob.typeMob == Mob.TYPE_MOB_TOOL)
                            {
                                ServerEffect.addServerEffect(21, mob.x, mob.y, 1);
                            }
                                
                            mob.setAttack(mob.cFocus);
                        }
                    }
                    break;
                case Cmd.REQUEST_MONSTER_INFO:
                    var idmob = msg.reader().readShort();
                    var namemob = msg.reader().readUTF();
                    var maxhp = msg.reader().readInt();
                    var hp = msg.reader().readInt();
                    for (var i = 0; i < GameScr.vMob.size(); i++)
                    {
                        var modInMap = (Mob) GameScr.vMob.elementAt(i);

                        if (modInMap != null && modInMap.mobId == idmob)
                        {
                            modInMap.mobName = namemob;
                            modInMap.maxHp = maxhp;
                            modInMap.hp = hp;
                        }
                    }
                    break;
                case Cmd.CHAT:
                    var typeChat = msg.reader().readByte();
                    if (typeChat == ChatType.CHAT_MAP)
                    {
                        // char map

                        var useriD = msg.reader().readShort();
                        var text = msg.reader().readUTF();
                        if (Char.myChar().charID == useriD)
                            c = Char.myChar();
                        else
                            c = GameScr.findCharInMap(useriD);
                        if (c == null)
                            return;
                        ChatPopup.addChatPopup(text, 200, c);
                        ChatManager.gI().addChat(mResources.PUBLICCHAT[0], c.cName, text);
                    }
                    else if (typeChat == ChatType.CHAT_WORLD)
                    {
                        ChatWorld.ChatWorldd(msg);
                    }
                    else if (typeChat == ChatType.CHAT_FRIEND)
                    {
                        ChatPrivate.ChatFriend(msg);
                    }
                    break;
                case Cmd.GET_ITEM_INVENTORY:
                    var charInventoryData = IOUtils.ReadMessage(msg);
                    var charInventoryDto = MessagePackSerializer
                        .Deserialize<CharInventoryDTO>(charInventoryData);

                    Char.myChar().luong = charInventoryDto.Gold;
                    Char.myChar().xu = charInventoryDto.Xu;
                    Char.myChar().arrItemBag = new Item[charInventoryDto.Items.Count];
                    TabNangCap.numberItemInBag = Char.myChar().arrItemBag.Length;
                    for (var i = 0; i < Char.myChar().arrItemBag.Length; i++)
                    {
                        var charItemDto = charInventoryDto.Items[i];
                        var idItem = charItemDto.PositionIndex;
                        var itemTemplateId = charItemDto.ItemTemplateId;
                        var lvItem = charItemDto.Level;
                        if (itemTemplateId != -1)
                        {
                            Char.myChar().arrItemBag[i] = new Item
                            {
                                itemId = idItem,
                                typeUI = Item.UI_BAG,
                                indexUI = i,
                                template = ItemTemplates.get((short) itemTemplateId)
                            };
                            Char.myChar().arrItemBag[i].template.lvItem = (short) lvItem;
                            Char.myChar().arrItemBag[i].isUseInventory = charItemDto.IsUseInventory;
                            
                            if (Char.myChar().arrItemBag[i].template != null &&
                                Item.isBlood(Char.myChar().arrItemBag[i].template.type)
                                || Char.myChar().arrItemBag[i].template != null &&
                                Item.isMP(Char.myChar().arrItemBag[i].template.type))
                            {
                                Char.myChar().arrItemBag[i].quantity = charItemDto.Quantity;
                                Char.myChar().arrItemBag[i].value = charItemDto.ValueBuff;
                            }
                        }
                    }
                    break;
                case Cmd.PLAYER_REMOVE:
                    var CharIDRemove = msg.reader().readShort();
                    if (CharIDRemove == Char.myChar().charID)
                        return;
                    for (var i = 0; i < GameScr.vCharInMap.size(); i++)
                    {
                        c = (Char) GameScr.vCharInMap.elementAt(i);
                        if (CharIDRemove == c.charID)
                            GameScr.vCharInMap.removeElementAt(i);
                    }
                    break;
                case Cmd.PLAYER_INFO:
                    LogDebug.println("CHAR INFO");
                    var charID = msg.reader().readLong();
                    var cMaxHP = msg.reader().readInt();
                    var cHP = msg.reader().readInt();
                    var clevel = msg.reader().readShort();
                    var typeBePhongAn = msg.reader().readByte();
                    var ninjaLuuDai = msg.reader().readByte();
                    var cIdDB = msg.reader().readLong();
                    var cIdClanGlobal = msg.reader().readInt();
                    var cIdClanLocal = msg.reader().readInt();
                    for (var i = 0; i < GameScr.vCharInMap.size(); i++)
                    {
                        var ch = (Char) GameScr.vCharInMap.elementAt(i);
                        if (ch.charID == charID)
                        {
                            ch.cMaxHP = cMaxHP;
                            ch.cHP = cHP;
                            if (ch.clevel > 0 && ch.clevel < clevel)
                            {
                                Music.play(Music.LENLV, 10);
                                ServerEffect.addServerEffect(22, ch.cx, ch.cy, 1);
                            }
                            if (typeBePhongAn == 1)
                                ServerEffect.addServerEffect(40, ch, true);
                            else
                                ServerEffect.removeEffect(40);
                            ch.clevel = clevel;
                            if (ch.cMaxHP <= 0) ch.cMaxHP = ch.cHP <= 0 ? 1 : ch.cHP;
                            ch.cIdDB = cIdDB;
                            ch.cIdClanGlobal = cIdClanGlobal;
                            ch.cIdClanLocal = cIdClanLocal;
                        }
                    }

                    break;
                case Cmd.REQUEST_IMAGE: // yeu cau hinh tu server
                    sbyte subImg = msg.reader().readByte();
                    var idimg = msg.reader().readInt();
                    LogDebug.println(subImg + "REQUEST_IMAGE receive  " + idimg);
                    if (subImg == 0)
                    {
                        var dataImg = Utils.readByteArray(msg);
                        if (GameCanvas.currentScreen == DownloadImageScreen.gI())
                            DownloadImageScreen.isOKNext = true;
                        var path = SmallImage.getPathImage(idimg) + "" + idimg;
                        //LogDebug.println("REQUEST_IMAGE dataImg  " + dataImg.Length);
                        Rms.saveRMS(mSystem.getPathRMS(path), dataImg);
                    }
                    else if (subImg == 1)
                    {
                        int sizeImg = msg.reader().readByte();
                        //LogDebug.println(subImg+ "REQUEST_IMAGE sizeImg  "+sizeImg);

                        for (var i = 0; i < sizeImg; i++)
                        {
                            var lengread = msg.reader().readInt();

                            var dataImg = Utils.readByteArray(msg, lengread);
                            var path = SmallImage.getPathImage(idimg) + "" + idimg + "/_" +
                                       (sizeImg == 2 ? (i == 0 ? 1 : 4) : i + 1);
                            //LogDebug.println(dataImg.Length+ "  REQUEST_IMAGE path  "+path);
                            Rms.saveRMS(mSystem.getPathRMS(path), dataImg);
                        }
                    }
                    break;
                case Cmd.PLAYER_MOVE: // char khac move lai kt
                    var typemove = msg.reader().readByte();
                    try
                    {
                        if (typemove == Const.CAT_MONSTER)
                        {
                            var idmobb = msg.reader().readShort();
                            var idmobx = msg.reader().readShort();
                            var idmoby = msg.reader().readShort();
                            var namemobb = msg.reader().readUTF();
                            var hpmob = msg.reader().readInt();
                            for (var i = 0; i < GameScr.vMob.size(); i++)
                            {
                                var mobb = (Mob) GameScr.vMob.elementAt(i);
                                if (mobb.mobId == idmobb && mobb.status == Mob.MA_INHELL)
                                {
                                    //LogDebug.println("moveeee mob  "+mobb.mobId);
                                    mobb.injureThenDie = false;
                                    mobb.status = Mob.MA_WALK;
                                    mobb.x = idmobx;
                                    mobb.y = idmoby - 10;
                                    mobb.mobName = namemobb;
                                    ServerEffect.addServerEffect(37, mobb.x, mobb.y - mobb.getH() / 4 - 10, 1);
                                    mobb.hp = hpmob > mobb.maxHp ? mobb.maxHp : hpmob;
                                }
                            }
                        }
                        else if (typemove == Const.CAT_PLAYER)
                        {
                            LogDebug.println(" -----> NHAN PLAYER KHAC DI CHUYEN");
                            var charId = msg.reader().readLong();
                            var cxMoveLast = msg.reader().readShort();
                            var cyMoveLast = msg.reader().readShort();
                            var cName = msg.reader().readUTF();
                            var typePk = msg.reader().readByte();
                            for (var i = 0; i < GameScr.vCharInMap.size(); i++)
                            {
                                c = (Char) GameScr.vCharInMap.elementAt(i);
                                if (c.charID == charId)
                                {
                                    c.cxMoveLast = cxMoveLast;
                                    c.cyMoveLast = cyMoveLast;
                                    c.cName = cName;
                                    c.typePk = typePk;

                                    c.moveTo(c.cxMoveLast, c.cyMoveLast);
                                    c.lastUpdateTime = mSystem.currentTimeMillis();
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                    }

                    break;
                case Cmd.CHANGE_MAP:
                    LogDebug.println("CHANGE_MAPPPPPPP");
                    GameScr.gI().guiMain.moveClose = false;
                    GameScr.gI().guiMain.menuIcon.indexpICon = 0;
                    MenuIcon.isShowTab = false;
                    GameScr.vNpc.removeAllElements();
                    TileMap.vGo.removeAllElements();
                    GameScr.vMob.removeAllElements();
                    GameScr.vItemMap.removeAllElements();
                    GameScr.vNhatItemMap.removeAllElements();
                    Char.ischangingMap = true;
                    GameScr.vCharInMap.removeAllElements();
                    GameScr.vCharInMap.addElement(Char.myChar());
                    TileMap.mapID = msg.reader().readShort();
                    Effect.vEffect2.removeAllElements();
                    TileMap.mapName = (string) TileMap.listNameAllMap.get(TileMap.mapID + "");
                    TileMap.zoneID = msg.reader().readByte();
                    var vsize = msg.reader().readShort();
                    for (var i = 0; i < vsize; i++)
                        TileMap.vGo.addElement(new Waypoint(msg.reader().readShort(), msg.reader().readShort(),
                            msg.reader().readShort(), msg.reader().readShort(), msg.reader().readShort()));
                    Char.myChar().cx = msg.reader().readShort();
                    Char.myChar().cy = msg.reader().readShort();
                    Char.myChar().statusMe = Char.A_FALL;
                    TileMap.tileID = msg.reader().readByte();
                    var nTile = msg.reader().readByte();
                    TileMap.tileIndex = new int[nTile][][];
                    TileMap.tileType = new int[nTile][];

                    for (var i = 0; i < nTile; i++)
                    {
                        var nTypeSize = msg.reader().readByte();
                        TileMap.tileType[i] = new int[nTypeSize];
                        TileMap.tileIndex[i] = new int[nTypeSize][];
                        for (var a = 0; a < nTypeSize; a++)
                        {
                            TileMap.tileType[i][a] = msg.reader().readInt();
                            var sizeIndex = msg.reader().readByte();
                            TileMap.tileIndex[i][a] = new int[sizeIndex];
                            for (var b = 0; b < sizeIndex; b++)
                                TileMap.tileIndex[i][a][b] = msg.reader()
                                    .readByte();
                        }
                    }
                    TileMap.loadimgTile(TileMap.tileID); // load hinh tile
                    TileMap.loadMapfile(TileMap.mapID); // load file map
                    TileMap.loadMap(TileMap.tileID); // load va cham 
                    Laroi.load();

                    GameScr.loadMapItem(); // load toan bo file data object
                    GameScr.loadMapTable(TileMap.mapID); // object trong 1 map filedata
                    TileMap.vItemBg.removeAllElements();
                    BgItem.imgPathLoad.clear();
                    Mob.imgMob.clear();
                    var idObjMap = new int [TileMap.vCurrItem.size()];
                    for (var i = 0; i < TileMap.vCurrItem.size(); i++)
                    {
                        var biMap = (BgItem) TileMap.vCurrItem.elementAt(i);
                        if (biMap != null)
                            idObjMap[i] = biMap.idImage;
                    }
                    var sizeMod = msg.reader().readByte();
                    Mob.arrMobTemplate = new MobTemplate[sizeMod];
                    for (var i = 0; i < Mob.arrMobTemplate.Length; i++)
                    {
                        Mob.arrMobTemplate[i] = new MobTemplate();
                        var idModtemp = msg.reader().readByte();
                        var idloadmob = msg.reader().readShort();
                        Mob.arrMobTemplate[i].idloadimage = idloadmob;
                        var mob = new Mob(idModtemp, msg.reader().readInt(), msg.reader().readByte(),
                            msg.reader().readInt(),
                            msg.reader().readShort(), msg.reader().readShort(), msg.reader().readShort(),
                            msg.reader().readShort(), msg.reader().readShort(),
                            msg.reader().readShort());
                        var isBoss = msg.reader().readBool();
                        mob.isBoss = isBoss;
                        if (isBoss)
                        {
                            var sizeSmall = msg.reader().readInt();


                            var dataSmall = Utils.readByteArray(msg, sizeSmall);

                            var sizeFrame = msg.reader().readInt();


                            var dataFrame = Utils.readByteArray(msg, sizeFrame);
                            mob.typeMob = Mob.TYPE_MOB_TOOL;
                            Mob.arrMobTemplate[i].data = new EffectData(); // doc file data quai trong tool
                            DataInputStream iss = new DataInputStream(dataSmall);

                            Mob.arrMobTemplate[i].data.readData222(iss);
                            DataInputStream is2 = new DataInputStream(dataFrame);

                            Mob.arrMobTemplate[i].data.readhd(is2);
                        }

                        mob.idloadimage = idloadmob;
                        GameScr.vMob.addElement(mob);
                    }
                    try
                    {
                        int sizeItemMap = msg.reader().readShort();
                        for (var i = 0; i < sizeItemMap; i++)
                        {
                            var iditemm = msg.reader().readInt();
                            var idtemitem = msg.reader().readShort();
                            int xitem = msg.reader().readShort();
                            int yitem = msg.reader().readShort();
                            var itemDropmap = new ItemMap(iditemm, idtemitem, xitem, yitem);
                            //					
                            GameScr.vItemMap.addElement(itemDropmap);
                            GameScr.vNhatItemMap.addElement(itemDropmap);
                        }
                    }
                    catch (Exception)
                    {
                    }

                    Service.getInstance().requestinventory();
                    if (GameCanvas.currentScreen != GameCanvas.gameScr)
                        GameCanvas.gameScr = new GameScr();
                    DownloadImageScreen.gI().switchToMe(SmallImage.ID_ADD_MAPOJECT, idObjMap);
                    Char.ischangingMap = false;
                    Char.myChar().statusMe = Char.A_FALL;
                    GameScr.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
                    GameCanvas.endDlg();

                    Music.play(CRes.random(1, 4), 0.3f);
                    break;
                case Cmd.ITEM:
                    try
                    {
                        GameScr.currentCharViewInfo = Char.myChar();
                        var type = msg.reader().readByte();
                        if (type == ItemTemplate.ITEM_TEMPLATE)
                        {
                            var nItemTemplate = msg.reader().readShort(); // so luong template ID
                            LogDebug.println("  nItemTemplate  " + nItemTemplate);
                            for (var i = 0; i < nItemTemplate; i++)
                            {
                                var templateID = msg.reader().readShort(); // id temp
                                var typeItem = msg.reader().readByte(); // loai
                                var gender = msg.reader().readByte(); //  gioi tinh
                                var name = msg.reader().readUTF(); // ten 
                                var des = msg.reader().readUTF(); // mo ta
                                var level = msg.reader().readByte(); // level
                                var iconID = msg.reader().readShort(); // idIcon
                                var isUptoUp = msg.reader().readbool(); // cÃ³ nÃ¢ng cáº¥p Ä‘c k 
                                var giaitem = msg.reader().readLong();
                                var clazz = msg.reader().readByte();
                                var quocgia = msg.reader().readByte();
                                var part = msg.reader().readShort(); // part cÆ¡ báº£n cháº¯c cháº¯n tháº±ng nÃ o cÅ©ng pháº£i cÃ³
                                short partquan = -1, partdau = -1;
                                if (typeItem == Item.TYPE_AO)
                                {
                                    partquan = msg.reader().readShort();
                                    partdau = msg.reader().readShort();
                                }
                                var typeSell = msg.reader().readByte();
                                ItemTemplate itt = null;
                                if (typeItem != Item.TYPE_AO)
                                    itt = new ItemTemplate(templateID, typeItem, gender, name, des, level, iconID, part,
                                        isUptoUp);
                                else
                                    itt = new ItemTemplate(templateID, typeItem, gender, name, des, level, iconID, part,
                                        partquan, partdau, isUptoUp);
                                itt.despaint = mFont.tahoma_7_white.splitFontArray(des, 140);
                                itt.gia = giaitem;
                                itt.clazz = clazz;
                                itt.quocgia = quocgia;
                                itt.typeSell = typeSell;
                                ItemTemplates.add(itt);
                            }
                        }
                       
                    }
                    catch (Exception e)
                    {
                        //e.printStackTrace();
                    }


                    break;
                case Cmd.CHAR_INFO:
                    var data_char_info = IOUtils.ReadMessage(msg);
                    var maincharinfoDto = MessagePackSerializer.Deserialize<MainCharInfoDTO>(data_char_info);
                
                    Char.myChar().charID = (int)maincharinfoDto.Id;
                    Char.myChar().cName = maincharinfoDto.CharName;
                    Char.myChar().cClass = maincharinfoDto.IdClass;
                    Char.myChar().cgender = maincharinfoDto.Gender;
                    var clevel2 = maincharinfoDto.Level;
                    if (Char.myChar().clevel > 0 && Char.myChar().clevel < clevel2)
                    {
                        Music.play(Music.LENLV, 10);
                        ServerEffect.addServerEffect(22, Char.myChar().cx, Char.myChar().cy, 1);
                    }
                    Char.myChar().clevel = clevel2;
                    Char.myChar().cEXP = maincharinfoDto.Percent;
                    Char.myChar().cHP = maincharinfoDto.Hp;
                
                    Char.myChar().cMaxHP = maincharinfoDto.MaxHp;
                
                    Char.myChar().cMP = maincharinfoDto.Mp;
                
                    Char.myChar().cMaxMP = maincharinfoDto.MaxMp;
                                Char.myChar().diemTN = null;
                                Char.myChar().diemTN = new int[8];
                
                    Char.myChar().diemTN[0] = maincharinfoDto.KhaiMon;
                    Char.myChar().diemTN[1] = maincharinfoDto.HuuMon;
                    Char.myChar().diemTN[2] = maincharinfoDto.SinhMon;
                    Char.myChar().diemTN[3] = maincharinfoDto.DoMon;
                    Char.myChar().diemTN[4] = maincharinfoDto.CanhMon;
                    Char.myChar().diemTN[5] = maincharinfoDto.ThuongMon;
                    Char.myChar().diemTN[6] = maincharinfoDto.KinhMon;
                    Char.myChar().diemTN[7] = maincharinfoDto.TuMon;
                    Char.myChar().totalTN = maincharinfoDto.BasePoint;
                
                                //gui ve thong tin sub
                    var sizeSub = (short)maincharinfoDto.SizeAttribute;
                                Char.myChar().subTn = new int[sizeSub];
                                for (int i = 0; i < sizeSub; i++)
                                {
                     MainCharInfoDTO.CharAttributeDTO charAttribute = maincharinfoDto.CharAttribute[i];
                     var indexMon = (sbyte)charAttribute.Id;
                     var valueSub = charAttribute.Value;
                                }
                                if (GameScr.findCharInMap((short)Char.myChar().charID) == null)
                                    GameScr.vCharInMap.addElement(Char.myChar());
                
                    Char.myChar().cCountry = maincharinfoDto.Country;
                    Debug.Log("Country: "+Char.myChar().cCountry);
 
                    break;
                case Cmd.CHAR_LIST:
                    var charListData = IOUtils.ReadMessage(msg);
                    var charListDto = MessagePackSerializer
                        .Deserialize<CharListResponseDTO>(charListData);
                    
                    LoginScr.isLoggingIn = false;
                    SelectCharScr.gI().initSelectChar();
                    GameCanvas.endDlg();
                    if (charListDto.Chars.Count > 0)
                    {
                        for (var i = 0; i < charListDto.Chars.Count; i++)
                        {
                            var charBasicInfo = charListDto.Chars[i];
                           
                            SelectCharScr.gI().charIDDB[i] = charBasicInfo.Id;
                            SelectCharScr.gI().name[i] = charBasicInfo.CharName;
                            SelectCharScr.gI().gender[i] = charBasicInfo.Gender;
                            SelectCharScr.gI().type[i] = charBasicInfo.CharClass;
                            SelectCharScr.gI().lv[i] = charBasicInfo.Level;
                            SelectCharScr.gI().parthead[i] = charBasicInfo.HeadId;
                            SelectCharScr.gI().partbody[i] = charBasicInfo.BodyId;
                            SelectCharScr.gI().partleg[i] = charBasicInfo.LegId;
                        }
                        DownloadImageScreen.gI().switchToMe();
                        GameCanvas.endDlg();
                    }
                    else
                    {
                        DownloadImageScreen.gI().switchToMe(1);
                        GameCanvas.endDlg();
                    }
                    break;
                case Cmd.LOGIN:
                    createData(new DataInputStream(msg.reader()));


                    // SAVE RMS NEW DATA
                    msg.reader().reset();
                    var newData = new sbyte[msg.reader().available()];
                    msg.reader().readFully(ref newData);
                    
                    // SAVE RMS NEW DATA VERSION
                    GameScr.readEfect(); // doc ef
                    SmallImage.readImage();
                    GameScr.readPart();
                    GameScr.readSkill();
                    break;
                case Cmd.DIALOG:
                    var typeDialog = msg.reader().readByte();
                    LogDebug.println("typeDialog  " + typeDialog);
                    if (typeDialog == 0)
                        GameCanvas.startOKDlg(msg.reader().readUTF());
                    else if (typeDialog == 1)
                        GameCanvas.startYesNoDlg(msg.reader().readUTF(), new Command("Yes", GameScr.gI(), 1, null),
                            new Command("No", GameScr.gI(), 0, null));
                    else if (typeDialog == 2)
                        GameCanvas.startDlgTime(msg.reader().readUTF(),
                            new Command("Yes", GameCanvas.instance, 8882, null),
                            msg.reader().readInt());
                    else if (typeDialog == 3)
                        GameCanvas.StartDglThongBao(msg.reader().readUTF());
                    else if (typeDialog == 4)
                        GameCanvas.StartDglThongBao(msg.reader().readUTF());
                    break;
                case Cmd.NPC_TEMPLATE:
                    var npcTemplateData = IOUtils.ReadMessage(msg);
                    var npcTemplateResponseDto = MessagePackSerializer.Deserialize<NpcTemplateResponseDTO>(npcTemplateData);
                    for (var i = 0; i < npcTemplateResponseDto.NpcTemplates.Count; i++)
                    {
                        var npcTemplate =
                            DTOMapper.mapNpcTemplateDTOToModel(npcTemplateResponseDto.NpcTemplates.ElementAt(i));
                        Npc.arrNpcTemplate.put(npcTemplate.npcTemplateId + "", npcTemplate);
                    }

                    break;
                case Cmd.MAP_TEMPLATE:
                    int sizeee = msg.reader().readShort();
                    for (var i = 0; i < sizeee; i++)
                    {
                        var idmap = msg.reader().readShort();
                        var idtile = msg.reader().readShort();
                        var name = msg.reader().readUTF();
                        TileMap.listNameAllMap.put(idmap + "", name);
                    }
                    break;
                case Cmd.CHAR_SKILL_STUDIED:
                    var dsSkillDaHoc = msg.reader().readShort();
                    LogDebug.println("CHAR_SKILL_STUDIED size  " + dsSkillDaHoc);
                    for (var i = 0; i < dsSkillDaHoc; i++)
                    {
                        var idskill = msg.reader().readByte();
                        if (i == 0)
                            QuickSlot.idSkillCoBan = idskill;
                        var idtem = msg.reader().readShort(); //icon skill tron
                        var levelskill = msg.reader().readShort();

                        if (TabSkill.skillIndex != null && idskill == TabSkill.skillIndex.id)
                        {
                            if (TabSkill.skillIndex.level == -1 && levelskill > -1)
                            {
                                TabSkill.skillIndex.level = levelskill;
                                TabSkill.isDaHoc = true;
                            }
                            TabSkill.skillIndex.level = levelskill;
                        }
                        var skill = (SkillTemplate) SkillTemplates.hSkilltemplate.get("" + idskill);

                        skill.level = levelskill;
                        if (skill.level >= skill.nlevelSkill)
                            skill.level = (short) (skill.nlevelSkill - 1);
                    }

                    QuickSlot.loadRmsQuickSlot();
                    break;
                case Cmd.UPDATE_CHAR
                : //UPDATE_BLOOD = 0;//UPDATE_EXP = 1;//su dung tang exp/// UPDATE_HP = 2; //UPDATE_MP = 3;
                    //LogDebug.println("update charrr ");
                    var idchar = msg.reader().readShort();
                    var typee = msg.reader().readByte();
                    //LogDebug.println(type+" update charrr  "+idchar);
                    var ccharupdate = GameScr.findCharInMap(idchar);
                    if (ccharupdate == null) return;
                    switch (typee)
                    {
                        case 0:
                            var sizeThucAn = msg.reader().readByte();
                            GuiMain.vecItemOther.removeAllElements();
                            for (var i = 0; i < sizeThucAn; i++)
                            {
                                var idtemplate = msg.reader().readShort();
                                var time = msg.reader().readShort();
                                GuiMain.vecItemOther.add(new ItemThucAn((short) i,
                                    0, idtemplate, time, 100));
                            }

                            break;
                        case 1:
                            Char.myChar().cEXP = msg.reader().readByte();
                            Char.myChar().totalTN = msg.reader().readInt();
                            break;
                        case 2: //hp
                            ccharupdate.cMaxHP = msg.reader().readInt(); //maxhp
                            ccharupdate.cHP = msg.reader().readInt(); //hp
                            var hpcong = msg.reader().readInt(); //hp cong them
                            GameScr.startFlyText("+" + hpcong, ccharupdate.cx, ccharupdate.cy - 50, 0, -2, mFont.RED);
                            break;
                        case 3: //hp
                            ccharupdate.cMaxMP = msg.reader().readInt(); //maxhp
                            ccharupdate.cMP = msg.reader().readInt(); //hp
                            var mpcong = msg.reader().readInt(); //hp cong them
                            GameScr.startFlyText("+" + mpcong, ccharupdate.cx, ccharupdate.cy - 50, 0, -2, mFont.GREEN);
                            break;
                    }
                    break;
                case Cmd.COME_HOME_DIE:
                    var typehs = msg.reader().readByte();
                    switch (typehs)
                    {
                        case 0: //ve lang
                            Char.myChar().statusMe = Char.A_FALL;
                            Char.myChar().cHP = msg.reader().readShort();
                            Char.myChar().cMP = msg.reader().readShort();
                            break;

                        case 1: //hs tai cho
                            var idcharhs = msg.reader().readShort();
                            var chs = GameScr.findCharInMap(idcharhs);

                            if (chs != null)
                            {
                                ServerEffect.addServerEffect(34, chs.cx, chs.cy, 3);
                                chs.statusMe = Char.A_FALL;
                                chs.cHP = msg.reader().readShort();
                                chs.cMP = msg.reader().readShort();
                            }
                            break;
                    }
                    break;
                case Cmd.REGISTER:
                    var isDK = msg.reader().readbool();
                    var textt = msg.reader().readUTF();
                    GameCanvas.startOK(textt, 8882, null);
                    break;
                case Cmd.XP_CHAR:
                    var idc = msg.reader().readShort();
                    var xp = msg.reader().readInt(); //m.dos.writeInt((int) dxp);
                    var acc = GameScr.findCharInMap(idc);
                    if (acc != null)
                        GameScr.startFlyText("+" + xp + "exp",
                            acc.cx, acc.cy - acc.ch - 5, 0, -2, mFont.YELLOW);
                    break;
                case Cmd.BUY_ITEM_FROM_SHOP:
                    LogDebug.println("nhan ve BUY_ITEM_FROM_SHOP ");
                    break;
                case Cmd.REQUEST_REGION:
                    ThongTinKhu(msg);
                    break;
                case Cmd.CHANGE_REGION:
                    ChangeKhu(msg);
                    break;
                case Cmd.COMPETED_ATTACK:
                    ThachDau(msg);
                    break;
                case Cmd.STATUS_ATTACK:
                    StatusAttack(msg);
                    break;
                case Cmd.SERVER_CHAT:
                    var type_info_server = msg.reader().readByte();
                    var text_info_server = msg.reader().readUTF();
                    var info_server = new InfoServer(InfoServer.CHATSERVER, text_info_server);
                    GameScr.listInfoServer.add(info_server);
                    //GameCanvas.StartDglThongBao(m);
                    break;
                case Cmd.ERROR_VERSION:
                    var link = msg.reader().readUTF();
                    GameCanvas.startOK("Bạn đang dùng phiên bản cũ. Vui lòng tải phiên bản mới !",
                        GameCanvas.cTaiVersionMoi, link);

                    break;
                case Cmd.DAPDO: //-----------------------------------------------> CMD = 66
                    var type_dapdo = msg.reader().readByte();
                    switch (type_dapdo)
                    {
                        case TabNangCap.SUB_UPDATE_INFO_UPGRADE
                        : // ----------------------> Server trả về 3, xử lý sau khi nâng cấp, đã pass

                            int leng_item = msg.reader().readByte();
                            var listiditem = new short[leng_item];
                            for (var i = 0; i < leng_item; i++)
                            {
                                var tempIdrece = msg.reader().readShort();
                                listiditem[i] = tempIdrece;
                            }
                            var tab_nc = (TabNangCap) GameCanvas.AllInfo.VecTabScreen.elementAt(4);
                            if (tab_nc != null)
                                tab_nc.updateListItemNangCap(listiditem);
                            break;
                        case TabNangCap.SUB_UPGRADE_FAIL: // -----------------------------> Server trả về 5
                            TabNangCap.isFail = true;
                            break;
                        case TabNangCap.SUB_UPGRADE_SUCCESS: // -------------------------------> Server trả về 4
                            TabNangCap.isSuccess = true;
                            break;
                    }
                    break;
                case Cmd.CLAN:
                    var typeClan = msg.reader().readByte();
                    switch (typeClan)
                    {
                        case ClanScreen.REQUETS_NAME:
                            // nhap ten clan
                            GameCanvas.startDlgTField("Tạo Bang",
                                new Command("Tạo", GameCanvas.instance, GameCanvas.cTaoClan, null), null,
                                new Command("Cancel", GameCanvas.instance, GameCanvas.cEndDglTField, null));
                            return;
                        case ClanScreen.GET_LIST_LOCAL:
                            ClanScreen.gI().listUser_local.removeAllElements();
                            ClanScreen.gI().clanLocalName = "";
                            ClanScreen.gI().clanLocalName = msg.reader().readUTF();
                            int size_User = msg.reader().readByte();
                            ClanScreen.gI().isBoss = false;
                            ClanScreen.gI().listUser_local.removeAllElements();
                            var idClanUserLocal = msg.reader().readShort();
                            if (idClanUserLocal == Char.myChar().cIdDB)
                                ClanScreen.gI().isBoss = true;
                            Debug.Log("Size local: " + size_User);
                            for (var i = 0; i < size_User; i++)
                            {
                                var iduser = msg.reader().readShort();
                                var nameClanUser = msg.reader().readUTF();
                                short lvClanUser = msg.reader().readByte();
                                var isHost = false;
                                var partHead = msg.reader().readShort();
                                Debug.Log("PartHead local: " + partHead);

                                if (Char.myChar().cIdDB != iduser)
                                {
                                    var otherClan = new OtherChar(idClanUserLocal, nameClanUser, lvClanUser,
                                        isHost, iduser);
                                    ClanScreen.gI().listUser_local.add(otherClan);
                                }
                            }
                            break;
                        case ClanScreen.GET_LIST_GOBAL:
                            ClanScreen.gI().listUser_gobal.removeAllElements();
                            ClanScreen.gI().clanGolbalName = "";
                            ClanScreen.gI().clanGolbalName = msg.reader().readUTF();
                            int size_User_gobal = msg.reader().readByte();

                            ClanScreen.gI().isBossGobal = false;
                            ClanScreen.gI().listUser_gobal.removeAllElements();

                            var idClanUser = msg.reader().readShort();
                            if (idClanUser == Char.myChar().cIdDB)
                                ClanScreen.gI().isBossGobal = true;
                            for (var i = 0; i < size_User_gobal; i++)
                            {
                                var iduser = msg.reader().readShort();
                                var nameClanUser = msg.reader().readUTF();
                                var lvClanUser = msg.reader().readShort();
                                var partHead = msg.reader().readShort();
                                var isHost = false;

                                if (Char.myChar().cIdDB != iduser)
                                {
                                    var otherClan = new OtherChar(idClanUser, nameClanUser, lvClanUser, isHost,
                                        iduser);
                                    ClanScreen.gI().listUser_gobal.add(otherClan);
                                }
                            }
                            break;
                    }

                    break;
                case Cmd.PAYMENT_GET:
                    var isSuccess = msg.reader().readBool();
                    if (isSuccess)
                        GameCanvas.StartDglThongBao("Bạn đã nạp thẻ thành công !!!");
                    else GameCanvas.StartDglThongBao("Bạn nạp không thành công, vui lòng kiểm tra lại !");

                    break;
                case Cmd.PAYMENT_INFO:
                    var json = msg.reader().readUTF();
                    TabBag.list_item = JSONParser.parseItemPayment(json);
                    break;

                case Cmd.PAYMENT:
                    TabBag.isShowPayment = true;
                    break;
                case Cmd.BOSS_APPEAR:
                    var isturnOnBoss = msg.reader().readBool();
                    Mob.isBossAppear = isturnOnBoss;
                    var appear = msg.reader().readShort();
                    if (Mob.isBossAppear)
                        for (var i = 0; i < GameScr.vMob.size(); i++)
                        {
                            var dem_mob = (Mob) GameScr.vMob.elementAt(i);
                            if (dem_mob != null && dem_mob.isBoss)
                            {
                                dem_mob.injureThenDie = false;
                                dem_mob.status = Mob.MA_WALK;
                                dem_mob.x = dem_mob.xFirst;
                                dem_mob.y = dem_mob.yFirst;
                                ServerEffect.addServerEffect(37, dem_mob.x, dem_mob.y - dem_mob.getH() / 4 - 10, 1);
                                dem_mob.hp = dem_mob.maxHp;
                                break;
                            }
                        }
                    break;
                case Cmd.DIALOG_SERVER:

                    cmdTroupleShoot = msg.reader().readByte(); // noi dung dialog giai quyet cho cmd 
                    Debug.Log("CMDDDD:" + cmdTroupleShoot);
                    string noidung;
                    if (cmdTroupleShoot == Cmd.CLAN)
                    {
                        cmdClanAvtive = msg.reader().readByte();
                        typeClanGL = msg.reader().readByte();
                        if (cmdClanAvtive == Clan.KICKED)
                        {
                            var iduser = msg.reader().readShort();
                        }
                        else if (cmdClanAvtive == Clan.INVITE)
                        {
                            Clan.idClan = msg.reader().readShort();
                        }
                        noidung = msg.reader().readUTF();
                    }
                    else
                    {
                        noidung = msg.reader().readUTF();
                    }

                    Debug.Log("type: " + typeClanGL);
                    GameCanvas.startYesNoDlg(noidung,
                        new Command("Yes", GameCanvas.instance, GameCanvas.cmdYesDialogServer, noidung),
                        new Command("No", GameCanvas.instance, GameCanvas.cEndDglTField, noidung));
                    break;
                case Cmd.CHAR_WEARING:
                    LogDebug.Log("CONTROLLER CHAR_WEARING");
                   // var listCharWearingData = IOUtils.ReadMessage(msg);
                   // var listCharWearingDTO = MessagePackSerializer
                   //     .Deserialize<ListCharWearingDTO>(listCharWearingData);
                    var listCharWearingData = IOUtils.ReadMessage(msg);
                    var charWearingDTO = MessagePackSerializer.Deserialize<CharWearingResponseDTO>(listCharWearingData);
                    //LogDebug.println("CHAR WEARING CONTROLLER ------> "+listCharWearingDTO.listCharWearingDTO.Count);
                   // for (var j = 0;  j < listCharWearingDTO.listCharWearingDTO.Count; j++)
                   // {
                   //     var charWearingResponseDTO = listCharWearingDTO.listCharWearingDTO[j];
                        LogDebug.println("CHAR WEARING CHAR BY CHAR -----> "+charWearingDTO.length);
                    
                    if (Char.myChar().charID != charWearingDTO.id)
                    {
                        LogDebug.println("NHAN THONG TIN THANG KHAC TRONG MAP");
                        var isAdded = false;
                        for (var i = 0; i < GameScr.vCharInMap.size(); i++)
                        {
                            var dem = (Char) GameScr.vCharInMap.elementAt(i);
                            if (dem != null && dem.charID == charWearingDTO.id)
                            {
                                isAdded = true;
                                c = dem;
                            }
                        }
                        if (!isAdded)
                        {
                            c = new Char();
                            c.charID = charWearingDTO.id;
                        }
                        readcharInmap(c, charWearingDTO);
                        if (!isAdded)
                            GameScr.vCharInMap.addElement(c);
                        LogDebug.println("  char.Id CHAR_WEARING add new char" + GameScr.vCharInMap.size());
                    }
                    else
                    {

                        Char.myChar().arrItemBody = new Item[charWearingDTO.length];
                        
                        for (var i = 0; i < Char.myChar().arrItemBody.Length; i++)
                            {
                               // for (var t = 0; t < charWearingDTO.listItem.Count; t++)
                               // {
                                    var idtemplate = charWearingDTO.listItem[i];
                                    LogDebug.println(" CHAR WEARING FOR ME -------> "+idtemplate);
                                    if (idtemplate != -1)
                                    {
                                        var template = ItemTemplates.get(idtemplate);

                                        var indexUI = Item.getPosWearingItem(template.type);

                                        if (indexUI >= Char.myChar().arrItemBody.Length)
                                            continue;

                                        Char.myChar().arrItemBody[indexUI] = new Item();

                                        Char.myChar().arrItemBody[indexUI].indexUI = indexUI;

                                        Char.myChar().arrItemBody[indexUI].typeUI = Item.UI_BODY;
                                        Char.myChar().arrItemBody[indexUI].template = template;
                                        Char.myChar().arrItemBody[indexUI].isLock = true;
                                        if (template.type == Item.TYPE_AO)
                                        {
                                            Char.myChar().body = GameScr.currentCharViewInfo.arrItemBody[indexUI]
                                                .template.part;
                                            Char.myChar().leg = GameScr.currentCharViewInfo.arrItemBody[indexUI]
                                                .template.partquan;
                                            Char.myChar().head = GameScr.currentCharViewInfo.arrItemBody[indexUI]
                                                .template.partdau;
                                            LogDebug.println(
                                                Char.myChar().leg + "head ----> " + Char.myChar().head);
                                        }
                                        else if (template.type == Item.TYPE_NON)
                                        {
                                            Char.myChar().head = GameScr.currentCharViewInfo.arrItemBody[indexUI]
                                                .template.part;
                                        }
                                    }

                                //}

                            }
                    //}
                    }
                  
                   
                    break;
            }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (msg != null)
                msg.cleanup();
        }
    }


    public static Controller gI()
    {
        if (me == null)
            me = new Controller();
        return me;
    }

    public void ThongTinKhu(Message msg)
    {
        try
        {
            LogDebug.println("ThongTinKhu");
            var nkhu = msg.reader().readByte();
            LogDebug.println("ThongTinKhu  " + nkhu);
            KhuScreen.gI().listKhu = new sbyte[nkhu][];
            for (var i = 0; i < nkhu; i++)
                KhuScreen.gI().listKhu[i] = new sbyte[1];
            for (var i = 0; i < nkhu; i++)
                KhuScreen.gI().listKhu[i][0] = msg.reader().readByte();

            LogDebug.println("ThongTinKhu KhuScreen ");
            KhuScreen.gI().srclist.selectedItem = -1;
            KhuScreen.gI().switchToMe();
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
    }

    public void ThachDau(Message msg)
    {
        try
        {
            var type = msg.reader().readByte();
            var idchar = msg.reader().readShort();
            var loimoi = msg.reader().readUTF();
            GameCanvas.startYesNoDlg(loimoi,
                new Command("Đồng ý", GameCanvas.instance, GameCanvas.cDongYThachDau, idchar + ""),
                new Command("Không", GameCanvas.instance, 8882, null));
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
    }

    public void StatusAttack(Message msg)
    {
        try
        {
            var idchar = msg.reader().readLong();
            var typeAttack = msg.reader().readByte();
            LogDebug.println(" ------- > "+typeAttack);
            if (Char.myChar().charID == idchar)
                FlagScreen.time = mSystem.currentTimeMillis();
            var ch = GameScr.findCharInMap(idchar);
            if (ch != null) ch.typePk = typeAttack;
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
    }

    public void ChangeKhu(Message msg)
    {
        try
        {
            TileMap.zoneID = msg.reader().readByte();
            Char.myChar().cx = msg.reader().readShort();
            Char.myChar().cy = msg.reader().readShort();

            for (var i = 0; i < GameScr.vMob.size(); i++)
            {
                var dem = (Mob) GameScr.vMob.elementAt(i);
                dem.ResetToDie();
            }
            GameScr.vItemMap.removeAllElements();
            GameScr.vCharInMap.removeAllElements();
            XinChoScreen.isChangeKhu = true;
            XinChoScreen.gI().switchToMe();
            GameScr.vCharInMap.add(Char.myChar());
            Char.ischangingMap = false;
            Char.myChar().statusMe = Char.A_FALL;
            GameScr.loadCamera(true, Char.myChar().cx, Char.myChar().cy);
            GameCanvas.endDlg();
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
    }

    private void createData(DataInputStream d)
    {
        try
        {
            Rms.saveRMS("nj_arrow", Utils.readByteArray(d));
            Rms.saveRMS("nj_effect", Utils.readByteArray(d));
            Rms.saveRMS("nj_image", Utils.readByteArray(d));
            Rms.saveRMS("nj_part", Utils.readByteArray(d));
            Rms.saveRMS("nj_skill", Utils.readByteArray(d));
        }
        catch (Exception e)
        {
        }
    }

    private static void readcharInmap(Char c, CharWearingResponseDTO charWearingResponseDTO)
    {
        // add player trong map

        LogDebug.println("DEBUG READ CHAR IN MAPPPP -----> ");
        try
        {
            c.arrItemBody = new Item[13];

            for (var i = 0; i < c.arrItemBody.Length; i++)
            {
                for (int j = 0; j < charWearingResponseDTO.listItem.Count; j++)
                {
                    var idtemlate = charWearingResponseDTO.listItem[j];
                    LogDebug.println("ID CHAR WEARING THANG KHAC ----> "+idtemlate);
                    if (idtemlate != -1)
                    {
                        var template = ItemTemplates.get(idtemlate);
                        var indexUI = Item.getPosWearingItem(template.type);


                        c.arrItemBody[indexUI] = new Item();

                        c.arrItemBody[indexUI].indexUI = indexUI;

                        c.arrItemBody[indexUI].typeUI = Item.UI_BODY;
                        c.arrItemBody[indexUI].template = template;
                        c.arrItemBody[indexUI].isLock = true;
                        if (template.type == Item.TYPE_AO)
                        {
                            c.body = c.arrItemBody[indexUI].template.part;
                            c.leg = c.arrItemBody[indexUI].template.partquan;
                            c.head = c.arrItemBody[indexUI].template.partdau;
                        }
                        else if (template.type == Item.TYPE_NON)
                        {
                            c.head = c.arrItemBody[indexUI].template.part;
                        }
                    }
                }
                
            }
        }
        catch (Exception e)
        {
        }
    }

}