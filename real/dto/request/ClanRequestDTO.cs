﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class ClanRequestDTO
    {
        [Key("requestclan")]
        public sbyte RequestClan { get; set; }

        [Key("requestname")]
        public sbyte RequestName { get; set; }

        [Key("inviteclan")]
        public sbyte InviteClan { get; set; }

        [Key("inviteclangolbal")]
        public sbyte InviteClanGolbal { get; set; }

        [Key("kick")]
        public sbyte Kick { get; set; }

        [Key("deleteclan")]
        public sbyte DeleteClan { get; set; }

        [Key("leave")]
        public sbyte Leave { get; set; }

        [Key("getlistlocal")]
        public sbyte GetListLocal { get; set; }

        [Key("getlistgolbal")]
        public sbyte GetListGolbal { get; set; }

        [Key("name")]
        public string Name { get; set; }

        [Key("idchar")]
        public long IdChar { get; set; }

        [Key("idcharkicked")]
        public long IdCharKicked { get; set; }
    }
}