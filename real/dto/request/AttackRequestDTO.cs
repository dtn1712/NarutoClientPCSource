﻿using System.Collections.Generic;
using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class AttackRequestDTO
    {
        [Key("skillAttackId")]
        public sbyte SkillAttackId { get; set; }

        [Key("targetType")]
        public sbyte TargetType { get; set; }

        [Key("targetIds")]
        public List<long> TargetIds { get; set; }
    }
}