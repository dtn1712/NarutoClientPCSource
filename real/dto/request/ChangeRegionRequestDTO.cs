﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class ChangeRegionRequestDTO
    {
        [Key("regionId")]
        public sbyte RegionId { get; set; }
    }
}