﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class UpgradeItemRequestDTO
    {
        [Key("id")]
        public sbyte Id { get; set; }

        [Key("subupgrade")]
        public sbyte SubUpgrade { get; set; }

        [Key("subtakeon")]
        public sbyte SubTakeOn { get; set; }

        [Key("subtakeoff")]
        public sbyte SubTakeOff { get; set; }

        [Key("indexitem")]
        public sbyte IndexItem { get; set; }
    }
}