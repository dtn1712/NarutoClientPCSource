﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class ImageRequest
    {
        [Key("imageId")]
        public int ImageId { get; set; }
    }
}