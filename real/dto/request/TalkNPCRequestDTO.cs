﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class TalkNPCRequestDTO
    {
        [Key("npcTemplateId")]
        public int NpcTemplateId { get; set; }
    }
}