﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class LoginRequestDTO
    {
        [Key("userName")]
        public string userName { get; set; }

        [Key("passWord")]
        public string passWord { get; set; }

        [Key("zoomLevel")]
        public byte zoomLevel { get; set; }

        [Key("clientVersion")]
        public string clientVersion { get; set; }
    }
}