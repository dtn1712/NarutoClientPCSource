﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class PartyRequestDTO
    {
        [Key("type")]
        public sbyte Type { get; set; }

        [Key("charid")]
        public short CharId { get; set; }
    }
}