﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class BuyItemRequestDTO
    {
        [Key("menuId")]
        public sbyte MenuId { get; set; }

        [Key("itemIndex")]
        public short ItemIndex { get; set; }
    }
}