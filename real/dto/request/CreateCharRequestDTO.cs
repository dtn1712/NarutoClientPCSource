﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class CreateCharRequestDTO
    {
        [Key("country")]
        public sbyte Country { get; set; }

        [Key("charClass")]
        public sbyte CharClass { get; set; }

        [Key("gender")]
        public sbyte Gender { get; set; }

        [Key("charName")]
        public string CharName { get; set; }
    }
}