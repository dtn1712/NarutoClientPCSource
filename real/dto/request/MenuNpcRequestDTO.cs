﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class MenuNpcRequestDTO
    {
        [Key("idnpc")]
        public long IdNpc { get; set; }

        [Key("idmenu")]
        public sbyte IdMenu { get; set; }
    }
}