﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class PlayerInfoRequestDTO
    {
        [Key("otherPlayerId")]
        public long OtherPlayerId { get; set; }
    }
}