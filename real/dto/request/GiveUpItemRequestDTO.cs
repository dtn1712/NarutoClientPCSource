﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class GiveUpItemRequestDTO
    {
        [Key("index")]
        public sbyte Index { get; set; }
    }
}