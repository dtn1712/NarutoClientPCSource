﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class ChatRequestDTO
    {
        [Key("type")]
        public sbyte Type { get; set; }

        [Key("text")]
        public string Text { get; set; }

        [Key("playerChatId")]
        public long PlayerChatId { get; set; }
    }
}