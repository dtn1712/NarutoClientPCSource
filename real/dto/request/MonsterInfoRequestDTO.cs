﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class MonsterInfoRequestDTO
    {
        [Key("idmod")]
        public short IdMod { get; set; }
    }
}