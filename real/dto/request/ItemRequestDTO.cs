﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class ItemRequestDTO
    {
        [Key("index")]
        public sbyte Index { get; set; }
    }
}