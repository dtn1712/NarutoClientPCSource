﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class ShopRequestDTO
    {
        [Key("idmenushop")]
        public sbyte IdMenuShop { get; set; }
    }
}