﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class AddBasePointRequestDTO
    {
        [Key("type")]
        public sbyte Type { get; set; }

        [Key("index")]
        public sbyte Index { get; set; }

        [Key("value")]
        public short Value { get; set; }
    }
}