﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class SkillStudyRequestDTO
    {
        [Key("type")]
        public sbyte Type { get; set; }

        [Key("idskill")]
        public int IdSkill { get; set; }
    }
}