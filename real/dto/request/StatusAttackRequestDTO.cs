﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class StatusAttackRequestDTO
    {
        [Key("indexco")]
        public sbyte IndexCo { get; set; }
    }
}