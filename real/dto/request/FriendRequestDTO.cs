﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class FriendRequestDTO
    {
        [Key("type")]
        public sbyte Type { get; set; }

        [Key("charid")]
        public short CharId { get; set; }

        [Key("mycharid")]
        public short MyCharId { get; set; }
    }
}