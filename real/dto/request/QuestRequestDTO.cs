﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class QuestRequestDTO
    {
        [Key("actionType")]
        public sbyte ActionType { get; set; }

        [Key("questType")]
        public string QuestType { get; set; }
    }
}