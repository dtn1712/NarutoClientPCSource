﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class TradeRequestDTO
    {
        [Key("typetrade")]
        public sbyte TypeTrade { get; set; }

        [Key("charid")]
        public short CharId { get; set; }

        [Key("typemoveitem")]
        public sbyte typeMoveItem { get; set; }

        [Key("iditem")]
        public short Iditem { get; set; }
    }
}