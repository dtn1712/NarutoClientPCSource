﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class SelectCharRequestDTO
    {
        [Key("charId")]
        public long charId { get; set; }
    }
}