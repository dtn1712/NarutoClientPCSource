﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class SendPlayerAttackRequestDTO
    {
        [Key("type")]
        public sbyte Type { get; set; }

        [Key("idskill")]
        public sbyte IdSkill { get; set; }

        [Key("ntarget")]
        public sbyte NTarget { get; set; }

        [Key("mobid")]
        public short MobId { get; set; }

        [Key("charid")]
        public short CharId { get; set; }
    }
}