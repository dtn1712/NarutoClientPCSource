﻿using MessagePack;

namespace src.real.dto.request
{
    [MessagePackObject]
    public class PickItemRequestDTO
    {
        [Key("type")]
        public sbyte Type { get; set; }

        [Key("iditem")]
        public short IdItem { get; set; }
    }
}