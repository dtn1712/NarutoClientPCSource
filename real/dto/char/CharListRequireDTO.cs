﻿using System.Collections.Generic;
using MessagePack;

namespace src.real.dto.@char
{
    public class CharListRequireDTO
    {
        [Key("chars")] 
        public List<ListCharDTO> listChar;
    }
}


