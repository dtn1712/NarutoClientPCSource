﻿using MessagePack;

namespace src.real.dto
{
    public class CharComeHomeRequestDTO {
        [Key("hp")]
        public short mp { get; set; }

        [Key("mp")]
        public short hp { get; set; }
    }
}


