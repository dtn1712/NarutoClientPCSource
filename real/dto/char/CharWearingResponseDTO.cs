﻿
using System.Collections.Generic;
using MessagePack;

namespace src.real.dto
{   
    [MessagePackObject] 
    public class CharWearingResponseDTO {
        [Key("id")]
        public long id { get; set; }

        [Key("length")]
        public sbyte length { get; set; }

        [Key("listItem")]
        public List<short> listItem { get; set; }
    }
}


