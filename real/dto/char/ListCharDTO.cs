﻿using MessagePack;

namespace src.real.dto
{
    [MessagePackObject]   
    public class ListCharDTO {
        
        [Key("id")]
        public long Id { get; set; }

        [Key("charName")]
        public string name { get; set; }

        [Key("gender")]
        public byte gender { get; set; }

        [Key("charClass")]
        public byte charClass { get; set; }

        [Key("level")]
        public short level { get; set; }

        [Key("headId")]
        public short headId { get; set; }

        [Key("bodyId")]
        public short bodyId { get; set; }

        [Key("legId")]
        public short legId { get; set; }

    }
}


