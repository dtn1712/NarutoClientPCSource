﻿using MessagePack;

namespace src.real.dto.@char
{
    public class PickUpItemResponseDTO
    {

	    [Key("id")] 
	    public long id { get; set; }

	    [Key("type")]
	    public byte type { get; set; }

	    [Key("idItem")]
	    public short idItem { get; set; }

    }

}

