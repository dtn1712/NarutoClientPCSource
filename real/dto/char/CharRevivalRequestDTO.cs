﻿using MessagePack;


namespace src.real.dto.@char
{
    public class CharRevivalRequestDTO  
    {
	    [Key("id")]
	    public long id { get; set; }

	    [Key("hp")]
	    public short hp { get; set; }
	    
	    [Key("mp")]
	    public short mp { get; set; }

    }
}


