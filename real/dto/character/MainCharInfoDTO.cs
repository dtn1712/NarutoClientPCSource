﻿using System.Collections.Generic;
using MessagePack;
namespace src.real.dto
{
    [MessagePackObject]
    public class MainCharInfoDTO
    {
        [Key("id")]
        public long Id { get; set; }

        [Key("charName")]
        public string CharName { get; set; }

        [Key("idClass")]
        public byte IdClass { get; set; }

        [Key("gender")]
        public byte Gender { get; set; }

        [Key("level")]
        public short Level { get; set; }

        [Key("percent")]
        public byte Percent { get; set; }

        [Key("hp")]
        public int Hp { get; set; }

        [Key("maxHp")]
        public int MaxHp { get; set; }

        [Key("mp")]
        public int Mp { get; set; }

        [Key("maxMp")]
        public int MaxMp { get; set; }

        [Key("khaiMon")]
        public int KhaiMon { get; set; }

        [Key("huuMon")]
        public int HuuMon { get; set; }

        [Key("sinhMon")]
        public int SinhMon { get; set; }

        [Key("doMon")]
        public int DoMon { get; set; }

        [Key("canhMon")]
        public int CanhMon { get; set; }

        [Key("thuongMon")]
        public int ThuongMon { get; set; }

        [Key("kinhMon")]
        public int KinhMon { get; set; }

        [Key("tuMon")]
        public int TuMon { get; set; }

        [Key("basePoint")]
        public int BasePoint { get; set; }

        [Key("country")]
        public byte Country { get; set; }

        [Key("sizeAttribute")]
        public int SizeAttribute { get; set; }


        [Key("charAttribute")]
        public List<CharAttributeDTO> CharAttribute { get; set; }

        [MessagePackObject]
        public class CharAttributeDTO
        {
            [Key("id")]
            public byte Id { get; set; }

            [Key("value")]
            public int Value { get; set; }

        }
    }
}