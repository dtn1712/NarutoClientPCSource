using System;
using System.Collections.Generic;
using MessagePack;
using src.lib;
using src.network;
using src.real.dto;
using src.real.dto.request;
using src.utils;
using UnityEngine;

public class Service
{
    private static Service instance;
    private readonly ISession session = Session.gI();


    public static Service getInstance()
    {
        return instance ?? (instance = new Service());
    }

    public void doLogin(string user, string pass, byte zoomlevel)
    {
        IOUtils.WriteMessage(Cmd.LOGIN, MessagePackSerializer.Serialize(new LoginRequestDTO
        {
            userName = user,
            passWord = pass,
            zoomLevel = zoomlevel,
            clientVersion = GameMidlet.VERSION
        }));
    }

    public void createChar(sbyte typeChar, sbyte country, sbyte gender, string charName)
    {
        // tạo char
        IOUtils.WriteMessage(Cmd.CREATE_CHAR, MessagePackSerializer.Serialize(new CreateCharRequestDTO
        {
            Country = country,
            CharClass = typeChar,
            Gender = gender,
            CharName = charName
        }));
    }

    public void selectChar(long charIDDB)
    {
        // select char to play
        IOUtils.WriteMessage(Cmd.SELECT_CHAR, MessagePackSerializer.Serialize(new SelectCharRequestDTO
        {
            charId = charIDDB
        }));
    }

    public void charMove()
    {
        // gui char di chuyen
        var dx = Char.myChar().cx - Char.myChar().cxSend;
        var dy = Char.myChar().cy - Char.myChar().cySend;
        var isGuilen = CRes.abs(dx) > 24 || CRes.abs(dy) > 24;
        if (Char.ischangingMap || dx == 0 && dy == 0 || !isGuilen && Char.myChar().statusMe != Char.A_STAND &&
            Char.myChar().statusMe != Char.A_DEAD &&
            Char.myChar().statusMe != Char.A_DEADFLY)
            return;
        try
        {
            if (GameScr.isSendMove)
                GameScr.isSendMove = false;

            Char.myChar().cxSend = Char.myChar().cx;
            Char.myChar().cySend = Char.myChar().cy;
            Char.myChar().cdirSend = Char.myChar().cdir;
            Char.myChar().cactFirst = Char.myChar().statusMe;
            IOUtils.WriteMessage(Cmd.PLAYER_MOVE, MessagePackSerializer.Serialize(new CharMoveRequestDTO
            {
                PosX = (short) Char.myChar().cx,
                PosY = (short) Char.myChar().cy
            }));
        }
        catch (Exception ex)
        {
            //ex.printStackTrace();
        }
    }

    public void requestImage(int id, sbyte sub)
    {
        // yeu cau hinh tu server
        IOUtils.WriteMessage(Cmd.REQUEST_IMAGE, MessagePackSerializer.Serialize(new ImageRequest
        {
            ImageId = id
        }));
    }

    // server tự biết nó đứng đâu mà chuyển đến map nào
    public void requestChangeMap()
    {
        IOUtils.WriteMessage(Cmd.CHANGE_MAP);
    }

    // yeu cau request Inventory
    public void requestinventory()
    {
        IOUtils.WriteMessage(Cmd.GET_ITEM_INVENTORY);
    }

    public void chat(string text, sbyte type)
    {
        // yeu cau chat trong map
        IOUtils.WriteMessage(Cmd.CHAT, MessagePackSerializer.Serialize(new ChatRequestDTO
        {
            Type = type,
            Text = text
        }));
    }

    public void chatGlobal(string text, sbyte type)
    {
        // yeu cau chat kenh the gioi
        IOUtils.WriteMessage(Cmd.CHAT, MessagePackSerializer.Serialize(new ChatRequestDTO
        {
            Type = type,
            Text = text
        }));
    }

    public void requetsInfoMod(short idMod)
    {
        // yeu cau thong tin quai khi focus
        IOUtils.WriteMessage(Cmd.REQUEST_MONSTER_INFO, MessagePackSerializer.Serialize(new MonsterInfoRequestDTO
        {
            IdMod = idMod
        }));
    }

    //Đang stuck send DTO MsgPack
    public void sendPlayerAttack(Vector vMob, Vector vChar, int type, int idskill, bool isMob)
    {
        // 0 player
        // 1 monter
        
        GameScr.isSendMove = true;
        var targetIds = new List<long>();
        if (isMob)
        {
            for (var i = 0; i < vMob.size(); i++)
            {
                var b = (Mob) vMob.elementAt(i);
                if (b != null)
                    targetIds.Add(b.mobId);
            }
        }
        else
        {
            for (var i = 0; i < vChar.size(); i++)
            {
                var b = (Char) vChar.elementAt(i);
                if (b != null)
                    targetIds.Add(b.charID);
            }
        }

        IOUtils.WriteMessage(Cmd.ATTACK, MessagePackSerializer.Serialize(new AttackRequestDTO
        {
            TargetType = (sbyte) type,
            SkillAttackId =  (sbyte) idskill,
            TargetIds = targetIds
        }));
    }

    public void itemPick(sbyte type, short idItem)
    {
        // yêu cầu nhặt item
        IOUtils.WriteMessage(Cmd.PICK_REMOVE_ITEM, MessagePackSerializer.Serialize(new PickItemRequestDTO
        {
            Type = type,
            IdItem = idItem
        }));
    }


    public void giveUpItem(sbyte index)
    {
        IOUtils.WriteMessage(Cmd.GIVEUP_ITEM, MessagePackSerializer.Serialize(new GiveUpItemRequestDTO
        {
            Index = index
        }));
    }

    public void useItem(sbyte index)
    {
        IOUtils.WriteMessage(Cmd.ITEM, MessagePackSerializer.Serialize(new ItemRequestDTO
        {
            Index = index
        }));
    }

    public void inviteParty(short charID, sbyte type)
    {
        IOUtils.WriteMessage(Cmd.PARTY, MessagePackSerializer.Serialize(new PartyRequestDTO
        {
            Type = type,
            CharId = charID
        }));
    }

    public void AcceptParty(sbyte type, short charID)
    {
        IOUtils.WriteMessage(Cmd.PARTY, MessagePackSerializer.Serialize(new PartyRequestDTO
        {
            Type = type,
            CharId = charID
        }));
    }

    public void leaveParty(sbyte type, short charID)
    {
        IOUtils.WriteMessage(Cmd.PARTY, MessagePackSerializer.Serialize(new PartyRequestDTO
        {
            Type = type,
            CharId = charID
        }));
    }

    public void kickPlayeleaveParty(sbyte type, short charID)
    {
        IOUtils.WriteMessage(Cmd.PARTY, MessagePackSerializer.Serialize(new PartyRequestDTO
        {
            Type = type,
            CharId = charID
        }));
    }

    public void removeParty(sbyte type)
    {
        IOUtils.WriteMessage(Cmd.PARTY, MessagePackSerializer.Serialize(new PartyRequestDTO
        {
            Type = type,
            CharId = (short) Char.myChar().charID
        }));
    }

    public void requestjoinParty(sbyte type, short CharID)
    {
        // yeu cau vao party
        IOUtils.WriteMessage(Cmd.PARTY, MessagePackSerializer.Serialize(new PartyRequestDTO
        {
            Type = type,
            CharId = CharID
        }));
    }


    public void requestAddfriend(sbyte type, short CharID)
    {
        IOUtils.WriteMessage(Cmd.FRIEND, MessagePackSerializer.Serialize(new FriendRequestDTO
        {
            Type = type,
            CharId = CharID
        }));
    }

    public void AcceptFriend(sbyte type, short CharID)
    {
        IOUtils.WriteMessage(Cmd.FRIEND, MessagePackSerializer.Serialize(new FriendRequestDTO
        {
            Type = type,
            CharId = CharID
        }));
    }

    public void requestFriendList(sbyte type, short CharID)
    {
        IOUtils.WriteMessage(Cmd.FRIEND, MessagePackSerializer.Serialize(new FriendRequestDTO
        {
            Type = type,
            CharId = CharID
        }));
    }

    public void deleteFriend(sbyte type, short myCharID, short CharID)
    {
        // xóa bạn 
        IOUtils.WriteMessage(Cmd.FRIEND, MessagePackSerializer.Serialize(new FriendRequestDTO
        {
            Type = type,
            MyCharId = myCharID,
            CharId = CharID
        }));
    }

    public void chatPrivate(sbyte type, short userid, string contend)
    {
        IOUtils.WriteMessage(Cmd.CHAT, MessagePackSerializer.Serialize(new ChatRequestDTO
        {
            Type = type,
            PlayerChatId = userid,
            Text = contend
        }));
    }

    //Huy Code
    //Request chuyển tiền (khi click ok)
    public void requestOkXu(long amount)
    {
        var m = new Message(Cmd.TRADE);
        try
        {
            Debug.Log("CMD->" + Cmd.TRADE + " Type->" + TradeGui.TRADE_XU + " Action->" + TradeGui.REQUEST_MOVE_MONEY +
                      " Số lượng->" + amount);
            // --> 34
            m.writer().writeByte(TradeGui.TRADE_XU); // -> 7
            //m.writer().writeShort(TradeGui.REQUEST_MOVE_MONEY); //-> 1
            m.writer().writeUTF("FUK");
            //m.writer().writeLong(amount);
            session.sendMessage(m);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
        finally
        {
            m.cleanup();
        }
    }

    //End Huy Code
    public void inviteTrade(sbyte typeTrade, short CharID)
    {
        // mời trao đổi
        IOUtils.WriteMessage(Cmd.TRADE, MessagePackSerializer.Serialize(new TradeRequestDTO
        {
            TypeTrade = typeTrade,
            CharId = CharID
        }));
    }

    public void acceptTrade(sbyte typeTrade, short CharID)
    {
        // đồng ý trao đổi 

        IOUtils.WriteMessage(Cmd.TRADE, MessagePackSerializer.Serialize(new TradeRequestDTO
        {
            TypeTrade = typeTrade,
            CharId = CharID
        }));
    }

    public void moveItemTrade(sbyte typeTrade, short charID, sbyte typeMoveItem, short Iditem)
    {
        // chuyển item trao đổi

        IOUtils.WriteMessage(Cmd.TRADE, MessagePackSerializer.Serialize(new TradeRequestDTO
        {
            TypeTrade = typeTrade,
            CharId = charID,
            typeMoveItem = typeMoveItem,
            Iditem = Iditem
        }));
    }

    public void cancelTrade(sbyte typeTrade, short CharID)
    {
        // hủy trao đổi 

        IOUtils.WriteMessage(Cmd.TRADE, MessagePackSerializer.Serialize(new TradeRequestDTO
        {
            TypeTrade = typeTrade,
            CharId = (short) Char.myChar().partnerTrade.charID
        }));
    }

    public void comfirmTrade(sbyte typeTrade, short CharID)
    {
        // lock
        IOUtils.WriteMessage(Cmd.TRADE, MessagePackSerializer.Serialize(new TradeRequestDTO
        {
            TypeTrade = typeTrade,
            CharId = CharID
        }));
    }

    public void doTrade(sbyte typeTrade, short CharID)
    {
        // giao dịch 
        IOUtils.WriteMessage(Cmd.TRADE, MessagePackSerializer.Serialize(new TradeRequestDTO
        {
            TypeTrade = typeTrade,
            CharId = CharID
        }));
    }

    public void requestShop(int idMenuShop)
    {
        // thong tin shop
        IOUtils.WriteMessage(Cmd.REQUEST_SHOP, MessagePackSerializer.Serialize(new ShopRequestDTO
        {
            IdMenuShop = (sbyte) idMenuShop
        }));
    }

    public void BuyItemShop(sbyte idmenu, short iditem)
    {
        // thong tin shop
        IOUtils.WriteMessage(Cmd.BUY_ITEM_FROM_SHOP, MessagePackSerializer.Serialize(new BuyItemRequestDTO
        {
            MenuId = idmenu,
            ItemIndex = iditem
        }));
    }

    public void requestMenuShop()
    {
        IOUtils.WriteMessage(Cmd.CMD_DYNAMIC_MENU);
    }

    public void Quest(sbyte type, string questType)
    {
        IOUtils.WriteMessage(Cmd.QUEST, MessagePackSerializer.Serialize(new QuestRequestDTO
        {
            ActionType = type,
            QuestType = questType
        }));
    }

    public void PlayerOut()
    {
        IOUtils.WriteMessage(Cmd.PLAYER_REMOVE);
    }

    public void HocSkill(sbyte type, int idskill)
    {
        IOUtils.WriteMessage(Cmd.CHAR_SKILL_STUDIED, MessagePackSerializer.Serialize(new SkillStudyRequestDTO
        {
            Type = type,
            IdSkill = idskill
        }));
    }

    public void revivalChar()
    {
        IOUtils.WriteMessage(Cmd.CHAR_REVILE, MessagePackSerializer.Serialize(new byte[]
        {
           
        }));
    }
    public void goHome()
    {
        IOUtils.WriteMessage(Cmd.COME_HOME_DIE, MessagePackSerializer.Serialize(new byte[]
        {
           
        }));
    }

    public void HoiSinh(sbyte type) //0 ve lang//1 hoi sinh tai cho
    {
        IOUtils.WriteMessage(Cmd.COME_HOME_DIE, MessagePackSerializer.Serialize(new HoiSinhRequestDTO
        {
            Type = type
        }));
    }

    public void requestRegister(string username, string password)
    {
        IOUtils.WriteMessage(Cmd.REGISTER, MessagePackSerializer.Serialize(new RegisterRequestDTO()
        {
            Username = username,
            Password = password,
            ClientVersion = GameMidlet.VERSION
        }));
    }

    public void requestPlayerInfo(short idchar)
    {
        IOUtils.WriteMessage(Cmd.PLAYER_INFO, MessagePackSerializer.Serialize(new PlayerInfoRequestDTO
        {
            OtherPlayerId = idchar
        }));
    }

    public void requestRegionInfo()
    {
        IOUtils.WriteMessage(Cmd.REQUEST_REGION);
    }

    public void requestChangeRegion(sbyte regionId)
    {
        IOUtils.WriteMessage(Cmd.CHANGE_REGION, MessagePackSerializer.Serialize(new ChangeRegionRequestDTO
        {
            RegionId = regionId
        }));
    }

    // số 1 chưa thống nhất vơi server là đặt tên gì
    public void requestAddBasePoint(sbyte idnex, short value)
    {
        IOUtils.WriteMessage(Cmd.ADD_BASE_POINT, MessagePackSerializer.Serialize(new AddBasePointRequestDTO
        {
            Type = 1,
            Index = idnex,
            Value = value
        }));
    }

    public void ThachDau(sbyte index, short idchar)
    {
        IOUtils.WriteMessage(Cmd.COMPETED_ATTACK, MessagePackSerializer.Serialize(new CompetedAttackRequestDTO
        {
            Type = index,
            PlayerInvitedId = idchar
        }));
    }

    public void ChangeFlag(sbyte indexco)
    {
        IOUtils.WriteMessage(Cmd.STATUS_ATTACK, MessagePackSerializer.Serialize(new StatusAttackRequestDTO
        {
            IndexCo = indexco
        }));
    }

    public void SellItem(sbyte index)
    {
        IOUtils.WriteMessage(Cmd.SELL_ITEM, MessagePackSerializer.Serialize(new SellItemRequestDTO
        {
            Index = index
        }));
    }

    public void GoItemTrangBi(sbyte id)
    {
        IOUtils.WriteMessage(Cmd.DAPDO, MessagePackSerializer.Serialize(new UpgradeItemRequestDTO
        {
            Id = id
        }));
    }

    public void MenuNpc(long idNpc, sbyte idmenu)
    {
        IOUtils.WriteMessage(Cmd.MENU_NPC, MessagePackSerializer.Serialize(new MenuNpcRequestDTO
        {
            IdNpc = idNpc,
            IdMenu = idmenu
        }));
    }

    public void TalkNpc(int idNpc)
    {
        IOUtils.WriteMessage(Cmd.TALK_NPC, MessagePackSerializer.Serialize(new TalkNPCRequestDTO
        {
            NpcTemplateId = idNpc
        }));
    }

    //Nâng cấp item
    public void NangCap_NangCap()
    {
        IOUtils.WriteMessage(Cmd.DAPDO, MessagePackSerializer.Serialize(new UpgradeItemRequestDTO
        {
            SubUpgrade = TabNangCap.SUB_UPGRADE
        }));
    }

    public void NangCap_Dualen(sbyte indexItem)
    {
        IOUtils.WriteMessage(Cmd.DAPDO, MessagePackSerializer.Serialize(new UpgradeItemRequestDTO
        {
            SubTakeOn = TabNangCap.SUB_TAKE_ON,
            IndexItem = indexItem
        }));
    }

    public void NangCap_ThaoXuong(sbyte indexItem)
    {
        IOUtils.WriteMessage(Cmd.DAPDO, MessagePackSerializer.Serialize(new UpgradeItemRequestDTO
        {
            SubTakeOff = TabNangCap.SUB_TAKE_OFF,
            IndexItem = indexItem
        }));
    }

    public void Clan_getInfo()
    {
        IOUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
        {
            RequestClan = ClanScreen.REQUEST_CLAN
        }));
    }

    public void Clan_createClanName(string name)
    {
        IOUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
        {
            RequestName = ClanScreen.REQUETS_NAME,
            Name = name
        }));
    }

    public void Clan_MoiBang(short idChar)
    {
        IOUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
        {
            InviteClan = ClanScreen.INVITA_CLAN,
            IdChar = idChar
        }));
    }

    public void Clan_MoiBang_GOBAL(short idChar)
    {
        Debug.Log("TOI MOI: " + ClanScreen.INVITE_CLAN_GLOBAL + " / idChar: " + idChar);
        Debug.Log("CharFocus IDDB: " + Char.myChar().charFocus.cIdDB + " | idGlobal: " +
                  Char.myChar().charFocus.cIdClanGlobal + " |idLocal: " + Char.myChar().charFocus.cIdClanLocal);
        IOUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
        {
            InviteClanGolbal = ClanScreen.INVITE_CLAN_GLOBAL,
            IdChar = idChar
        }));
    }

    public void Clan_KichKhoiBang(short idChar)
    {
        IOUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
        {
            Kick = (sbyte) ClanScreen.gI().KICK_CLAN,
            IdCharKicked = ClanScreen.idCharClan
        }));
    }

    public void Clan_XoaBang()
    {
        IOUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
        {
            DeleteClan = ClanScreen.DELETE_CLAN
        }));
    }

    public void Clan_RoiBang()
    {
        IOUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
        {
            Leave = (sbyte) ClanScreen.gI().LEAVE_CLAN
        }));
    }

    public void Clan_GetlistUser_local()
    {
        IOUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
        {
            GetListLocal = ClanScreen.GET_LIST_LOCAL
        }));
    }

    public void Clan_GetlistUser_gobal()
    {
        IOUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
        {
            GetListGolbal = ClanScreen.GET_LIST_GOBAL
        }));
    }

    public void Clan_GetlistClan_gobal()
    {
        IOUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
        {
            GetListGolbal = ClanScreen.GET_LISTCLAN_GOLBAL
        }));
    }

    public void Clan_GetlistClan_local()
    {
        IOUtils.WriteMessage(Cmd.CLAN, MessagePackSerializer.Serialize(new ClanRequestDTO
        {
            GetListLocal = ClanScreen.GET_LISTCLAN_LOCAL
        }));
    }

    public void Payment_Sendcode(string code, string seri, sbyte typeThe)
    {
        IOUtils.WriteMessage(Cmd.PAYMENT_GET, MessagePackSerializer.Serialize(new PaymentRequestDTO
        {
            Code = code,
            Seri = seri,
            TypeThe = typeThe
        }));
    }

    public void Payment_GetInfo()
    {
        IOUtils.WriteMessage(Cmd.PAYMENT_INFO);
    }

    public void replyDialogFromServer(int cmd)
    {
        try
        {
            var m = new Message(Cmd.DIALOG_SERVER);

            //.Log("T send: Cmd: "+cmd+" |TypeActive: "+Controller.cmdClanAvtive+" |TypeClan: "+Controller.typeClanGL+" |IdChar: "+Char.myChar().cIdDB+" ||IdClan: "+Clan.idClan);

            m.writer().writeByte(cmd);
            if (cmd == Cmd.CLAN)
            {
                m.writer().writeByte(Controller.cmdClanAvtive);
                if (Controller.cmdClanAvtive == Clan.KICKED)
                {
                    m.writer().writeByte(Clan.typeKick);
                    m.writer().writeShort((short) ClanScreen.idCharClan);
                }
                else if (Controller.cmdClanAvtive == Clan.LEAVE)
                {
                    m.writer().writeByte(Clan.typeLeave);
                    ClanScreen.gI().listUser_gobal.removeAllElements();
                    ClanScreen.gI().clanGolbalName = "";
                }
                else if (Controller.cmdClanAvtive == Clan.INVITE)
                {
                    m.writer().writeByte(Controller.typeClanGL);
                    m.writer().writeLong(Char.myChar().cIdDB);
                    m.writer().writeShort(Clan.idClan);
                }
            }
            session.sendMessage(m);
            m.cleanup();
        }
        catch (Exception e)
        {
            //e.printStackTrace();
        }
    }
}