using src.lib;

public class MsgDlg : Dialog
{
    public static mBitmap[] imgpopup = new mBitmap[2];
    public static mBitmap[] imgbtnpopup = new mBitmap[2];
    public Command cmdYes, cmdCancel, cmdNo;
    private readonly int h = 110;
    public string[] info;
    public bool isDlgTime;
    public bool isPaintCham, ispaintTField;
    public bool isWait;
    private int padLeft;
    public string text = "";
    private TField tf_reques;
    public string tieude = "Thông báo";
    public int timeLive;
    public int timeShow;
    public long timestart;
    public int w = 240, y;

    public MsgDlg()
    {
        padLeft = 30;
        if (GameCanvas.w <= 176)
            padLeft = 10;
    }

    public static void loadbegin()
    {
        for (var i = 0; i < imgpopup.Length; i++)
            imgpopup[i] = GameCanvas.loadImage("/wood_bar" + i + ".png");
        for (var i = 0; i < imgbtnpopup.Length; i++)
            imgbtnpopup[i] = GameCanvas.loadImage("/btnpopup" + i + ".png");
    }

    public void setTimeLive(int time)
    {
        isDlgTime = true;
        timeLive = time;
        timestart = mSystem.currentTimeMillis();
    }

    public void pleasewait()
    {
        setInfo(mResources.PLEASEWAIT, null, null, null);
        GameCanvas.currentDialog = this;
    }

    public void show()
    {
        GameCanvas.currentDialog = this;
    }

    //Hide MsgDlg Huy code (21/8/2017)
    public void hide()
    {
        GameCanvas.currentDialog = null;
    }


    public void setInfo(string infoo, Command left, Command center, Command right)
    {
        tieude = "Thông báo";
        ispaintTField = false;
        info = mFont.tahoma_7_white.splitFontArray(infoo, 240);
        text = infoo;
        cmdYes = left;
        cmdCancel = center;
        cmdNo = right;

        y = GameCanvas.h - h - 35;
        y = GameCanvas.h / 2 - h / 2 + 40;
        if (cmdCancel != null)
        {
            cmdCancel.x = GameCanvas.w / 2 - 35;
            cmdCancel.y = GameCanvas.h - 26;

            if (cmdYes != null)
            {
                cmdYes.x = GameCanvas.w / 2 - 115;
                cmdYes.y = GameCanvas.h - 26;
            }

            if (cmdNo != null)
            {
                cmdNo.x = GameCanvas.w / 2 + 45;
                cmdNo.y = GameCanvas.h - 26;
            }
            if (this.left == null && this.right == null)
            {
                y = GameCanvas.h / 2 - h / 2 + 40;
                cmdCancel.x = GameCanvas.w / 2 - imgbtnpopup[1].getWidth() / 2;
                cmdCancel.y = y + h - 54;
                cmdCancel.img = imgbtnpopup[1];
                cmdCancel.imgFocus = imgbtnpopup[0];
                cmdCancel.w = imgbtnpopup[1].getWidth();
                cmdCancel.h = imgbtnpopup[1].getHeight();
            }
        }
        else
        {
            if (cmdYes != null)
            {
                cmdYes.x = GameCanvas.w / 2 - 3 * imgbtnpopup[1].getWidth() / 2;
                cmdYes.y = y + h - 54;
                cmdYes.img = imgbtnpopup[1];
                cmdYes.imgFocus = imgbtnpopup[0];
                cmdYes.w = imgbtnpopup[1].getWidth();
                cmdYes.h = imgbtnpopup[1].getHeight();
            }
            if (cmdNo != null)
            {
                cmdNo.x = GameCanvas.w / 2 + imgbtnpopup[1].getWidth() / 2;
                cmdNo.y = y + h - 54;
                cmdNo.img = imgbtnpopup[1];
                cmdNo.imgFocus = imgbtnpopup[0];
                cmdNo.w = imgbtnpopup[1].getWidth();
                cmdNo.h = imgbtnpopup[1].getHeight();
            }
            isDlgTime = false;
        }

        if (tf_reques == null)
        {
            tf_reques = new TField();
            tf_reques.x = GameCanvas.hw - 75;
            tf_reques.y = GameCanvas.h / 2 - h / 2 + 40;
            tf_reques.width = 80;
            tf_reques.height = 12;
            tf_reques.isFocus = false;
        }

        isWait = false;
    }

    public void setInfoTF(string info, Command left, Command center, Command right)
    {
        tieude = info;
        ispaintTField = true;
        this.info = mFont.tahoma_7_white.splitFontArray(info, 240);
        text = info;
        this.left = left;
        this.center = center;
        this.right = right;

        y = GameCanvas.h - h - 35;
        y = GameCanvas.h / 2 - h / 2 + 40;
        if (center != null)
        {
            this.center.x = GameCanvas.w / 2 - 35;
            this.center.y = GameCanvas.h - 26;

            if (left != null)
            {
                this.left.x = GameCanvas.w / 2 - 115;
                this.left.y = GameCanvas.h - 26;
            }

            if (right != null)
            {
                this.right.x = GameCanvas.w / 2 + 45;
                this.right.y = GameCanvas.h - 26;
            }
            if (this.left == null && this.right == null)
            {
                y = GameCanvas.h / 2 - h / 2 + 40;
                this.center.x = GameCanvas.w / 2 - imgbtnpopup[1].getWidth() / 2;
                this.center.y = y + h - 54;
                this.center.img = imgbtnpopup[1];
                this.center.imgFocus = imgbtnpopup[0];
                this.center.w = imgbtnpopup[1].getWidth();
                this.center.h = imgbtnpopup[1].getHeight();
            }
        }
        else
        {
            if (left != null)
            {
                this.left.x = GameCanvas.w / 2 - 3 * imgbtnpopup[1].getWidth() / 2;
                this.left.y = y + h - 54;
                this.left.img = imgbtnpopup[1];
                this.left.imgFocus = imgbtnpopup[0];
                this.left.w = imgbtnpopup[1].getWidth();
                this.left.h = imgbtnpopup[1].getHeight();
            }
            if (right != null)
            {
                this.right.x = GameCanvas.w / 2 + imgbtnpopup[1].getWidth() / 2;
                this.right.y = y + h - 54;
                this.right.img = imgbtnpopup[1];
                this.right.imgFocus = imgbtnpopup[0];
                this.right.w = imgbtnpopup[1].getWidth();
                this.right.h = imgbtnpopup[1].getHeight();
            }
        }

        isDlgTime = false;

        isWait = false;


        tf_reques = new TField();

        tf_reques.x = GameCanvas.hw - 60;
        tf_reques.y = GameCanvas.h / 2 - h / 2 + 65;
        tf_reques.width = 120;
        tf_reques.height = 12;
        tf_reques.isFocus = false;
    }

    public override void paint(mGraphics g)
    {
        var yDlg = y;

        g.drawImage(imgpopup[1], GameCanvas.w / 2 - imgpopup[1].getWidth() / 2, yDlg + imgpopup[0].getHeight() / 8,
            mGraphics.VCENTER | mGraphics.HCENTER);
        g.drawRegion(imgpopup[1], 0, 0, imgpopup[1].getWidth(), imgpopup[1].getHeight(), 2,
            GameCanvas.w / 2 + imgpopup[1].getWidth() / 2, yDlg + imgpopup[0].getHeight() / 8,
            mGraphics.VCENTER | mGraphics.HCENTER);
        mFont.tahoma_10b.drawString(g, tieude, GameCanvas.hw, yDlg - imgpopup[0].getHeight() / 3, 2);
        g.drawImage(imgpopup[0], GameCanvas.w / 2 - imgpopup[0].getWidth() / 2, yDlg + imgpopup[0].getHeight(),
            mGraphics.VCENTER | mGraphics.HCENTER);
        g.drawRegion(imgpopup[0], 0, 0, imgpopup[0].getWidth(), imgpopup[0].getHeight(), 2,
            GameCanvas.w / 2 + imgpopup[0].getWidth() / 2, yDlg + imgpopup[0].getHeight(),
            mGraphics.VCENTER | mGraphics.HCENTER);

        g.drawImage(imgpopup[0], GameCanvas.w / 2 - imgpopup[0].getWidth() / 2, yDlg + 2 * imgpopup[0].getHeight() - 1,
            mGraphics.VCENTER | mGraphics.HCENTER);
        g.drawRegion(imgpopup[0], 0, 0, imgpopup[0].getWidth(), imgpopup[0].getHeight(), 2,
            GameCanvas.w / 2 + imgpopup[0].getWidth() / 2, yDlg + 2 * imgpopup[0].getHeight() - 1,
            mGraphics.VCENTER | mGraphics.HCENTER);
        var yStart = yDlg + (h - (info.Length == 1 ? 1 : 2) * mFont.tahoma_8b.getHeight()) / 2 - 2 - 16;
        if (isWait)
            yStart += 8;

        if (!ispaintTField)
            if (isPaintCham)
            {
                var nCham = GameCanvas.gameTick / 10 % 4;
                var textt = "";
                for (var i = 0; i < nCham; i++)
                    textt += ".";
                for (var i = 0; i < 3 - nCham; i++)
                    textt += " ";
                mFont.tahoma_7_white.drawString(g, text + textt, GameCanvas.hw, yStart - 10, 2);
            }
            else
            {
                //int yy = yStart;
                for (int i = 0, yyz = yStart; i < info.Length; i++, yyz += mFont.tahoma_8b.getHeight() + 4)
                    mFont.tahoma_7_white.drawString(g, info[i], GameCanvas.hw,
                        yyz - 6 + (i > 1 ? mFont.tahoma_7_white.getHeight() : 0), 2);
            }
        else tf_reques.paint(g);
        if (cmdCancel != null)
            cmdCancel.paint(g);
        if (cmdYes != null)
            cmdYes.paint(g);
        if (cmdNo != null)
            cmdNo.paint(g);

        base.paint(g);
    }


    public override void update()
    {
        if (isDlgTime)
        {
            if ((mSystem.currentTimeMillis() - timestart) / 1000 > timeLive) GameCanvas.endDlg();
        }
        else if (isWait && timeShow > 0)
        {
            timeShow--;
            if (timeShow == 1)
            {
                GameCanvas.endDlg();
                timeShow = 0;
            }
        }
        else if (!ispaintTField && GameCanvas.isPointerJustRelease && !GameCanvas.isPoint(
                     GameCanvas.w / 2 - imgpopup[1].getWidth(), y, 2 * imgpopup[1].getWidth(),
                     imgpopup[1].getHeight() * 3))
        {
            GameCanvas.clearPointerEvent();
            GameCanvas.endDlg();
        }
        if (tf_reques != null && ispaintTField)
            tf_reques.update();
        if (tScreen.getCmdPointerLast(cmdNo))
        {
            cmdNo.performAction();
            GameCanvas.clearAllPointerEvent();
        }
        if (tScreen.getCmdPointerLast(cmdYes))
        {
            cmdYes.performAction();
            GameCanvas.clearAllPointerEvent();
        }
        if (tScreen.getCmdPointerLast(cmdCancel))
        {
            cmdCancel.performAction();
            GameCanvas.clearAllPointerEvent();
        }
        base.update();
    }

    public void keyPress(int keyCode)
    {
        if (tf_reques != null && ispaintTField)
            tf_reques.keyPressed(keyCode);
    }

    public string getTextTF()
    {
        if (tf_reques != null && ispaintTField)
            return tf_reques.getText();
        return null;
    }
}