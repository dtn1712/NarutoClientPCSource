using System;
using src.lib;

public class SmallImage
{
    public static int[][] smallImg;
    public static SmallImage instance;
    public static mBitmap[] imgbig;
    public static MyHashtable imgNew = new MyHashtable();
    public static MyHashtable img_big = new MyHashtable();
    public static mBitmap imgEmpty;

    public static string pathBigImage = "x" + mGraphics.zoomLevel + "/img/big_";
    public static string pathObjectMap = "x" + mGraphics.zoomLevel + "/mapobject/";
    public static string pathAvataNPC = "x" + mGraphics.zoomLevel + "/npc/";
    public static string pathMob = "x" + mGraphics.zoomLevel + "/mob/";
    public static string keyOKDownloaded = "download_bigimg";
    public static int nBigImage = 400;

    public static int ID_ADD_MAPOJECT = 1000;
    public static int ID_ADD_AVATARNPC = 20000;
    public static int ID_ADD_MOB = 21000;

    public static int[] IdBigImage;


    public SmallImage()
    {
        readImage();
    }

    public static string getPathImage(int id)
    {
        if (id < 1000) return pathBigImage;
        if (id < 20000)
            return pathObjectMap;
        if (id < 21000)
            return pathAvataNPC;
        return pathMob;
    }


    public static void loadBigImage()
    {
        imgbig = null;
        mSystem.gcc();
        imgbig = new mBitmap[35];
        BgItem.imgobj = new mBitmap[400];
      
        imgEmpty = new mBitmap(Image.createImage(1, 1));
    }

    public static void init()
    {
        instance = null;
        instance = new SmallImage();
    }

    public static void readImage()
    {
        try
        {
            DataInputStream file;
            file = new DataInputStream(Rms.loadRMS("nj_image"));
            nBigImage = file.readShort();

            imgbig = new mBitmap[nBigImage];
            int sum = file.readShort();
            LogDebug.println("SUM IMAGE ---> " + sum);
            IdBigImage = new int[sum];
            smallImg = new int[sum][];
            for (var i = 0; i < smallImg.Length; i++)
                smallImg[i] = new int[7];
            for (var i = 0; i < sum; i++)
            {
                IdBigImage[i] = i;
                smallImg[i][0] = file.readShort(); // id hình lớn //
                smallImg[i][1] = file.readShort(); // x cắt
                smallImg[i][2] = file.readShort(); // y cắt
                smallImg[i][3] = file.readShort(); // w cắt
                smallImg[i][4] = file.readShort(); // h cắt
            }


        }
        catch (Exception ex)
        {
        }
    }


    public static void drawSmallImage(mGraphics g, int id, int x, int y, int transform, int anchor)
    {
        //loadFromServer(id);
        //		System.out.println(smallImg[id][0]+" smallImg[id][0] ----> "+imgbig.length);
        try
        {
            mBitmap img = null;
            if (imgbig[smallImg[id][0]] != null &&
                (id >= smallImg.Length || smallImg[id][1] >= imgbig[smallImg[id][0]].getWidth()
                 || smallImg[id][3] >= imgbig[smallImg[id][0]].getWidth()
                 || smallImg[id][2] >= imgbig[smallImg[id][0]].getHeight()
                 || smallImg[id][4] >= imgbig[smallImg[id][0]].getHeight()))
            {
                LogDebug.println(id + " BIG IMAGE ---->  1111111  ");
              

                img = (mBitmap) imgNew.get(id + "");
          
                if (img != null)
                    g.drawRegion(img, 0, 0, img.getWidth(), img.getHeight(), transform, x, y, anchor);
            }
            else
            {

                if (imgbig[smallImg[id][0]] != null)
                {
                    g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3],
                        smallImg[id][4], transform, x, y, anchor);
                }
                else
                {
                    var imgput = createBigImage(smallImg[id][0]);
                    if (imgput != null)
                        g.drawRegion(imgput, smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4],
                            transform, x, y, anchor);
                }
            }
        }
        catch (Exception e)
        {
        }
    }

    public static mBitmap createBigImage(int id)
    {

        LogDebug.println(id + " createBigImage ----> " + smallImg.Length);
        if (imgbig[id] == null)
            imgbig[id] = GameCanvas.loadImage("/img/big_" + id + ".png");
        return imgbig[id];
    }

    public static void CleanImg()
    {
        try
        {
            for (var i = 0; i < imgbig.Length; i++)
            {
                if (imgbig[i] != null && imgbig[i].image != null)
                    imgbig[i].cleanImg();
                if (imgbig[i] != null)
                    imgbig[i] = null;
            }
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
    }

    public static void drawSmallImage(mGraphics g, int id, int x, int y, int transform, int anchor, bool isUclip)
    {
        try
        {
            mBitmap img = null;
            if (imgbig[smallImg[id][0]] != null && (id >= smallImg.Length
                                                    || smallImg[id][1] >= imgbig[smallImg[id][0]].getWidth()
                                                    || smallImg[id][3] >= imgbig[smallImg[id][0]].getWidth()
                                                    || smallImg[id][2] >= imgbig[smallImg[id][0]].getHeight()
                                                    || smallImg[id][4] >= imgbig[smallImg[id][0]].getHeight()
                    ))
            {
              


                img = (mBitmap) imgNew.get(id + "");

                if (img != null)
                    g.drawRegion(img, 0, 0, img.getWidth(), img.getHeight(), transform, x, y, anchor, isUclip);
            }
            else
            {
         
                if (imgbig[smallImg[id][0]] != null)
                {
                    g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3],
                        smallImg[id][4], transform, x, y, anchor, isUclip);
                }
                else
                {
                    var imgput = createBigImage(smallImg[id][0]);
                    if (imgput != null)
                        g.drawRegion(imgput, smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4],
                            transform, x, y, anchor, isUclip);
                }
            }
        }
        catch (Exception e)
        {
        }
    }

    public static void drawSmallImageScalse(mGraphics g, int id, int x, int y, int transform, int anchor,
        bool isUclip, int perScale)
    {
        try
        {
            mBitmap img = null;
            if (imgbig[smallImg[id][0]] != null && (id >= smallImg.Length
                                                    || smallImg[id][1] >= imgbig[smallImg[id][0]].getWidth()
                                                    || smallImg[id][3] >= imgbig[smallImg[id][0]].getWidth()
                                                    || smallImg[id][2] >= imgbig[smallImg[id][0]].getHeight()
                                                    || smallImg[id][4] >= imgbig[smallImg[id][0]].getHeight()
                 ))
            {
                img = (mBitmap) imgNew.get(id + "");
                if (img != null)
                    g.drawRegion(img, 0, 0, img.getWidth(), img.getHeight(), transform, x, y, anchor, isUclip);
            }
            else
            {
                if (id == 1029 || id == 1030 || id == 1028 || id == 1031 || id == 1038)
                {
                }
                if (imgbig[smallImg[id][0]] != null)
                {
                    g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3],
                        smallImg[id][4], transform, x, y, anchor, isUclip);
                }
                else
                {
                    var imgput = createBigImage(smallImg[id][0]);
                    if (imgput != null)
                        g.drawRegion(imgput, smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4],
                            transform, x, y, anchor, isUclip);
                }
            }
        }
        catch (Exception e)
        {

        }
    }

//drawRegionScalse
    public static void drawSmallImage(mGraphics g, int id, int x, int y, int transform, int anchor, bool isUclip,
        int opacity)
    {
        try
        {
            mBitmap img = null;
            if (imgbig[smallImg[id][0]] != null && (id >= smallImg.Length
                                                    || smallImg[id][1] >= imgbig[smallImg[id][0]].getWidth()
                                                    || smallImg[id][3] >= imgbig[smallImg[id][0]].getWidth()
                                                    || smallImg[id][2] >= imgbig[smallImg[id][0]].getHeight()
                                                    || smallImg[id][4] >= imgbig[smallImg[id][0]].getHeight()
                   ))
            {
               

                img = (mBitmap) imgNew.get(id + "");
         
                if (img != null)
                    g.drawRegion(img, 0, 0, img.getWidth(), img.getHeight(), transform, x, y, anchor, isUclip, opacity);
            }
            else
            {
               
                if (imgbig[smallImg[id][0]] != null)
                {
                    g.drawRegion(imgbig[smallImg[id][0]], smallImg[id][1], smallImg[id][2], smallImg[id][3],
                        smallImg[id][4], transform, x, y, anchor, isUclip, opacity);
                }
                else
                {
                    var imgput = createBigImage(smallImg[id][0]);
                    if (imgput != null)
                        g.drawRegion(imgput, smallImg[id][1], smallImg[id][2], smallImg[id][3], smallImg[id][4],
                            transform, x, y, anchor, isUclip, opacity);
                }
            }
        }
        catch (Exception e)
        {
            
        }
    }

}