using System;
using src.lib;

public class QuickSlot
{
    public const sbyte TYPE_SKILL = 1;
    public const sbyte TYPE_ITEM = 2;
    public const sbyte TYPE_POTION = 2;
    public static sbyte idSkillCoBan = -1;
    public static mBitmap img;
    public static sbyte[] idSkillGan = {-1, -1, -1, -1, -1};
    public int cooldown, mp;
    public int ideff;
    public short idicon;
    public sbyte idSkill; // Must set
    public bool isBuff;
    public sbyte quickslotType = -1; // Skill or Potion
    private long timewait, timecoolDown;

    public QuickSlot()
    {
        idSkill = -1;
    }


    public void setIsSkill(int skillType, bool isBuff)
    {
        var skill = (SkillTemplate) SkillTemplates.hSkilltemplate.get("" + skillType);
        quickslotType = TYPE_SKILL;
        idSkill = (sbyte) skillType;
        ideff = skill.ideff;
        this.isBuff = isBuff;
        if (skill.level < skill.mphao.Length)
            mp = skill.mphao[skill.level];
        else mp = skill.mphao[0];
        idicon = (short) skill.iconId;
        cooldown = skill.getCoolDown((sbyte) skill.level);
    }

    public static void SaveRmsQuickSlot()
    {
        for (var i = 0; i < Char.myChar().mQuickslot.Length; i++)
        {
            var ql = Char.myChar().mQuickslot[i];
            if (ql != null && idSkillGan != null)
                idSkillGan[i] = ql.idSkill;
        }
        Rms.saveRMS("quickslot", idSkillGan);
    }

    public static void loadRmsQuickSlot()
    {
        idSkillGan = Rms.loadRMS("quickslot");
        if (idSkillGan != null)
        {
            var isMyOldChar = false;
            for (var i = 0; i < idSkillGan.Length; i++)
            {
                var skill = (SkillTemplate) SkillTemplates.hSkilltemplate.get("" + idSkillGan[i]);
                if (skill != null && skill.level >= 0)
                {
                    isMyOldChar = true;
                    Char.myChar().mQuickslot[i].setIsSkill(idSkillGan[i], false);
                }
            }
            if (!isMyOldChar)
            {
                idSkillGan = new sbyte[] {-1, -1, -1, -1, -1};
                var skill =
                    (SkillTemplate) SkillTemplates.hSkilltemplate.get(
                        "" + (idSkillCoBan > -1 ? idSkillCoBan : (sbyte) 0));
                if (skill != null && skill.level >= 0)
                {
                    idSkillGan[0] = skill.id;
                    Char.myChar().mQuickslot[0].setIsSkill(0, false);
                }
                Rms.saveRMS("quickslot", idSkillGan);
            }
        }
        else
        {
            idSkillGan = new sbyte[] {-1, -1, -1, -1, -1};
            var skill =
                (SkillTemplate) SkillTemplates.hSkilltemplate.get("" + (idSkillCoBan > -1 ? idSkillCoBan : (sbyte) 0));
            if (skill != null && skill.level >= 0)
            {
                idSkillGan[0] = skill.id;
                Char.myChar().mQuickslot[0].setIsSkill(0, false);
            }
            Rms.saveRMS("quickslot", idSkillGan);
        }
    }


    public void startCoolDown()
    {
        timewait = mSystem.currentTimeMillis() + cooldown * 1000;
    }

    public bool canfight()
    {
        return timewait - mSystem.currentTimeMillis() < 0 && quickslotType == TYPE_SKILL;
    }

    public bool isEnoughtMp()
    {
        return Char.myChar().cMP >= mp ? true : false;
    }

    public bool canUse()
    {
        return timewait - mSystem.currentTimeMillis() < 0;
    }

    public string gettimeCooldown()
    {
        var time = (timewait - mSystem.currentTimeMillis()) / 1000;
        long t = 0;
        if (time == 0)
        {
            t = (timewait - mSystem.currentTimeMillis()) % 1000 / 100;
            if (t > 0)
                return "0." + t;
        }
        else if (time > 0)
        {
            return "" + time;
        }
        return "";
    }

}