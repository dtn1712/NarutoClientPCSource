using src.lib;

public class ChatPopup : Effect, IActionListener
{
    public static ChatPopup currentMultilineChatPopup;
    public Char c;
    public Command cmdNextLine;
    private int currentLine;

    private int cx, cy, ch;

    private string[] lines;
    private bool outSide;
    public string[] says;
    public int sayWidth = 100, delay, sayRun;

    public void perform(int idAction, object p)
    {
        if (idAction == 8000)
        {
            var currentLine = currentMultilineChatPopup.currentLine;
            currentLine++;
            if (currentLine >= currentMultilineChatPopup.lines.Length)
            {
                currentMultilineChatPopup.c.chatPopup = null;
                currentMultilineChatPopup = null;
                return; // END
            }
            var cp = addChatPopup(currentMultilineChatPopup.lines[currentLine], currentMultilineChatPopup.delay,
                currentMultilineChatPopup.c);
            cp.currentLine = currentLine;
            cp.lines = currentMultilineChatPopup.lines;
            cp.cmdNextLine = currentMultilineChatPopup.cmdNextLine;
            currentMultilineChatPopup = cp;
        }
    }



    public static ChatPopup addChatPopup(string chat, int howLong, Char c)
    {
        var cp = new ChatPopup();
        if (chat.Length < 10)
            cp.sayWidth = 64;
        if (GameCanvas.w == 128)
            cp.sayWidth = 128;
        cp.says = mFont.tahoma_7_red.splitFontArray(chat, cp.sayWidth - 10);
        cp.delay = howLong;
        cp.c = c;
        cp.cx = c.cx;
        cp.cy = c.cy;
        c.chatPopup = cp;
        cp.sayRun = 7;
        if (c != null)
        {
            cp.cx = c.cx;
            cp.cy = c.cy;
            cp.ch = c.ch - 15;
        }
        vEffect2.addElement(cp);
        return cp;
    }


    public override void update()
    {
        if (delay > 0)
            delay--;

        if (c != null)
        {
            cx = c.cx;
            cy = c.cy;
            ch = c.ch - 5;
        }
        if (sayRun > 1)
            sayRun--;

        if (c != null && c.chatPopup != null && c.chatPopup != this || c != null && c.chatPopup == null ||
            delay == 0)
        {
            vEffect2Outside.removeElement(this);
            vEffect2.removeElement(this);
        }
    }

    public override void paint(mGraphics g)
    {
        var cx = this.cx;
        var cy = this.cy;
        if (outSide)
        {
            cx -= GameScr.cmx;
            cy -= GameScr.cmy;
            cy += 35;
        }
        g.setColor(0x000000);
        g.fillRect(cx - sayWidth / 2 + 2, cy - ch - 15 + sayRun - says.Length * 12 - 10, sayWidth - 4,
            (says.Length + 1) * 12 - 1 + 2);
        g.fillRect(cx - sayWidth / 2 - 1, cy - ch - 15 + sayRun - says.Length * 12 - 7, sayWidth + 2,
            (says.Length + 1) * 12 - 5);

        g.setColor(0xFFFFFF);
        g.fillRect(cx - sayWidth / 2, cy - ch - 15 + sayRun - says.Length * 12 - 9, sayWidth,
            (says.Length + 1) * 12 - 1);


        g.drawImage(LoadImageInterface.imgGocChat, cx - sayWidth / 2 - 1, cy - ch - 15 + sayRun - says.Length * 12 - 10,
            mGraphics.TOP | mGraphics.LEFT, true);
        g.drawRegion(LoadImageInterface.imgGocChat, 0, 0, LoadImageInterface.imgGocChat.getWidth(),
            LoadImageInterface.imgGocChat.getHeight(), 2,
            cx - sayWidth / 2 - 1 + sayWidth + 2 - LoadImageInterface.imgGocChat.getWidth(),
            cy - ch - 15 + sayRun - says.Length * 12 - 10, mGraphics.TOP | mGraphics.LEFT, true);
        g.drawRegion(LoadImageInterface.imgGocChat, 0, 0, LoadImageInterface.imgGocChat.getWidth(),
            LoadImageInterface.imgGocChat.getHeight(), 6,
            cx - sayWidth / 2 - 1,
            cy - ch - 15 + sayRun - says.Length * 12 - 10 + (says.Length + 1) * 12 -
            LoadImageInterface.imgGocChat.getHeight(), mGraphics.TOP | mGraphics.LEFT, true);
        g.drawRegion(LoadImageInterface.imgGocChat, 0, 0, LoadImageInterface.imgGocChat.getWidth(),
            LoadImageInterface.imgGocChat.getHeight(), 3,
            cx - sayWidth / 2 - 1 + sayWidth + 2 - LoadImageInterface.imgGocChat.getWidth(),
            cy - ch - 15 + sayRun - says.Length * 12 - 10 + (says.Length + 1) * 12 -
            LoadImageInterface.imgGocChat.getHeight() + 1, mGraphics.TOP | mGraphics.LEFT, true);

        g.drawImage(LoadImageInterface.imgGoc, cx - sayWidth / 4,
            cy - ch - 15 + sayRun + LoadImageInterface.imgGoc.getHeight() / 2 - 1, mGraphics.TOP | mGraphics.LEFT,
            true);

        for (var i = 0; i < says.Length; i++)
            mFont.tahoma_7.drawString(g, says[i], cx, cy - ch - 15 + sayRun + i * 12 - says.Length * 12 - 4, 2);
    }

}