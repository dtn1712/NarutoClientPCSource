using src.lib;
using UnityEngine;

public abstract class tScreen
{
    public static iCommand left1, right1, center1;

    public static int ITEM_HEIGHT = 18 * mGraphics.zoomLevel;
    public static int cmdW = 70 * mGraphics.zoomLevel;
    public static int cmdH = 22;
    public static int keyTouch = -1;
    public static int height, width;
    public static tScreen lastScreen;
    public static ScreenOrientation orientation;
    public static bool fullScreen = false;
    public bool isConnect = false, connectCallBack = false, isCloseConnect = false;
    public Command left, center;
    public Command right, cmdClose;

    public virtual void switchToMe()
    {
        // chỉ vào chỗ này 
        GameCanvas.currentScreen = this;
        GameCanvas.clearKeyPressed();
        GameCanvas.clearPointerEvent();
        keyTouch = -1;
    }

    public virtual void keyPress(int keyCode)
    {
    }

    public virtual void update()
    {
    }

    public virtual void updateKey()
    {
        if (GameCanvas.keyPressed[5] || getCmdPointerLast(GameCanvas.currentScreen.center))
        {
            GameCanvas.keyPressed[5] = false;
            keyTouch = -1;
            GameCanvas.isPointerJustRelease = false;
            if (center != null)
                center.performAction();
            if (center1 != null)
            {
                GameCanvas.clearKeyPressed();
                GameCanvas.clearKeyHold();
                center1.perform();
            }
        }
        if (GameCanvas.keyPressed[12] || getCmdPointerLast(GameCanvas.currentScreen.left))
        {
            GameCanvas.keyPressed[12] = false;
            keyTouch = -1;
            GameCanvas.isPointerJustRelease = false;
            if (ChatTextField.gI().isShow)
            {
                if (ChatTextField.gI().left != null) ChatTextField.gI().left.performAction();
            }
            else if (left != null)
            {
                left.performAction();
            }
            if (left1 != null)
            {

                GameCanvas.clearKeyPressed();
                GameCanvas.clearKeyHold();
                left1.perform();
            }
        }
        if (getCmdPointerLast(GameCanvas.currentScreen.right))
        {
            keyTouch = -1;
            GameCanvas.isPointerJustRelease = false;
            if (ChatTextField.gI().isShow)
            {
                if (ChatTextField.gI().right != null) ChatTextField.gI().right.performAction();
            }
            else if (right != null)
            {
                right.performAction();
            }

            if (right1 != null)
            {
                GameCanvas.clearKeyPressed();
                GameCanvas.clearKeyHold();
                right1.perform();
            }
        }
    }


    public static bool getCmdPointerLast(Command cmd)
    {
        if (cmd == null)
            return false;

        if (GameScr.ispaintChat)
            if (cmd == GameScr.chat || cmd == GameScr.bntIconChat)
                if (GameCanvas.isPointerHoldIn(cmd.x, cmd.y, cmd.w, cmd.h + 10))
                {
                    keyTouch = 0;
                    if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                        return cmd.input();
                }

        if (GameCanvas.currentScreen == GameCanvas.languageScr)
            if (cmd == LanguageScr.cmdEng || cmd == LanguageScr.cmdVn)
                if (GameCanvas.isPointerHoldIn(cmd.x, cmd.y, cmd.w, cmd.h + 10))
                {
                    keyTouch = 0;
                    if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                        return cmd.input();
                }

        if (GameCanvas.isPointerHoldIn(cmd.x, cmd.y, cmd.w, cmd.h + 10))
        {
            keyTouch = 0;
            if (GameCanvas.isPointerClick && GameCanvas.isPointerJustRelease)
                return cmd.input();
        }
        return false;
    }

    public virtual void updatePointer()
    {
        if (GameCanvas.isTouch)
        {
            if (left1 != null)
                if (left1.isPosCmd())
                {
                    left1.updatePointer();
                }
                else
                {
                    if (GameCanvas.isPointSelect(0, GameCanvas.h
                                                    - GameCanvas.hCommand - 5, GameCanvas.wCommand * 2,
                        GameCanvas.hCommand + 10))
                        left1.perform();
                }
            if (right1 != null)
                if (right1.isPosCmd())
                {
                    right1.updatePointer();
                }
                else
                {
                    if (GameCanvas.isPointSelect(GameCanvas.w
                                                 - GameCanvas.wCommand * 2, GameCanvas.h
                                                                            - GameCanvas.hCommand - 5,
                        GameCanvas.wCommand * 2,
                        GameCanvas.hCommand + 10))
                        right1.perform();
                }
            if (center1 != null)
                if (center1.isPosCmd())
                {
                    center1.updatePointer();
                }
                else
                {
                    if (GameCanvas.isPointSelect(GameCanvas.hw
                                                 - GameCanvas.wCommand, GameCanvas.h
                                                                        - GameCanvas.hCommand - 5,
                        GameCanvas.wCommand * 2,
                        GameCanvas.hCommand + 10))
                        center1.perform();
                }
        }
    }

    public static void resetTranslate(mGraphics g)
    {
        g.translate(-g.getTranslateX(), -g.getTranslateY());
        g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
    }

    public virtual void paint(mGraphics g)
    {
        g.translate(-g.getTranslateX(), -g.getTranslateY());
        g.setClip(0, 0, GameCanvas.w, GameCanvas.h + 1);
        GameCanvas.paintt.paintTabSoft(g);
    }
}