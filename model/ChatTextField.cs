using src.lib;

public class ChatTextField : IActionListener
{
    private static ChatTextField instance;
    public Command center = null;
    public bool isShow;
    private long lastChatTime;

    public Command left;
    public IChatable parentScreen;
    public Command right;
    public TField tfChat;

    private string to;

    protected ChatTextField()
    {
        tfChat = new TField();
        tfChat.x = 16;
        tfChat.width = GameCanvas.w - 32;
        tfChat.height = tScreen.ITEM_HEIGHT + 2;
        tfChat.isFocus = true;
        tfChat.setMaxTextLenght(40);
        left = new Command(mResources.CHAT, this, 8000, null, 1, GameCanvas.h - tScreen.cmdH + 1);
        right = new Command(mResources.DELETE, this, 8001, null, GameCanvas.w - 53, GameCanvas.h - tScreen.cmdH + 1);
    }

    public void perform(int idAction, object p)
    {
        switch (idAction)
        {
            case Constants.BUTTON_SEND_CHAT_WORLD: //chat world
                if (parentScreen != null)
                {
                    var now = mSystem.currentTimeMillis();
                    if (now - lastChatTime < 1000)
                        return;
                    lastChatTime = now;

                    parentScreen.onChatFromMe(tfChat.getText(), to);
                    tfChat.setText("");
                    right.caption = mResources.CLOSE;
                }
                break;
            case 8001:
                tfChat.clear();
                if (tfChat.getText().Equals(""))
                {
                    isShow = false;
                    parentScreen.onCancelChat();
                }
                break;
        }
    }

    public void keyPressed(int keyCode)
    {
        if (isShow)
            tfChat.keyPressed(keyCode);
        if (tfChat.getText().Equals(""))
            right.caption = mResources.CLOSE;
        else right.caption = mResources.DELETE;
    }

    public static ChatTextField gI()
    {
        return instance == null ? instance = new ChatTextField() : instance;
    }

    public void startChat(IChatable parentScreen, string to)
    {
        right.caption = mResources.CLOSE;
        this.to = to;
        if (GameCanvas.currentDialog == null)
        {
            isShow = true;
        }
    }

    public void update()
    {
        if (!isShow)
            return;
        tfChat.update();
        if (tfChat.justReturnFromTextBox)
        {
            tfChat.justReturnFromTextBox = false;
            parentScreen.onChatFromMe(tfChat.getText(), to);
            tfChat.setText("");
            right.caption = mResources.CLOSE;
        }
    }

    public void close()
    {
        tfChat.setText("");
        isShow = false;
    }

    public void paint(mGraphics g)
    {
        if (!isShow)
            return;

        Paint.paintFrame(tfChat.x - 14, tfChat.y - 18, tfChat.width + 28, tfChat.height + 26, g);
        mFont.tahoma_7b_white.drawString(g, "Chat " + to, tfChat.x, tfChat.y - 13, 0);
        tfChat.paint(g);
    }
}