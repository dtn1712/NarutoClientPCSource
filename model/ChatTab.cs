using src.lib;

public class ChatTab
{
    public Vector contents = new Vector();
    public string ownerName;

    public int type; // 0:public, 1:party, 2:PM, 3:Global, 4:Clan

    public ChatTab(string ownerName, int type)
    {
        this.ownerName = ownerName;
        this.type = type;
    }


    public void addChat(string whoChat, string text)
    {
        contents.addElement("c3@" + whoChat);
        var v = mFont.tahoma_7_white.splitFontVector(text, 160);
        for (var i = 0; i < v.size(); i++)
            contents.addElement("c0" + v.elementAt(i));

        clear();
    }

    private void clear()
    {
        while (true)
            if (contents.size() > 50)
                contents.removeElementAt(1);
            else
                break;
    }

    public void addInfo(string text)
    {
        var v = mFont.tahoma_7_white.splitFontVector(text, 160);
        for (var i = 0; i < v.size(); i++)
            contents.addElement(v.elementAt(i));
        clear();
    }
}