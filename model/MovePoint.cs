public class MovePoint
{
    public int xEnd, yEnd, dir, cvx, cvy, status;

    public MovePoint(int xEnd, int yEnd, int act, int dir)
    {
        this.xEnd = xEnd;
        this.yEnd = yEnd;
        this.dir = dir;
        status = act;
    }

}