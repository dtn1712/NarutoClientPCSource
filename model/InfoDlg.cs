using src.lib;

public class InfoDlg
{
    private static bool isShow;
    private static string title, subtitke;
    public static int delay;
    public static bool isLock;

    public static void show(string title, string subtitle, int delay)
    {
        if (title == null)
            return;
        isShow = true;
        InfoDlg.title = title;
        subtitke = subtitle;
        InfoDlg.delay = delay;
    }

    public static void showWait()
    {
        show(mResources.PLEASEWAIT, null, 5000);
        isLock = true;
    }


    public static void paint(mGraphics g)
    {
        var tt = title;
        if (!isShow)
            return;
        if (isLock && delay > 4990)
            return;

        var yDlg = 10;
        Paint.paintFrame(GameCanvas.hw - 64, yDlg, 128, 40, g);
        if (isLock)
        {
            mFont.tahoma_8b.drawString(g, tt, GameCanvas.hw + 5, yDlg + 13, 2);
        }
        else if (subtitke != null)
        {
            mFont.tahoma_8b.drawString(g, tt, GameCanvas.hw, yDlg + 8, 2);
            mFont.tahoma_7_white.drawString(g, subtitke, GameCanvas.hw, yDlg + 22, 2);
        }
        else
        {
            mFont.tahoma_8b.drawString(g, tt, GameCanvas.hw, yDlg + 13, 2);
        }
    }

    public static void update()
    {
        if (delay > 0)
        {
            delay--;
            if (delay == 0)
                hide();
        }
    }

    public static void hide()
    {
        title = "";
        subtitke = null;
        isLock = false;
        delay = 0;
        isShow = false;
    }
}