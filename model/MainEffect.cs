using src.lib;

public class MainEffect : MainObject
{
    // Direction
    public const int DIR_UP = 1;

    public const int DIR_DOWN = 0;
    public const int DIR_LEFT = 2;
    public const int DIR_RIGHT = 3;
    public FrameImage fraImgEff;
    public int fRemove;
    private bool isPaint = true;
    private long timeBegin;
    public int typeEffect = 0;

    public virtual void paint(mGraphics g)
    {
        base.paint(g);
    }

    public virtual void update()
    {
        base.update();
    }

}