using System;
using src.lib;

public class EffectData
{
    public short[] arrFrame;
    public short[][] arrFrameMove = new short[6][];
    public Frame[] frame;
    public int ID;
    public mBitmap img;
    public ImageInfo[] imgInfo;

    public int width, height;

    public ImageInfo getImageInfo(sbyte id)
    {
        for (var i = 0; i < imgInfo.Length; i++)
            if (imgInfo[i].ID == id)
                return imgInfo[i];
        return null;
    }

    public static DataInputStream readdatamobFromRes(string path)
    {
        DataInputStream iss = null;
        try
        {
            iss = new DataInputStream("/mapitem/mapItem");
            if (iss != null)
                return iss;
            
            return null;
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
        return null;
    }

    public void readData(string patch)
    {
        InputStream iss = null;
        DataInputStream isss = null;
        try
        {
            isss = new DataInputStream(patch);
        }
        catch (Exception e)
        {
            return;
        }
        readData(isss);
    }

    public void readData(DataInputStream isss)
    {
        int left = 0, top = 0, right = 0, bottom = 0;
        try
        {
            var smallNumImg = isss.readByte();
            imgInfo = new ImageInfo[smallNumImg];
            for (var i = 0; i < smallNumImg; i++)
            {
                imgInfo[i] = new ImageInfo();
                imgInfo[i].ID = isss.readByte();
                imgInfo[i].x0 = isss.readByte();
                imgInfo[i].y0 = isss.readByte();
                imgInfo[i].w = isss.readByte();
                imgInfo[i].h = isss.readByte();
            }
            var nFrame = isss.readShort();

            frame = new Frame[nFrame];
            for (var i = 0; i < nFrame; i++)
            {
                frame[i] = new Frame();
                var nPart = isss.readByte();
                frame[i].dx = new short[nPart];
                frame[i].dy = new short[nPart];
                frame[i].idImg = new sbyte[nPart];
                for (var a = 0; a < nPart; a++)
                {
                    frame[i].dx[a] = isss.readShort();
                    frame[i].dy[a] = isss.readShort();
                    frame[i].idImg[a] = isss.readByte();
                    if (i == 0)
                    {
                        if (left > frame[i].dx[a])
                            left = frame[i].dx[a];
                        if (top > frame[i].dy[a])
                            top = frame[i].dy[a];
                        if (right < frame[i].dx[a] + imgInfo[frame[i].idImg[a]].w)
                            right = frame[i].dx[a] + imgInfo[frame[i].idImg[a]].w;
                        if (bottom < frame[i].dy[a] + imgInfo[frame[i].idImg[a]].h)
                            bottom = frame[i].dy[a] + imgInfo[frame[i].idImg[a]].h;
                        width = right - left;
                        height = bottom - top;
                    }
                }
            }
            var nFrameCount = isss.readShort();
         
            arrFrame = new short[nFrameCount];
            for (var i = 0; i < nFrameCount; i++)
                arrFrame[i] = isss.readShort();
        }
        catch (Exception e)
        {
            //e.printStackTrace();
            // TODO: handle exception
        }
    }

    public void readData(sbyte[] data)
    {
        DataInputStream isss = null;
        isss = new DataInputStream(data);
        readData(isss);
    }

    public void readhd(DataInputStream iss)
    {
        try
        {
            var nFramestand = iss.readByte(); // 0
            arrFrameMove[0] = new short[nFramestand];
            if (nFramestand > 0)
                for (var i = 0; i < nFramestand; i++)
                    arrFrameMove[0][i] = iss.readByte();
            var nFramewalk = iss.readByte(); //1
            if (nFramewalk > 0)
            {
                arrFrameMove[1] = new short[nFramewalk];
                for (var i = 0; i < nFramewalk; i++)
                    arrFrameMove[1][i] = iss.readByte();
            }
            var nFrameAttack1 = iss.readByte(); //2
            if (nFrameAttack1 > 0)
            {
                arrFrameMove[2] = new short[nFrameAttack1];
                for (var i = 0; i < nFrameAttack1; i++)
                    arrFrameMove[2][i] = iss.readByte();
            }
            var nFrameAttack2 = iss.readByte(); //3
            if (nFrameAttack2 > 0)
            {
                arrFrameMove[3] = new short[nFrameAttack2];
                for (var i = 0; i < nFrameAttack2; i++)
                    arrFrameMove[3][i] = iss.readByte();
            }
            var nFramesit = iss.readByte(); //4
            if (nFramesit > 0)
            {
                arrFrameMove[4] = new short[nFramesit];
                for (var i = 0; i < nFramesit; i++)
                    arrFrameMove[4][i] = iss.readByte();
            }
            var nFrameDead = iss.readByte(); //5
            if (nFrameDead > 0)
            {
                arrFrameMove[5] = new short[nFramesit];
                for (var i = 0; i < nFrameDead; i++)
                    arrFrameMove[5][i] = iss.readByte();
            }
            var nFrameAttack = iss.readByte(); //6
            if (nFrameAttack > 0)
                for (var i = 0; i < nFrameAttack; i++)
                {
                    var frame = iss.readByte();
                }
            var nFrameBullet1 = iss.readByte(); //7
            if (nFrameBullet1 > 0)
                for (var i = 0; i < nFrameBullet1; i++)
                {
                    var frame = iss.readByte();
                }
            var nFrameBullet2 = iss.readByte(); //8
            if (nFrameBullet2 > 0)
                for (var i = 0; i < nFrameBullet2; i++)
                {
                    var frame = iss.readByte();
                }
        }
        catch (Exception e)
        {
            // e.printStackTrace();
        }
    }

    public void readData222(DataInputStream iss)
    {
        try
        {
            int left = 0, top = 0, right = 0, bottom = 0;
            if (iss != null)
            {
                var smallNumImg = iss.readByte();
                imgInfo = new ImageInfo[smallNumImg];
                for (var i = 0; i < smallNumImg; i++)
                {
                    imgInfo[i] = new ImageInfo();
                    imgInfo[i].ID = iss.readByte();
                    imgInfo[i].x0 = iss.readUnsignedByte();
                    imgInfo[i].y0 = iss.readUnsignedByte();
                    imgInfo[i].w = iss.readUnsignedByte();
                    imgInfo[i].h = iss.readUnsignedByte();

                }
                var nFrame = iss.readByte();
                frame = new Frame[nFrame];

                for (var i = 0; i < nFrame; i++)
                {
                    frame[i] = new Frame();
                    var nPart = iss.readByte();

                    frame[i].dx = new short[nPart];
                    frame[i].dy = new short[nPart];
                    frame[i].idImg = new sbyte[nPart];
                    for (var a = 0; a < nPart; a++)
                    {
                        frame[i].dx[a] = iss.readByte();
                        frame[i].dy[a] = iss.readByte();
                        frame[i].idImg[a] = iss.readByte();

                    }
                }
                short nFrameCount = iss.readByte();
                arrFrame = new short[nFrameCount];
                for (var i = 0; i < nFrameCount; i++)
                    arrFrame[i] = iss.readByte();
            }

            width = iss.readByte();
            height = iss.readByte();
        }
        catch (Exception e)
        {
            // TODO: handle exception
        }
    }

    public void paintFrame(mGraphics g, int f, int x, int y, int trans, int layer)
    {
        if (frame != null && frame.Length != 0)
        {
            var fr = frame[f];
            for (var i = 0; i < fr.dx.Length; i++)
            {
                var im = getImageInfo(fr.idImg[i]);
                try
                {
                    if (trans == 0)
                        g.drawRegion(img, im.x0, im.y0, im.w, im.h, 0, x + fr.dx[i], y + fr.dy[i]
                                                                                     - (layer < 4 && layer > 0
                                                                                         ? trans
                                                                                         : 0), 0);
                    if (trans == 1)
                        g.drawRegion(img, im.x0, im.y0, im.w, im.h, Sprite.TRANS_MIRROR, x - fr.dx[i], y + fr.dy[i]
                                                                                                       - (layer < 4 &&
                                                                                                          layer > 0
                                                                                                           ? trans
                                                                                                           : 0),
                            StaticObj.TOP_RIGHT);
                }
                catch (Exception e)
                {
                }
            }
        }
    }
}