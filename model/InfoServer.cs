using src.lib;

public class InfoServer
{
    public const sbyte CHATSERVER = 0;
    public const sbyte CHATRAO = 1;
    public const sbyte CHATWORLD = 2;
    public short idplayer;
    public bool isStop, isThongBaoDuoi;
    public NodeChat node;
    public string text;
    public int tgdung = 50;
    public int type, wString, hpaint;
    public int x, y;

    public InfoServer(sbyte type, string text)
    {
        this.text = text;
        hpaint = 10;
        this.type = type;
        wString = mFont.tahoma_7_white.getWidth(text);
        x = 3 * GameCanvas.w / 4 + wString / 2;
    }

    public InfoServer(sbyte type, NodeChat text)
    {
        node = text;
        hpaint = 10;
        this.type = type;
        wString = mFont.tahoma_7_white.getWidth(text.textdai);
        LogDebug.println("wString  " + wString);
        x = 3 * GameCanvas.w / 4 + wString / 2;
    }


    public void update()
    {
        switch (type)
        {
            case CHATSERVER:
                updateInfoServer();
                break;
            case CHATWORLD:
                updateChatWorld();
                break;
        }
    }

    public void paint(mGraphics g)
    {
        g.setColor(0, 60);

        g.setClip(GameCanvas.w / 4 - 4, hpaint, GameCanvas.w / 2 + 8, 20);
        g.fillRect(GameCanvas.w / 4 - 4, hpaint, GameCanvas.w / 2 + 8, 20, true);
        g.disableBlending();
        switch (type)
        {
            case CHATSERVER:
                mFont.tahoma_7_yellow.drawString(g, text, x, hpaint + 3, 2, true);
                break;
            case CHATWORLD:
                if (node == null)
                    mFont.tahoma_7_white.drawString(g, text, x, hpaint + 3, 2, true);
                else node.paint(g, x, hpaint + 3, true, true);
                break;
        }
        GameCanvas.resetTrans(g);
    }

    public void updateChatWorld()
    {
        if (x > GameCanvas.w / 4 - wString && !isStop)
            if (x > GameCanvas.w / 4)
            {
                x -= 10;
            }
            else
            {
                if (tgdung <= 0)
                {
                    tgdung--;
                    if (wString < GameCanvas.w / 2)
                        x += tgdung / 2;
                    else x -= 2;
                }
                else
                {
                    x = GameCanvas.w / 4 /*+wString/2*/;
                    tgdung--;
                }
            }
        else
            GameScr.listWorld.removeElement(this);
    }

    public void updateInfoServer()
    {
        if (x > GameCanvas.w / 4 - wString)
            x--;
        else
            GameScr.listInfoServer.removeElement(this);
    }
}