using src.lib;

public class Input2Dlg : Dialog, IActionListener
{
    public Command cmdClose = new Command("", 1000, null);
    private int focus, line, titleLenght;
    protected string[] info;
    private int padLeft;
    public TField tfInput;
    public TField tfInput2;

    public Input2Dlg()
    {
        tfInput = new TField();
        tfInput2 = new TField();
    }

    public void perform(int idAction, object p)
    {
    }


    public override void paint(mGraphics g)
    {
        GameCanvas.paintt.paintInputDlg(g, padLeft, GameCanvas.h - 85 - tScreen.cmdH - line * 13,
            GameCanvas.w - padLeft * 2, 80 + line * 13, info);
        mFont.tahoma_8b.drawString(g, tfInput.title + ": ", tfInput.x - titleLenght - 5, tfInput.y + 5, 0);
        tfInput.paint(g);
        g.setClip(0, 0, GameCanvas.w, GameCanvas.h);
        mFont.tahoma_8b.drawString(g, tfInput2.title + ": ", tfInput2.x - titleLenght - 5, tfInput2.y + 5, 0);
        tfInput2.paint(g);
        base.paint(g);
    }

    public override void keyPress(int keyCode)
    {
        if (focus == 0)
            tfInput.keyPressed(keyCode);
        else
            tfInput2.keyPressed(keyCode);
        base.keyPress(keyCode);
    }

    public override void update()
    {
        if (GameCanvas.keyPressed[2])
            focus = 0;
        if (GameCanvas.keyPressed[8])
            focus = 1;
        if (focus == 0)
        {
            tfInput.isFocus = true;
            tfInput2.isFocus = false;
            right = tfInput.cmdClear;
            tfInput.update();
        }
        else
        {
            tfInput.isFocus = false;
            tfInput2.isFocus = true;
            right = tfInput2.cmdClear;
            tfInput2.update();
        }
        if (GameCanvas.isTouch)
            if (GameCanvas.isPointerJustRelease && GameCanvas.isPointerClick)
            {
                if (GameCanvas.isPointerHoldIn(tfInput.x, tfInput.y, tfInput.width, tfInput.height))
                    focus = 0;
                if (GameCanvas.isPointerHoldIn(tfInput2.x, tfInput2.y, tfInput2.width, tfInput2.height))
                    focus = 1;
            }
        if (left != null)
        {
            left.x = GameCanvas.w / 2 - 160;
            left.y = GameCanvas.h - 26;
        }
        if (center != null)
        {
            center.x = GameCanvas.w / 2 - 35;
            center.y = GameCanvas.h - 26;
        }

        if (right != null)
        {
            right.x = GameCanvas.w / 2 + 88;
            right.y = GameCanvas.h - 26;
        }
        base.update();
    }

    public void show()
    {
        GameCanvas.currentDialog = this;
    }

}